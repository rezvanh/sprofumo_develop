// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2014-2020; Rezvan Farahibozorg 2018-2021
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include <string>
#include <vector>
#include <iostream>
#include <iomanip>    // std::put_time
#include <fstream>
#include <sstream>    // std::ostringstream
#include <sys/stat.h>
#include <locale>     // std::locale, std::isspace, std::isdigit
#include <armadillo>
#include <algorithm>
#include <omp.h>
#include <stdexcept>
#include <chrono>     // std::system_clock
#include <ctime>      // std::time_t, std::localtime
#include <tclap/CmdLine.h>
#include <nlohmann/json.hpp>
#include <dirent.h>
#include <math.h> 
#include <experimental/filesystem>

#include "ModelManager.h"

#include "DataLoader.h"
#include "DataLoaders/FullRankDataLoader.h"
#include "DataLoaders/LowRankDataLoader.h"
#include "Utilities/DataIO.h"
#include "Utilities/BuildTime.h"


// compile and build like this: 
// $ cd /well/win-biobank/users/tli633/sPFM_Dev_BDI
// $ scl enable devtoolset-6 bash
// $ export LD_LIBRARY_PATH=/users/win/tli633/lib
// $ export FSLDIR=/well/win/software/packages/fsl/6.0.0
// $ export OMP_NUM_THREADS=20
// line below or just $make (it will update the modified files)
// $ make unistall then $ make clean then $ make then $ make all then $ make install
//
// Structure for the arguments 
struct Arguments
{
public:
    std::string dataLocationsFile;
    unsigned int M;
    std::string outputDir;
    PROFUMO::ModelManager::SpatialModel spatialModel;
    bool useMask;
    std::string maskFile;
    bool useFixedParameters;
    bool useGroupMeans;
    std::string groupMeansFile;
    bool useGroupPrecisions;
    std::string groupPrecisionsFile;
    bool useGroupMemberships;
    std::string groupMembershipsFile;
    bool useSpatialNeighbours;
    std::string spatialNeighboursFile;
    bool usePredefinedInitialMaps;
    bool usePredefinedSpatialBasis;
    std::string predefinedInitialMapsFile;
    std::string predefinedSpatialBasisFile;
    bool useHRF;
    float TR;
    std::string hrfFile;
    PROFUMO::ModelManager::TemporalPrecisionModel temporalPrecisionModel;
    unsigned int K;
    unsigned int groupIter;
    float subjectIter;
    float dof;
    float BetaVal;
    unsigned int batchSize;
    unsigned int batchUpdates;
    unsigned int initialBatchUpdates;
    bool normaliseData;
    bool bigData;
    bool fullPredefined;
    std::string fullPredefinedInPath;
    unsigned int fullPredefinedIter;
    bool MIGP;
    unsigned int multiStartIterations;
};
//    unsigned int BatchS;

// Makes sure output directory is legit and ends in .pfm
std::string cleanOutputDir(const std::string outputDir);

// Copy! C++17 filesystem library please hurry up ;-p
void copyFile(const std::string fromFile, const std::string toFile);

void removeDirectory(const std::string directory);

// Loads a set of spatial neighbours from file
std::shared_ptr<std::vector<arma::uvec>> loadNeighbours(const std::string neighboursFile);

const char* const dateFormat = "%a %e %b %Y; %T %Z (%z)"; // Fri 21 Jul 2017; 15:09:52 BST (+0100)
const char* const timeFormat = "%T %Z, %e %b %Y"; // 15:09:52 BST, 21 Jul 2017


int main(const int argc, const char** argv)
{
    // Set the number of threads
    #ifdef _OPENMP
        omp_set_num_threads(20);
    #endif
    
    // Set the seed to a random value
    arma::arma_rng::set_seed_random();
    
    
    ///////////////////////////////////////////////////////////////////////////
    // Parse command line args
    
    Arguments args;
   
    try{
        
        const std::string EMPTY_DEFAULT = "**DEFAULT**";
        
        // Set everything up (description, delimiter, version)
        TCLAP::CmdLine cmd("PROFUMO: infers PRObabilistic FUnctional MOdes from fMRI data.", ' ', "Development version");
        
        // Add the required arguments
        // (name, description, required, default, type)
        // N.B. The help message is more useful if type = name
        TCLAP::UnlabeledValueArg<std::string> dataLocationsFile("DataLocations.json", "File containing data locations in JSON format.", true, EMPTY_DEFAULT, "DataLocations.json");
        cmd.add( dataLocationsFile );
        
        TCLAP::UnlabeledValueArg<int> M("M", "Number of modes to infer.", true, 0, "M");
        cmd.add( M );
        
        TCLAP::UnlabeledValueArg<std::string> outputDir("outputDirectory", "Output directory.", true, EMPTY_DEFAULT, "outputDirectory");
        cmd.add( outputDir );
        
        // Optional arguments
        // (flag*, name, description, required, default, type)
        // * flag can only be one character
        // The help text displays these in reverse order!
        
        // Normalisation
        TCLAP::SwitchArg noNormalisation("", "noDataNormalisation", "BE VERY CAREFUL! This turns off the internal data normalisation, which scales the data such that it matches the generative model. If you are confident in your own preprocessing this can be disabled here...", cmd, false);

        // big data
        TCLAP::SwitchArg bigData("", "bigData", "Switch on for large datasets of hundreds/thousands of subjects (e.g. Biobank)", cmd, false);
        
        // use MIGP for PCA
        TCLAP::SwitchArg MIGP("", "MIGP", "Switch on to use MIGP algorithm for initial group-level spatial basis decomposition (default for large datasets e.g. Biobank)", cmd, false);
        
        // DoF
        TCLAP::ValueArg<float> dof("d", "dofCorrection", "Spatial degrees of freedom correction factor.", false, 1.0, "float");
        cmd.add( dof );
        
        // K (data rank)
        TCLAP::ValueArg<int> K("k", "lowRankData", "Use a low rank approximation to the raw data. Supply this argument with the required rank. If not specifed, will use full rank data.", false, 0, "int");
        cmd.add( K );  

        // stochastic beta
        TCLAP::ValueArg<float> BetaVal("", "stochasticBeta", "forget rate in case of stochastic PROFUMO", false, 0.6, "float");
        cmd.add( BetaVal );
        
        // K (data rank) 
        TCLAP::ValueArg<int> batchSize("", "stochasticBatchSize", "If bigData option switched on, divide subjects into batches for stochastic inference. Supply this argument with the required batch size. Default 100.", false, 100, "int");
        cmd.add( batchSize );
        
        TCLAP::ValueArg<int> batchUpdates("", "stochasticBatchUpdates", "If bigData option switched on, divide subjects into batches for stochastic inference. Supply this argument with the required number of updates per batch. Default 20.", false, 20, "int");
        cmd.add( batchUpdates );
        
        TCLAP::ValueArg<int> initialBatchUpdates("", "stochInitBatchUpdates", "If bigData option switched on, divide subjects into batches for stochastic inference. Supply this argument with the required number of initial updates (subject-level only) per batch. Default 20.", false, 20, "int");
        cmd.add( initialBatchUpdates );

        // average group iteration across batches
        TCLAP::ValueArg<int> groupIter("", "groupIteration", "If bigData option switched on, specify number of group iterations. Supply this argument with the required number of overall group updates. Default 4000.", false, 4000, "int");
        cmd.add( groupIter );

        // average subject picks across batches
        TCLAP::ValueArg<float> subjectIter("", "subjectIteration", "If bigData option switched on, specify average number of times to pick a subject in a batch. Default 2.5", false, 2.5, "float");
        cmd.add( subjectIter );
        
        // Multi start
        TCLAP::ValueArg<int> multiStartIterations("", "multiStartIterations", "Number of iterations of the group-level spatial decomposition to do before inferring the full model. [Default: 5]", false, 5, "int");
        cmd.add( multiStartIterations );
        
        // Cov model
        std::vector<std::string> allowedCM {"Subject", "Run", "Categorised_Runs"};
        TCLAP::ValuesConstraint<std::string> allowedCMVals = TCLAP::ValuesConstraint<std::string>( allowedCM );
        TCLAP::ValueArg<std::string> temporalPrecisionModel("", "covModel", "Covariance model type. [Default: Subject]", false, "Subject", &allowedCMVals);
        cmd.add( temporalPrecisionModel );
        
        // HRF file
        TCLAP::ValueArg<std::string> hrfFile("", "hrfFile", "Alternative shape for the HRF (uses FLOBS by default). Can only be supplied with the --useHRF argument.", false, EMPTY_DEFAULT, "file");
        cmd.add( hrfFile );
        
        // HRF / TR
        TCLAP::ValueArg<float> TR("", "useHRF", "Include if an HRF-based temporal model is desired. Supply the TR with this argument.", false, 0.0, "TR");
        cmd.add( TR );
        
        // Neighbours
        TCLAP::ValueArg<std::string> spatialNeighboursFile("", "spatialNeighbours", "File containing the list of spatial neighbours for each voxel.", false, EMPTY_DEFAULT, "file");
        cmd.add( spatialNeighboursFile );
        
        // predefined precisions- only for initialisation, unless fixedParams is true
        TCLAP::ValueArg<std::string> groupPrecisionsFile("", "SpatialPrecisions", "Use this option to supply a set of voxelwise precisions. Subject parameters will then be inferred based on these group values.", false, EMPTY_DEFAULT, "file");
        cmd.add( groupPrecisionsFile );
        
        // predefined  memberships
        TCLAP::ValueArg<std::string> groupMembershipsFile("", "SpatialMemberships", "Use this option to supply a set of voxelwise memberships. Subject parameters will then be inferred based on these group values.", false, EMPTY_DEFAULT, "file");
        cmd.add( groupMembershipsFile );
        
        // Fixed means
        TCLAP::ValueArg<std::string> groupMeansFile("", "SpatialMeans", "Use this option to supply a set of voxelwise means. Subject parameters will then be inferred based on these group values.", false, EMPTY_DEFAULT, "file");
        cmd.add( groupMeansFile );
        
        // predefined initial maps
        TCLAP::ValueArg<std::string> predefinedInitialMapsFile("", "PredefinedInitialMaps", "Use this option to supply a set of voxelwise InitialMaps. If not specified, PROFUMO will calculate spatial ICA as initialisation", false, EMPTY_DEFAULT, "file");
        cmd.add( predefinedInitialMapsFile );
        
        // predefined initial maps
        TCLAP::ValueArg<std::string> predefinedSpatialBasisFile("", "PredefinedSpatialBasis", "Use this option to supply a set of voxelwise SpatialBasis. If not specified, PROFUMO will calculate spatial basis", false, EMPTY_DEFAULT, "file");
        cmd.add( predefinedSpatialBasisFile );

        // fixed parameters 
        TCLAP::SwitchArg fixedParams("", "fixedParams", "To determine whether spatial parameters (i.e. means, memberships, precisions) should be kept fixed or be estimated", cmd, false);
        
        // Fully predefined
        TCLAP::SwitchArg fullPredefined("", "fullPredefined", "Switch on to keep the group model fully fixed and only infer subject models", cmd, false);

        // Fully Predefined path
        TCLAP::ValueArg<std::string> fullPredefinedInPath("", "fullPredefinedInPath", "Path to .pfm directory to take group model from, should be the standard PFM output. Can only be supplied with the --fullPredefined argument.", false, EMPTY_DEFAULT, "file");
        cmd.add( fullPredefinedInPath );

        // Fully Predefined number of updates
        TCLAP::ValueArg<int> fullPredefinedIter("", "fullPredefinedIter", "If fully-predefined option switched on, estimate subject models based on a fully pre-fixed group model. Supply this argument with the required number iterations (subject-level only). Default 50.", false, 50, "int");
        cmd.add( fullPredefinedIter );

        // Mask
        TCLAP::ValueArg<std::string> maskFile("m", "mask", "Use to mask the raw data / any pre-specified parameters.", false, EMPTY_DEFAULT, "file");
        cmd.add( maskFile );
        
        // Spatial model
        std::vector<std::string> allowedSM {"Modes", "Parcellation"};
        TCLAP::ValuesConstraint<std::string> allowedSMVals = TCLAP::ValuesConstraint<std::string>( allowedSM );
        TCLAP::ValueArg<std::string> spatialModel("", "spatialModel", "Spatial model type. [Default: Modes]", false, "Modes", &allowedSMVals);
        cmd.add( spatialModel );
        
        
        // Parse!
        cmd.parse( argc, argv );
        
        
        // And store everything
        
        // Spec file
        args.dataLocationsFile = dataLocationsFile.getValue();
        
        // Modes
        const int iM = M.getValue();
        if ( iM > 0 ) { args.M = (unsigned int) iM; }
        else { throw std::invalid_argument("Number of modes (M) must be a positive integer."); }
        
        // Output dir
        args.outputDir = cleanOutputDir( outputDir.getValue() );
        
        // Spatial model
        const std::string sM = spatialModel.getValue();
        if (sM == "Modes") {
            args.spatialModel = PROFUMO::ModelManager::SpatialModel::MODES;
        }
        else if (sM == "Parcellation") {
            args.spatialModel = PROFUMO::ModelManager::SpatialModel::PARCELLATION;
        }
        else {
            throw std::invalid_argument("\"" + sM + "\" is not a valid spatial model.");
        }
        
        // Mask
        args.maskFile = maskFile.getValue();
        args.useMask = (args.maskFile != EMPTY_DEFAULT);
        
        // Group parameters
        args.groupMeansFile = groupMeansFile.getValue();
        args.useGroupMeans = (args.groupMeansFile != EMPTY_DEFAULT);
        args.predefinedInitialMapsFile = predefinedInitialMapsFile.getValue();
        args.predefinedSpatialBasisFile = predefinedSpatialBasisFile.getValue();
        args.usePredefinedInitialMaps = (args.predefinedInitialMapsFile != EMPTY_DEFAULT);
        args.usePredefinedSpatialBasis = (args.predefinedSpatialBasisFile != EMPTY_DEFAULT);
        args.groupPrecisionsFile = groupPrecisionsFile.getValue();
        args.useGroupPrecisions = (args.groupPrecisionsFile != EMPTY_DEFAULT);
        args.groupMembershipsFile = groupMembershipsFile.getValue();
        args.useGroupMemberships = (args.groupMembershipsFile != EMPTY_DEFAULT);
        args.useFixedParameters = fixedParams.getValue(); // (args.useGroupMeans || args.useGroupPrecisions || args.useGroupMemberships);
        if (args.useFixedParameters && !((args.useGroupMeans || args.useGroupPrecisions || args.useGroupMemberships))) {
            throw std::invalid_argument("Please specify SpatialMeans, SpatialMemberships or SpatialPrecisions with fixed parameters...");
        }
        args.bigData=bigData.getValue();
        args.fullPredefined=fullPredefined.getValue();
        args.fullPredefinedIter=fullPredefinedIter.getValue();
        args.fullPredefinedInPath=fullPredefinedInPath.getValue();
        args.MIGP=MIGP.getValue();
        if (args.bigData) {
            args.MIGP=true;
        }
        // if ((args.fullPredefined == true) && (args.fullPredefinedIter == 0)){args.fullPredefinedIter = 50};
        // Spatial neighbours
        args.spatialNeighboursFile = spatialNeighboursFile.getValue();
        args.useSpatialNeighbours = (args.spatialNeighboursFile != EMPTY_DEFAULT);
        if ((args.useSpatialNeighbours) && (args.spatialModel == PROFUMO::ModelManager::SpatialModel::MODES)) {
            throw std::invalid_argument("Spatial neighbourhood only applies to the Parcellation model.");
        }
        // Temporal covariance model
        const std::string tpM = temporalPrecisionModel.getValue();
        if (tpM == "Subject") {
            args.temporalPrecisionModel = PROFUMO::ModelManager::TemporalPrecisionModel::SUBJECT_PRECISION_MATRICES;
        }
        else if (tpM == "Run") {
            args.temporalPrecisionModel = PROFUMO::ModelManager::TemporalPrecisionModel::RUN_PRECISION_MATRICES;
        }
        else if (tpM == "Categorised_Runs") {
            args.temporalPrecisionModel = PROFUMO::ModelManager::TemporalPrecisionModel::CATEGORISED_RUN_PRECISION_MATRICES;
        }
        else {
            throw std::invalid_argument("\"" + tpM + "\" is not a valid covariance model.");
        }
        
        // HRF / TR 
        args.TR = TR.getValue();
        if (args.TR > 0.0) { args.useHRF = true; }
        else if (args.TR == 0.0) { args.useHRF = false; }
        else { throw std::invalid_argument("TR must be positive."); }
        // HRF file
        args.hrfFile = hrfFile.getValue();
        if ((!args.useHRF) && (args.hrfFile != EMPTY_DEFAULT)) { throw std::invalid_argument("--hrfFile can only be used with --useHRF!"); }
        // Check it exists / set the default (if we are going to use it...)
        if (args.useHRF) {
            
            // FLOBS default
            if (args.hrfFile == EMPTY_DEFAULT) {
                // http://stackoverflow.com/a/5867281
                // http://stackoverflow.com/q/3483327
                char const* tmp = std::getenv( "FSLDIR" );
                if ( tmp == nullptr ) { throw std::runtime_error("Unable to find $FSLDIR!"); }
                const std::string fslDir( tmp );
                
                args.hrfFile = fslDir + "/etc/default_flobs.flobs/PROFUMO_HRF.txt";
            }
            
            // And check file exists!
            std::ifstream hrfTest(args.hrfFile);
            if (!hrfTest.good()) { throw std::runtime_error("Unable to find HRF file! " + args.hrfFile); };
        }
        
        // Low rank data
        int iK = K.getValue();
        if ( iK >= 0 ) { args.K = (unsigned int) iK; }
        else { throw std::invalid_argument("Rank of reduced data (K) must be a positive integer."); }
        
        // Batch sizes, ignore if arg.bigData is false
        //if (args.bigData) {
        //   int iB = BatchS.getValue();
        //    if ( iB >= 0 ) { args.BatchS = (unsigned int) iB; }
        //    else { throw std::invalid_argument("Batch size (B) must be a positive integer."); }
        //}
        // DoF
        args.dof = dof.getValue();
        args.BetaVal = BetaVal.getValue();
        args.batchSize = batchSize.getValue();
        args.batchUpdates = batchUpdates.getValue();
        args.initialBatchUpdates = initialBatchUpdates.getValue();
        args.groupIter=groupIter.getValue();
        args.subjectIter=subjectIter.getValue();
        
        if (args.dof <= 0.0) { throw std::invalid_argument("Spatial degrees of freedom correction factor (dof) must be positive."); }
        if (args.BetaVal <= 0.5) { throw std::invalid_argument("For stochastic inference, beta (i.e. forget rate) value must be > 0.5"); }
        if (args.batchSize <= 0.0) { throw std::invalid_argument("For stochastic inference, batch size must be positive"); }
        if (args.batchUpdates <= 0.0) { throw std::invalid_argument("For stochastic inference, number of batch updates must be positive"); }
        if (args.initialBatchUpdates <= 0.0) { throw std::invalid_argument("For stochastic inference, number of initial batch updates must be positive"); }
        if (args.groupIter <= 0.0) { throw std::invalid_argument("For stochastic inference, number of group iterations must be positive integer"); }
        if (args.subjectIter <= 1.0) { throw std::invalid_argument("For stochastic inference, number of subject picks must be at least 1.0"); }
        
        // Normalisation
        args.normaliseData = ! noNormalisation.getValue();
        
        // Multi start
        int iMSI = multiStartIterations.getValue();
        if ( iMSI >= 0 ) { args.multiStartIterations = (unsigned int) iMSI; }
        else { throw std::invalid_argument("Number of multi-start iterations must be a positive integer."); }
    }
    catch (TCLAP::ArgException &e)  // Catch any exceptions
    {
        std::cerr << "Error: " << e.error() << " for arg " << e.argId() << std::endl;
        return -1;
    }
    
    
    std::cout << "Beginning PROFUMO analysis." << std::endl;
    //arma::fmat APtest = PROFUMO::Utilities::loadDataMatrix("/vols/Scratch/rezvanh/HCP_Profumo_cifti2_bigdata_200subs_subin_M150.pfm/BigDataInitialDir/Subjects/589567/Runs/R2_LR/RunPreprocessed_Ad.bin");
    //std::cout << APtest.min() << std::endl;
    // Date and time
    /*const*/ std::time_t t = std::time(nullptr);
    std::cout << "Started at " << std::put_time(std::localtime(&t), timeFormat) << "." << std::endl << std::endl;
    
    ////////////////////////////////////////////////////////////////////////
    // Make key directories and copy key files
    
    // Make output directory
    mkdir( args.outputDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    
    // Make directory for preprocessing output
    const std::string preprocessingDir = args.outputDir + "Preprocessing/";
    mkdir( preprocessingDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    
    // Make directory for intermediate output
    const std::string intermediatesBaseDir = args.outputDir + "Intermediates/";
    mkdir( intermediatesBaseDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    
    // Make directory for final output
    const std::string resultsDir = args.outputDir + "FinalModel/";
    mkdir( resultsDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    
    // Make subdirectory for bigdata initialisation if the argument is switched on
    const std::string bigData_initialDir= args.outputDir + "BigDataInitialDir/";
    if (args.bigData) {        
        mkdir( bigData_initialDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH ); 
    }

    // Make subdirectory for predefined initialisation if the argument is switched on
    const std::string fullPredefined_OutPath= args.outputDir + "fullPredefinedOutPath/";
    if (args.fullPredefined) {        
        mkdir( fullPredefined_OutPath.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH ); 
    }
    
    // Copy spec file
    copyFile(args.dataLocationsFile, args.outputDir + "DataLocations.json");
    
    // And neighbours if necessary
    if (args.useSpatialNeighbours) {
        copyFile(args.spatialNeighboursFile, args.outputDir + "SpatialNeighbours.txt");
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Create a config file with key parameters
    
    std::cout << "Creating config file..." << std::endl;
    nlohmann::json config;

    // Information about the code (time & date, version etc)
    // Use "> " to force some key information to be displayed first
    config["> Description"] = "PROFUMO configuration file";
    config["> Version"] = "Development version";
    config["> Version [build time]"] = PROFUMO::Utilities::getBuildDate() + "; " + PROFUMO::Utilities::getBuildTime();
    // This is a mess! No C++1x support for timezones etc, so we have to fall 
    // back on std::localtime from <ctime> to do the dirty work. Urgh...
    /*const*/ /*std::time_t*/ t = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::ostringstream time; time << std::put_time(std::localtime(&t), dateFormat);
    config["> Timestamp"] = time.str();
    
    // Include full command
    std::ostringstream command;
    for (int i = 0; i < argc; ++i) {
        command << argv[i];
        if (i < argc-1) {
            command << " ";
        }
    }
    config["Command"] = command.str();
    
    // And then all the parsed / default arguments
    config["Data locations file"] = args.dataLocationsFile;
    config["Number of modes"] = args.M;
    config["Output directory"] = args.outputDir;
    config["DoF correction"] = args.dof;
    config["Data normalisation"] = args.normaliseData;
    config["Big Data"] = args.bigData;
    config["Multi-start iterations"] = args.multiStartIterations;
    if ( args.useMask ) {
        config["Mask file"] = args.maskFile;
    }
    if ( args.K == 0 ) {
        config["Data type"] = "Full rank";
    } else {
        std::ostringstream rank; rank << "Low rank (" << args.K << " components)";
        config["Data type"] = rank.str();
    }
    
    // Spatial model
    switch (args.spatialModel) {
        case PROFUMO::ModelManager::SpatialModel::MODES : {
            config["Spatial model"] = "Modes";
            break;
        }
        case PROFUMO::ModelManager::SpatialModel::PARCELLATION : {
            config["Spatial model"] = "Parcellation";
            break;
        }
        default :
            throw std::invalid_argument("PROFUMO: bad SpatialModel identifier.");
    }
    
    // Spatial parameters
    if (args.useGroupMeans) {
        config["Fixed group parameters"]["Spatial means file"] = args.groupMeansFile;
    }
    if (args.useGroupPrecisions) {
        config["Fixed group parameters"]["Spatial precisions file"] = args.groupPrecisionsFile;
    }
    if (args.useGroupMemberships) {
        config["Fixed group parameters"]["Spatial memberships file"] = args.groupMembershipsFile;
    }
    if (args.useSpatialNeighbours) {
        config["Spatial neighbours file"] = args.spatialNeighboursFile;
    }
    if (args.usePredefinedInitialMaps) {
        config["Predefined Initial Maps file"] = args.predefinedInitialMapsFile;
    }
    if (args.usePredefinedSpatialBasis) {
        config["Predefined Spatial Basis file"] = args.predefinedSpatialBasisFile;
    }
    
    // Temporal model
    if (args.useHRF) {
        config["Temporal model"] = "HRF based";
        config["HRF parameters"]["TR (s)"] = args.TR;
        config["HRF parameters"]["HRF file"] = args.hrfFile;
    } else {
        config["Temporal model"] = "Multivariate normal";
    }
    
    // Covariance model
    switch (args.temporalPrecisionModel) {
        case PROFUMO::ModelManager::TemporalPrecisionModel::CONSTANT : {
            config["Covariance model"] = "Constant";
            break;
        }
        case PROFUMO::ModelManager::TemporalPrecisionModel::CONSTANT_GROUP : {
            config["Covariance model"] = "Subject (constant group-level prior)";
            break;
        }
        case PROFUMO::ModelManager::TemporalPrecisionModel::GROUP_PRECISION_MATRIX : {
            config["Covariance model"] = "Group";
            break;
        }
        case PROFUMO::ModelManager::TemporalPrecisionModel::SUBJECT_PRECISION_MATRICES : {
            config["Covariance model"] = "Subject";
            break;
        }
        case PROFUMO::ModelManager::TemporalPrecisionModel::RUN_PRECISION_MATRICES : {
            config["Covariance model"] = "Run";
            break;
        }
        case PROFUMO::ModelManager::TemporalPrecisionModel::CATEGORISED_RUN_PRECISION_MATRICES : {
            config["Covariance model"] = "Categorised Runs";
            break;
        }
        default :
            throw std::invalid_argument("PROFUMO: bad TemporalPrecisionModel identifier.");
    }
    
    
    // Save!
    const std::string configFileName = args.outputDir + "Configuration.json";
    std::ofstream configFile; configFile.open( configFileName.c_str() );
    // Spit out the JSON
    configFile << std::setw(4) << config << std::endl;  // pretty printing via setw, sets number of spaces to indent
    configFile.close();
    
    
    // And log
    std::cout << "Done! See: " << configFileName << std::endl << std::endl;
    
    std::cout << std::string(29, '-') << " Config file contents " << std::string(29, '-') << std::endl;
    std::ifstream printConfig; printConfig.open( configFileName.c_str() );
    std::cout << printConfig.rdbuf();
    printConfig.close();
    std::cout << std::string(80, '-') << std::endl << std::endl;
    
    ////////////////////////////////////////////////////////////////////////
    // Set key options and load any necessary parameters from file
    
    PROFUMO::ModelManager::Options options;
    options.K = args.K;
    options.normaliseData = args.normaliseData;
    options.bigData = args.bigData;
    options.fullPredefined=args.fullPredefined;
    options.fullPredefinedIter=args.fullPredefinedIter;
    if (args.fullPredefined){ options.fullPredefinedInPath = args.fullPredefinedInPath;} else {options.fullPredefinedInPath = "";}
    if (args.fullPredefined){ options.fullPredefinedOutPath = fullPredefined_OutPath;} else {options.fullPredefinedOutPath = "";}
    options.MIGP = args.MIGP;
    if (args.bigData){ options.bigDataDir = bigData_initialDir;} else {options.bigDataDir = "";}
    options.dofCorrectionFactor = args.dof;
    options.multiStartIterations = args.multiStartIterations;
    options.batchNum=60;
    //options.batchUpdates=20;
    
    if (args.useHRF) {
        options.TR = args.TR;
        options.hrfFile = args.hrfFile;
    }
    
    // Mask
    if (args.useMask) {
        // Take a copy first
        // THIS IS A F*** FILE MANIPULATION DISASTER
        // Get the basename
        std::string baseName = args.maskFile;
        if (baseName.find_last_of("/") != std::string::npos) {
            baseName = baseName.substr( args.maskFile.find_last_of("/") + 1 );
        }
        // And then find the extension
        std::string extension = "";
        if (baseName.find_first_of(".") != std::string::npos) {
            extension = baseName.substr( baseName.find_first_of(".") );
        }
        // And copy!
        copyFile(args.maskFile, preprocessingDir + "Mask" + extension);
        
        // Load mask and transform to indices
        const arma::fmat maskData = PROFUMO::Utilities::loadDataMatrix( args.maskFile );
        options.maskInds = std::make_shared<const arma::uvec>(arma::find( maskData != 0.0 ));
    }
    
    // Load group parameters
    PROFUMO::ModelManager::SpatialParameters spatialParameters;
    // Means
    if (args.useGroupMeans) {
        // Load
        arma::fmat groupMeans = PROFUMO::Utilities::loadDataMatrix( args.groupMeansFile );
        // Mask, but ignore if the data is already the same size (i.e. it came from an already masked posterior)
        if (args.useMask && (groupMeans.n_rows != options.maskInds->n_elem)) {
            groupMeans = groupMeans.rows( *options.maskInds );
        }
        // Rescale
        /* groupMeans /= arma::stddev( arma::vectorise(groupMeans) );
        groupMeans *= 0.25; // ... */
        // Save
        groupMeans.save(args.outputDir + "GroupMapMeans.hdf5", arma::hdf5_binary);
        // Record
        spatialParameters.GroupMeans = std::make_shared<arma::fmat>( groupMeans );
    }
    // Precisions
    if (args.useGroupPrecisions) {
        arma::fmat groupPrecisions = PROFUMO::Utilities::loadDataMatrix( args.groupPrecisionsFile );
        if (args.useMask && (groupPrecisions.n_rows != options.maskInds->n_elem)) {
            groupPrecisions = groupPrecisions.rows( *options.maskInds );
        }
        groupPrecisions.save(args.outputDir + "GroupMapPrecisions.hdf5", arma::hdf5_binary);
        spatialParameters.GroupPrecisions = std::make_shared<arma::fmat>( groupPrecisions );
    }
    // Memberships
    if (args.useGroupMemberships) {
        arma::fmat groupMemberships = PROFUMO::Utilities::loadDataMatrix( args.groupMembershipsFile );
        if (args.useMask && (groupMemberships.n_rows != options.maskInds->n_elem)) {
            groupMemberships = groupMemberships.rows( *options.maskInds );
        }
        // Relax the parcellation ever so slightly - allow some subject variability!
        /* const float relaxation = 0.01; // Max p = 0.99
        groupMemberships *= (1.0 - relaxation);
        groupMemberships += (relaxation / args.M); */
        groupMemberships.save(args.outputDir + "GroupMapMemberships.hdf5", arma::hdf5_binary);
        spatialParameters.GroupMemberships = std::make_shared<arma::fmat>( groupMemberships );
    }
    
    // Load spatial memberships 
    if (args.useSpatialNeighbours) {
        spatialParameters.spatialNeighbours = loadNeighbours(args.spatialNeighboursFile);
    }
    
    // Precisions
    if (args.usePredefinedInitialMaps) {
        arma::fmat predefinedInitialMaps = PROFUMO::Utilities::loadDataMatrix( args.predefinedInitialMapsFile );
        if (args.useMask && (predefinedInitialMaps.n_rows != options.maskInds->n_elem)) {
            predefinedInitialMaps = predefinedInitialMaps.rows( *options.maskInds );
        }
        predefinedInitialMaps.save(args.outputDir + "PredefinedInitialMaps.hdf5", arma::hdf5_binary);
        spatialParameters.predefinedInitialMaps = std::make_shared<arma::fmat>( predefinedInitialMaps );
    }
    
    if (args.usePredefinedSpatialBasis) {
        arma::fmat predefinedSpatialBasis = PROFUMO::Utilities::loadDataMatrix( args.predefinedSpatialBasisFile );
        if (args.useMask && (predefinedSpatialBasis.n_rows != options.maskInds->n_elem)) {
            predefinedSpatialBasis = predefinedSpatialBasis.rows( *options.maskInds );
        }
        predefinedSpatialBasis.save(args.outputDir + "PredefinedSpatialBasis.hdf5", arma::hdf5_binary);
        spatialParameters.predefinedSpatialBasis = std::make_shared<arma::fmat>( predefinedSpatialBasis );
    }
    spatialParameters.randomInitialisation=true;
    
    ////////////////////////////////////////////////////////////////////////////
    // Make full model
    
    std::cout << "Initialising model..." << std::endl << std::endl;
    
    PROFUMO::ModelManager::SpatialModel           spatialModel;
    PROFUMO::ModelManager::WeightModel            weightModel;
    PROFUMO::ModelManager::TimeCourseModel        timeCourseModel, initialTimeCourseModel;
    PROFUMO::ModelManager::TemporalPrecisionModel temporalPrecisionModel;
    PROFUMO::ModelManager::NoiseModel             noiseModel;
    
    // Spatial maps
    spatialModel = args.spatialModel;
    
    // Weights
    weightModel = PROFUMO::ModelManager::WeightModel::RECTIFIED_MULTIVARIATE_NORMAL;
    
    // Time courses
    if (args.useHRF) {
        timeCourseModel = PROFUMO::ModelManager::TimeCourseModel::NOISY_HRF;
    }
    else {
        timeCourseModel = PROFUMO::ModelManager::TimeCourseModel::MULTIVARIATE_NORMAL;
    }
    // Simpler initial model if we are inferring group maps too 
    if (( args.useFixedParameters ) || ( args.fullPredefined )) {
        initialTimeCourseModel = timeCourseModel;
    }
    else {
        initialTimeCourseModel = PROFUMO::ModelManager::TimeCourseModel::MULTIVARIATE_NORMAL;
    }
    
    // Temporal precision matrices
    temporalPrecisionModel = args.temporalPrecisionModel;
    
    // Noise
    noiseModel = PROFUMO::ModelManager::NoiseModel::INDEPENDENT_RUNS;
    
    // Constructor: Make the model for non-stochastic Profumo, preprocess data and make initial maps otherwise. Note: the Datastore is empty in the latter
    // we can't keep all the data in memory! instead, we return a subjectlist to pick batches from.
    PROFUMO::ModelManager model(
            args.dataLocationsFile,
            args.M,
            spatialModel,
            spatialParameters,
            weightModel,
            initialTimeCourseModel,
            temporalPrecisionModel,
            noiseModel,
            options,
            preprocessingDir
        );
    
    std::cout << "model initialised!" << std::endl;
    // Save model initialisation (based on all the subjects) for bigdata option
    // Make subdirectory for bigdata initialisation if the argument is switched on

    if (options.bigData) {
        //model.save(bigData_initialDir);

        ////////////////////////////////////////////////////////////////////////////
        // setup batches
        arma::fmat initialMaps;
        const std::string subListDir = options.bigDataDir + "SubjectNamesList.txt";
        std::vector<std::string> subNamesList;
        std::ifstream inList(subListDir.c_str());     
    	// Check if object is valid
    	if(!inList.good())
    	{
    		std::cerr << "Cannot open the File : "<< subListDir <<std::endl;
    		return false;
    	} 
    	std::string str;
    	while (std::getline(inList, str))
    	{
    		if(str.size() > 0)
    			subNamesList.push_back(str);
    	}
    	inList.close();
    	
        unsigned int nsubs=subNamesList.size();
        //unsigned int batchSizeFlexi=nsubs/10;
        //unsigned int batchSizeFix=20;
        unsigned int batchSize=args.batchSize;
        std::cout << "batchSize" << std::endl;
        std::cout << batchSize << std::endl;
        
        options.batchUpdates=args.batchUpdates;
        unsigned int batchUpdates=options.batchUpdates;
        std::cout << "batchUpdates" << std::endl;
        std::cout << batchUpdates << std::endl;
        
        options.initialBatchUpdates=args.initialBatchUpdates;
        unsigned int initialBatchUpdates=options.initialBatchUpdates;
        unsigned int groupIters = args.groupIter;
        float subjectIters = args.subjectIter;
        std::cout << "initialBatchUpdates" << std::endl;
        std::cout << initialBatchUpdates << std::endl;
        
        // We want to make sure that group is updated at least 4000 times 
        // (we have noisy updates so makes sense to allow more iterations compared to classic)
        // We also want to make sure that each subject is updated at least batchUpdates*3 times alongside the group, plus initial update:
        unsigned int minBatchNumGroup=std::round(groupIters/options.batchUpdates);
        unsigned int minBatchNumSubject=std::round((subjectIters*nsubs*options.batchUpdates)/(args.batchSize*options.batchUpdates));
        options.batchNum = std::max(minBatchNumGroup,minBatchNumSubject);
        std::cout << "Number of Batches" << std::endl;
        std::cout << options.batchNum << std::endl;
        
        unsigned int batchNum=options.batchNum;
        unsigned int initialBatch0Updates=10;
        bool getPopulationFreeEnergy=false;
        
        double Ti=0.3;
        arma::uvec subPicked=arma::zeros<arma::uvec>(nsubs);
        arma::uvec linOrderProb = arma::linspace<arma::uvec>(0, nsubs-1, nsubs);
        arma::uvec randOrder;
        arma::uvec randOrderBatch;
        arma::uvec thisSubRep;
        std::vector<std::string> batchSubNamesList;
        int rhoCnt = 0;
        int rhoAlpha = 5;
        float rhoBeta = -args.BetaVal;//0.6;
        std::cout << "Beta" <<std::endl;
        std::cout << rhoBeta <<std::endl;
	    std::cout << batchNum <<std::endl;
        float currentRho;
        for (unsigned int batchcnt=0; batchcnt<batchNum; ++batchcnt) {
            options.batchCnt=batchcnt;
            std::srand(std::time(0));
            randOrder = arma::shuffle(linOrderProb);
            randOrderBatch=arma::zeros<arma::uvec>(batchSize);
            
            for (unsigned int elcnt=0; elcnt<batchSize; ++elcnt){
                unsigned long long thisElem = randOrder[0];
                randOrderBatch[elcnt]=thisElem;
                batchSubNamesList.push_back(subNamesList[thisElem]);
                randOrder=randOrder(arma::find(randOrder!=thisElem));
            }
            
            std::cout << "randOrderBatch: " << randOrderBatch.t() << std::endl;
            std::string initialMapDir = preprocessingDir + "InitialMaps.hdf5";
            initialMaps.load(initialMapDir, arma::hdf5_binary);
            model.initialiseBatchData(args.dataLocationsFile, batchSubNamesList, initialMaps, spatialModel, spatialParameters, weightModel, timeCourseModel, timeCourseModel, temporalPrecisionModel, noiseModel, options, 0, nsubs);
            
            const std::string bigData_intermediateDir= options.bigDataDir + "BigDataIntermediateDir/";
            mkdir( bigData_intermediateDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH ); 
            
            if (batchcnt==0){
                for (unsigned int updatecnt=0; updatecnt<initialBatch0Updates; ++updatecnt) {
                    model.update(1, options.bigDataDir, true, true, 1.0);
                    model.saveIntermediates(intermediatesBaseDir, batchcnt, batchNum, true, nsubs, updatecnt, initialBatch0Updates, bigData_intermediateDir); 
                }
                model.saveIntermediates(intermediatesBaseDir, batchcnt, batchNum, true, nsubs, initialBatch0Updates, batchUpdates, bigData_intermediateDir); 
                for (unsigned int updatecnt=0; updatecnt<batchUpdates; ++updatecnt) {                
                    model.update(1, options.bigDataDir, false, true, 1.0);
                }
                
            } else {
                rhoCnt++;
                currentRho = std::pow((rhoCnt+rhoAlpha),rhoBeta);
                for (unsigned int updatecnt=0; updatecnt<batchUpdates; ++updatecnt) {                
                    model.update(1, options.bigDataDir, false, true, currentRho); 
                    //model.saveIntermediates(intermediatesBaseDir, batchcnt, batchNum, true, nsubs, updatecnt+1, batchUpdates, bigData_intermediateDir); 
                }
            }
            
            model.saveIntermediates(intermediatesBaseDir, batchcnt, batchNum, true, nsubs, 0, 0, bigData_intermediateDir); 
            model.save(bigData_intermediateDir);
            
            
            //intermediatesDir = intermediatesBaseDir + "Model" + std::to_string(modelNumber) + "/";
            //mkdir( intermediatesDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH ); modelNumber++;
            // initialise this batch
            
            // model updates here
            
            subPicked.elem(randOrderBatch)+=1;
            linOrderProb.reset();
            for (unsigned int subcnt=0; subcnt<nsubs; ++subcnt){
                unsigned int subCoef = std::round(100*std::pow(Ti,subPicked[subcnt]));
                if (subCoef<1){
                    subCoef=1;
                }
                thisSubRep = subcnt*arma::ones<arma::uvec> (subCoef);
                linOrderProb=arma::join_vert( linOrderProb, thisSubRep );
            }
            batchSubNamesList.clear();
            if (getPopulationFreeEnergy) {
                std::cout << "Getting free energy for entire population based on the current group estimation..." << std::endl;
                if ((remainder(batchcnt,8)==1) || (batchcnt>244)){
                    for (unsigned int subi=0; subi<subNamesList.size(); ++subi) {
                        const std::string thisSubjectDirName = bigData_intermediateDir + "Subjects" + "/" + subNamesList[subi] + "/"; //+ "/" + "SpatialMaps.post" + "/" + "Signal" + "/" + "Means.hdf5";
                        DIR* thisSubjectDir = opendir(thisSubjectDirName.c_str());
                        if ((thisSubjectDir) && (options.batchCnt>0)) { 
                            batchSubNamesList.push_back(subNamesList[subi]);
                            model.initialiseBatchData(args.dataLocationsFile, batchSubNamesList, initialMaps, spatialModel, spatialParameters, weightModel, timeCourseModel, timeCourseModel, temporalPrecisionModel, noiseModel, options, 2, nsubs);
                        }
                        closedir(thisSubjectDir);
                        //model.update(1, options.bigDataDir, false, true, 1.0);
                        batchSubNamesList.clear();
                    }
                }
            }
        }
        model.alignModelSigns(true,false); 
        for (unsigned int subi=0; subi<subNamesList.size(); ++subi) {
            batchSubNamesList.push_back(subNamesList[subi]);
            model.initialiseBatchData(args.dataLocationsFile, batchSubNamesList, initialMaps, spatialModel, spatialParameters, weightModel, timeCourseModel, timeCourseModel, temporalPrecisionModel, noiseModel, options, 1, nsubs);
            //model.update(1, options.bigDataDir, false, true, 1.0);
            std::cout << "Saving results..." << std::endl << std::endl;
            if (subi==subNamesList.size()-1){
                model.save(resultsDir);
            } else {
                model.save(resultsDir,true, false);
            }
            batchSubNamesList.clear();
        }
        //model.reinitialiseBatch(args.dataLocationsFile, thisBatchList, options);  
        std::cout << "Deleting preprocessed data from disk" << std::endl;
        const std::string toRemove_directory= options.bigDataDir + "Subjects/";
        removeDirectory(toRemove_directory);
    } else if (options.fullPredefined){

        model.update(10, {""}, true, options.bigData, options.fullPredefined);  // Normalise weights
        model.update(options.fullPredefinedIter, {""},false, options.bigData, options.fullPredefined);
        std::cout << "Saving results..." << std::endl << std::endl;
        model.save(resultsDir, true, true);
        
    } else {     
    
        ////////////////////////////////////////////////////////////////////////////
        // Solve full model
        
        std::cout << "Model initialised, starting inference." << std::endl << std::endl;
        std::string intermediatesDir;
        unsigned int modelNumber = 1;
        
        // Updates for full inference
        if ( ! args.useFixedParameters ) {
            
            //----------------------------------------------------------------------
            
            // Initial updates
            
            // First batch with normalised time courses to hold everything in check
            intermediatesDir = intermediatesBaseDir + "Model" + std::to_string(modelNumber) + "/";
            mkdir( intermediatesDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH ); modelNumber++;
            
            model.update(10, intermediatesDir, true);  // Normalise weights
            
            // And then the next round without normalisation
            intermediatesDir = intermediatesBaseDir + "Model" + std::to_string(modelNumber) + "/";
            mkdir( intermediatesDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH ); modelNumber++;
            
            model.update(40, intermediatesDir);
            
            //-----------------------------------------------------------------------
            
            // Reboot model - group maps should now be better than decomposed basis
            
            std::cout << "Reinitialising model..." << std::endl;
            model.reinitialiseModel( spatialModel, spatialParameters, weightModel, timeCourseModel, temporalPrecisionModel );
            std::cout << "Models reinitialised." << std::endl << std::endl;
            
            intermediatesDir = intermediatesBaseDir + "Model" + std::to_string(modelNumber) + "/";
            mkdir( intermediatesDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH ); modelNumber++;
            
            model.update(100, intermediatesDir);
            
            //----------------------------------------------------------------------
            
            // Reboot model - subject-specific (artefact?) maps can persist long 
            // after group maps have been eliminated
            
            std::cout << "Reinitialising model..." << std::endl;
            model.reinitialiseModel( spatialModel, spatialParameters, weightModel, timeCourseModel, temporalPrecisionModel );
            std::cout << "Models reinitialised." << std::endl << std::endl;
            
            intermediatesDir = intermediatesBaseDir + "Model" + std::to_string(modelNumber) + "/";
            mkdir( intermediatesDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH ); modelNumber++;
            
            model.update(150, intermediatesDir);
            
            //----------------------------------------------------------------------
            
            // And one final reboot for luck
            // Aim is to prevent subject maps from diverging too far from the group
            std::cout << "Reinitialising model..." << std::endl;
            model.reinitialiseModel( spatialModel, spatialParameters, weightModel, timeCourseModel, temporalPrecisionModel );
            std::cout << "Models reinitialised." << std::endl << std::endl;
            
            // At this stage we have a consensus set of group maps, so we can use 
            // the same updates as predefined :-)
        }
            
        // Do the updates!
        intermediatesDir = intermediatesBaseDir + "Model" + std::to_string(modelNumber) + "/";
        mkdir( intermediatesDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH ); modelNumber++;
        
        model.update(150, intermediatesDir);
        
        
        // Save!
        std::cout << "Saving results..." << std::endl << std::endl;
        model.save(resultsDir);
    }
    
    std::cout << "Done!" << std::endl;
    // Date and time
    /*const*/ /*std::time_t*/ t = std::time(nullptr);
    std::cout << "Finished at " << std::put_time(std::localtime(&t), timeFormat) << "." << std::endl;
    
    ////////////////////////////////////////////////////////////////////////////
    
    return 0;
}

////////////////////////////////////////////////////////////////////////////////

// Makes sure output directory is legit and ends in .pfm
std::string cleanOutputDir(const std::string outputDir)
{
    std::string cleanDir = outputDir;
    
    // Remove any slashes at the end
    while ( cleanDir.back() == '/' ) { cleanDir.pop_back(); }
    
    // Add .pfm if necessary
    if ( cleanDir.length() < 4 || cleanDir.substr( cleanDir.length() - 4) != ".pfm" )
    {
        cleanDir += ".pfm";
    }
    
    // And add the slash back in
    cleanDir += "/";
    
    return cleanDir;
}

////////////////////////////////////////////////////////////////////////////////

void copyFile(const std::string fromFile, const std::string toFile)
{
    // http://stackoverflow.com/q/10195343
    std::ifstream source(fromFile, std::ios::in|std::ios::binary);
    std::ofstream dest(toFile, std::ios::out|std::ios::binary);
    
    dest << source.rdbuf();
    
    source.close();
    dest.close();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void removeDirectory(const std::string directory)
{
    std::experimental::filesystem::remove_all(directory);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

std::shared_ptr<std::vector<arma::uvec>> loadNeighbours(const std::string neighboursFile)
{
    // Load neighbours from file
    // Open file
    std::ifstream file( neighboursFile.c_str() );
    std::string line;
    
    // First line has number of voxels
    std::getline(file, line);
    
    // Check for mask
    if (line.substr(0,7) != "Voxels:") {
        throw std::invalid_argument("Spatial neighbours file does not contain a size line.");
    }
    const unsigned int N = std::stoul(line.substr(7)); // Everything after "Voxels:"
    
    // Make the store
    std::shared_ptr<std::vector<arma::uvec>> neighbourList = std::make_shared<std::vector<arma::uvec>>(N, arma::uvec());
    
    // Parse each line
    while ( std::getline(file, line) ) {
        // Get the index of this set of neighbours
        std::size_t pos = line.find(":");
        const unsigned int index = std::stoul(line.substr(0, pos));
        
        // And parse the neighbours from the remainder of the line
        line = line.substr(pos + 1);
        std::vector<arma::uword> neighbours;
        // http://stackoverflow.com/a/14266139
        do {
            pos = line.find(",");
            const std::string nb = line.substr(0, pos);
            // Convert to uint (but check it isn't just whitespace!)
            // http://stackoverflow.com/a/9642381
            // http://stackoverflow.com/a/15039964
            if ( std::any_of(nb.begin(), nb.end(), static_cast<int(*)(int)>(std::isdigit)) ) {
                neighbours.push_back( std::stoul(nb) );
            }
            line.erase(0, pos + 1);
        } while (pos != std::string::npos);
        
        // And store!
        (*neighbourList)[index] = arma::uvec(neighbours);
    }
    
    return neighbourList;
}
////////////////////////////////////////////////////////////////////////////////
