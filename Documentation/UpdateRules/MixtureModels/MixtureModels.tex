% !TEX TS-program = lualatex
% !TEX encoding = UTF-8 Unicode

\documentclass[a4paper, 10pt]{article}

\usepackage{PROFUMOstyle}

\bibliography{../../../Documentation/Papers.bib}

% Define the title 
\author{Sam Harrison}
\title{Variational Inference Of Mixture Models}
\date{\today}

%Define header text
\header{VB Mixture Models}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document} 

% Generate the title
\maketitle 

\microtypesetup{protrusion=false} % disables protrusion locally in the document
\tableofcontents % prints Table of Contents
\microtypesetup{protrusion=true} % enables protrusion

\vspace{3em}

For the most part, this is a pointless rehash of \textit{Variational Bayesian Model Selection for Mixture Distributions} by Corduneanu \& Bishop\superbcite{Corduneanu2001}. Enjoy.

\newpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Mixture Modelling}

\begin{equation}
\cDist{ \p }{ x }{ λ, μ, σ^{2} } = \sum_{c = 1}^{C} λ_{c} \cDist{ \normalDist }{ x }{ μ_{c}, σ_{c}^{2} }
\label{eq:MM_PDF}
\end{equation}

It is convenient to introduce a latent indicator variable, $s \in \{1, \ldots, C\}$. The distribution then becomes:
\begin{equation}
\begin{split}
\cDist{ \p }{ x }{ s, μ, σ^{2} } & = \prod_{c = 1}^{C} \cDist{ \normalDist }{ x }{ μ_{c}, σ_{c}^{2} }^{[s = c]} \\
\cDist{ \p }{ s }{ λ } & = \prod_{c = 1}^{C} λ_{c}^{[s = c]}
\end{split}
\label{eq:LatentMM_PDF}
\end{equation}
where $[s = c]$ is the Iverson bracket.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{VB approximation to a mixture model posterior}
We derive the update rules for a simple mixture model, where $x$ is drawn from the distribution in \Ref{equation}{eq:LatentMM_PDF}.
The observations, $d$, are simply these latent variables with additive Gaussian noise with precision $ψ$.
This gives
\begin{equation}
\cDist{ \p }{ d }{ x, ψ } = \cDist{ \normalDist }{ d }{ x, ψ^{-1} }
\label{eq:VB:Likelihood}
\end{equation}

For notational simplicity, we re-parameterise the mixture model with precision $α = σ^{-2}$, so \Ref{equation}{eq:LatentMM_PDF} becomes
\begin{equation}
\begin{split}
\cDist{ \p }{ x }{ s, μ, α } & = \prod_{c = 1}^{C} \cDist{ \normalDist }{ x }{ μ_{c}, α_{c}^{-1} }^{[s = c]} \\
\cDist{ \p }{ s }{ λ } & = \prod_{c = 1}^{C} λ_{c}^{[s = c]}
\end{split}
\label{eq:VB:MM_Prior}
\end{equation}

The approximate VB posterior takes the form
\begin{equation}
\begin{split}
\cDist{ \q }{ x }{ s, \hat{μ}, \hat{α} } & = \prod_{c = 1}^{C} \cDist{ \normalDist }{ x }{ \hat{μ}_{c}, \hat{α}_{c}^{-1} }^{[s = c]} \\
\cDist{ \q }{ s }{ \hat{λ} } & = \prod_{c = 1}^{C} \hat{λ}_{c}^{[s = c]}
\end{split}
\label{eq:VB:MM_Posterior}
\end{equation}

We take the standard VB approach and set
\begin{equation}
\lnFunc{ \dist{ \q }{ θ } } = \expecWRT{ \lnFunc{ \cDist{ \p }{ D }{ θ }  \dist{ \p }{ θ } } }{ \bm{Θ} \neq θ } + \const
\label{eq:VB:GeneralApproach}
\end{equation}

We infer this as a single distribution, rather than factoring over $x$ and $s$. This yields:
\begin{equation}
\begin{split}
% Explicitly write out
\lnFunc{ \cDist{ \q }{ x }{ s = c } } = & - \tfrac{1}{2} ψ (d - x)^{2} + \tfrac{1}{2} \lnFunc{ α_{c} } - \tfrac{1}{2} α_{c} (x - μ_{c})^{2} \\
& + \lnFunc{ λ_{c} } + \const \\
% Collect terms
 = & - \tfrac{1}{2} \bigl( ψ + α_{c} \bigr) x^{2} + \bigl( ψ d + α_{c} μ_{c} \bigr) x \\
& + \tfrac{1}{2} \lnFunc{ α_{c} } - \tfrac{1}{2} α_{c} μ_{c}^{2} + \lnFunc{ λ_{c} } + \const \\
\end{split}
\label{eq:VB:PosteriorFullExpression}
\end{equation}

We can equate terms with \Ref{equation}{eq:VB:MM_Posterior}, giving the following update rules
\begin{equation}
\begin{split}
 \hat{α}_{c} = & \, \bigl( ψ + α_{c} \bigr) \\[5pt]
 \hat{μ}_{c} = & \, \hat{α}_{c}^{-1} \bigl( ψ d + α_{c} μ_{c} \bigr) \\[5pt]
 \lnFunc{ \hat{λ}_{c} } = & \, \lnFunc{ λ_{c} } + \tfrac{1}{2} \lnFunc{ α_{c} } - \tfrac{1}{2} \lnFunc{ \hat{α}_{c} } \\
& - \tfrac{1}{2} α_{c} μ_{c}^{2} + \tfrac{1}{2} \hat{α}_{c} \hat{μ}_{c}^{2} \\
\end{split}
\label{eq:VB:PosteriorParams}
\end{equation}



\subsection{Delta functions}
Like spike-slab\superbcite{Titsias2011}.

To examine this, let the prior precision tend to infinity. If $α_{c} \to \infty$ then it is clear that $\hat{α}_{c} \to α_{c}$ and $\hat{μ}_{c} \to μ_{c}$, or in other words, the delta function is not perturbed by the observation, as expected.
However, as $\hat{μ}_{c}$ depends on $\hat{α}_{c}$, not all the terms in the expression for $λ_{c}$ cancel.
The term we are interested in is
\begin{equation}
\begin{split}
\hat{α}_{c} \hat{μ}_{c}^{2} - α_{c} μ_{c}^{2} & = \hat{α}_{c}^{-1} \bigl( ψ d + α_{c} μ_{c} \bigr)^{2} - α_{c} μ_{c}^{2} \\
& = \frac{ψ^{2} d^{2}}{α_{c} + ψ} +  2 \frac{ψ d α_{c} μ_{c}}{α_{c} + ψ} + \frac{α_{c}^{2} μ_{c}^{2}}{α_{c} + ψ} - α_{c} μ_{c}^{2} \\
%%
\hat{α}_{c} \hat{μ}_{c}^{2} - α_{c} μ_{c}^{2} & \to 2 ψ d μ_{c} - ψ μ_{c}^{2}
\end{split}
\end{equation}

Taking limits of the update rule for $λ$ in \Ref{equation}{eq:VB:PosteriorParams}, using this expression, yields
\begin{equation}
\lim_{α_{c} \to \infty} \Bigl( \lnFunc{ \hat{λ}_{c} } \Bigr) = \lnFunc{ λ_{c} } + ψ d μ_{c} - \tfrac{1}{2} ψ μ_{c}^{2}
\label{eq:Delta:PosteriorMixingProportion}
\end{equation}



\subsection{Multivariate factorisations}
Note that we are essentially forced to factorise any mixture model posteriors into scalar distributions as the evaluation of the different combinations of the indicator variables quickly becomes unmanageable.

For example, consider an $N$ dimensional multivariate `version' of the spike-slab distribution:
\begin{equation*}
\begin{split}
\cDist{ \p }{ \bm{P} }{ \bm{Q} = \bm{1} } & = \cDist{ \normalDist }{ \bm{P} }{ \bm{μ}, \bm{α}^{-1} } \\
\cDist{ \p }{ \bm{P} }{ \bm{Q} = \bm{0} } & = \dist{ \deltaDist }{ \bm{P} } \\
\dist{ \p }{ \bm{Q} } & = \prod_{n=1}^{N} (π)^{\bm{Q}_{n}} (1 - π)^{1-\bm{Q}_{n}} \\
\end{split}
\end{equation*}

The issue is that while we can evaluate the above expressions, we also need to compute the cases for all $2^{N}$ possible versions of $\bm{Q}$, which is almost always infeasible.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{A hybrid mode-parcellation model}
We can describe a weighted parcellation---whereby each voxel belongs to one parcel, but with a weighting to account for differing signal intensities across voxels---using a multivariate mixture model.
This bridges the gap between traditional mode models, which try and explain the full data using a spatio-temporal decomposition, with parcellation models, which try and divide the whole cortex into different regions.
In this way, we force the mode decomposition to have a meaningful presence from one mode in each voxel, but we still explain the full data by extracting a set of time courses too.

Consider the multivariate extension to the above problem, namely
\begin{equation}
\cDist{ \p }{ \bm{d} }{ \bm{x}, \bm{Ψ} } = \cDist{ \normalDist }{ \bm{d} }{ \bm{x}, \bm{Ψ}^{-1} } \,\, .
\end{equation}

\begin{equation}
\begin{split}
\cDist{ \p }{ \bm{x} }{ s, \bm{μ}, \bm{Ω} } & = \prod_{c = 1}^{C} \cDist{ \normalDist }{ \bm{x} }{ \bm{μ}_{c}, \bm{Ω}_{c}^{-1} }^{[s = c]} \\
\cDist{ \p }{ s }{ λ } & = \prod_{c = 1}^{C} λ_{c}^{[s = c]}
\end{split}
\end{equation}

If we interpret $\bm{d}$ as being the observed weights for each of the modes in a given voxel (in which case $\bm{d}$ and $\bm{Ψ}$ naturally fall out of the matrix factorisation equations), then we can derive a weighted parcellation model as follows.
Firstly, let $\bm{μ}_{c}$ and $\bm{Ω}_{c}$ be structured such that, for example, 
\begin{equation}
\bm{μ}_{2} = \begin{bmatrix}
    0 \\
    μ_{2} \\
    0 \\
    \vdots \\
    0 \\
\end{bmatrix}
\qquad
\bm{Ω}_{2} = \begin{bmatrix}
    α & 0 & 0 & \hdots & 0 \\
    0 & ω_{2} & 0 & \hdots & 0 \\
    0 & 0 & α & \ddots & 0 \\
    \vdots & \vdots & \ddots & \ddots & 0 \\
    0 & 0 & 0 & 0 & α \\
\end{bmatrix}
\end{equation}

The update rules take the same form as before:
\begin{equation}
\begin{split}
 \hat{\bm{Ω}}_{c} = & \, \bigl( \bm{Ψ} + \bm{Ω}_{c} \bigr) \\[5pt]
 \hat{\bm{μ}}_{c} = & \, \hat{\bm{Ω}}_{c}^{-1} \bigl( \bm{Ψ} \bm{d} + \bm{Ω}_{c} \bm{μ}_{c} \bigr) \\[5pt]
 \ln(\hat{λ}_{c}) = & \, \ln(λ_{c}) + \tfrac{1}{2} \ln( | \bm{Ω}_{c} | ) - \tfrac{1}{2} \ln( | \hat{\bm{Ω}}_{c} | ) \\
& - \tfrac{1}{2}  \bm{Ω}_{c} \bm{μ}_{c}^{2} + \tfrac{1}{2} \hat{ \bm{Ω}}_{c} \hat{\bm{μ}}_{c}^{2} \\
\end{split}
\end{equation}

The crucial point to note is that as $α \to \infty$, the model collapses such that each mixture component represents one parcel (albeit with a weighting).
In that case, the update rules simplify to:
\begin{equation}
\hat{\bm{μ}}_{c} \to \begin{bmatrix}
    0 \\
    \hat{μ}_{c} \\
    0 \\
    \vdots \\
    0 \\
\end{bmatrix}
\qquad
\hat{\bm{Ω}}_{c}^{-1} \to \begin{bmatrix}
    0 & 0 & 0 & \hdots & 0 \\
    0 & \hat{ω}_{c}^{-1} & 0 & \hdots & 0 \\
    0 & 0 & 0 & \ddots & 0 \\
    \vdots & \vdots & \ddots & \ddots & 0 \\
    0 & 0 & 0 & 0 & 0 \\
\end{bmatrix}
\end{equation}

\begin{equation}
\begin{split}
 \hat{ω}_{c}^{-1} = & \, \bigl( \bm{Ψ}_{c,c} + ω_{c} \bigr)^{-1} \\[5pt]
 \hat{μ}_{c} = & \, \hat{ω}_{c}^{-1} \bigl( (\bm{Ψ} \bm{d})_{c,c} + ω_{c} μ_{c} \bigr) \\[5pt]
 \ln(\hat{λ}_{c}) = & \, \ln(λ_{c}) + \tfrac{1}{2} \ln( ω_{c} ) - \tfrac{1}{2} \ln( \hat{ω}_{c} ) \\
& - \tfrac{1}{2} ω_{c} μ_{c}^{2} + \tfrac{1}{2} \hat{ω}_{c} \hat{μ}_{c}^{2} \\
\end{split}
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Spatial regularisation via the Potts model}
An obvious extension to the above is to introduce a coupling between different mixture distributions.
One natural way of doing this is to formulate a prior such that \enquote{coupled} distributions are more likely to result in observations drawn from the same component.
The Potts model, introduced by Renfrey Potts\superbcite{Potts1952}, is one such way in which we can do this.

The Potts model is a generalisation of the Ising model, where, as ever, David Mackay provides an excellent overview\superbcite{MacKayITILA}.
The following also uses some of the results from McGrory et al.'s overview of VB inference on Potts models\superbcite{McGrory2008}.

\subsection{Potts model}
The basic Potts model decribes the behaviour of a set of spins, $\bm{s} \in \mathbb{R}^{N}; s_{n} \in \{1, \ldots, C\}$, given a set of couplings, $J$, and an external field, $h$:
\begin{equation}
\begin{split}
\dist{ \p }{ \bm{s} } & = \frac{1}{ \func{ \operatorname{Z} }{ β, J, h } } \func{ \exp }{ - β \func{ \operatorname{E} }{ \bm{s}; J, h } } \\[5pt]
\func{ \operatorname{E} }{ \bm{s}; J, h } & = - \sum_{\forall i,j} J_{ij} \func{ \deltaFunc }{s_{i}, s_{j}} - \sum_{\forall i,c} h_{ic} [s_{i} = c] \\
\end{split}
\label{eq:PottsModel}
\end{equation}
In our case, we use the Kronecker delta such that $\func{ \deltaFunc }{s_{i}, s_{j}} \equiv [s_{i} = s_{j}]$.

In practice, the partition function, $\func{ \operatorname{Z} }{ β, J, h }$, is too demanding to evaluate for all problems of interest.
Therefore, we use the mean-field approximation to proceed.

\subsection{Regularised mixture models}
We extend the standard mixture model formulation (\Ref{equation}{eq:VB:MM_Prior}) as follows:
\begin{equation}
\begin{split}
\cDist{ \p }{ \bm{x} }{ \bm{s}, μ, α } & = \prod_{\forall i} \prod_{\forall c} \cDist{ \normalDist }{ x_{i} }{ μ_{c}, α_{c}^{-1} }^{[s_{i} = c]} \\
\cDist{ \p }{ \bm{s} }{ λ } & = \frac{1}{ \func{ \operatorname{Z} }{ β, J, λ } } \prod_{\forall i} \prod_{\forall c} λ_{c}^{[s_{i} = c]} \prod_{\forall j} \func{ \exp }{ β J_{ij} }^{[s_{i} = s_{j}]}
\end{split}
\label{eq:VB:PottsMM_Prior}
\end{equation}
This is a slight reformulation of \Ref{equation}{eq:PottsModel}, but the relationship should be obvious.

In practice, we can use this model to provide spatial regularisation by setting $J_{ij} = 1$ if $x_{i}$ and $x_{j}$ are spatially adjacent, and zero otherwise.

\subsubsection*{Update rules}
Again, we let the approximate VB posterior take the form
\begin{equation}
\begin{split}
\cDist{ \q }{ \bm{x} }{ \bm{s}, \hat{μ}, \hat{α} } & = \prod_{\forall i} \prod_{\forall c} \cDist{ \normalDist }{ x }{ \hat{μ}_{ic}, \hat{α}_{ic}^{-1} }^{[s_{i} = c]} \\
\cDist{ \q }{ \bm{s} }{ \hat{λ} } & = \prod_{\forall i} \prod_{\forall c} \hat{λ}_{ic}^{[s_{i} = c]}
\end{split}
\label{eq:VB:PottsMM_Posterior}
\end{equation}

This gives the following set of update rules:
\begin{equation}
\begin{split}
 \hat{α}_{ic} = & \, \bigl( ψ + α_{c} \bigr) \\[5pt]
 \hat{μ}_{ic} = & \, \hat{α}_{ic}^{-1} \bigl( ψ d_{i} + α_{c} μ_{c} \bigr) \\[5pt]
 \lnFunc{ \hat{λ}_{ic} } = & \, \lnFunc{ λ_{c} } + \sum_{\forall j} β J_{ij} \, \expec{[s_{j} = c]} \\
& + \tfrac{1}{2} \lnFunc{ α_{c} } - \tfrac{1}{2} \lnFunc{ \hat{α}_{ic} } \\
& - \tfrac{1}{2} α_{c} μ_{c}^{2} + \tfrac{1}{2} \hat{α}_{ic} \hat{μ}_{ic}^{2} \\
\end{split}
\label{eq:VB:PottsMM_PosteriorParams}
\end{equation}

\subsubsection*{Free energy}
Clearly, any calculation of the free energy will have to take into account the more complex form of the prior.
Note however, that we will only be able to calculate this up to an additive constant as we cannot evaluate the partition function.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\printbibliography

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}
