%% Tests limits of expression in update rules document

clear all; clc

%% Set params

% Mixture component prior
pMu = 2;
pAlpha = 1e5;
pLambda = 0.1;

% Observation
D = 1.3;
psi = 0.9;


%% Do calculations

% True posterior
qAlpha = psi + pAlpha;
qMu = qAlpha^(-1) * (psi*D + pMu*pAlpha);
qLambda = log(pLambda) ...
    + 0.5*log(pAlpha) - 0.5*log(qAlpha) ...
    - 0.5*pAlpha*pMu^2 + 0.5*qAlpha*qMu^2;
fprintf('True: %f\n', qLambda)

% Delta approximation
qLambda = log(pLambda) + (psi * D * pMu) - (psi * pMu^2)/2;
fprintf('Limit: %f\n', qLambda)



%% Tests limits of expression for weighted parcellation in update rules document

clear all; clc

%% Set params

% Mixture component prior
pMu = {[1 0]', [0 1]'};
pAlpha = 1e3;
pOmega = {[1 0; 0 pAlpha], [pAlpha 0; 0 1]};
pLambda = {0.5, 0.5};

% Observation
D = [1.2 0.5]';
Psi = [3 0.5; 0.5 4];

%% Do calculations

fprintf(repmat('*',1,30))
fprintf('\nFull calculations:\n\n')

% Full update rules to get full posterior
for i = 1:2
    qOmega{i} = Psi + pOmega{i}; %qOmega{i}^(-1)
    
    qMu{i} = qOmega{i}^(-1) * (Psi*D + pOmega{i}*pMu{i}); %qMu{i}
    
    qlnLambda{i} = log(pLambda{i}) ...
        + 0.5*log(det(pOmega{i})) - 0.5*log(det(qOmega{i})) ...
        - 0.5*pMu{i}'*pOmega{i}*pMu{i} + 0.5*qMu{i}'*qOmega{i}*qMu{i};
end

% Print results
qO = qOmega{1}^(-1); qO = qO(1,1);
fprintf('True iOmega1(1,1): %f\n', qO)
fprintf('True mu1(1): %f\n', qMu{1}(1))
fprintf('True lnLambda1: %f\n', qlnLambda{1})
p1 = exp(qlnLambda{1}) / (exp(qlnLambda{1}) + exp(qlnLambda{2}));
fprintf('True p(1): %f\n', p1)
fprintf('\n\n')

fprintf(repmat('*',1,25))
fprintf('\nApproximate calculations:\n\n')

% Delta approximation
for i = 1:2
    qomega{i} = Psi(i,i) + pOmega{i}(i,i); %qomega{i}^(-1)
    
    pd = Psi * D;
    qmu{i} = qomega{i}^(-1) * (pOmega{i}(i,i) * pMu{i}(i) + pd(i)); %qmu{i}
    
    qlnlambda{i} = log(pLambda{i}) ...
        + 0.5*log(pOmega{i}(i,i)) - 0.5*log(qomega{i}) ...
        - 0.5*pOmega{i}(i,i)*pMu{i}(i) + 0.5*qomega{i}*qmu{i}^2;
end

% Print results
fprintf('Limit iOmega1(1,1): %f\n', qomega{1}^(-1))
fprintf('Limit mu1(1): %f\n', qmu{1})
fprintf('Limit lnLambda1: %f\n', qlnlambda{1})
p1 = exp(qlnlambda{1}) / (exp(qlnlambda{1}) + exp(qlnlambda{2}));
fprintf('Limit p(1): %f\n', p1)
fprintf('\n\n')
