// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2014
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Tests the speed of a few different ways of doing some of the linear
// algebra manipulations required

// g++ LinearAlgebraProfiling.cpp -o Test.o -O3 -I/opt/local/include -L/opt/local/lib -larmadillo -Wall -Wextra

#include <iostream>
#include <iomanip>
#include <armadillo>
#include <ctime>
#include <string>
#include <vector>

////////////////////////////////////////////////////////////////////////////////

// Tr(DtPA) = Tr(PtDAt) = Tr(AtPtD)
// Tr(XtY) = sum(sum( X % Y )) = vec(X)t * vec(Y)

//Generic class to calculate TrDtPA
class TrDtPA
{
public:
    virtual double calculateTrDtPA(const arma::mat* D, const arma::mat* P, const arma::mat* A) = 0;
    
    std::string getName(){
        return name_;
    }
    
protected:
    TrDtPA(std::string name) : name_(name) {}
    
private:
    std::string name_;
};



// Stupid: Tr(At(PtD))
class TrDt_PA : public TrDtPA
{
public:
    TrDt_PA() : TrDtPA("Tr(Dt * PA)") {}
    
    double calculateTrDtPA(const arma::mat* D, const arma::mat* P, const arma::mat* A){
        return arma::trace( (*D).t() * ((*P) * (*A)) );
    }
};

// Straight out the box: Tr(At(PtD))
class TrAt_PtD : public TrDtPA
{
public:
    TrAt_PtD() : TrDtPA("Tr(At * PtD)") {}
    
    double calculateTrDtPA(const arma::mat* D, const arma::mat* P, const arma::mat* A){
        //return arma::trace( (*D).t() * (*P) * (*A) );
        return arma::trace( (*A).t() * ((*P).t() * (*D)) );
    }
};

// Straight out the box: Tr(Pt(DAt))
class TrPt_DAt : public TrDtPA
{
public:
    TrPt_DAt() : TrDtPA("Tr(Pt * DAt)") {}
    
    double calculateTrDtPA(const arma::mat* D, const arma::mat* P, const arma::mat* A){
        return arma::trace( (*P).t() * ((*D) * (*A).t()) );
    }
};

// sum(P % DAt)
class sPoDAt : public TrDtPA
{
public:
    sPoDAt() : TrDtPA("sum(P % DAt)") {}
    
    double calculateTrDtPA(const arma::mat* D, const arma::mat* P, const arma::mat* A){
        return arma::sum(arma::sum( (*P) % ((*D) * (*A).t()) ));
    }
};

// sum(A % PtD)
class sAoPtD : public TrDtPA
{
public:
    sAoPtD() : TrDtPA("sum(A % PtD)") {}
    
    double calculateTrDtPA(const arma::mat* D, const arma::mat* P, const arma::mat* A){
        return arma::sum(arma::sum( (*A) % ((*P).t() * (*D)) ));
    }
};

// vec(P)t * vec(DAt)
class vPtvDAt : public TrDtPA
{
public:
    vPtvDAt() : TrDtPA("vec(P)t * vec(DAt)") {}
    
    double calculateTrDtPA(const arma::mat* D, const arma::mat* P, const arma::mat* A){
        return arma::sum( arma::vectorise((*P)).t() * arma::vectorise((*D) * (*A).t()) );
    }
};

// vec(A)t * vec(PtD)
class vAtvDPt : public TrDtPA
{
public:
    vAtvDPt() : TrDtPA("vec(A)t * vec(PtD)") {}
    
    double calculateTrDtPA(const arma::mat* D, const arma::mat* P, const arma::mat* A){
        return arma::sum( arma::vectorise((*A)).t() * arma::vectorise((*P).t() * (*D)) );
    }
};

////////////////////////////////////////////////////////////////////////////////

//Timing function
double timeTrDtPAcalculation(TrDtPA* method, const arma::mat* D, const arma::mat* P, const arma::mat* A){
    clock_t begin, end;
    double timeElapsed, TrDtPA;
    
    //Do calculation
    begin = clock();
    TrDtPA = method->calculateTrDtPA(D, P, A);
    end = clock();
    timeElapsed = double(end - begin) / CLOCKS_PER_SEC;
    
    //Print results
    std::cout << std::setw(20) << method->getName() << ": " << timeElapsed << " " << TrDtPA << std::endl;
    
    //Return time
    return timeElapsed;
}

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
    arma::arma_rng::set_seed_random();  // set the seed to a random value
    std::cout << std::setprecision(3) << std::scientific;
    
    //Size of tests
    //int S = 5;   //Subjects
    int M = 50;  //Modes
    int T = 1200; //Time points
    int V = 90000; //Voxels
    int nRepeats = 25;
    
    ////////////////////////////////////////////////////////////////////////////
    //Trace operation
    
    std::cout << "Tr(DtPA)" << std::endl;
    
    //Concat all tests we want to do
    std::vector<TrDtPA*> methods;
    //methods.push_back( new TrDt_PA() );
    methods.push_back( new TrAt_PtD() );
    methods.push_back( new TrPt_DAt() );
    methods.push_back( new sPoDAt() );
    methods.push_back( new sAoPtD() );
    methods.push_back( new vPtvDAt() );
    methods.push_back( new vAtvDPt() );
    
    //Where results will be saved
    std::vector<arma::vec> times( methods.size(), arma::zeros<arma::vec>(nRepeats) );
    
    //Loop where repeats are done
    for(int i=0; i<nRepeats; ++i){
        
        std::cout << "Iteration " << i+1 << ":" << std::endl;
        
        //Generate matrices
        arma::mat D = arma::randn<arma::mat>(V,T);
        arma::mat P = arma::randn<arma::mat>(V,M);
        arma::mat A = arma::randn<arma::mat>(M,T);
        
        //Do tests!
        for(unsigned int j=0; j<methods.size(); ++j){
            times[j][i] = timeTrDtPAcalculation(methods[j], &D, &P, &A);
        }
        
        std::cout << std::endl;
    }
    
    //Print results
    std::cout << "Summary:" << std::endl;
    
    for(unsigned int j=0; j<methods.size(); ++j){
        std::cout << std::setw(20) << methods[j]->getName() << ": "
        << arma::mean(times[j]) << " (±" << arma::stddev(times[j]) << ")" << std::endl;
    }
    
    std::cout << std::endl;
    
    //DELETE METHODS PROPERLY?
    
    ////////////////////////////////////////////////////////////////////////////
    
    return 0;
}
