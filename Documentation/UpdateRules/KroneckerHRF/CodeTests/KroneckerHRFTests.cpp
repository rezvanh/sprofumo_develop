// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2014
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Test of the Kronecker matrix manipulations used to speed up computations
// Derivations can be found in the update rules document

// g++ KroneckerHRFTests.cpp -o Test.o -O3 -I/opt/local/include -L/opt/local/lib -larmadillo -Wall -Wextra

#include <iostream>
#include <armadillo>
#include <ctime>

int main(int /*argc*/, char** /*argv*/)
{
    arma::arma_rng::set_seed_random();  // set the seed to a random value
    clock_t begin, end;
    
    //Size of tests
    int S = 5;   //Subjects
    int M = 50;  //Modes
    int T = 100; //Time points
    int V = 200; //Voxels
    
    //Generate random matrices
    std::vector<arma::mat> D;
    std::vector<arma::mat> P;
    std::vector<arma::mat> PtP;
    std::vector<double> psi;
    
    for(int s=0; s<S; ++s){
        //Data
        D.push_back( arma::randn<arma::mat>(V,T) );
        
        //Spatial maps
        P.push_back( arma::randn<arma::mat>(V,M) );
        PtP.push_back( arma::randn<arma::mat>(M,M) ); PtP[s] = PtP[s] * PtP[s].t();
        PtP[s] += P[s].t() * P[s];
        
        //Noise precision
        arma::mat R = arma::randu<arma::mat>(1,1);
        psi.push_back( R(0) );
    }
    
    //HRF covariance
    arma::mat K = arma::randn<arma::mat>(T,T); K = K * K.t();
    //These are cached results
    arma::mat iK = arma::inv_sympd(K);
    arma::vec L_K;
    arma::mat U_K;
    arma::eig_sym(L_K, U_K, K);
    //std::cout << L_K.n_cols << " " << L_K.n_rows << std::endl;
    //std::cout << U_K.n_cols << " " << U_K.n_rows << std::endl;
    
    //Mode covariance
    arma::mat alpha = arma::randn<arma::mat>(M,M); alpha = alpha * alpha.t();
    
    
    //Results
    double timeElapsed, timeElapsed_kron;
    arma::mat Ma, Ma_kron;
    double logDet, logDet_kron;
    arma::mat AAt, AAt_kron;
    arma::mat AiKAt, AiKAt_kron;
    
    ////////////////////////////////////////////////////////////////////////////
    //Brute force solution
    {
        begin = clock();
        
        //Collect data over subjects
        arma::mat psiPtP = arma::zeros(M,M);
        arma::mat psiDtP = arma::zeros(T,M);
        for(int s=0; s<S; ++s){
            psiPtP += psi[s] * PtP[s];
            psiDtP += psi[s] * D[s].t() * P[s];
        }
        
        arma::mat sigmaA = arma::inv_sympd( arma::kron(alpha, iK) + arma::kron(psiPtP, arma::eye(T,T)) );
        
        //Posterior mean
        Ma =  sigmaA * arma::vectorise(psiDtP);
        Ma.reshape(T,M); Ma = Ma.t();
        
        //AAt
        AAt = arma::zeros(M,M);
        for(int m1=0; m1<M; ++m1){
            for(int m2=0; m2<M; ++m2){
                AAt(m1,m2) = arma::trace( sigmaA.submat(m1*T, m2*T, (m1+1)*T-1, (m2+1)*T-1) );
            }
        }
        AAt += Ma * Ma.t();
        
        //AiKAt
        AiKAt = arma::zeros(M,M);
        for(int m1=0; m1<M; ++m1){
            for(int m2=0; m2<M; ++m2){
                AiKAt(m1,m2) = arma::sum(arma::sum( iK % sigmaA.submat(m1*T, m2*T, (m1+1)*T-1, (m2+1)*T-1) ));
            }
        }
        AiKAt += Ma * iK * Ma.t();
        
        //Determinant
        logDet = std::real(arma::log_det(sigmaA));
        
        end = clock();
        timeElapsed = double(end - begin) / CLOCKS_PER_SEC;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    //Kronecker solution
    {
        begin = clock();
        
        //Collect data over subjects
        arma::mat psiPtP = arma::zeros(M,M);
        arma::mat psiPtD = arma::zeros(M,T);
        for(int s=0; s<S; ++s){
            psiPtP += psi[s] * PtP[s];
            psiPtD += psi[s] * P[s].t() * D[s];
        }
        
        // Invert alpha
        arma::mat iAlpha = arma::inv_sympd(alpha);
        
        //Eigendecomposition of between mode correlation structure
        arma::cx_vec cL_PA;
        arma::cx_mat cU_PA;
        arma::eig_gen(cL_PA, cU_PA, psiPtP * iAlpha);
        arma::vec L_PA = arma::real(cL_PA);
        arma::mat U_PA = arma::real(cU_PA);
        arma::mat iU_PA = arma::inv(U_PA);
        
        //std::cout << L_PA << std::endl;
        //std::cout << U_PA << std::endl;
        //std::cout << psiPtP * iAlpha << std::endl << U_PA * diagmat(L_PA) * iU_PA << std::endl;
        
        //Collect and combine eigenvalues of Sigma_A
        const arma::mat L = 1.0 / (L_PA * L_K.t() + 1.0);
        //And the modified version for AiKAt
        const arma::mat Lk = (L.each_row() % L_K.t());
        
        
        //Posterior mean
        arma::mat MaUk = iAlpha * U_PA * ( Lk % (iU_PA * psiPtD * U_K) );
        Ma_kron = MaUk * U_K.t();
        
        //AAt
        AAt_kron = Ma_kron * Ma_kron.t() + iAlpha * U_PA * arma::diagmat(arma::sum(Lk,1)) * iU_PA;
        
        //AiKAt
        AiKAt_kron = MaUk * arma::diagmat(1.0 / L_K.t()) * MaUk.t() + iAlpha * U_PA * arma::diagmat(arma::sum(L,1)) * iU_PA;
        
        //Determinant
        logDet_kron = - T * std::real(arma::log_det(alpha));
        logDet_kron += arma::sum( arma::sum( arma::log(Lk) ) );
        
        end = clock();
        timeElapsed_kron = double(end - begin) / CLOCKS_PER_SEC;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    //Spit out some result statistics
    
    //Timings
    std::cout << "Timings:" << std::endl;
    
    std::cout << "Full matrix: " << timeElapsed << std::endl;
    
    std::cout << "Kronecker approach: " << timeElapsed_kron << std::endl;
    
    std::cout << std::endl;
    
    
    //Posterior mean
    std::cout << "Posterior mean:" << std::endl;
    
    std::cout << "Max difference: " << arma::max(arma::max( arma::abs(Ma - Ma_kron) )) << std::endl;
    
    std::cout << "RMS error: " << std::sqrt( arma::sum(arma::sum( arma::pow(Ma - Ma_kron, 2.0) )) / Ma.n_elem ) << std::endl;
    
    std::cout << std::endl;
    
    
    //AAt
    std::cout << "AAt:" << std::endl;
    
    std::cout << "Max difference: " << arma::max(arma::max( arma::abs(AAt - AAt_kron) )) << std::endl;
    
    std::cout << "RMS error: " << std::sqrt( arma::sum(arma::sum( arma::pow(AAt - AAt_kron, 2.0) )) / Ma.n_elem ) << std::endl;
    
    std::cout << std::endl;
    
    
    //AiKAt
    std::cout << "AiKAt:" << std::endl;
    
    std::cout << "Max difference: " << arma::max(arma::max( arma::abs(AiKAt - AiKAt_kron) )) << std::endl;
    
    std::cout << "RMS error: " << std::sqrt( arma::sum(arma::sum( arma::pow(AiKAt - AiKAt_kron, 2.0) )) / Ma.n_elem ) << std::endl;
    
    std::cout << std::endl;
    
    
    //Determinant
    std::cout << "Log determinant:" << std::endl;
    
    std::cout << "Full matrix: " << logDet << std::endl;
    
    std::cout << "Kronecker: " << logDet_kron << std::endl;
    
    std::cout << std::endl;
    
    
    //Print matrices
    if(Ma.n_elem < 100){
        std::cout << "Ma:" << std::endl;
        std::cout << Ma << std::endl;
        
        std::cout << "Ma_kron:" << std::endl;
        std::cout << Ma_kron << std::endl;
    }
    
    if(AAt.n_elem < 100){
        std::cout << "AAt:" << std::endl;
        std::cout << AAt << std::endl;
        
        std::cout << "AAt_kron:" << std::endl;
        std::cout << AAt_kron << std::endl;
    }
    
    if(AiKAt.n_elem < 100){
        std::cout << "AiKAt:" << std::endl;
        std::cout << AiKAt << std::endl;
        
        std::cout << "AiKAt_kron:" << std::endl;
        std::cout << AiKAt_kron << std::endl;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    return 0;
}
