% !TEX TS-program = lualatex
% !TEX encoding = UTF-8 Unicode

\documentclass[a4paper, 10pt]{article}

\usepackage{PROFUMOstyle}

\bibliography{../../../Documentation/Papers.bib}

% Define the title 
\author{Sam Harrison}% \hfill \today}
\title{Variational Inference of Matrix Factorisation Models}
\date{\today}

%Define header text
\header{Matrix Factorisation Models}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document} 

% Generate the title
\maketitle 

\microtypesetup{protrusion=false} % disables protrusion locally in the document
\tableofcontents % prints Table of Contents
\microtypesetup{protrusion=true} % enables protrusion

\vspace{3em}

The original, and still the best, overview of VB solutions of matrix factorisation models is \enquote{Variational Principal Components} by Bishop\superbcite{Bishop1999}.

\newpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Basic model}
In its most general form, the problem is finding a decomposition of our data matrix, $\bm{D}$, based on the product of two smaller matrices, which we will denote $\bm{P}$ and $\bm{A}$.

As we are primarily concerned with neuroimaging data, we will focus on $\bm{D} \in \mathbb{R}^{V \times T}$, where $V$ is the number of voxels and $T$ is the number of timepoints.
The aim will be to identify a set of $M$ modes, where a mode is described by a spatial map---that is to say a column of $\bm{P} \in \mathbb{R}^{V \times M}$---and a timecourse---which refers to a row of $\bm{A} \in \mathbb{R}^{M \times T}$.

Note that we add a third matrix, $\bm{H} \in \mathbb{R}^{M \times M}$.
We use this to soak up the differences in variances between components, but it is by no means an essential addition to the basic framework.

To proceed with this in a probabilistic manner, we specify a noise process, $\bm{ε}$, to model the inexactness of our factored approximation.
This gives the following fundamental equation:
\begin{equation}
\bm{D} = \bm{P} \bm{H} \bm{A} + \bm{ε}
%\bm{D} \sim \dist{ \wishartDist{n} }{ a, \bm{B} }
\label{eq:Notation}
\end{equation}

For example, we can model the noise process as a matrix normal distribution:
\begin{equation}
\begin{split}
\bm{ε} & \sim \dist{ \matrixNormalDist }{ \bm{0}, \bm{Ψ}^{-1}, \bm{Ω}^{-1} } \\
\bm{D} & \sim \dist{ \matrixNormalDist }{ \bm{P} \bm{H} \bm{A}, \bm{Ψ}^{-1}, \bm{Ω}^{-1} } \\
\label{eq:MFModelLikelihood}
\end{split}
\end{equation}

As we will see in more detail later, this approach is amenable to VB inference.
For example, note how the expectation of the likelihood only contains terms in $\bm{P}$ and $\bm{P}^{\transpose} \bm{P}$, which means that this is conjugate to a Gaussian prior on the elements of $\bm{P}$.
\begin{equation}
\begin{split}
\expecWRT{ \lnFunc{ \cDist{ \p }{ \bm{D} }{ \bm{Θ} } } }{ \bm{Θ} \neq \bm{P} } = & -\frac{1}{2} \expecWRT{ \func{ \Tr }{ \bm{Ω} (\bm{P} \bm{H} \bm{A})^{\transpose} \bm{Ψ} (\bm{P} \bm{H} \bm{A}) - 2 \bm{Ω} \bm{D} \bm{Ψ} (\bm{P} \bm{H} \bm{A}) } }{ \bm{Ω}, \bm{Ψ}, \bm{H}, \bm{A} } \\
& + \func{\const}{\bm{P}}
\end{split}
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Simplified model}
For primarily computational reasons, we make several simplifications to the above.
We remove any spatio-temporal dependencies in the noise process $\bm{ε}$ and restrict $\bm{H}$ to be a diagonal matrix.
\begin{equation}
\begin{split}
\bm{Ω} & = \bm{I}_{T} \\
\bm{Ψ} & = ψ \bm{I}_{V} \\
%\bm{Ψ} & = \func{ \diag }{ \bm{ψ} } \\
\bm{H} & = \func{ \diag }{ \bm{h} } \\
\end{split}
\end{equation}
% Note we can't have $\bm{Ψ} = \func{ \diag }{ \bm{ψ} }$ as it violates the PtP constant for all V assumption - but we can (approximately) normalise the voxels to unit standard deviation.
% Data normalisation: have to normalise to get roughly same noise in each voxel. Then need flexible enough spatial prior, as may get different weights in different voxels.

This means we can simplify \Ref{equation}{eq:MFModelLikelihood} to:
\begin{equation}
\begin{split}
\bm{D}_{v}^{\transpose} & \sim \dist{ \normalDist }{ (\bm{P}_{v} \bm{H} \bm{A})^{\transpose}, \, ψ \bm{I}_{T} } \\
\bm{D}_{t} & \sim \dist{ \normalDist }{ \bm{P} \bm{H} \bm{A}_{t}, \, ψ \bm{I}_{V} } \\
\end{split}
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Update rules}

%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Spatial maps and time courses}
In this section we derive the updates for the spatial maps, $\bm{P}$.
The updates for the timecourses can be recovered by judicious use of the transpose operator.

\subsubsection*{Row-level updates}
We place a multivariate normal prior over the rows of $\bm{P}$, which gives:
\begin{equation}
\bm{P}_{v}^{\transpose} \sim \dist{ \normalDist }{ \bm{μ}, \bm{Σ} }
\end{equation}
\begin{equation}
\expecWRT{ \lnFunc{ \cDist{ \p }{ \bm{P}_{v}^{\transpose} }{ \bm{μ}, \bm{Σ} } } }{ \bm{Θ} \neq \bm{P} } = -\frac{1}{2} \bm{P}_{v} \expec{ \bm{Σ}^{-1} } \bm{P}_{v}^{\transpose} + \bm{P}_{v} \expec{ \bm{Σ}^{-1} } \expec{ \bm{μ} }
+ \func{\const}{\bm{P}_{v}}
\end{equation}

Similarly, the log-likelihood simplifies to:
\begin{equation}
\begin{split}
%%
\expecWRT{ \lnFunc{ \cDist{ \p }{ \bm{D} }{ \bm{Θ} } } }{ \bm{Θ} \neq \bm{P} } = & -\frac{1}{2} \sum_{v=1}^{V} \expec{ ψ (\bm{P}_{v} \bm{H} \bm{A}) (\bm{P}_{v} \bm{H} \bm{A})^{\transpose} } - 2 \expec{ ψ \bm{D}_{v} (\bm{P}_{v} \bm{H} \bm{A})^{\transpose} } \\
& + \func{\const}{\bm{P}_{v}} \\[5pt]
%%
= & -\frac{1}{2} \sum_{v=1}^{V} \expec{ ψ } \bm{P}_{v} \expec{ \bm{H} \bm{A} \bm{A}^{\transpose} \bm{H}^{\transpose} } \bm{P}_{v}^{\transpose} - 2 \expec{ ψ } \bm{P}_{v} \expec{ \bm{H} } \expec{ \bm{A} } \bm{D}_{v}^{\transpose} \\
& + \func{\const}{\bm{P}_{v}} \\
%%
\end{split}
\end{equation}

Comparing these to the approximate posterior yields the update rules:
\begin{equation}
\cDist{ \q }{ \bm{P}_{v} }{ \hat{\bm{μ}}, \hat{\bm{Σ}} } = \cDist{ \normalDist }{ \bm{P}_{v}^{\transpose} }{ \hat{\bm{μ}}, \hat{\bm{Σ}} }
\end{equation}

\begin{equation}
\begin{split}
% Sigma
\hat{\bm{Σ}} & = \Bigl( \expec{ \bm{Σ}^{-1} } + \expec{ ψ } \bigl( \expec{\bm{h} \bm{h}^{\transpose} } \circ \expec{ \bm{A} \bm{A}^{\transpose} } \bigr) \Bigr)^{-1} \\[5pt]
% Mu
\hat{\bm{μ}} & = \hat{\bm{Σ}} \, \Bigl( \expec{ \bm{Σ}^{-1} } \expec{ \bm{μ} } + \expec{ ψ } \expec{ \bm{H} } \expec{ \bm{A} } \bm{D}_{v}^{\transpose} \Bigr)
\end{split}
\end{equation}

It is straightforward to show that if $\bm{X} = \func{ \diag }{ \bm{x} }$ then $\bm{X} \bm{Y} \bm{X}^{\transpose} = (\bm{x} \bm{x}^{\transpose}) \circ \bm{Y}$.


\subsubsection*{Element-level updates}
It may well be the case that we wish to introduce a prior over the spatial maps that cannot be expressed as a prior over $\bm{P}_{v}$---see the mixture models document for examples of this.
In order to achieve this, we introduce an additional posterior factorisation, namely independence over modes as well as voxels.

Firstly, we let the prior take the following form:
\begin{equation}
\bm{P}_{vm} \sim \dist{ \normalDist }{ μ, σ^{2} }
\end{equation}
\begin{equation}
\expecWRT{ \lnFunc{ \cDist{ \p }{ \bm{P}_{vm} }{ μ, σ^{2} } } }{ \bm{Θ} \neq \bm{P} } = -\frac{1}{2} \expec{ σ^{-2} } \bm{P}_{vm}^{2} + \expec{ σ^{-2} } \expec{ μ } \bm{P}_{vm}
+ \func{\const}{\bm{P}_{vm}}
\end{equation}

Similarly, if we denote the set of all modes other than mode $m$ as $\neg m$, then the log-likelihood simplifies to:
\begin{equation}
\begin{split}
%%
\expecWRT{ \lnFunc{ \cDist{ \p }{ \bm{D}_{v} }{ \bm{Θ} } } }{ \bm{Θ} \neq \bm{P} } = & -\frac{1}{2} \expec{ ψ } \bm{P}_{v} \expec{ \bm{H} \bm{A} \bm{A}^{\transpose} \bm{H}^{\transpose} } \bm{P}_{v}^{\transpose} + \expec{ ψ } \bm{P}_{v} \expec{ \bm{H} } \expec{ \bm{A} } \bm{D}_{v}^{\transpose} \\
& + \func{\const}{\bm{P}_{vm}} \\[5pt]
%%
= & -\frac{1}{2} \expec{ ψ } \expec{ \bm{H} \bm{A} \bm{A}^{\transpose} \bm{H}^{\transpose} }_{mm} \bm{P}_{vm}^{2}
- \expec{ ψ } \bm{P}_{vm} \expec{ \bm{H} \bm{A} \bm{A}^{\transpose} \bm{H}^{\transpose} }_{m \neg m} \bm{P}_{v \neg m}^{\transpose} \\
& + \expec{ ψ } \bm{P}_{vm} \bigl( \expec{ \bm{H} } \expec{ \bm{A} } \bm{D}_{v}^{\transpose} \bigr)_{m}
+ \func{\const}{\bm{P}_{vm}} \\[5pt]
%%
\end{split}
\end{equation}

Comparing these to the approximate posterior yields the update rules:
\begin{equation}
\cDist{ \q }{ \bm{P}_{vm} }{ \hat{μ}, \hat{σ}^{2} } = \cDist{ \normalDist }{ \bm{P}_{vm} }{ \hat{μ}, \hat{σ}^{2} }
\end{equation}

\begin{equation}
\begin{split}
% Sigma
\hat{σ}^{2} & = \Bigl( \expec{ σ^{-2} } + \expec{ ψ } \bigl( \expec{\bm{h} \bm{h}^{\transpose} } \circ \expec{ \bm{A} \bm{A}^{\transpose} } \bigr)_{mm} \Bigr)^{-1} \\[5pt]
% Mu
\hat{μ} & = \hat{σ}^{2} \, \Bigl( \expec{ σ^{-2} } \expec{ μ }
+ \expec{ ψ } \bigl( \expec{ \bm{H} } \expec{ \bm{A} } \bm{D}_{v}^{\transpose} \bigr)_{m} 
- \expec{ ψ } \bigl( \expec{\bm{h} \bm{h}^{\transpose} } \circ \expec{ \bm{A} \bm{A}^{\transpose} } \bigr)_{m \neg m} \expec{ \bm{P}_{v \neg m} }^{\transpose} \Bigr)
\end{split}
\end{equation}

Intuitively, the last term in the expression for $\hat{μ}$ regresses out the contribution of the other maps at this voxel.
Otherwise, the computation is essentially unchanged.

%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Weights}
The mode weights, $\bm{h}$ are also conjugate to a multivariate normal distribution.
 
\begin{equation}
\bm{h} \sim \dist{ \normalDist }{ \bm{μ}, \bm{Σ} }
\end{equation}

\begin{equation}
\begin{split}
\expecWRT{ \lnFunc{ \cDist{ \p }{ \bm{D} }{ \bm{Θ} } } }{ \bm{Θ} \neq \bm{h} }
= & -\frac{1}{2} \expec{ ψ \func{ \Tr }{ (\bm{P} \bm{H} \bm{A})^{\transpose} (\bm{P} \bm{H} \bm{A}) 
- 2 ψ \bm{D}^{\transpose} (\bm{P} \bm{H} \bm{A}) } } \\
& + \func{\const}{\bm{h} } \\[5pt]
%%
= & -\frac{1}{2} \bm{h}^{\transpose} \Bigl( \expec{ ψ } \expec{ \bm{P}^{\transpose} \bm{P} } \circ \expec{ \bm{A} \bm{A}^{\transpose} } \Bigr) \bm{h}
+ \func{ \diag }{ \expec{ ψ } \expec{ \bm{P}^{\transpose} } \bm{D} \expec{ \bm{A}^{\transpose} } }^{\transpose} \bm{h} \\
& + \func{\const}{\bm{h} } \\[5pt]
\end{split}
\end{equation}

The update rules are therefore:
\begin{equation}
\cDist{ \q }{ \bm{h} }{ \hat{\bm{μ}}, \hat{\bm{Σ}} } = \cDist{ \normalDist }{ \bm{h} }{ \hat{\bm{μ}}, \hat{\bm{Σ}} }
\end{equation}

\begin{equation}
\begin{split}
% Sigma
\hat{\bm{Σ}} & = \Bigl( \expec{ \bm{Σ}^{-1} } + \expec{ ψ } \bigl( \expec{ \bm{P}^{\transpose} \bm{P} } \circ \expec{ \bm{A} \bm{A}^{\transpose} } \bigr) \Bigr)^{-1} \\[5pt]
% Mu
\hat{\bm{μ}} & = \hat{\bm{Σ}} \, \Bigl( \expec{ \bm{Σ}^{-1} } \expec{ \bm{μ} } + \func{ \diag }{ \expec{ ψ } \expec{ \bm{P}^{\transpose} } \bm{D} \expec{ \bm{A}^{\transpose} } } \Bigr)
\end{split}
\end{equation}

% MATLAB code
% M = 3; V = 10; T = 5;
% P = randn(V,M); PtP = P' * P;
% A = randn(M,T); AAt = A * A';
% h = randn(M,1); H = diag(h);
% D = randn(V,T);

% trace(H' * PtP * H * AAt)
% h' * (PtP .* AAt) * h

% trace(D' * P * H * A)
% diag(P' * D * A')' * h

% trace((D - P*H*A)' * (D - P*H*A))
% trace(D'*D) - 2*trace(D'*(P*H*A)) + trace((P*H*A)'*(P*H*A))
% trace(D'*D) - diag(P' * D * A')' * h + h' * (PtP .* AAt) * h

%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Noise precision}
Finally, the gamma distribution is the conjugate prior on the noise precision.
\begin{equation}
ψ \sim \dist{ \gammaDist }{ a, b }
\end{equation}
\begin{equation}
\expecWRT{ \lnFunc{ \cDist{ \p }{ ψ }{ μ, σ^{2} } } }{ \bm{Θ} \neq ψ } = \left( \frac{a}{2} - 1 \right) \lnFunc{ ψ } - \frac{1}{2} b ψ
+ \func{\const}{ ψ }
\end{equation}

\begin{equation}
\begin{split}
\expecWRT{ \lnFunc{ \cDist{ \p }{ \bm{D} }{ \bm{Θ} } } }{ \bm{Θ} \neq ψ } = & \, \frac{1}{2} VN \lnFunc{ ψ } - \frac{1}{2} ψ \expec{ \func{ \Tr }{ (\bm{D} - \bm{P} \bm{H} \bm{A})^{\transpose} (\bm{D} - \bm{P} \bm{H} \bm{A}) } } \\
& + \func{\const}{ ψ } \\[5pt]
= & \, \frac{1}{2} VN \lnFunc{ ψ } - \frac{1}{2} ψ \Bigl( \func{ \Tr }{ \bm{D}^{\transpose} \bm{D} } - 2 \func{ \Tr }{ \bm{D}^{\transpose} \expec{ \bm{P} } \expec{ \bm{H} } \expec{ \bm{A} } } \\ 
& \qquad + \expec{ \func{ \Tr }{ \bm{H}^{\transpose} \bm{P}^{\transpose} \bm{P} \bm{H} \bm{A} \bm{A}^{\transpose} } } \Bigr) + \func{\const}{ ψ } \\[5pt]
= & \, \frac{1}{2} VN \lnFunc{ ψ } - \frac{1}{2} ψ \Bigl( \func{ \Tr }{ \bm{D}^{\transpose} \bm{D} } - 2 \func{ \Tr }{ \bm{D}^{\transpose} \expec{ \bm{P} } \expec{ \bm{H} } \expec{ \bm{A} } } \\ 
& \qquad + \sum_{\forall i,j} \expec{ \bm{P}^{\transpose} \bm{P} } \circ \expec{\bm{h} \bm{h}^{\transpose} } \circ \expec{ \bm{A} \bm{A}^{\transpose} } \Bigr) + \func{\const}{ ψ } \\
\end{split}
\end{equation}

The update rules are therefore:
\begin{equation}
\cDist{ \q }{ ψ }{ \hat{a}, \hat{b} } = \cDist{ \gammaDist }{ ψ }{ \hat{a}, \hat{b} }
\end{equation}

\begin{equation}
\begin{split}
% a
\hat{a} & = a + VN \\[5pt]
% a
\hat{b} & = b + \func{ \Tr }{ \bm{D}^{\transpose} \bm{D} } - 2 \func{ \Tr }{ \bm{D}^{\transpose} \expec{ \bm{P} } \expec{ \bm{H} } \expec{ \bm{A} } } \\ 
& \qquad + \sum_{\forall i,j} \expec{ \bm{P}^{\transpose} \bm{P} } \circ \expec{\bm{h} \bm{h}^{\transpose} } \circ \expec{ \bm{A} \bm{A}^{\transpose} }
\end{split}
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\printbibliography

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}