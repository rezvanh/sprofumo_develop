% !TEX TS-program = lualatex
% !TEX encoding = UTF-8 Unicode

\documentclass[a4paper, 10pt]{article}

\usepackage{PROFUMOstyle}

\bibliography{../../../Documentation/Papers.bib}

% Define the title 
\author{Sam Harrison}% \hfill \today}
\title{Variational Inference of Standard Distributions}
\date{\today}

%Define header text
\header{Key Distributions}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document} 

% Generate the title
\maketitle 

\microtypesetup{protrusion=false} % disables protrusion locally in the document
\tableofcontents % prints Table of Contents
\microtypesetup{protrusion=true} % enables protrusion

\vspace{3em}

Most of these results are standard, or straightforward to derive.
However, there is hopefully still some value in having them all in one place.

Note that Will Penny has also put together a useful collection of results \cite{Penny2001}.

\newpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Gaussian
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Gaussian distribution}
%Notation
\begin{equation*}
x \sim \dist{ \normalDist }{ μ, σ^{2} }
\end{equation*}

%PDF
\begin{equation*}
\cDist{ \p }{ x }{ μ, σ^{2} } = \frac{1}{ σ \sqrt{2π} } \bigFunc{ \exp }{ - \frac{ (x - μ)^{2} }{ 2 σ^{2} } }
\end{equation*}

%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Expectations}
% X
\begin{equation*}
\expec{ x } = μ
\end{equation*}

% X^2
\begin{equation*}
\expec{ x^{2} } = μ^{2} + σ^{2}
\end{equation*}

%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{VB update rules}
\begin{equation*}
\dist{ \p }{ {d}_{n} } = \dist{ \normalDist }{ x, ψ^{-1} }
\qquad
\dist{ \p }{ x } = \dist{ \normalDist }{ μ_{x}, σ_{x}^{2} }
\end{equation*}

The update rules for $x$ take the form:
\begin{equation*}
\begin{split}
\dist{ \q }{ x } & = \dist{ \normalDist }{ \hat{μ}_{x}, \hat{σ}_{x}^{2} } \\[12pt]
% a
\hat{σ}_{x}^{2} & = \Bigl( σ_{x}^{-2} + N \expec{ ψ } \Bigr)^{-1} \\
% B
\hat{μ}_{x} & = \hat{σ}_{x}^{2} \left( σ_{x}^{-2} μ_{x} + \expec{ ψ } \sum_{n = 1}^{N} d_{n} \right) \\
\end{split}
\end{equation*}

%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{KL divergence}
\begin{equation*}
\KL \Bigl( \q \big\Vert \p \Bigr) = \lnFunc{ \hat{σ}_{x} } - \lnFunc{ σ_{x} } +\frac{ (\hat{μ}_{x} - μ_{x})^{2} + \hat{σ}_{x}^{2} }{ 2 σ_{x}^{2} } - \frac{1}{2}
\end{equation*}

%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Summary}
Therefore, we require $\expec{ ψ }$ and $\expec{ ψ d }$ from the level below.

If we place hyperpriors on the mean and precision, so $\dist{ \p }{ x } = \dist{ \normalDist }{ μ, α^{-1} }$, then we require $\expec{ α }$ and $\expec{ μ }$.
Furthermore, we need $\expec{\lnFunc{ α }}$ and $\expec{ μ^{2} }$ for the free energy calculation.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% MVN
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage

\section{Multivariate normal distribution}
This follows almost exactly from the above.
%Notation
\begin{equation*}
\bm{x} \sim \dist{ \normalDist }{ \bm{μ}, \bm{Σ} } \qquad \bm{x}, \bm{μ} \in \mathbb{R}^{K}; \, \bm{Σ} \in \mathbb{R}^{K \times K}
\end{equation*}

%PDF
\begin{equation*}
\cDist{ \p }{ \bm{x} }{ \bm{μ}, \bm{Σ} } = \frac{1}{ (2π)^{\nicefrac{K}{2}} \determinant{\bm{Σ}}^{\nicefrac{1}{2}} } \bigFunc{ \exp }{ - \frac{1}{2} \bm{μ}^{\transpose} \bm{Σ}^{-1} \bm{μ} }
\end{equation*}

%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Expectations}
% X
\begin{equation*}
\expec{ \bm{x} } = \bm{μ}
\end{equation*}

% X Xt
\begin{equation*}
\expec{ \bm{x} \bm{x}^{\transpose} } = \bm{μ} \bm{μ}^{\transpose} + \bm{Σ}
\end{equation*}

% Xt X
\begin{equation*}
\expec{ \bm{x}^{\transpose} \bm{x} } = \bm{μ}^{\transpose} \bm{μ} + \func{ \Tr }{ \bm{Σ} }
\end{equation*}

%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{VB update rules}
\begin{equation*}
\dist{ \p }{ \bm{d}_{n} } = \dist{ \normalDist }{ \bm{x}, \bm{Ψ}^{-1} }
\qquad
\dist{ \p }{ \bm{x} } = \dist{ \normalDist }{ \bm{μ}, \bm{Φ}^{-1} }
\end{equation*}

The update rules for $x$ take the form:
\begin{equation*}
\begin{split}
\dist{ \q }{ x } & = \dist{ \normalDist }{ \hat{\bm{μ}}_{x}, \hat{\bm{Σ}}_{x} } \\[12pt]
% a
\hat{\bm{Σ}}_{x} & = \Bigl( \expec{ \bm{Φ} } + N \expec{ \bm{Ψ} } \Bigr)^{-1} \\
% B
\hat{\bm{μ}}_{x} & = \hat{\bm{Σ}}_{x} \left( \expec{ \bm{Φ} } \expec{ \bm{μ} } + \sum_{n = 1}^{N} \expec{ \bm{Ψ} \bm{d}_{n} } \right) \\
\end{split}
\end{equation*}

%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{KL divergence}
\begin{equation*}
\begin{split}
\KL \Bigl( \q \big\Vert \p \Bigr) = & \, \frac{1}{2} \logdet{ \hat{\bm{Σ}}_{x} } - \frac{1}{2} \expec{ \logdet{ \bm{Φ} } } \\
% Rearrange (mux - mu)^t E^-1 (mux - mu)
& + \frac{1}{2} \hat{\bm{μ}}_{x}^{\transpose} \hat{\bm{Σ}}_{x}^{-1} \hat{\bm{μ}}_{x}
- \hat{\bm{μ}}_{x}^{\transpose} \hat{\bm{Σ}}_{x}^{-1} \expec{ \bm{μ} }
+ \frac{1}{2} \func{ \Tr }{ \hat{\bm{Σ}}_{x}^{-1} \expec{ \bm{μ} \bm{μ}^{\transpose} } } \\
%
& + \frac{1}{2} \func{ \Tr }{ \hat{\bm{Σ}}_{x} \expec{ \bm{Φ} } } - \frac{K}{2} \\
\end{split}
\end{equation*}

%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Summary}
Therefore, we require $\expec{ \bm{Ψ} }$ and $\expec{ \bm{Ψ} \bm{d} }$ from the level below.

Similary, we require $\expec{ \bm{Φ} }$ and $\expec{ \bm{μ} }$ from the hyperpriors for the update rules, and we need $\expec{ \logdet{ \bm{Φ} } }$ and $\expec{ \bm{μ} \bm{μ}^{\transpose} }$ for the free energy calculation.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Gamma
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage

\section{Gamma distribution}
Frustratingly, the standard parameterisation of the gamma distribution does not match the one-dimensional version of the Wishart distribution. Therefore, we use the following non-standard definition of the gamma distribution:
%Notation
\begin{equation*}
x \sim \dist{ \gammaFunc }{ a, b }
\end{equation*}

%PDF
\begin{equation*}
\cDist{ \p }{ x }{ a, b } = \frac{ b^{\nicefrac{a}{2}} x^{\left( \nicefrac{a}{2} - 1 \right)} e^{ -\nicefrac{1}{2} b x} }{2^{\nicefrac{a}{2}} \, \func{ \gammaFunc }{ \nicefrac{a}{2} } }
\end{equation*}

%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Expectations}
% X
\begin{equation*}
\expec{ x } = \frac{a}{b}
\end{equation*}

% ln(X)
\begin{equation*}
\expec{ \lnFunc{ x } } = \func{ \psiFunc }{ \nicefrac{a}{2} } - \lnFunc{ \nicefrac{b}{2} }
\end{equation*}

%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{VB update rules}
\begin{equation*}
\dist{ \p }{ {d}_{n} } = \dist{ \normalDist }{ μ, α^{-1} }
\qquad
\dist{ \p }{ α } = \dist{ \gammaDist }{ a_{α}, b_{α} }
\end{equation*}

The update rules for $α$ take the form:
\begin{equation*}
\begin{split}
\dist{ \q }{ α } & = \dist{ \gammaDist }{ \hat{a}_{α}, \hat{b}_{α} } \\[12pt]
% a
\hat{a}_{α} & = a_{α} + N \\
% b
\hat{b}_{α} & = b_{α}  +  \sum_{n = 1}^{N} \expec{ (d_{n} - μ)^{2} } \\
\end{split}
\end{equation*}

%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{KL divergence}
\begin{equation*}
\begin{split}
\KL \Bigl( \q \big\Vert \p \Bigr) = & \,
\frac{\hat{a}}{2} \lnFunc{ \hat{b} } - \frac{a}{2} \lnFunc{ b } 
+ \frac{\hat{a} - a}{2} \expecWRT{ \lnFunc{ x } }{ \q }
- \frac{1}{2} ( \hat{b} - b ) \expecWRT{ x }{ \q } \\
& - \frac{(\hat{a} - a)}{2} \lnFunc{  2 }
- \lnFunc{ \func{ \gammaFunc }{ \nicefrac{\hat{a}}{2} } } + \lnFunc{ \func{ \gammaFunc }{ \nicefrac{a}{2} } } \\[8pt]
%% 
= & \,
\frac{a}{2} \Bigl( \lnFunc{ \hat{b} } - \lnFunc{ b } \Bigr)
+ \frac{\hat{a}}{2} \Bigl( \frac{b}{\hat{b}} - 1 \Bigr) \\
& + \frac{\hat{a} - a}{2} \func{ \psiFunc }{ \nicefrac{\hat{a}}{2} }
- \lnFunc{ \func{ \gammaFunc }{ \nicefrac{\hat{a}}{2} } } + \lnFunc{ \func{ \gammaFunc }{ \nicefrac{a}{2} } } \\
\end{split}
\end{equation*}

%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Summary}
We require $\expec{ (x - μ)^{2} }$ and $\expec{ N }$ from the level below.

There is no standard hyperprior for the shape parameter $a$, but the conjugate prior for $b$ is another gamma distribution.
However, we will not deal with this here: see the next section on hierarchical Wishart distributions for more information in that regard.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Wishart
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage

\section{Wishart distribution}
This is the generalisation of the gamma distribution to symmetric, nonnegative-definite matrices, first formulated by John Wishart in 1928\superbcite{Wishart1928}. If the random matrix $\bm{X} \in \mathbb{R}^{n \times n}$ follows a Wishart distribution with $a$ degrees of freedom and rate matrix $\bm{B} \in \mathbb{R}^{n \times n}$, then:
%Notation
\begin{equation*}
\bm{X} \sim \dist{ \wishartDist{n} }{ a, \bm{B} }
\label{eq:Notation}
\end{equation*}

%PDF
\begin{equation*}
\dist{ \p }{ \bm{X} | a, \bm{B} } = \frac{ \determinant{\bm{B}}^{\nicefrac{a}{2}} \determinant{\bm{X}}^{\nicefrac{a-n-1}{2}} e^{ -\nicefrac{1}{2} \func{ \Tr }{ \bm{B}\bm{X} }} }{2^{\nicefrac{an}{2}} \, \func{ \multGammaFunc{n} }{ \nicefrac{a}{2} } }
\label{eq:PDF}
\end{equation*}
Where $\func{ \multGammaFunc{n} }{ x }$ is the multivariate gamma function.
Note that the distribution is often parameterised in terms of $\bm{B}^{-1}$; the notation used here has been adopted to maintain consistency with the gamma distribution.

%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection*{Multivariate gamma and digamma functions}
$\func{ \gammaFunc }{ x }$ and $\func{ \multGammaFunc{n} }{ x }$ are the univariate and multivariate versions of the gamma functions respectively. They are related by the following:
\begin{equation*}
\func{ \multGammaFunc{n} }{ x } = π^{\nicefrac{n(n-1)}{4}} \prod_{i=1}^{n} \bigFunc{ \gammaFunc }{ x - \frac{i - 1}{2} }
\end{equation*}

Similarly, $\func{ \digammaFunc }{ x }$ and $\func{ \multDigammaFunc{n} }{ x }$ are the univariate and multivariate versions of the digamma functions respectively. They are related by the following:
\begin{equation*}
\func{ \multDigammaFunc{n} }{ x } = \sum_{i=1}^{n} \bigFunc{ \digammaFunc }{ x - \frac{i - 1}{2} }
\end{equation*}

%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Expectations}
% X
\begin{equation*}
\expec{ \bm{X} } = a \bm{B}^{-1}
\label{eq:E_X}
\end{equation*}
% ? X^{r} ? Where is this from?!
%\begin{equation*}
%\langle \bm{X}^{r} \rangle = \frac{2^{r} \gammaFunc(\frac{a}{2}+r)}{\gammaFunc(\frac{a}{2})} \bm{B}^{-r}
%\end{equation*}
%Expectations: log |X|
\begin{equation*}
\expec{ \logdet{ \bm{X} } } = \func{ \multDigammaFunc{n} }{ \nicefrac{a}{2} } - \logdet{ \bm{B} } + n \lnFunc{ 2 } 
\label{eg:E_logDetX}
\end{equation*}
$\func{ \multDigammaFunc{n} }{ x }$ is the multivariate digamma function.

%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{VB update rules}
Consider the following ridiculously simple hierarchical model:
\begin{equation*}
\bm{x}_{mn} \sim \dist{ \normalDist }{ \bm{μ}, \bm{α}_{n}^{-1} }
\qquad
\bm{α}_{n} \sim \dist{ \wishartDist{d} }{ a_{\bm{α}_{n}}, \bm{β} }
\qquad
\bm{β} \sim \dist{ \wishartDist{d} }{ a_{\bm{β}}, \bm{B}_\bm{β} }
\end{equation*}


The update rules for $\bm{α}_{n}$ take the form:
\begin{equation*}
\begin{split}
% Explicitly write out
\lnFunc{ \dist{ \q }{ \bm{α}_{n} } } = & \, \frac{M}{2} \logdet{ \bm{α}_{n} }  - \frac{1}{2} \sum_{m = 1}^{M} \bigl( \bm{x}_{mn} - \bm{μ} \bigr)^{\transpose} \bm{α}_{n} \bigl( \bm{x}_{mn} - \bm{μ} \bigr) \\
& + \frac{a_{\bm{α}_{n}} - d - 1}{2} \logdet{ \bm{α}_{n} } - \frac{1}{2} \func{ \Tr }{ \expec{ \bm{β} } \bm{α}_{n} } + \const \\
\end{split}
\end{equation*}

\begin{equation*}
\begin{split}
% a
\hat{a}_{\bm{α}_{n}} & = a_{\bm{α}_{n}} + M \\
% B
\hat{\bm{B}}_{\bm{α}_{n}} & = \expec{ \bm{β} }  +  \sum_{m = 1}^{M} \bm{x}_{mn} \bm{x}_{mn}^{\transpose} \\
\end{split}
\end{equation*}


And similarly for $\bm{β}$:
\begin{equation*}
\begin{split}
% Explicitly write out
\lnFunc{ \dist{ \q }{ \bm{β} } } = & \, \frac{1}{2} \sum_{n = 1}^{N} a_{\bm{α}_{n}} \logdet{ \bm{β} }  - \frac{1}{2} \sum_{n = 1}^{N} \func{ \Tr }{ \expec{ \bm{α}_{n} } \bm{β} } \\
& + \frac{a_{\bm{β}} - d - 1}{2} \logdet{ \bm{β} } - \frac{1}{2} \func{ \Tr }{ \bm{B}_\bm{β} \, \bm{β} } + \const \\
\end{split}
\end{equation*}

\begin{equation*}
\begin{split}
% a
\hat{a}_{\bm{β}} & = a_{\bm{β}} + \sum_{n = 1}^{N} a_{\bm{α}_{n}} \\
% B
\hat{\bm{B}}_{\bm{β}} & = \bm{B}_{\bm{β}}  +  \sum_{n = 1}^{N} \expec{ \bm{α}_{n} } \\
\end{split}
\end{equation*}

%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{KL divergence}
\begin{equation*}
\DKL{ \cDist{ \q }{ \bm{X} }{ \hat{a}, \hat{\bm{B}} } }{ \cDist{ \p }{ \bm{X} }{ a, \bm{B} } } = \int \bigFunc{ \ln }{ \frac{\cDist{ \q }{ \bm{X} }{ \hat{a}, \hat{\bm{B}} }}{\cDist{ \p }{ \bm{X} }{ a, \bm{B} }} } \cDist{ \q }{ \bm{X} }{ \hat{a}, \hat{\bm{B}} } \d{\bm{X}}
\end{equation*}

Adopting the notation that $ \expecWRT{ \func{ \operatorname{f} }{ \bm{X} } }{ \q }  = \int \func{ \operatorname{f} }{ \bm{X} } \dist{\q}{ \bm{X} } \d{\bm{X}}$, this becomes:
\begin{equation*}
\begin{split}
\DKL{ \q }{ \p } = & \,
\frac{\hat{a}}{2} \logdet{ \hat{\bm{B}} } - \frac{a}{2} \logdet{ \bm{B} }
+ \frac{\hat{a} - a}{2} \expecWRT{ \logdet{ \bm{X} } }{ \q }
- \frac{1}{2} \func{ \Tr }{ ( \hat{\bm{B}} - \bm{B} ) \expecWRT{ \bm{X} }{ \q } } \\
& - \frac{(\hat{a} - a)}{2} n \lnFunc{ 2 }
- \lnFunc{ \func{ \multGammaFunc{n} }{ \frac{\hat{a}}{2} } } + \lnFunc{ \func{ \multGammaFunc{n} }{ \frac{a}{2} } } \\[8pt]
%%
= & \,
\frac{a}{2} \Bigl( \logdet{ \hat{\bm{B}} } - \logdet{ \bm{B} } \Bigr)
+ \frac{\hat{a}}{2} \Bigl( \func{ \Tr }{ \bm{B} \hat{\bm{B}}^{-1} } - n \Bigr) \\
& + \frac{\hat{a} - a}{2} \func{ \multDigammaFunc{n} }{ \frac{\hat{a}}{2} }
 - \lnFunc{ \func{ \multGammaFunc{n} }{ \frac{\hat{a}}{2} } } + \lnFunc{ \func{ \multGammaFunc{n} }{ \frac{a}{2} } } \\[8pt]
\end{split}
\end{equation*}

The terms should all be recognisable as multivariate versions of the terms in the KL divergence between two gamma distributions.

%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Summary}
The Wishart forms a prior on the precision matrix of a Gaussian distribution. It is then possible to stack the hierarchically, by as the Wishart distribution is it's own conjugate prior for the rate matrix $\bm{B}$.
Note however, that the expectations \enquote{flip} as we progress up the chain (observe how the mean of the distribution relates to $\bm{B}^{-1}$).
Therefore, in our example, $\bm{α}$ is a prior on the Gaussian precision, but in essence $\bm{β}$ is a hyperprior on the Gaussian covariance.

We require $\expec{ \bm{x}_{m} \bm{x}_{m}^{\transpose} }$ and $\expec{ M }$ from the level below, though these can come from either a Gaussian distribution or Wishart distribution.
They can either be derived from $\expec{ \bigl( \bm{x}_{m} - \bm{μ} \bigr) \bigl( \bm{x}_{m} - \bm{μ} \bigr)^{\transpose} }$ in the case of a Gaussian, or $\expec{ \bm{α} }$ and $a_{\bm{α}}$ in the case of a Wishart.

The hyperpriors need to provide $\expec{ \bm{β} }$ for the update rules, and $\expec{ \logdet{ \bm{β} } }$ for the free energy.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Dirichlet
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage

\section{Dirichlet distribution}
This is a prior over a discrete probability distribution, and this comes in particularly useful for mixture models.
Note that there is another document on them specifically.

%Notation
\begin{equation*}
\bm{x} \sim \dist{ \dirichletDist }{ \bm{c} }
\end{equation*}

%PDF
\begin{equation*}
\cDist{ \p }{ \bm{x} }{ \bm{c} } = \frac{1}{ \func{\operatorname{B}}{ \bm{c} } } \prod_{\forall k} x_{k}^{c_{k} - 1}
\end{equation*}
where $\func{\operatorname{B}}{ \bm{c} }$ is the multivariate beta function.

%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Expectations}
% X
\begin{equation*}
\expec{ x_{k} } = \frac{ c_{k} }{ \sum_{\forall m} c_{m} }
\end{equation*}

% ln(X)
\begin{equation*}
\expec{ \lnFunc{x_{k}} } = \func{ \psiFunc }{ c_{k} } - \func{ \psiFunc }{ {\textstyle\sum_{\forall m}} c_{m} }
\end{equation*}

%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{VB update rules}
The Dirichlet distribution is conjugate to the categorical distribution.
In the toy model below, $z_{n} \in \{1, \ldots, K\} $ is one realisation of an indicator variable.
It indicates class $k$ with probability $\bm{x}_{k}$.

\begin{equation*}
\dist{ \p }{ z_{n} } = \dist{ \operatorname{Cat} }{ \bm{x} }
\qquad
\dist{ \p }{ \bm{x} } = \dist{ \dirichletDist }{ \bm{c} }
\end{equation*}

The update rules for $x$ take the form:
\begin{equation*}
\begin{split}
\dist{ \q }{ \bm{x} } & = \dist{ \dirichletDist }{ \hat{\bm{c}} } \\[12pt]
% c
\hat{c}_{k} & = c_{k} + \sum_{\forall n} \expec{ [ z_{n} = k ] } \\
\end{split}
\end{equation*}
where $[P]$ is the Iverson bracket.

%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{KL divergence}
\begin{equation*}
\begin{split}
\KL \Bigl( \q \big\Vert \p \Bigr) = & + \func{ \ln \gammaFunc }{ {\textstyle\sum_{\forall k}} \hat{c}_{k} } - \func{ \ln \gammaFunc }{ {\textstyle\sum_{\forall k}} c_{k} }
- \sum_{\forall k} \func{ \ln \gammaFunc }{ \hat{c}_{k} } + \sum_{\forall k} \func{ \ln \gammaFunc }{ c_{k} } \\[8pt]
& + \sum_{\forall k} \bigl( \hat{c}_{k} - c_{k} \bigr) \Bigl( \func{ \psiFunc }{ \hat{c}_{k} } - \func{ \psiFunc }{ {\textstyle\sum_{\forall m}} \hat{c}_{m} } \Bigr) \\
\end{split}
\end{equation*}

%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Summary}
We require $\expec{ [ z_{n} = k ] }$ from the level below.
It turns out the categorical distribution requires $\expec{ \lnFunc{x_{k}} }$ for its updates, and $\expec{ x_{k} }$ for the free energy calculations.

Again, a hyperprior on $\bm{c}$ is beyond the scope of this document.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\printbibliography

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}