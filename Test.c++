// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2014
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Some useful tests for all sorts of random stuff

#include <iostream>
#include <memory>
#include <armadillo>
#include <cmath>
#include <limits>
#include <algorithm>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <chrono>
#include <thread>  // std::this_thread::sleep_for( std::chrono::seconds(1) );
#include <omp.h>

#include "NewNifti/NewNifti.h"

#include "DataTypes.h"
#include "DataLoader.h"

#include "MFModel.h"
#include "MFVariableModels.h"
#include "MFModels/FullRankMFModel.h"
#include "MFModels/LowRankMFModel.h"

#include "MFModels/P/RandConst.h"
#include "MFModels/P/IndependentMixtureModels.h"
#include "MFModels/P/AdditiveGaussian.h"
#include "MFModels/P/DGMM.h"
#include "MFModels/P/Parcellation.h"
#include "MFModels/P/PottsParcellation.h"

#include "MFModels/H/MultivariateNormal.h"
#include "MFModels/H/SpikeSlab.h"

#include "MFModels/A/KroneckerHRF.h"
#include "MFModels/A/MultivariateNormal.h"
#include "MFModels/A/AdditiveMultivariateNormal.h"
#include "MFModels/A/RandConst.h"

#include "MFModels/Psi/GammaPrecision.h"

#include "ModuleList.h"
#include "VBModules/Constant.h"
#include "VBModules/PrecisionMatrices/SharedGammaPrecision.h"
#include "VBModules/PrecisionMatrices/ComponentwiseGammaPrecision.h"
#include "VBModules/PrecisionMatrices/Wishart.h"
#include "VBModules/PrecisionMatrices/HierarchicalWishart.h"
#include "VBModules/MatrixMeans/IndependentMeans.h"
#include "VBModules/MatrixPrecisions/IndependentGamma.h"
#include "VBModules/MembershipProbabilities/IndependentDirichlet.h"

#include "Utilities/Timer.h"
#include "Utilities/MiscMaths.h"
#include "Utilities/FastApproximateMaths.h"
#include "Utilities/RandomSVD.h"
#include "Utilities/DataNormalisation.h"
#include "Utilities/MatrixManipulations.h"
#include "Utilities/HRFModelling.h"


using namespace PROFUMO;


// Count lines
// find Source/ PROFUMO.c++ Test.c++ -name '*.h' -o -name '*.c++' | xargs wc -l
// find Scripts/ Visualisation/ -name '*.py' -o -name '*.sh' | xargs wc -l

// Check for licence tags
// http://unix.stackexchange.com/q/26836
// grep -rL "CCOPYRIGHT" --include="*.h" --include="*.c++" --include="*.cpp" .
// grep -rL "SHBASECOPYRIGHT" --include="*.py" --include="*.sh" .

// Sort blank lines at EOF
// https://stackoverflow.com/questions/729692/why-should-text-files-end-with-a-newline
// http://stackoverflow.com/a/4865030
// Test: for i in $(find . -name '*.h' -o -name '*.c++'); do  if diff /dev/null "$i" | tail -1 |   grep '^\\ No newline' > /dev/null; then  echo $i;  fi; done
// Fix: for i in $(find . -name '*.h' -o -name '*.c++'); do  if diff /dev/null "$i" | tail -1 |   grep '^\\ No newline' > /dev/null; then echo >> "$i";  fi; done

// Convert between float & double
// https://softwareengineering.stackexchange.com/questions/188721/when-do-you-use-float-and-when-do-you-use-double
// https://stackoverflow.com/questions/2386772/difference-between-float-and-double
// THIS SECTION WILL GET MANGLED BY THE COMMANDS BELOW! COPY IT SOMEWHERE SAFE FIRST!
// https://unix.stackexchange.com/questions/112023/how-can-i-replace-a-string-in-a-files
// http://unix.stackexchange.com/q/102191
// Check for any existing usage of floats / doubles that aren't in a code context
/*
find Source/ PROFUMO.c++ Test.c++ -type f \( -name '*.h' -o -name '*.c++' \) \
  -exec grep -E 'arma::fmat|arma::fvec|arma::frowvec|float' {} +
find Source/ PROFUMO.c++ Test.c++ -type f \( -name '*.h' -o -name '*.c++' \) \
  -exec grep -E 'arma::mat[^ >(&*]|arma::vec[^ >(&*]|arma::rowvec[^ >(&*]|double[^ >(&*]' {} +
*/
// Replace!
/*
find Source/ PROFUMO.c++ Test.c++ -type f \( -name '*.h' -o -name '*.c++' \) \
  -exec sed -i 's/arma::mat/arma::fmat/g; s/arma::vec/arma::fvec/g; s/arma::rowvec/arma::frowvec/g; s/double/float/g; s/arma::fvectorise/arma::vectorise/g' {} +
*/

// Namespace usage
// http://stackoverflow.com/questions/1452721/why-is-using-namespace-std-considered-bad-practice
// http://stackoverflow.com/questions/8681714/correct-way-to-define-c-namespace-methods-in-cpp-file

// Const pointers etc
// http://stackoverflow.com/questions/3888470/c-const-member-function-that-returns-a-const-pointer-but-what-type-of-const

// Profiling
// http://stackoverflow.com/questions/375913/what-can-i-use-to-profile-c-code-in-linux
// valgrind --tool=callgrind ./(Your binary)
// Then look at with kcachegrind or Gprof2Dot
// e.g. ./gprof2dot.py -f callgrind callgrind.out.x | dot -Tsvg -o output.svg

// How memory layout makes polymorphism work. Awesome.
// http://www.cs.bgu.ac.il/~spl121/Inheritance

// Useful multiple inheritance stuff
// http://www.cprogramming.com/tutorial/multiple_inheritance.html
// http://www.cprogramming.com/tutorial/virtual_inheritance.html
// http://stackoverflow.com/questions/2659116/how-does-virtual-inheritance-solve-the-diamond-problem
// http://stackoverflow.com/questions/18398409/c-inherit-from-multiple-base-classes-with-the-same-virtual-function-name
// http://stackoverflow.com/questions/6944014/c-multiple-inheritance-and-templates

// Pointers v references
// http://www.embedded.com/electronics-blogs/programming-pointers/4023307/References-vs-Pointers
// http://stackoverflow.com/questions/8005514/is-returning-references-of-member-variables-bad-pratice/8005559#8005559
// http://stackoverflow.com/questions/2182408/return-a-const-reference-or-a-copy-in-a-getter-function
// http://stackoverflow.com/questions/7058339/c-when-to-use-references-vs-pointers


int main(int, char**)
{
    std::cout << std::string(80, '*') << std::endl;
    
    // Set the number of threads
    #ifdef _OPENMP
        omp_set_num_threads(6);
        std::cout << "Multi-threaded using OpenMP (max " << omp_get_max_threads() << " threads)" << std::endl;
    #else
        std::cout << "Single threaded (compiled without OpenMP)" << std::endl;
    #endif
    
    /*
    std::cout << omp_get_max_threads() << std::endl;
    #pragma omp parallel for num_threads(std::min(6, omp_get_max_threads()))
    for (unsigned int i = 0; i < 10; ++i) {
        #pragma omp critical
        {
        std::cout << "Thread: " << i << " of " << omp_get_num_threads() << std::endl;
        }
    }
    */
    
    arma::arma_rng::set_seed_random();  // set the seed to a random value
    //arma::arma_rng::set_seed(100);  // set the seed to a fixed value
    
    ////////////////////////////////////////////////////////////////////////////
    // Gaussian mixture model (VBDistribution)
    /*
    {
        std::cout << std::string(80, '*') << std::endl;
        std::cout << "Gaussian mixture model" << std::endl << std::endl;
        
        VBDistributions::GaussianMixtureModel::GaussianParameters slabPrior;
        slabPrior.parameters.mu = 0.5; slabPrior.parameters.sigma2 = 1.0; slabPrior.probability = 0.1;
        std::vector<VBDistributions::GaussianMixtureModel::GaussianParameters> gp = {slabPrior};
        VBDistributions::GaussianMixtureModel::DeltaParameters spikePrior;
        spikePrior.parameters.mu = 0.0; spikePrior.probability = 0.9;
        std::vector<VBDistributions::GaussianMixtureModel::DeltaParameters> dp = {spikePrior};
        
        VBDistributions::GaussianMixtureModel GMM(gp, dp, true);
        
        std::cout << "Prior" << std::endl;
        std::cout << "E[x]: " << GMM.getExpectations().x << std::endl;
        std::cout << "KL: " << GMM.getKL() << std::endl << std::endl;
        
        VBDistributions::DataExpectations::Gaussian d;
        unsigned int N = 10; float precision = 0.5; float mean = 1.5;
        d.psi = N * precision; d.psi_d = N * precision * mean;
        
        std::cout << "Data" << std::endl;
        std::cout << "Mean: " << mean << std::endl;
        std::cout << "Precision: " << precision << std::endl;
        std::cout << "N: " << N << std::endl << std::endl;
        
        GMM.update(d);
        
        std::cout << "Posterior" << std::endl;
        std::cout << "E[x]: " << GMM.getExpectations().x << std::endl;
        std::cout << "E[x2]: " << GMM.getExpectations().x2 << std::endl;
        std::cout << "KL: " << GMM.getKL() << std::endl << std::endl;
    }
    */
    ////////////////////////////////////////////////////////////////////////////
    // Gamma distribution
    /*
    {
        std::cout << std::string(80, '*') << std::endl;
        std::cout << "Gamma distribution" << std::endl << std::endl;
        
        namespace VBDists = PROFUMO::VBDistributions;
        
        VBDists::Gamma::Parameters prior;
        prior.a = 1.0;
        prior.b = 1.0;
        
        VBDists::Gamma gDist(prior);
        
        std::cout << "Prior" << std::endl;
        std::cout << "E[std]: " << sqrt( 1.0 / gDist.getExpectations().x ) << std::endl;
        std::cout << "KL: " << gDist.getKL() << std::endl << std::endl;
        
        arma::fvec r = 8.0 * arma::randn<arma::fvec>(1000);
        
        VBDists::DataExpectations::Gamma D;
        D.n = r.n_elem;
        D.d2 = arma::accu(arma::square(r));
        gDist.update(D);
        
        std::cout << "Posterior" << std::endl;
        std::cout << "Data std: " << stddev(r) << std::endl;
        std::cout << "E[std]: " << sqrt( 1.0 / gDist.getExpectations().x ) << std::endl;
        std::cout << "KL: " << gDist.getKL() << std::endl << std::endl;
    }
    */
    ////////////////////////////////////////////////////////////////////////////
    // Dirichlet distribution
    /*
    {
        std::cout << std::string(80, '*') << std::endl;
        std::cout << "Dirichlet distribution" << std::endl << std::endl;
        
        namespace VBDists = PROFUMO::VBDistributions;
        
        VBDists::Dirichlet::Parameters prior;
        prior.counts = arma::randu<arma::fvec>(5);
        
        VBDists::Dirichlet dDist(prior);
        
        std::cout << "Prior" << std::endl;
        std::cout << "Probabilities:" << std::endl << dDist.getExpectations().p << std::endl;
        std::cout << "sum(P): " << arma::sum(dDist.getExpectations().p) << std::endl;
        std::cout << "KL: " << dDist.getKL() << std::endl << std::endl;
        
        VBDists::DataExpectations::Dirichlet D;
        D.counts = 10.0 * arma::randu<arma::fvec>(5);
        dDist.update(D);
        
        std::cout << "Posterior" << std::endl;
        std::cout << "Probabilities:" << std::endl << dDist.getExpectations().p << std::endl;
        std::cout << "sum(P): " << arma::sum(dDist.getExpectations().p) << std::endl;
        std::cout << "KL: " << dDist.getKL() << std::endl << std::endl;
    }
    */
    ////////////////////////////////////////////////////////////////////////////
    // Profile PtD
    /*
    {
        std::cout << std::string(80, '*') << std::endl;
        std::cout << "Profile PtD" << std::endl << std::endl;
        
        Utilities::Timer timer;
        
        //Generate some random data
        int V = 90000;
        int T = 1200;
        int M = 100;
        
        arma::fmat D = arma::randn<arma::fmat>(V,T);
        arma::fmat P = arma::randn<arma::fmat>(V,M);
        arma::fmat DP = arma::randn<arma::fmat>(V,300);
        arma::fmat DA = arma::randn<arma::fmat>(300,T);
        
        timer.reset();
        arma::fmat DtP = P.t() * D;
        timer.printTimeElapsed();
        
        P = arma::randn<arma::fmat>(V,M);
        timer.reset();
        DtP = P.t() * D;
        timer.printTimeElapsed();
        
        timer.reset();
        DtP = (P.t() * DP) * DA;
        timer.printTimeElapsed();
    }
    */
    ////////////////////////////////////////////////////////////////////////////
    // NIFTI Shizzle
    /*
    {
        std::cout << std::string(80, '*') << std::endl;
        std::cout << "NIFTI Shizzle" << std::endl << std::endl;
        
        arma::fmat data;
        
        {
            // Set up NIFTI reader
            NiftiIO Reader;
            std::string filename = "???";
            char* buffer;
            std::vector<NiftiExtension> extensions;
            bool allocateBuffer = true;
            
            // Load data
            NiftiHeader header = Reader.loadImage(filename, buffer, extensions, allocateBuffer);
            
            // Few sanity checks
            std::cout << header.dim[5] << std::endl << header.dim[6] << std::endl;
            std::cout << header.bitsPerVoxel << std::endl;
            
            // Make an arma matrix using this (header.bitsPerVoxel tells us this is a load of floats)
            bool copyAuxMem = false, strict = true;
            arma::fmat tempData = arma::fmat((float*) buffer, header.dim[5], header.dim[6], copyAuxMem, strict);
            
            // Convert to a matrix of floats
            data = arma::conv_to<arma::fmat>::from( tempData.t() );
            
            // Clean up
            delete buffer;
        }
        
        // Demean
        arma::fvec mean = arma::mean(data,1);
        data.each_col() -= mean;

        // Normalise variances
        arma::fvec std = arma::stddev(data,0,1);
        data.each_col() /= std;
        
        // Save to file
        std::string fileName = "testNIFTI.txt";
        std::ofstream file;
        file.open( fileName.c_str() );
        file << data;
        file.close();
    }
    */
    ////////////////////////////////////////////////////////////////////////////
    // HDF5 Shizzle
    /*
    //#include <hdf5.h>
    //#include <H5Cpp.h>
    // NEED TO ADD -lhdf5_cpp TO LDLIBS IN MAKEFILE!
    {
        std::cout << std::string(80, '*') << std::endl;
        std::cout << "HDF5 Shizzle" << std::endl << std::endl;
        
        // Generate some data
        const unsigned int M = 10000, N = 100;
        arma::fmat X = arma::randn<arma::fmat>(M,N);
        X.elem( arma::find(arma::abs(X) < 1.0e-10) ).zeros();
        X.elem( arma::find(arma::abs(X) < 1.0) ).zeros();
        
        // Save with Armadillo
        Utilities::Timer timer;
        X.save("arma.hdf5", arma::hdf5_binary);
        timer.printTimeElapsed(3); timer.reset();
        
        // Save with HDF5
        // https://support.hdfgroup.org/HDF5/Tutor/compress.html
        // https://support.hdfgroup.org/ftp/HDF5/current/src/unpacked/c++/examples/h5tutr_cmprss.cpp
        // arma::diskio_meat
        const hsize_t dims[2] = {M,N};
        const hsize_t chunkDims[2] = {M,1};
        
        H5::H5File *file = new H5::H5File("bespoke.hdf5", H5F_ACC_TRUNC);
        
        H5::DataSpace *dataspace = new H5::DataSpace(2, dims);
        
        H5::DSetCreatPropList *plist = new H5::DSetCreatPropList;
        plist->setChunk(2, chunkDims);
        plist->setDeflate(9);
        
        H5::DataSet *dataset = new H5::DataSet( file->createDataSet("dataset", H5T_NATIVE_DOUBLE, *dataspace, *plist) );
        
        // Write data to dataset.
        dataset->write(X.mem, H5T_NATIVE_DOUBLE);
        
        // Close objects and file.  Either approach will close the HDF5 item.
        delete dataspace;
        delete dataset;
        delete plist;
        //file->close();
        delete file;
        
        timer.printTimeElapsed(3);
    }
    */
    ////////////////////////////////////////////////////////////////////////////
    // Check the HRF shizzle
    /*
    {
        const std::string hrfFile = "Scripts/DefaultHRF.phrf";
        
        std::cout << std::string(80, '*') << std::endl;
        std::cout << "HRF Shizzle" << std::endl << std::endl;
        
        std::cout << Utilities::generateHRF(hrfFile, 10, 2.0) << std::endl;
        
        MFModels::A::KroneckerHRF::Parameters hrfPrior = Utilities::generateHRFPrior(hrfFile, 1200, 0.72, 50, 0.01, 0.05);
        std::cout << hrfPrior.K->submat(0,0,9,9) << std::endl;
        //hrfPrior.hrf->save("HRF.txt", arma::raw_ascii);
        //hrfPrior.K->save("K.txt", arma::raw_ascii);
    }
    */
    ////////////////////////////////////////////////////////////////////////////
    // Check the random SVD
    
    {
        std::cout << std::string(80, '*') << std::endl;
        std::cout << "Random SVD" << std::endl << std::endl;
        
        // Generate some random data
        const unsigned int M = 10000, N = 1250, C = 50, K = 100;
        //const unsigned int M = 2500, N = 5000, C = 50, K = 100;
        // Data: MxN matrix; C: true subspace rank; K: requested rank.
        arma::fmat Pd = 2.0 * (arma::randu<arma::fmat>(M,C) - 0.5);
        arma::fmat D1 = Pd * arma::randn<arma::fmat>(C,N) + arma::randn<arma::fmat>(M,N);
        arma::fmat D2 = Pd * arma::randn<arma::fmat>(C,N) + arma::randn<arma::fmat>(M,N);
        arma::fmat D = arma::zeros<arma::fmat>(M,2*N);
        D.cols(0,N-1) = D1; D.cols(N,2*N-1) = D2;
        
        
        // Compute standard SVD
        Utilities::Timer timer;
        arma::fmat U, V; arma::fvec s;
        svd_econ(U,s,V,D);
        U = U.head_cols(K); s = s.head(K); V = V.head_cols(K);
        timer.printTimeElapsed("Full SVD", 3);
        
        // Compute random SVD
        timer.reset();
        SVD svd = Utilities::computeRandomSVD(D, K);
        timer.printTimeElapsed("Random SVD", 3);
        
        // Compute random concatenated SVD
        timer.reset();
        std::vector<const arma::fmat*> Dv; Dv.push_back( &D1 ); Dv.push_back( &D2 );
        ConcatenatedSVD csvd = Utilities::computeRandomConcatenatedSVD(Dv, K);
        timer.printTimeElapsed("Random concatenated SVD", 3);
        
        std::cout << std::endl;
        
        
        // Look at variance explained
        std::cout << "Standard deviations:" << std::endl;
        std::cout << std::endl;
        
        std::cout << "D1: " << arma::stddev(arma::vectorise( D1 )) << std::endl;
        std::cout << "D1 - Full SVD:    " << arma::stddev(arma::vectorise( D1 - U * arma::diagmat(s) * V.rows(0,N-1).t() )) << std::endl;
        std::cout << "D1 - Random SVD:  " << arma::stddev(arma::vectorise( D1 - svd.U * arma::diagmat(svd.s) * svd.V.rows(0,N-1).t() )) << std::endl;
        std::cout << "D1 - Random cSVD: " << arma::stddev(arma::vectorise( D1 - csvd.U * arma::diagmat(csvd.s) * csvd.V[0].t() )) << std::endl;
        
        std::cout << std::endl;
        
        std::cout << "D2: " << arma::stddev(arma::vectorise( D2 )) << std::endl;
        std::cout << "D2 - Full SVD:    " << arma::stddev(arma::vectorise( D2 - U * arma::diagmat(s) * V.rows(N,2*N-1).t() )) << std::endl;
        std::cout << "D2 - Random SVD:  " << arma::stddev(arma::vectorise( D2 - svd.U * arma::diagmat(svd.s) * svd.V.rows(N,2*N-1).t() )) << std::endl;
        std::cout << "D2 - Random cSVD: " << arma::stddev(arma::vectorise( D2 - csvd.U * arma::diagmat(csvd.s) * csvd.V[1].t() )) << std::endl;
        
        std::cout << std::endl;
        
        
        // Look at accuracy
        std::cout << "Signal subspace accuracies:" << std::endl;
        std::cout << std::endl;
        
        arma::fmat D1_svd = U.head_cols(C) * arma::diagmat(s.head(C)) * V.submat(0,0,N-1,C-1).t();
        std::cout << "D1: Full SVD - Random SVD:  " << arma::mean(arma::vectorise(arma::abs( D1_svd - svd.U.head_cols(C) * arma::diagmat(svd.s.head(C)) * svd.V.submat(0,0,N-1,C-1).t() ))) << std::endl;
        std::cout << "D1: Full SVD - Random cSVD: " << arma::mean(arma::vectorise(arma::abs( D1_svd - csvd.U.head_cols(C) * arma::diagmat(csvd.s.head(C)) * csvd.V[0].head_cols(C).t() ))) << std::endl;
        
        std::cout << std::endl;
        
        arma::fmat D2_svd = U.head_cols(C) * arma::diagmat(s.head(C)) * V.submat(N,0,2*N-1,C-1).t();
        std::cout << "D2: Full SVD - Random SVD:  " << arma::mean(arma::vectorise(arma::abs( D2_svd - svd.U.head_cols(C) * arma::diagmat(svd.s.head(C)) * svd.V.submat(N,0,2*N-1,C-1).t() ))) << std::endl;
        std::cout << "D2: Full SVD - Random cSVD: " << arma::mean(arma::vectorise(arma::abs( D2_svd - csvd.U.head_cols(C) * arma::diagmat(csvd.s.head(C)) * csvd.V[1].head_cols(C).t() ))) << std::endl;
        
        std::cout << std::endl;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Check MiscMaths
    /*
    {
        std::cout << std::string(80, '*') << std::endl;
        std::cout << "Maths routines from Utilities" << std::endl << std::endl;
        
        // Check exponential approximation
        std::cout << Utilities::FastApprox::exp( float(22.5) ) << " (" << std::exp( float(22.5) ) << ")" << std::endl;
        std::cout << Utilities::FastApprox::exp( float(-22.15) ) << " (" << std::exp( float(-22.15) ) << ")"  << std::endl;
        
        //for (int i = 0; i < 101; ++i) {
        //    float p = 4.0 + 0.04 * i;
        //    std::cout << p << "(" << std::pow(2,p) << "): " << std::pow(2,p) - Utilities::FastApprox::pow2(p) << " (" << std::pow(2,p) - fastpow2(p) << ")" << std::endl;
        //    std::cout << p << " (" << std::pow(2,p) << "): " << Utilities::FastApprox::pow2(p) << " (" << fastpow2(p) << ")" << std::endl << std::endl;
        //}
        
        int V = 100000, M = 100;
        
        for (int z=0; z<5; ++z) {
            
            // Check accuracy
            arma::fmat A = 10.0 * arma::randn<arma::fmat>(V,M);
            arma::fmat A2 = A;
            arma::fmat B = A;
            arma::fmat C = A;
            
            Utilities::Timer timer;
            //A2 = arma::exp(A2);
            for (auto& a : A2) {
                a = std::exp(a);
            }
            timer.printTimeElapsed();
            A2 = arma::log(A2);
            
            timer.reset();
            for (auto& b : B) {
                b = Utilities::FastApprox::exp(b);
            }
            timer.printTimeElapsed();
            B = arma::log(B);
            
            std:: cout << "A: " << arma::mean(arma::mean( arma::abs( A ) )) << std::endl;
            std:: cout << "A2: " << arma::mean(arma::mean( arma::abs( A2 ) )) << std::endl;
            std:: cout << "A - A2: " << arma::mean(arma::mean( arma::abs( A - A2 ) )) << std::endl;
            std:: cout << "B: " << arma::mean(arma::mean( arma::abs( B ) )) << std::endl;
            std:: cout << "A - B: " << arma::mean(arma::mean( arma::abs( A - B ) )) << std::endl;
            std::cout << std::endl;
            
            
            arma::fmat sA = 10.0 * arma::randn<arma::fmat>(V,M);
            arma::fmat sB = sA;
            arma::fmat sC = sA;
            
            //Random order to update maps
            arma::uvec inds = arma::sort_index( arma::randu<arma::fvec>(M) );
            
            // arma::pow( )
            timer.reset();
            for (arma::uvec::const_iterator m=inds.begin(); m!=inds.end(); ++m) {
                sA.col(*m) = arma::pow(1.0 + arma::trunc_exp(-sA.col(*m)), -1);
            }
            timer.printTimeElapsed();
            
            // 1.0 / arma::fmat
            timer.reset();
            for (arma::uvec::const_iterator m=inds.begin(); m!=inds.end(); ++m) {
                sB.col(*m) = 1.0 / (1.0 + arma::trunc_exp(-sB.col(*m)));
            }
            timer.printTimeElapsed();
            
            timer.reset();
            for (arma::uvec::const_iterator m=inds.begin(); m!=inds.end(); ++m) {
                for (int v = 0; v < V; ++v) {
                    //float& b = sC(v,*m);
                    //if ( b < -10.0 ) {
                    //    b = 0.0;
                    //}
                    //else if ( b > 10.0 ) {
                    //    b = 1.0;
                    //}
                    //else {
                    //    b = 1.0 / ( 1.0 + std::exp( -b ) );
                    //}
                    sC(v,*m) = Utilities::MiscMaths::sigmoid(sC(v,*m));
                }
            }
            timer.printTimeElapsed();
            
            std:: cout << "A: " << arma::mean(arma::vectorise( arma::abs( sA ) )) << std::endl;
            std:: cout << "B: " << arma::mean(arma::vectorise( arma::abs( sB ) )) << std::endl;
            std:: cout << "C: " << arma::mean(arma::vectorise( arma::abs( sC ) )) << std::endl;
            std:: cout << "A - B: " << arma::mean(arma::vectorise( arma::abs( sA - sB ) )) << std::endl;
            std:: cout << "A - C: " << arma::mean(arma::vectorise( arma::abs( sA - sC ) )) << std::endl;
            std::cout << std::endl;
        }
    }
    */
    ////////////////////////////////////////////////////////////////////////////
    // Check cosine similarity
    /*
    {
        const arma::fmat X = arma::randn<arma::fmat>(100,3);
        const arma::fmat Y = arma::randn<arma::fmat>(100,5);
        
        // Sanity check cosine similarity
        std::cout << Utilities::MatrixManipulations::calculateCosineSimilarity(X, Y) << std::endl;
        std::cout << Utilities::MatrixManipulations::calculateCosineSimilarity(X) << std::endl;
        std::cout << Utilities::MatrixManipulations::calculateCosineSimilarity(Y) << std::endl;
        std::cout << Utilities::MatrixManipulations::calculateCosineSimilarity(arma::join_horiz(X,Y)) << std::endl;
        std::cout << std::endl;
        
        // Find a pairing between X and Y
        std::cout << Utilities::MatrixManipulations::calculateCosineSimilarity(X, Y) << std::endl;
        
        Utilities::MatrixManipulations::Pairing pairing 
            = Utilities::MatrixManipulations::pairMatrixColumns(X, Y);
        
        std::cout << pairing.inds1 << std::endl;
        std::cout << pairing.inds2 << std::endl;
        std::cout << pairing.scores << std::endl;
    }
    */
    ////////////////////////////////////////////////////////////////////////////
    // Full MFModel
    
    {
        std::cout << std::string(80, '*') << std::endl;
        std::cout << "MFModel test" << std::endl << std::endl;
        
        std::cout << "Initialising..." << std::endl << std::endl;
        
        //Size of simulated data
        const unsigned int V = 10000;
        const unsigned int T = 1500;
        const unsigned int N = 30;     // True number of modes
        const unsigned int M = 50;     // Number to infer
        //const float dofCorrectionFactor = 1.0;
        const float dofCorrectionFactor = 0.1;
        const float TR = 0.5;
        
        ////////////////////////////////////////////////////////////////////////
        /*
        {
        VBModules::MatrixMeans::TDMMPrior tauPrior; tauPrior.pPos = 0.15; tauPrior.pZero = 0.80; tauPrior.pNeg = 0.05;
        VBModules::MatrixMeans::TDMM* tau = new VBModules::MatrixMeans::TDMM(tauPrior, V, M);
        
        VBDistributions::GammaParams betaPrior; betaPrior.a = 1; betaPrior.b = 1;
        VBModules::MatrixPrecisions::IndependentGamma* beta = new VBModules::MatrixPrecisions::IndependentGamma(betaPrior, V, M);
        
        VBDistributions::DirichletParams piPrior; piPrior.counts = arma::zeros<arma::fvec>(2); piPrior.counts[0] = 4.0; piPrior.counts[1] = 1.0;
        VBModules::MembershipProbabilities::IndependentDirichlet* pi = new VBModules::MembershipProbabilities::IndependentDirichlet(piPrior, V, M);
        
        
        //MFModels::P::DGMMPrior PPrior; PPrior.V = V; PPrior.M = M;
        //#pragma omp parallel for
        //for(int z=0; z<50; z++){
        //    MFModels::P_VBPosterior* P = new MFModels::P::DGMM(PPrior, tau, beta, pi, dofCorrectionFactor);
        //}
        
        
        MFModels::P::AdditiveGaussianPrior PPrior; PPrior.precisionPrior.a = 1.0; PPrior.precisionPrior.b = 1.0;
        #pragma omp parallel for
        for(int z=0; z<1; z++){
            MFModels::P_VBPosterior* P = new MFModels::P::AdditiveGaussian(PPrior, tau, V, M, dofCorrectionFactor);
        }
        
        for(int z=0; z<5; z++){
            tau->update();
        }
        }
        */
        ////////////////////////////////////////////////////////////////////////
        
        //**********************************************************************
        // Generate data
        
        // Modes
        arma::fmat Pd = 2.0 * (arma::randu<arma::fmat>(V,N) - 0.5)
                        + 0.1 * arma::randn<arma::fmat>(V,N);
        arma::fmat Ad = 1.0 * arma::randn<arma::fmat>(N,T);
        //Ad = 0.1 * arma::randn<arma::fmat>(N,N) * Ad;
        arma::fmat D = Pd * Ad + 4.0 * arma::randn<arma::fmat>(V,T);
        
        /*
        // Parcellation
        arma::fmat Pd = arma::zeros<arma::fmat>(V,N);
        for (int v = 0; v < V; ++v) {
            //int n = v % N; Pd(v,n) = 1.0;
            int n = std::floor(N * v / V); Pd(v,n) = 1.0;
        }
        arma::fmat Ad = 1.0 * arma::randn<arma::fmat>(N,T);
        arma::fmat D = Pd * Ad + 1.0 * arma::randn<arma::fmat>(V,T);
        */
        
        std::cout << "std(D): " << arma::stddev( arma::vectorise(D) ) << std::endl;
        std::cout << "std(P*A): " << arma::stddev( arma::vectorise(Pd * Ad) ) << std::endl;
        std::cout << "std(D - P*A): " << arma::stddev( arma::vectorise(D - Pd * Ad) ) << std::endl << std::endl;
        
        //**********************************************************************
        // Normalise the data
        
        //Utilities::DataNormalisation::demeanGlobally(D);
        //Utilities::DataNormalisation::demeanRows(D);
        //Utilities::DataNormalisation::demeanColumns(D);
        //Utilities::DataNormalisation::normaliseGlobally(D);
        //Utilities::DataNormalisation::normaliseRows(D);
        //Utilities::DataNormalisation::normaliseColumns(D);
        //Utilities::DataNormalisation::normaliseSignalSubspaceGlobally(D, M);
        //Utilities::DataNormalisation::normaliseSignalSubspaceRows(D, M);
        //Utilities::DataNormalisation::normaliseSignalSubspaceColumns(D, M);
        //Utilities::DataNormalisation::normaliseNoiseSubspaceGlobally(D, M);
        //Utilities::DataNormalisation::normaliseNoiseSubspaceRows(D, M);
        //Utilities::DataNormalisation::normaliseNoiseSubspaceColumns(D, M);
        
        Utilities::DataNormalisation::demeanRows(D);
        Utilities::DataNormalisation::normaliseRows(D);
        Utilities::DataNormalisation::normaliseNoiseSubspaceRows(D, M);
        Utilities::DataNormalisation::normaliseSignalSubspaceGlobally(D, M);
        D *= std::sqrt(M);
        
        //D *= 100.0;
        
        //std::cout << arma::accu( arma::abs(D) < 1.0e-10 ) << std::endl;
        //D.elem( arma::find(arma::abs(D) < 1.0e-10) ).zeros();
        
        std::cout << "std(D): " << arma::stddev( arma::vectorise(D) ) << std::endl;
        SVD svd = Utilities::computeRandomSVD(D, M);
        std::cout << "std(signal): " << arma::stddev( arma::vectorise(svd.U * arma::diagmat(svd.s) * svd.V.t()) ) << std::endl;
        std::cout << "std(noise): " << arma::stddev( arma::vectorise(D - svd.U * arma::diagmat(svd.s) * svd.V.t()) ) << std::endl << std::endl;
        
        
        // Full rank data
        DataTypes::FullRankData FRD(D);
        
        
        /*
        // Low rank approximation
        unsigned int N_LRD = 300;
        float TrDtD_LRD = arma::accu( D % D );
        // Calculate SVD
        SVD svd_LRD = Utilities::computeRandomSVD(D, N_LRD);
        // Take components
        std::shared_ptr<arma::fmat> Pd_LRD = std::make_shared<arma::fmat>();
        std::shared_ptr<arma::fmat> Ad_LRD = std::make_shared<arma::fmat>();
        *Pd_LRD = svd_LRD.U * arma::diagmat(arma::sqrt( svd_LRD.s ));
        *Ad_LRD = arma::diagmat(arma::sqrt( svd_LRD.s )) * svd_LRD.V.t();
        //std::cout << arma::accu( arma::abs(*Pd_LRD) < 1.0e-10 ) << " - " << arma::accu( arma::abs(*Ad_LRD) < 1.0e-10 ) << std::endl;
        DataTypes::LowRankData LRD(Pd_LRD, Ad_LRD, N_LRD, TrDtD_LRD);
        */
        
        //**********************************************************************
        // First, make a constant spatial model
        
        
        arma::fmat initialisation
            = Pd * arma::randn<arma::fmat>(N,M)
              + 0.1 * arma::randn<arma::fmat>(V,M);
        Utilities::DataNormalisation::normaliseRows(initialisation);
        
        MFModels::P::P2C Einit; Einit.P = initialisation;
        Einit.PtP = Einit.P.t() * Einit.P + 0.01 * V * arma::eye<arma::fmat>(M, M);
        MFModels::P_VBPosterior* Pconst = new VBModules::Constant<MFModels::P::C2P, MFModels::P::P2C>(Einit);
        
        
        //**********************************************************************
        // P
        
        /*
        // Constant
        MFModels::P::P2C EP;
        EP.P = arma::zeros<arma::fmat>(V,M); EP.P.cols(0, N-1) = Pd;
        EP.PtP = EP.P.t() * EP.P;
        EP.PtP += 1.0e-5 * V * arma::eye<arma::fmat>(M,M);
        //EP.PtP += 1.0e-10 * V * arma::diagmat(arma::randu<arma::fvec>(M));
        MFModels::P_VBPosterior* P = new VBModules::Constant<MFModels::P::C2P, MFModels::P::P2C>(EP);
        */
        
        /*
        // Group-level spike-slab
        MFModels::P::IndependentMixtureModels::Parameters PPrior; PPrior.V = V; PPrior.M = M;
        VBDistributions::GaussianMixtureModel::GaussianParameters slabPrior;
        slabPrior.parameters.mu = 0.5; slabPrior.parameters.sigma2 = 1.0; slabPrior.probability = 0.1;
        PPrior.gaussianPriors = {slabPrior};
        VBDistributions::GaussianMixtureModel::DeltaParameters spikePrior;
        spikePrior.parameters.mu = 0.0; spikePrior.probability = 0.9;
        PPrior.deltaPriors = {spikePrior};
        MFModels::P_VBPosterior* P = new MFModels::P::IndependentMixtureModels(PPrior, dofCorrectionFactor);
        // Would be nice, but perfect forwarding doesn't work with initialisation parameters...
        // http://stackoverflow.com/a/18001305 
        //MFModels::P_VBPosterior* P = new MFModels::P::IndependentMixtureModels({slabPrior}, {spikePrior}, V, M, dofCorrectionFactor);
        */
        
        
        // Group-level TGMM
        MFModels::P::IndependentMixtureModels::Parameters PPrior; PPrior.V = V; PPrior.M = M;
        VBDistributions::GaussianMixtureModel::GaussianParameters gaussianPrior;
        gaussianPrior.parameters.mu = 1.0; gaussianPrior.parameters.sigma2 = std::pow(0.5, 2); gaussianPrior.probability = 0.05;
        PPrior.gaussianPriors.push_back(gaussianPrior);
        gaussianPrior.parameters.mu = -1.0; gaussianPrior.parameters.sigma2 = std::pow(0.5, 2); gaussianPrior.probability = 0.025;
        PPrior.gaussianPriors.push_back(gaussianPrior);
        gaussianPrior.parameters.mu = 0.0; gaussianPrior.parameters.sigma2 = 1.0; gaussianPrior.probability = 0.925;
        PPrior.gaussianPriors.push_back(gaussianPrior);
        MFModels::P_VBPosterior* P = new MFModels::P::IndependentMixtureModels(PPrior, dofCorrectionFactor);
        
        
        /*
        // Additive Gaussian
        // - Spike-slab group means
        VBModules::MatrixMeans::IndependentMixtureModels::MixtureModelParameters tauPrior;
        tauPrior.nRows = V; tauPrior.nCols = M;
        VBDistributions::GaussianMixtureModel::GaussianParameters slabPrior;
        slabPrior.parameters.mu = 0.5; slabPrior.parameters.sigma2 = 1.0; slabPrior.probability = 0.2;
        VBDistributions::GaussianMixtureModel::DeltaParameters spikePrior;
        spikePrior.parameters.mu = 0.0; spikePrior.probability = 0.8;
        tauPrior.gaussianPriors = {slabPrior}; tauPrior.deltaPriors = {spikePrior};
        VBModules::MatrixMeans::IndependentMixtureModels* tau = new VBModules::MatrixMeans::IndependentMixtureModels(tauPrior);
        
        MFModels::P::AdditiveGaussian::Parameters PPrior; PPrior.V = V; PPrior.M = M;
        PPrior.precisionPrior.a = 1.0; PPrior.precisionPrior.b = 1.0;
        MFModels::P_VBPosterior* P = new MFModels::P::AdditiveGaussian(tau, PPrior, dofCorrectionFactor);
        */
        
        /*
        // DGMM
        VBModules::MatrixMeans::IndependentGaussian::GaussianParameters tauPrior;
        tauPrior.gaussianPrior.mu = 0.5; tauPrior.gaussianPrior.sigma2 = 1.0; tauPrior.nRows = V; tauPrior.nCols = M;
        VBModules::MatrixMeans::IndependentGaussian* tau = new VBModules::MatrixMeans::IndependentGaussian(tauPrior);
        
        VBModules::MatrixPrecisions::IndependentGamma::Parameters betaPrior;
        betaPrior.gammaPrior.a = 1.0; betaPrior.gammaPrior.b = 1.0; betaPrior.nRows = V; betaPrior.nCols = M;
        VBModules::MatrixPrecisions::IndependentGamma* beta = new VBModules::MatrixPrecisions::IndependentGamma(betaPrior);
        
        VBModules::MatrixPrecisions::IndependentGamma::Parameters kappaPrior;
        kappaPrior.gammaPrior.a = 0.5; kappaPrior.gammaPrior.b = 0.1; kappaPrior.nRows = V; kappaPrior.nCols = 1;
        VBModules::MatrixPrecisions::IndependentGamma* kappa = new VBModules::MatrixPrecisions::IndependentGamma(kappaPrior);
        
        VBModules::MembershipProbabilities::IndependentDirichlet::Parameters piPrior;
        piPrior.dirichletPrior.counts = {0.5, 0.5}; piPrior.nRows = V; piPrior.nCols = M;
        VBModules::MembershipProbabilities::IndependentDirichlet* pi = new VBModules::MembershipProbabilities::IndependentDirichlet(piPrior);
        
        MFModels::P::DGMM::Parameters PPrior; PPrior.V = V; PPrior.M = M;
        MFModels::P_VBPosterior* P = new MFModels::P::DGMM(tau, beta, kappa, pi, PPrior, dofCorrectionFactor);
        */
        
        /*
        // Parcellation
        VBModules::MatrixMeans::IndependentGaussian::GaussianParameters tauPrior;
        tauPrior.gaussianPrior.mu = 0.5; tauPrior.gaussianPrior.sigma2 = 1.0; tauPrior.nRows = V; tauPrior.nCols = M;
        VBModules::MatrixMeans::IndependentGaussian* tau = new VBModules::MatrixMeans::IndependentGaussian(tauPrior);
        //Modules::MatrixMeans::P2C Etau; Etau.X = 0.5 * arma::randu<arma::fmat>(V,M); Etau.X2 = arma::square(Etau.X);
        //Modules::MatrixMean_VBPosterior* tau = new VBModules::Constant<Modules::MatrixMeans::C2P, Modules::MatrixMeans::P2C>(Etau);
        
        VBModules::MatrixPrecisions::IndependentGamma::Parameters betaPrior;
        betaPrior.gammaPrior.a = 1.0; betaPrior.gammaPrior.b = 1.0; betaPrior.nRows = V; betaPrior.nCols = M;
        VBModules::MatrixPrecisions::IndependentGamma* beta = new VBModules::MatrixPrecisions::IndependentGamma(betaPrior);
        //Modules::MatrixPrecisions::P2C Ebeta; Ebeta.X = 0.01 * arma::ones<arma::fmat>(V,M); Ebeta.logX = arma::log(Ebeta.X);
        //Modules::MatrixPrecision_VBPosterior* beta = new VBModules::Constant<Modules::MatrixPrecisions::C2P, Modules::MatrixPrecisions::P2C>(Ebeta);
        
        VBModules::MembershipProbabilities::IndependentDirichlet::Parameters piPrior;
        piPrior.dirichletPrior.counts = 0.01 * arma::ones<arma::fvec>(M); piPrior.nRows = V; piPrior.nCols = 1;
        VBModules::MembershipProbabilities::IndependentDirichlet* pi = new VBModules::MembershipProbabilities::IndependentDirichlet(piPrior);
        //Modules::MembershipProbabilities::P2C Epi;
        //Epi.p = std::vector<arma::fmat>(M, (1.0 / M) * arma::ones<arma::fmat>(V,1));
        //Epi.logP = std::vector<arma::fmat>(M, arma::log((1.0 / M) * arma::ones<arma::fmat>(V,1)));
        //Epi.logP = std::vector<arma::fmat>(M, -0.1 * arma::ones<arma::fmat>(V,1));
        //Modules::MembershipProbability_VBPosterior* pi = new VBModules::Constant<Modules::MembershipProbabilities::C2P, Modules::MembershipProbabilities::P2C>(Epi);
        
        //MFModels::P::Parcellation::Parameters PPrior; PPrior.V = V; PPrior.M = M;
        //MFModels::P_VBPosterior* P = new MFModels::P::Parcellation(tau, beta, pi, PPrior, dofCorrectionFactor);
        MFModels::P::PottsParcellation::Parameters PPrior; PPrior.beta = 100.0; PPrior.V = V; PPrior.M = M;
        for (int v = 0; v < V; ++v) {
            arma::uvec inds;
            if ((v - 3 >= 0) && (v + 3 < V)){
                inds << v - 1 << v - 2 << v - 3 << v + 1 << v + 2 << v + 3;
            }
            PPrior.couplings->push_back(inds);
        }
        MFModels::P_VBPosterior* P = new MFModels::P::PottsParcellation(tau, beta, pi, PPrior, dofCorrectionFactor);
        */
        
        // Old group-level models:
        // Spike-slab
        // Delta: mu = 0.0;
        // Gaussian: mu = 0.5 (typically); sigma = 1.0 (typically)
        
        // Binary mixture model
        // Delta: mu = {0.0, 1.0};
        
        // Triple-delta mixture model (TDMM)
        // Delta: mu = {-1.0, 0.0, 1.0};
        
        // Triple-Gaussian mixture model (TGMM)
        // Gaussian: mu = {-1.0, 0.0, 1.0}; all sigma = 1.0 (typically)
        
        //**********************************************************************
        // A
        
        // Precision matrix
        //Modules::PrecisionMatrices::P2C EaA; EaA.X = 1.0 * arma::eye<arma::fmat>(M,M); EaA.logDetX = std::log(1.0) * M;
        //Modules::PrecisionMatrix_VBPosterior* alphaA = new VBModules::Constant<Modules::PrecisionMatrices::C2P, Modules::PrecisionMatrices::P2C>(EaA);
        //VBModules::PrecisionMatrices::SharedGammaPrecision::Parameters alphaAPrior;
        //alphaAPrior.gammaPrior.a = 1.0; alphaAPrior.gammaPrior.b = 1.0; alphaAPrior.size = M;
        //Modules::PrecisionMatrix_VBPosterior* alphaA = new VBModules::PrecisionMatrices::SharedGammaPrecision(alphaAPrior);
        //VBModules::PrecisionMatrices::ComponentwiseGammaPrecision::Parameters alphaAPrior;
        //alphaAPrior.gammaPrior.a = 1.0; alphaAPrior.gammaPrior.b = 1.0; alphaAPrior.size = M;
        //Modules::PrecisionMatrix_VBPosterior* alphaA = new VBModules::PrecisionMatrices::ComponentwiseGammaPrecision(alphaAPrior);
        VBModules::PrecisionMatrices::Wishart::Parameters alphaAPrior; alphaAPrior.a = float(M); alphaAPrior.B = float(M) * arma::eye<arma::fmat>(M,M);
        Modules::PrecisionMatrix_VBPosterior* alphaA = new VBModules::PrecisionMatrices::Wishart(alphaAPrior);
        //VBModules::PrecisionMatrices::Wishart::Parameters betaAPrior; betaAPrior.a = float(M); betaAPrior.B = arma::eye<arma::fmat>(M,M);
        //Modules::PrecisionMatrix_VBPosterior* betaA = new VBModules::PrecisionMatrices::Wishart(betaAPrior);
        //VBModules::PrecisionMatrices::HierarchicalWishart::Parameters alphaAPrior; alphaAPrior.a = float(M); alphaAPrior.size = M;
        //Modules::PrecisionMatrix_VBPosterior* alphaA = new VBModules::PrecisionMatrices::HierarchicalWishart(betaA, alphaAPrior);
        
        /*
        // Constant
        MFModels::A::P2C EA;
        EA.A = arma::zeros<arma::fmat>(M,T); EA.A.rows(0, N-1) = Ad;
        EA.AAt = EA.A * EA.A.t();
        MFModels::A_VBPosterior* A = new VBModules::Constant<MFModels::A::C2P, MFModels::A::P2C>(EA);
        */
        
        
        // Multivariate normal
        MFModels::A::MultivariateNormal::Parameters APrior; APrior.M = M; APrior.T = T;
        MFModels::A_VBPosterior* A = new MFModels::A::MultivariateNormal(alphaA, APrior);
        
        
        /*
        // Clean HRF
        MFModels::A::KroneckerHRF::Parameters hrfPrior = Utilities::generateHRFPrior("Scripts/DefaultHRF.phrf", T, TR, M); //, 0.0025, 0.01);
        MFModels::A_VBPosterior* A = new MFModels::A::KroneckerHRF(alphaA, hrfPrior);
        */
        
        /*
        // Noisy HRF
        // Make HRF TCs
        MFModels::A::KroneckerHRF::Parameters hrfPrior = Utilities::generateHRFPrior("Scripts/DefaultHRF.phrf", T, TR, M, 0.0, 0.01);
        std::unique_ptr<MFModels::A_VBPosterior> cleanA = std::make_unique<MFModels::A::KroneckerHRF>(alphaA, hrfPrior);
        // Noise precision matrix
        VBModules::PrecisionMatrices::ComponentwiseGammaPrecision::Parameters alphaNoisePrior;
        alphaNoisePrior.gammaPrior.a = 0.01; alphaNoisePrior.gammaPrior.b = 0.01; alphaNoisePrior.size = M;
        std::unique_ptr<Modules::PrecisionMatrix_VBPosterior> alphaNoise = std::make_unique<VBModules::PrecisionMatrices::ComponentwiseGammaPrecision>(alphaNoisePrior);
        // Combined TCs
        MFModels::A::AdditiveMultivariateNormal::Parameters APrior; APrior.M = M; APrior.T = T;
        MFModels::A_VBPosterior* A = new MFModels::A::AdditiveMultivariateNormal(std::move(cleanA), std::move(alphaNoise), APrior);
        */
        
        //**********************************************************************
        // H
        
        // Means
        VBModules::MatrixMeans::IndependentGaussian::GaussianParameters muHPrior;
        muHPrior.gaussianPrior.mu = 1.0; muHPrior.gaussianPrior.sigma2 = 0.01; muHPrior.nRows = M; muHPrior.nCols = 1;
        VBModules::MatrixMeans::IndependentGaussian* muH = new VBModules::MatrixMeans::IndependentGaussian(muHPrior);
        
        // Precision matrix
        //Modules::PrecisionMatrices::P2C EaH; EaH.X = 100.0 * arma::eye<arma::fmat>(M,M); EaH.logDetX = std::log(100.0) * M;
        //Modules::PrecisionMatrix_VBPosterior* alphaH = new VBModules::Constant<Modules::PrecisionMatrices::C2P, Modules::PrecisionMatrices::P2C>(EaH);
        VBModules::PrecisionMatrices::Wishart::Parameters alphaHPrior; alphaHPrior.a = float(M); alphaHPrior.B = 0.01 * float(M) * arma::eye<arma::fmat>(M,M);
        Modules::PrecisionMatrix_VBPosterior* alphaH = new VBModules::PrecisionMatrices::Wishart(alphaHPrior);
        
        
        // Pseudo-rectified MVN
        MFModels::H::MultivariateNormal::Parameters HPrior;
        HPrior.M = M; HPrior.rectifyWeights = true;
        HPrior.clipWeights = true; HPrior.clipRange_max = 3.0;
        MFModels::H_VBPosterior* H = new MFModels::H::MultivariateNormal(muH, alphaH, HPrior);
        
        
        /*
        // Pseudo-rectified spike-slab
        MFModels::H::SpikeSlab::Parameters HPrior;
        HPrior.M = M; HPrior.mu = 2.0; HPrior.sigma2 = 1.0; HPrior.p = 0.95; HPrior.rectifyWeights = true;
        MFModels::H_VBPosterior* H = new MFModels::H::SpikeSlab(HPrior);
        */
        
        /*
        // Constant
        MFModels::H::P2C EH; EH.h = arma::ones<arma::fvec>(M); EH.hht = arma::ones<arma::fmat>(M, M);
        MFModels::H_VBPosterior* H = new VBModules::Constant<MFModels::H::C2P, MFModels::H::P2C>(EH);
        */
        
        //**********************************************************************
        // Psi
        
        
        MFModels::Psi::GammaPrecision::Parameters psiPrior; psiPrior.a = 1.0; psiPrior.b = 1.0;
        MFModels::Psi_VBPosterior* Psi = new MFModels::Psi::GammaPrecision(psiPrior);
        
        
        //**********************************************************************
        // MFM
        
        
        MFModel* MFM = new MFModels::FullRankMFModel(FRD, Pconst, H, A, Psi, dofCorrectionFactor);
        
        //MFModel* MFM = new MFModels::LowRankMFModel(LRD, Pconst, H, A, Psi, dofCorrectionFactor);
        
        
        //**********************************************************************
        // Convenience lambdas
        
        auto calculateFreeEnergy = [&] () -> float {
            float F = MFM->getLogLikelihood() - P->getKL() - H->getKL() - A->getKL() - Psi->getKL();
            /*F -= dofCorrectionFactor * tau->getKL();
            F -= dofCorrectionFactor * beta->getKL();
            F -= dofCorrectionFactor * kappa->getKL();
            F -= dofCorrectionFactor * pi->getKL();*/
            F -= alphaA->getKL();
            //F -= betaA->getKL();
            F -= muH->getKL();
            F -= alphaH->getKL();
            return F;
        };
        
        auto printFreeEnergy = [&] () -> void {
            std::cout << "Free energy: " << calculateFreeEnergy() << std::endl;
            std::cout << "Neg LL: " << -MFM->getLogLikelihood() << std::endl;
            std::cout << "KL P:   " << P->getKL() << std::endl;
            std::cout << "KL H:   " << H->getKL() << std::endl;
            std::cout << "KL A:   " << A->getKL() << std::endl;
            std::cout << "KL Ψ:   " << Psi->getKL() << std::endl;
            /*std::cout << "KL τ:   " << dofCorrectionFactor * tau->getKL() << std::endl;
            std::cout << "KL β:   " << dofCorrectionFactor * beta->getKL() << std::endl;
            std::cout << "KL κ:   " << dofCorrectionFactor * kappa->getKL() << std::endl;
            std::cout << "KL π:   " << dofCorrectionFactor * pi->getKL() << std::endl;*/
            std::cout << "KL α_A: " << alphaA->getKL() << std::endl;
            //std::cout << "KL β_A: " << betaA->getKL() << std::endl;
            std::cout << "KL μ_H: " << muH->getKL() << std::endl;
            std::cout << "KL α_H: " << alphaH->getKL() << std::endl;
            return;
        };
        
        // Need to add capture to calculate free energy
        auto update = [] (auto* model, const std::string name) -> void {
            //float F1,F2;
            //F1 = calculateFreeEnergy();
            
            Utilities::Timer timer;
            model->update();
            timer.printTimeElapsed(name, 3); timer.reset();
            
            //F2 = calculateFreeEnergy();
            //std::cout << "Diff F: " << F2 - F1 << std::endl;
            
            return;
        };
        
        auto printParameterSummaries = [&] () -> void {
            std::cout << "P: " << arma::mean(arma::diagvec(P->getExpectations().PtP)) / V << std::endl;
            std::cout << "H: " << arma::mean(H->getExpectations().h) << std::endl;
            std::cout << "A: " << arma::mean(arma::diagvec(A->getExpectations().AAt)) / T << std::endl;
            std::cout << "Ψ: " << 1.0 / std::sqrt(Psi->getExpectations().psi) << std::endl;
            return;
        };
        
        //**********************************************************************
        
        std::cout << std::string(80, '-') << std::endl;
        std::cout << "Initial updates..." << std::endl << std::endl;
        
        // Initial updates
        std::cout << "Initialising A/H/Ψ..." << std::endl;
        MFM->setTimeCourseNormalisation(true);
        for (unsigned int z = 0; z < 5; z++) {
            std::cout << "Iteration " << z+1 << std::endl;
            A->update();
            H->update();
            Psi->update();
        }
        MFM->setTimeCourseNormalisation(false);
        std::cout << std::endl;
        
        // Switch to true spatial model
        std::cout << "Initialising P/H/Ψ..." << std::endl;
        MFM->setPModel(P);
        for (unsigned int z = 0; z < 5; ++z) {
            std::cout << "Iteration " << z+1 << std::endl;
            P->update();
            H->update();
            Psi->update();
        }
        std::cout << std::endl;
        
        printParameterSummaries();
        std::cout << std::endl;
        
        //**********************************************************************
        
        std::cout << std::string(80, '-') << std::endl;
        std::cout << "Full inference..." << std::endl << std::endl;
        
        float F, oldF;
        F = calculateFreeEnergy(); oldF = F;
        printFreeEnergy();
        std::cout << std::endl;
        
        for (unsigned int z = 0; z < 100; z++) {
            std::cout << std::string(40, '.') << std::endl << std::endl;
            std::cout << "Iteration " << z+1 << std::endl << std::endl;
            
            // Updates!
            Utilities::Timer timer;
            
            update(A, "A");
            update(H, "H");
            update(P, "P");
            update(H, "H");
            update(Psi, "Ψ");
            
            /*update(tau, "τ");
            update(beta, "β");
            update(kappa, "κ");
            update(pi, "π");*/
            
            update(alphaA, "α_A");
            //update(betaA, "β_A");
            update(muH, "μ_H");
            update(alphaH, "α_H");
            
            timer.printTimeElapsed();
            std::cout << std::endl;
            
            // Parameter summaries
            printParameterSummaries();
            std::cout << std::endl;
            
            // Free energy summary
            printFreeEnergy();
            std::cout << std::endl;
            
            // Free energy change
            F = calculateFreeEnergy();
            float diffF = F - oldF;
            if (diffF >= -5.0 * std::abs(F) * std::numeric_limits<float>::epsilon()) {
                std::cout << "Diff F: " << F - oldF << std::endl;
            }
            else {
                std::cout << "*** Diff F: " << F - oldF << " ***" << "\a" << std::endl;
            }
            oldF = F;
            std::cout << std::endl;
            
            // Gives you a bit of time to read, and stops timing from parallel
            // KL divergence sections messing with update timing
            std::this_thread::sleep_for( std::chrono::milliseconds(500) );
        }
        std::cout << std::string(40, '.') << std::endl << std::endl;
        
        // Extract individual component amplitudes
        arma::fmat amplitudes = arma::zeros<arma::fmat>(M,5);
        amplitudes.col(0) = arma::abs( arma::mean(P->getExpectations().P, 0).t() );
        amplitudes.col(1) = arma::diagvec(P->getExpectations().PtP) / V;
        amplitudes.col(2) = H->getExpectations().h;
        amplitudes.col(3) = arma::abs( arma::mean(A->getExpectations().A, 1) );
        amplitudes.col(4) = arma::diagvec(A->getExpectations().AAt) / T;
        // Sort by the amplitudes of H
        arma::uvec inds = arma::sort_index( amplitudes.col(2), "descend" );
        amplitudes = amplitudes.rows(inds);
        // And print!
        std::cout << "Final component amplitudes [P,PtP,H,A,AAt]:" << std::endl << amplitudes << std::endl;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    std::cout << std::string(80, '*') << std::endl;
    return 0;
}

////////////////////////////////////////////////////////////////////////////////
