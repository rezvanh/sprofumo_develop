# Module which provides some useful plotting functions

# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

import numpy, numpy.fft
import os, os.path

import matplotlib as mpl
import matplotlib.pyplot as plt
import samplots as splt

import PROFUMO

################################################################################
# Function to plot spatial maps

def plotMaps(maps, title, vmax=None, vmin=None, xlabel="Mode", ylabel="Voxel", clabel="Map weights", directory=None):
    if directory is None:
        fileName = None
    
    # Plot maps
    if directory is not None:
        fileName = os.path.join(directory, "Maps.png")
    splt.plotMatrix(maps, title=title, vmax=vmax, vmin=vmin, xlabel=xlabel, ylabel=ylabel, clabel=clabel, fileName=fileName)
    
    # Plot map correlations
    C = PROFUMO.calculateMapCorrelations(maps)
    if directory is not None:
        fileName = os.path.join(directory, "Correlations.png")
    splt.plotCorrelationMatrix(C, title=title+": corrcoefs", label=xlabel, fileName=fileName)
    
    # Plot map "sizes"
    V, M = maps.shape
    fig, ax = plt.subplots()
    mapSTDs = numpy.std(maps, axis=0)
    plt.plot( mapSTDs, label="std" )
    #plt.hold(True)
    mapMeans = numpy.abs(numpy.mean(maps, axis=0))
    plt.plot( mapMeans, 'r', label="mean" )
    ax.set_xlabel(xlabel); ax.set_ylabel("Map standard deviations")
    ax.set_xlim(-1,M); ax.set_ylim(0.0, 1.1 * max(numpy.max(mapMeans), numpy.max(mapSTDs)))
    ax.legend()
    if directory is not None:
        plt.savefig(os.path.join(directory, "MapSizes.eps"))
    ax.set_title(title + ": standard deviations")

################################################################################
# Function to plot time courses

def plotTimeCourses(timeCourses, title, vmax=None, vmin=None, xlabel="Time point", ylabel="Mode", directory=None):
    if directory is None:
        fileName = None
    
    # Plot TCs
    if directory is not None:
        fileName = os.path.join(directory, "TimeCourses.png")
    splt.plotMatrix(timeCourses, title=title, vmax=vmax, vmin=vmin, xlabel=xlabel, ylabel=ylabel, fileName=fileName)
    
    # Plot correlations
    C = numpy.corrcoef(timeCourses)
    if directory is not None:
        fileName = os.path.join(directory, "Correlations.png")
    splt.plotCorrelationMatrix(C, title=title+": corrcoefs", label=ylabel, fileName=fileName)
    
    # Calculate frequency spectra
    fft = numpy.abs(numpy.fft.rfft(timeCourses, axis=1))
    
    # Plot
    if directory is not None:
        fileName = os.path.join(directory, "FFTs.png")
    splt.plotMatrix(fft, title=title+": FFT", vmax=numpy.amax(fft), vmin=0.0, ylabel=ylabel, xlabel="FFT coefficient", fileName=fileName)
    
    # Plot mean frequency spectra
    fig, ax = plt.subplots()
    plt.plot( numpy.mean(fft, axis=0) )
    ax.set_xlabel("Frequency"); ax.set_ylabel("FFT")
    #ax.set_xlim(0,T/2)
    if directory is not None:
        plt.savefig(os.path.join(directory, "MeanFFT.eps"))
    ax.set_title(title + ": Mean FFT")

################################################################################

def plotPrecisionMatrix(P, title, label="Mode", directory=None):
    if directory is None:
        fileName = None
    
    # Plot prec matrix
    if directory is not None:
        fileName = os.path.join(directory, "Precisions.png")
    splt.plotCovarianceMatrix(P, title=title, label=label, clabel="Precision", fileName=fileName)
    
    # Convert to partial correlations
    D = numpy.diag( numpy.sqrt( 1 / numpy.diag(P) ) )
    PC = - numpy.dot(D, numpy.dot(P, D))
    
    # Plot
    if directory is not None:
        fileName = os.path.join(directory, "PartialCorrelations.png")
    splt.plotCorrelationMatrix(PC, title=title+" (partial correlations)", label=label, fileName=fileName)

################################################################################

def plotNetMatElems(netMatElems, title, vmax=None, directory=None):
    if directory is None:
        fileName = None
    
    # Plot unwrapped netmats
    if directory is not None:
        fileName = os.path.join(directory, "UnwrappedNetMats.png")
    splt.plotCovarianceMatrix(netMatElems, title=title, vmax=vmax, xlabel="Netmat elements", ylabel="Subjects", clabel="Fisher's Z", fileName=fileName)
    
    # Plot correlations
    C = numpy.corrcoef(netMatElems)
    if directory is not None:
        fileName = os.path.join(directory, "Similarities.png")
    splt.plotCorrelationMatrix(C, title=title+": similarity", label="Subject", fileName=fileName)

################################################################################
# Plot how quickly intermediates change

def plotRateOfConvergence(intermediates, title):
    
    # Sort iterations and store some key values
    iterations = sorted( intermediates.keys() )
    start = iterations[0]
    end = iterations[-1]
    
    iters = []
    mapDiffs = []
    # Loop over all consecutive pairs of iterations
    for iter1,iter2 in zip(iterations[:-1], iterations[1:]):
        iters.append( float(iter1 + iter2) / 2.0 )
        
        maps1 = intermediates[iter1]
        maps2 = intermediates[iter2]
        
        # Map difference, normalised by number of iterations between intermediates
        mapDiffs.append( numpy.mean(numpy.abs( maps2 - maps1 )) / float( iter2 - iter1 ) )
    
    # Plot!
    fig, ax = plt.subplots()
    plt.plot(iters, mapDiffs, color='blue', linestyle='-', marker='o',
     markerfacecolor='red', markeredgecolor='white', markersize=5)
    ax.set_xlim(start-1, end+1);
    ax.set_xlabel("Iteration"); ax.set_ylabel("Map difference")
    ax.set_title(title + ": rate of change of intermediates")

################################################################################
# Plot convergence of two sets of intermediate maps to each other
# N.B. No pairing/reordering is performed

def plotConvergence(intermediates1, intermediates2, title):
    
    # Sort iterations and store some key values
    iterations = sorted( set(intermediates1.keys()) & set(intermediates2.keys()) )
    start = iterations[0]
    end = iterations[-1]
    V,M = intermediates1[start].shape
    
    # Set up a colormap (how the lines change colour as a function of iterations)
    # Goes from light to dark blue - i.e. (0.8, 0.8, 1.0) to (0.0, 0.0, 1.0)
    cdict = {'red':   [(0.0,  0.8, 0.8),
                       (1.0,  0.0, 0.0)],
    
             'green': [(0.0,  0.8, 0.8),
                       (1.0,  0.0, 0.0)],
    
             'blue':  [(0.0,  1.0, 1.0),
                       (1.0,  1.0, 1.0)]}
    
    cmap = mpl.colors.LinearSegmentedColormap('blueIter', cdict)
    
    # Make figure
    fig, ax = plt.subplots()
    #plt.hold(True)
    
    # Loop through intermediates, comparing them to the final intermediate
    for iteration in iterations:
        maps1 = intermediates1[iteration]
        maps2 = intermediates2[iteration]
        
        # Compute corrcoefs
        similarity = PROFUMO.calculatePairedMapCorrelations(maps1, maps2)
        
        # Find plot colour as a function of iteration
        if iteration == start:
            c = (1.0,0.7,0.7)   # Pale pink
        elif iteration == end:
            c = (1.0,0.0,0.0)   # Red
        else:
            # Retrieve from colour map
            c = cmap( float(iteration - start) / float(end - start) )
        
        # Plot!
        plt.plot(similarity, color=c)
        
    # Add a colour bar by faking a ScalarMappable
    # http://stackoverflow.com/a/11558629
    sm = plt.cm.ScalarMappable(cmap=cmap, norm=mpl.colors.Normalize(vmin=start, vmax=end))
    # fake up the array of the scalar mappable. Urgh...
    sm._A = []
    cbar = plt.colorbar(sm)
    
    # Tidy up plot
    ax.set_xlim(-1, M); ax.set_ylim(-0.025, 1.025)
    ax.set_xlabel("Mode"); ax.set_ylabel("Correlation coefficient")
    cbar.set_label("Correlation coefficient", rotation=270)
    ax.set_title(title + ": convergence")
    #plt.tight_layout()

################################################################################
# Plot a vector of correlations

def plotCorrelations(correlations, title, modes=None):
    # Make plot
    fig, ax = plt.subplots()
    #plt.hold(True)
    # Add horizontal guide lines
    yticks = numpy.linspace(0.0, 1.0, 6)
    for y in yticks:
        plt.plot([0, len(correlations)-1], [y, y], color=[0.9,]*3)
    
    # Plot correlations
    plt.plot(correlations, linestyle='-', color='blue', marker='o', markerfacecolor='blue', markeredgecolor='blue', markersize=4)
    # Highlight a set of modes, if required
    if modes is not None:
        plt.plot(modes, [correlations[m] for m in modes], linestyle='None', marker='o', markerfacecolor='red', markeredgecolor='white', markersize=5)
    
    # Tidy up plot
    ax.set_xlim(-1, len(correlations)); ax.set_ylim(-0.025, 1.025)
    ax.yaxis.set_ticks(yticks)
    ax.set_xlabel("Mode"); ax.set_ylabel("Correlation coefficient")
    ax.set_title(title)

################################################################################
