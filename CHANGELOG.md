## PROFUMO Changelog ##

----------

**Version 0.8.0** --- 2017-10-27

More model changes, with the spatial changes in particular having a fairly 
large impact on performance.

 + *Spatial modelling*: The DGMM now has a noise model where the variance is 
 no longer homogeneous over space, and where the SNR can change from map 
 to map. This makes the inferred subject maps cleaner, and makes the group 
 parameters more meaningful as there is no longer so much bleed-through of the 
 noise properties into the signal posterior.

 + *Fixed HRF updates*: The HRF modelling has been refactored, with the aim of 
 increasingly the numerical stability of the updates.

 + *Reliability*: Further tweaks to the initialisation (in terms of the 
 spatial basis decomposition) and the update procedure to improve the 
 reliability and cross-subject consistency of the decomposition. 

----------

**Version 0.7.0** --- 2017-08-04

A mostly behind-the-scenes release, though with enough tweaks to the various 
files required that it warrants a new version.

 + *Python improvements*: The core Python code (i.e. PROFUMO.py, 
 preprocessData.py and postprocessResults.py) have been reasonably extensively 
 reworked. Lots of sanitisation, for example moving from os.path to pathlib and 
 switching to parsing JSON rather than globbing to interpret the .pfm results 
 directories. The visualisation code has not received so much attention as that 
 is due an almost complete rewrite.

 + *Fixed group parameters*: These can now be passed in after masking, which 
 makes it much easier to pass in the results from a previous analysis (i.e. 
 as the raw HDF5 files from another .pfm directory).

----------

**Version 0.6.0** --- 2017-08-01

The major change associated with this release is the standardisation of the 
configuration files based on the JSON format, though there have been other 
behind-the-scenes changes.

 + *Switch to [JSON][]*: The key configuration files (data locations, C++ 
 configuration, run and subject orders) have been moved over to JSON. This 
 finally removes the dependence on PROFUMO-specific file formats.

 + *Improved initialisation*: The spatial basis decomposition to get the 
 initial maps is now run multiple times, with the results collated to get 
 what should be a more reliable inititalisation.

 + *Postprocessing*: It is now possible to save out subject maps where the 
 data normalisation has been undone, or even expressed as percent signal 
 change.

----------

**Version 0.5.0** --- 2017-03-06

For the most part, this version includes a series of refinements rather than 
any major new features, though these have nevertheless added up to some 
substantial changes to behaviour.

 + *Switch to single precision*: The code has all been moved to single 
 precision (i.e. float rather than double) calculations. This offers some 
 large speed-ups and a much reduced memory footprint.

 + *HRF modelling*: Arbitrary HRF shapes can now be specified at the command 
 line.

 + *Spatial modelling*: The noise level in the subject-specific spatial maps 
 has been reduced by the use of a more appropriate null model.

 + *Code structure*: All the C++ code has been cleaned up. This includes a 
 complete overhaul of the code to compute the random SVD and a change to the 
 way PROFUMO constructs subject-level models, which now allows the number of 
 timepoints to vary from run-to-run.

 + *Version system*: From now on, releases will follow the [semantic versioning 
 system][SemVer].

----------

**Version 0.4** --- 2016-10-16

This version adds the options to infer a parcellation, and now allows inference 
of subject information around a fixed set of group parameters.

 + *Parcellation*: It is now possible to infer parcels, rather than modes, 
 with PROFUMO. A new spatial cost function, which prohibits spatial overlap at 
 the subject level, has been implemented and is available via a simple command 
 line switch. The parcels can also be regularised by a spatial Markov random 
 field which encourages spatial coherence.

 + *Fixed group-level modes/parcels*: It is now possible to supply a set of 
 group-level modes or parcels, from which all the subject-specific information 
 will be inferred (c.f. dual regression).

 + *HRF modelling*: The HRF-based temporal model can now generate its own 
 prior based on the [FLOBS][] basis set.

 + *Documentation*: The LaTeX documents describing the key VB update rules 
 have been expanded considerably.

----------

**Version 0.3** --- 2016-08-15

This version makes the modelling of subject-specific information more flexible, 
improves the initialisation strategy, switches to C++11, and improves the user 
interface (somewhat).

 + *Subject Modelling*: Behind the scenes, the internal representation of 
 subjects has been revamped. This allows more flexible specifications of 
 subject models, and now means that it is possible to infer on models with 
 either run- or subject-specific temporal covariances.

 + *Initialisation*: A simple, group-level spatial decomposition is performed 
 before switching to inference on the full model. This vastly improves the 
 repeatability of results.

 + *C++11*: The code base for the inference of the probabilistic model has 
 been moved over to [C++11][C++].

 + *Command line interface*: The argument parsing for the C++ code is now 
 handled by [TCLAP][], giving a much more intuitive user interface.

----------

**Version 0.2** --- 2016-04-01

This version adds a weighted matrix factorisation model, improves the 
integration with [FSL][], switches to Python 3, and improves the way the model 
is initialised.

 + *Weighted Matrix Factorisation Model*: The data is now factored as 
   *D = PHA*, where H is a diagonal matrix that 'soaks up' the differences 
   in variance across components. This allows the scale of the maps and 
   time courses to match the scale implied by the prior without compromising 
   the ability of the model to explain the data, and it makes the results 
   easier to visualise and interpret.

 + *FSL Integration*: There is now a set of Python scripts that help 
   preprocess data that has been [FIX cleaned][FIX], and that postprocess 
   results---including running statistics with [Randomise][] if requested.

 + *Python 3*: The aforementioned Python scripts, as well as the visualisation 
   code, have been switched to [Python 3][].

 + *Initialisation*: Removed the restriction that the first model used had 
   to be a set of group-level spatial maps following a spike-slab 
   distribution. Now any set of spatial maps can be used!

----------

**Version 0.1** --- 2016-02-10

The code is now in a state where it can be distributed as a binary on the
FMRIB cluster (though with a lot of help needed to get an analysis set up).
While obviously inchoate, it seems like a reasonable time to mark this as
a preliminary version.

----------

### Minor changes ###

**Version 0.8.2** --- 2018-01-07 --- Fixed bug in weight model specification 
when using predefined spatial parameters, improved time course initialisation 
with associated simplification of temporal covariance modelling, and proper 
modelling of the group-level means of the weight parameters.

**Version 0.8.1** --- 2017-12-14 --- Tweaks to the models used during the early 
stages of the inference pipeline, with a view to improving the consistency of 
the amplitudes of modes across subjects.

**Version 0.7.1** --- 2017-08-31 --- Several behind-the-scenes changes, mostly 
centred around improving the initialisation and some further sanitisation of 
the various Python scripts.

**Version 0.5.3** --- 2017-06-13 --- Fixed bug relating to the definition of 
spatial neighbourhoods in the parcellation model.

**Version 0.5.2** --- 2017-03-22 --- Fixed bug relating to 'missing' voxels 
in the group maps.

**Version 0.5.1** --- 2017-03-10 --- Fixed bugs relating to variance 
normalisation of voxels with no activity.

**Version 0.2.1** --- 2016-04-08 --- Fixed a bug relating to data I/O across 
multiple threads.

----------

Written in [Markdown][] using [Dillinger][].

[myWebsite]: http://www.ndcn.ox.ac.uk/team/samuel-harrison  "Sam Harrison's FMRIB Profile"
[FMRIB]: http://www.ndcn.ox.ac.uk/divisions/fmrib/ "FMRIB Website"
[FSL]: http://fsl.fmrib.ox.ac.uk "FSL Wiki"
[FIX]: http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FIX "FIX (FSL Wiki)"
[Randomise]: http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Randomise "Randomise (FSL Wiki)"
[Python 3]: https://docs.python.org/3/whatsnew/3.0.html "What’s New In Python 3.0"
[C++]: https://isocpp.org "C++ standards"
[TCLAP]: http://tclap.sourceforge.net "TCLAP (Templatized C++ Command Line Parser Library)"
[FLOBS]: http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FLOBS "FLOBS (FSL Wiki)"
[SemVer]: http://semver.org "Semantic versioning"
[JSON]: http://json.org

[Markdown]: http://daringfireball.net/projects/markdown/
[Dillinger]: http://dillinger.io
