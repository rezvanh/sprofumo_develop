#
# 'make'
# 'make all'
# 'make PROFUMO'
# 'make Test'
# 'make install'
# 'make uninstall'
# 'make clean'
#

#################################################################################
# Based on:
# http://www.cs.swarthmore.edu/~newhall/unixhelp/howto_makefiles.html
# http://stackoverflow.com/questions/5273374/make-file-with-source-subdirectories

# Convention used here:
# ${HOME} for imported environment variables
# $(OBJS) for variables defined in this file
#################################################################################



#################################################################################
# Options

# Define the compiler to use
CXX = g++

# Define any compile-time flags
CXXFLAGS = -std=c++14 -O3 -Wall -Wextra -w

# Define any directories containing header files other than /usr/include
CXXINCLUDES = -I. -ISource -IDependencies -I${FSLDIR}/src
CXXINCLUDES += -IDependencies/json/single_include
CXXINCLUDES += -IDependencies/spdlog/include
CXXINCLUDES += -IDependencies/tclap/include

# Define library paths in addition to /usr/lib
LDFLAGS = -LDependencies/NewNifti -L${FSLDIR}/lib

# Define any libraries to link into executable
LDLIBS = -larmadillo -lhdf5 -lNewNifti -lm -lznz -lz -lstdc++fs

# Machine specific changes to these
#ifeq (${FSLMACHTYPE},linux_64-gcc4.4)
CXXFLAGS += -fopenmp
# -ffast-math

CXXINCLUDES += -I${HOME}/include

LDFLAGS += -L/usr/lib64 -L${HOME}/lib

LDLIBS += -lopenblas
#else
#    CXXFLAGS += -Wno-unknown-pragmas
#    
#    CXXINCLUDES += -I/opt/local/include
#    
#    LDFLAGS += -L/opt/local/lib
#endif

# Define the C++ source files
SRCS := $(shell find Source -name '*.c++')
OBJS := $(SRCS:.c++=.o)

# Get a timestamp
TIMESTAMP := $(shell date +'%Y.%m.%d-%H:%M:%S')

#################################################################################
# Build rules

.PHONY: all install uninstall clean FORCE

all: PROFUMO Test

# Rules to link all the object files together to make the executables
PROFUMO: $(OBJS) PROFUMO.o
	$(CXX) $(CXXFLAGS) $(OBJS) PROFUMO.o -o PROFUMO $(LDFLAGS) $(LDLIBS)
    
Test: $(OBJS) Test.o
	$(CXX) $(CXXFLAGS) $(OBJS) Test.o -o Test $(LDFLAGS) $(LDLIBS)

# Rule to make object files from source
# $<: the name of the prerequisite of the rule(a .c++ file)
# $@: the name of the target of the rule (a .o file)
%.o: %.c++
	$(CXX) $(CXXFLAGS) $(CXXINCLUDES) -c $< -o $@

install: PROFUMO
	cp PROFUMO ${HOME}/bin/sPROFUMO_predefined_$(TIMESTAMP)
	ln -sf sPROFUMO_predefined_$(TIMESTAMP) ${HOME}/bin/sPROFUMO_predefined

uninstall:
	rm -f ${HOME}/bin/sPROFUMO_predefined*

clean:
	rm -f $(OBJS) PROFUMO PROFUMO.o Test Test.o

# Forcing rebuild of BuildTime.c++ means the build timestamp is correct
Source/Utilities/BuildTime.o: FORCE


