#!/usr/bin/env bash

set -e

for dep in znzlib NewNifti spline; do
  pushd Dependencies/${dep}
  make PREFIX=$PREFIX
  make install
  popd
done

make
make install
