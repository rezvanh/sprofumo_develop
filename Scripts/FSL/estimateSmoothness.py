#!/usr/bin/env python
# -*- coding: utf-8 -*-

################################################################################

import argparse

parser = argparse.ArgumentParser(description="Estimate degree of freedom correction for a NIFTI file.")
parser.add_argument("data", help="Input data.")
parser.add_argument("mask", help="Mask to apply to data.")
#parser.add_argument("-n", type=int, dest="N", default=None, help="Number of components to remove. [Default: prompt appears later]")

args = parser.parse_args()

################################################################################
print("Importing libraries...")

import math
import numpy, numpy.linalg
import scipy.stats

import nibabel

import subprocess

import matplotlib as mpl
import matplotlib.pyplot as plt

import sys, os
sys.path.append(os.path.expanduser( "~samh/PROFUMO/Visualisation" ))
import samplots as splt
import PROFUMO
import PFMplots

print()
################################################################################
print("Loading data...")

print("Data: " + args.data)
data = nibabel.load(args.data)
print("Mask: " + args.mask)
mask = nibabel.load(args.mask)
print()

# Unmask and normalise
maskedData = data.get_data()[mask.get_data() != 0.0]
maskedData = scipy.stats.mstats.zscore(maskedData, axis=1)
maskedData[numpy.isnan(maskedData)] = numpy.random.randn(numpy.sum(numpy.isnan(maskedData)))

nVoxels, nTimePoints = maskedData.shape
print("Voxels: {:d}".format(nVoxels))
print("Time points: {:d}".format(nTimePoints))

PFMplots.plotMaps(maskedData, "Masked data", xlabel="Time points", clabel=None)
plt.show()

print()
################################################################################
print("Calculating SVD...")

U,s,V = numpy.linalg.svd(maskedData, full_matrices=False)

plt.figure()
plt.plot(s)
plt.xlabel("Component"); plt.ylabel("Singular value")
plt.show()

print()
################################################################################
print("Estimating residuals...")

# Get how many components to remove
N = int(input("Number of signal components to remove: "))
print("N = {:d}".format(N))

# Form the residuals
maskedResiduals = U[:,N:] @ numpy.diag(s[N:]) @ V[:,N:].T

PFMplots.plotMaps(maskedResiduals, "Masked residuals", xlabel="Time points", clabel=None)
plt.show()

print()
################################################################################
print("Saving residuals...")

# Unmask
residuals = numpy.zeros( data.get_data().shape )
residuals[mask.get_data() != 0.0] = maskedResiduals

# Write to file
imageFormat = type(data) # Works for NIFTI1/2, PROBS NOT FOR ANYTHING ELSE...
residuals = imageFormat(residuals, affine=data.affine, header=data.header)
residualFile = os.path.expanduser("/tmp/Residuals.nii.gz")
nibabel.save(residuals, residualFile)

print()
################################################################################
print("Estimating temporal DoF...")

rankData = numpy.linalg.matrix_rank(maskedData)
rankResiduals = numpy.linalg.matrix_rank(maskedResiduals)

# Temporal DoF

# Method 1: just the rank remaining after FIX / signal removal
#tDoF = numpy.linalg.matrix_rank(maskedResiduals)

# Method 2: Depends on temporal covariance matrix
# Groves et al., "Linked independent component analysis for multimodal data fusion", NeuroImage, 2011 (Appendix A)
# DOI: 10.1016/j.neuroimage.2010.09.073
#KKt = maskedResiduals.T @ maskedResiduals
#tDoF = numpy.trace(KKt)**2 / numpy.trace(KKt @ KKt)

# Method 3:
# Woolrich et al., "Temporal Autocorrelation in Univariate Linear Modeling of FMRI Data", NeuroImage, 2001
# DOI: 10.1006/nimg.2001.0931
# Equation 3 with S = I; X = ones(T,1); c = 1
# i.e. keff = sum(V)

# Method 4: Fit AR model and use the equation from method 2
output = subprocess.run(["fslmaths", residualFile, "-Tar1", "-mas", args.mask, "/tmp/AR.nii.gz"], check=True, stdout=subprocess.PIPE)
output = subprocess.run(["fslstats", "/tmp/AR.nii.gz", "-a", "-M"], check=True, stdout=subprocess.PIPE)
alpha = float(output.stdout)

# Covariance matrix:
# en.wikipedia.org/wiki/Autoregressive_model
# C(i,j) = sigma**2 / (1 - alpha**2) * alpha**(abs(i-j))
# Normalise to correlation for simplicity:
# C(i,j) = alpha**(abs(i-j))

# Adrian's paper
# dof = Tr[KKt]**2 / Tr[KKtKKt]
# In our case, C = K @ Kt
# dof = T**2 / sum(C * C)
T = nTimePoints
denom = T
for t in range(1,T):
    denom += 2 * (T - t) * alpha**(2*t)
tDoF = T ** 2 / denom


tDoFCorrection = tDoF / nTimePoints

print()
print("Time points: {:d}".format(nTimePoints))
print("Rank raw data: {:d}".format(rankData))
print("Rank residuals: {:d}".format(rankResiduals))
print("Temporal degrees of freedom: {:.2f}".format(tDoF))
print("tDoF correction: {:.4f}".format(tDoFCorrection))

print()
################################################################################
print("Estimating spatial DoF...")

# Spatial DoF
smoothest = subprocess.run(["smoothest", "-d", "{:.2f}".format(tDoF), "-m", args.mask, "-r", residualFile], check=True, stdout=subprocess.PIPE)

# http://blogs.warwick.ac.uk/nichols/entry/fwhm_resel_details/
# The RESEL output from smoothest is the size of one RESEL in voxel units
for line in smoothest.stdout.splitlines():
    if "RESELS" in str(line):
        reselSize = float(line.split()[-1])

# From Adrian's paper (K = number of spatial dimensions):
# dofCorrection = (nRESELS / nVoxels) * (4 * log(2) / pi) ** (K/2)
#               = (nRESELS / nVoxels) * 0.83    (for 3D data)
#               = 0.83 / smoothest_RESELS

nResels = nVoxels / reselSize
sDoFCorrection = 0.83 / reselSize

print()
print("Voxels: {:d}".format(nVoxels))
print("Resels: {:.2f}".format(nResels))
print("sDoF correction: {:.4f}".format(sDoFCorrection))
print()

# Recommendation for PROFUMO
# Fudge! Don't be so harsh if there is temporal smoothness too
sDoFParam = sDoFCorrection / tDoFCorrection
print("sDoF corr / tDoF corr: {:.4f}".format(sDoFParam))
print()

# And round up to a nice number
def nextHighest(seq,x):
    return min([(i-x,i) for i in seq if x<=i] or [(0,None)])[1]
def roundPower(x):
    a = math.log10(x); b = a % 1; a = a - b
    b = nextHighest(numpy.log10([1.0,2.5,5.0,7.5,10.0]), b)
    return(10.0 ** (a+b))

sDoFParam = "{:.5f}".format(roundPower(sDoFParam)).rstrip("0")
print("*"*80)
print("Recommended --dofCorrection parameter: {}".format(sDoFParam))
print("*"*80)

################################################################################

raise SystemExit()






################################################################################
# HCP data


# wb_command -cifti-estimate-fwhm /vols/Data/HCP/Phase2/subjects900/922854/MNINonLinear/Results/rfMRI_REST2_LR/rfMRI_REST2_LR_Atlas_MSMAll_hp2000_clean.dtseries.nii -merged-volume -surface CORTEX_LEFT /vols/Data/HCP/workbench/GroupAverage32k/ParcellationPilot.L.midthickness.32k_fs_LR.surf.gii -surface CORTEX_RIGHT /vols/Data/HCP/workbench/GroupAverage32k/ParcellationPilot.R.midthickness.32k_fs_LR.surf.gii


import PROFUMO, PFMplots, numpy
import nibabel

D = nibabel.load("/vols/Data/HCP/Phase2/subjects900/922854/MNINonLinear/Results/rfMRI_REST2_LR/rfMRI_REST2_LR_Atlas_MSMAll_hp2000_clean.dtseries.nii")

D = numpy.squeeze(D.get_data()).T
D = (D.T - numpy.mean(D, axis=1)).T
D = (D.T / numpy.std(D, axis=1)).T

PFMplots.plotMaps(D, "Data")


U,s,V = numpy.linalg.svd(D, full_matrices=False)

residuals = D - U[:,:100] @ numpy.diag(s[:100]) @ V[:100,:]
PFMplots.plotMaps(residuals, "Residuals")

numpy.savetxt("/vols/Scratch/samh/PROFUMO.txt", residuals)

# module load MATLAB/2014b && matlab -nojvm -nodisplay -r txt2cifti

# wb_command -cifti-estimate-fwhm ~/scratch/PROFUMO.dtseries.nii -merged-volume -surface CORTEX_LEFT /vols/Data/HCP/workbench/GroupAverage32k/ParcellationPilot.L.midthickness.32k_fs_LR.surf.gii -surface CORTEX_RIGHT /vols/Data/HCP/workbench/GroupAverage32k/ParcellationPilot.R.midthickness.32k_fs_LR.surf.gii

dofCorrection = (0.93 / FWHM) ** nDim

# Surface: 0.93**2 / 2.5**2 ~ 0.15
# Subcortical: 0.93**3 / (2.3 * 2.0 * 2.4) ~ 0.075


