#!/usr/bin/env python
# -*- coding: utf-8 -*-

# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

# Given a set of FIX directories, this script:
#   Registers the masks and functionals to standard space
#   Smooths the functionals
#   Makes a group mask
#   Makes the spec file for PROFUMO

import argparse
import os, shutil
import subprocess
import json

# Import PROFUMO helper functions
import sys
sys.path.append(os.path.expanduser( "~/PROFUMO/Visualisation" ))
import PROFUMO

# Turn off stack trace on error
#sys.tracebacklimit=0

#-------------------------------------------------------------------------------

def dumpJSON(jsonObject, fileName, sort_keys=False):
    with open(fileName, "w") as jsonFile:
        json.dump(jsonObject, jsonFile, sort_keys=sort_keys, indent=4)

#-------------------------------------------------------------------------------

################################################################################
# Process arguments

parser = argparse.ArgumentParser(description="Processes FIXed data for PROFUMO.")


# Spec file
parser.add_argument("FIXLocations", type=PROFUMO.validFile, metavar="FIXLocations.json", help="Location of the JSON file containing the subject IDs and the locations of the FIX directories.")

# Output directory
parser.add_argument("outputDir", type=PROFUMO.validBaseDirectory, metavar="outputDirectory", help="Where to save the processed data.")

# Amount of smoothing
# Function that checks smoothing > 0
def validSmoothing(width):
    return PROFUMO.checkArgument(width, float, lambda x: x > 0.0, "Smoothing width")

parser.add_argument("-s", "--smoothing", type=validSmoothing, metavar="<s>", help="Width of the smoothing kernel (a Gaussian kernel with sigma = <s> mm is used).")

# Threshold for group mask
def validThreshold(threshold):
    return PROFUMO.checkArgument(threshold, float, lambda x: x >= 0.0 and x <= 1.0, "Threshold")

parser.add_argument("-t", "--threshold", type=validThreshold, default=0.25, metavar="<t>", help="Threshold for group mask. Voxels present in a higher proportion of subjects than <t> will be included. [Default: 0.25]")

# Functionals to use
parser.add_argument("-f", "--functional", default="filtered_func_data_clean.nii.gz", metavar="<func.nii.gz>", help="Name of the functional to extract from the FIX directory. [Default: filtered_func_data_clean.nii.gz]")


# And parse!
args = parser.parse_args()

################################################################################
print("*" * 80)
print("Preprocessing data for a PROFUMO analysis.")
# Date
# Input spec file
print()

# Print the parsed args
print("FIX locations file: \"{f}\"".format(f=args.FIXLocations))
print("Mask threshold: {t}".format(t=args.threshold))
if args.smoothing is None:
    print("No spatial smoothing to be applied.")
else:
    print("Spatial smoothing: {s} mm".format(s=args.smoothing))
print()


print("Making output directory...")

# Add .pfm to output directory name
if not args.outputDir.suffix == ".prof":
    args.outputDir = PROFUMO.addExtension(args.outputDir, ".prof")

# Add + to output directory name if it already exists
#while os.path.exists(args.outputDir):
#    args.outputDir = args.outputDir + "+"

# If the directory exists, can it
if os.path.exists(args.outputDir):
    raise FileExistsError("Cannot create output directory, it already exists!\n  " + str(args.outputDir))

# Make the directory
os.makedirs(args.outputDir)

print("Done. Results will be saved in:")
print("{f}".format(f=args.outputDir))
print()


print("Making subdirectories...")

# Other useful directories
logDir = os.path.join(args.outputDir, "Logs")
os.makedirs(logDir)
preprocessingLogDir = os.path.join(logDir, "Preprocessing")
os.makedirs(preprocessingLogDir)
queueLogDir = os.path.join(preprocessingLogDir, "Queues")
os.makedirs(queueLogDir)
analysisLogDir = os.path.join(logDir, "Analysis")
os.makedirs(analysisLogDir)

dataDir = os.path.join(args.outputDir, "Data")
os.makedirs(dataDir)
maskDir = os.path.join(dataDir, "Masks")
os.makedirs(maskDir)
funcDir = os.path.join(dataDir, "Functionals")
os.makedirs(funcDir)

print("Done.")
print()

# WOULD BE GREAT TO SAVE A LOAD OF INFO TO A LOG FILE
# DATE RUN, PARAMETERS, WHAT THE SCRIPT DID ETC
# i.e. SO IT'S NOT UNINTELLIGIBLE ONE WEEK LATER ;-p
# BUT CAN'T DO THAT 'TIL THE LOG DIRECTORIES ARE CREATED

################################################################################
print("*" * 80)
print("Processing the input spec file...")
print()

print("Taking a copy of the input spec file...")

# Take a copy first
shutil.copy(args.FIXLocations, os.path.join(preprocessingLogDir, "FIXLocations.json"))

print("Done.") # Say where!
print()


print("Parsing the spec file...")

with open(args.FIXLocations, "r") as FIXLocations:
    subjectData = json.load(FIXLocations)

#print(subjectData)

print("Done.")
print()

################################################################################
print("*" * 80)
print("Saving subject & run ordering...")
print()

# Subject order file
subjectOrderFileName = os.path.join(preprocessingLogDir, "SubjectOrder.json")
dumpJSON(sorted(list(subjectData.keys())), subjectOrderFileName)

# Run order file
runOrderFileName = os.path.join(preprocessingLogDir, "RunOrder.json")
runOrder = [{"Subject": subjectID, "Run": runID} for subjectID, runs in sorted(subjectData.items()) for runID in sorted(runs.keys())]
dumpJSON(runOrder, runOrderFileName)

print("Done.")
print()

################################################################################
# Farm the processing out to the queues  - make files containing commands
print("*" * 80)
print("Generating preprocessing scripts...")
print()

# Make text files in log directory with all the commands we need to run
funcRegFileName = os.path.join(preprocessingLogDir, "functionalRegistrations.sh")
if args.smoothing is not None:
    funcSmoothFileName = os.path.join(preprocessingLogDir, "functionalSmoothing.sh")
maskRegFileName = os.path.join(preprocessingLogDir, "maskRegistrations.sh")

# Save names of functionals
functionalLocations = {}

with open(funcRegFileName, "w") as funcRegFile, open(maskRegFileName, "w") as maskRegFile:
    for subjID, fixDirs in subjectData.items():
        funcLocs = {}
        for runID, fixDir in fixDirs.items():
            # Make filenames using subject & run IDs
            runPrefix = "{s}_{r}".format(s=subjID, r=runID)
        
            # Get the warp
            warp = os.path.join(fixDir, "reg", "example_func2standard_warp.nii.gz")
            #warp = os.path.join(fixDir, "..", "{s}.feat".format(s=subjID), "reg", "example_func2standard_warp.nii.gz")
            ref = os.path.join(fixDir, "reg", "standard.nii.gz")
            #ref = os.path.join(fixDir, "..", "{s}.feat".format(s=subjID), "reg", "standard.nii.gz")
            #if subjID == "s29":
            #    warp = "/home/fs0/asdahl/scratch/PINS/s29/s29.feat/reg/example_func2standard_warp.nii.gz"
            #    reg = "/home/fs0/asdahl/scratch/PINS/s29/s29.feat/reg/standard.nii.gz"
            
            # Register functional to standard space
            inFunc = os.path.join(fixDir, args.functional)
            #inFunc = os.path.join(fixDir, "filtered_func_data_clean.nii.gz")
            outFunc = os.path.join(funcDir, runPrefix + "_func.nii.gz")
            
            funcRegFile.write("applywarp --in={inFunc} --out={outFunc} --ref={ref} --warp={warp}\n".format(inFunc=inFunc, outFunc=outFunc, ref=ref, warp=warp))
            
            # Smooth functional in standard space
            if args.smoothing is not None:
                inFunc = outFunc
                outFunc = os.path.join(funcDir, runPrefix + "_smoothFunc.nii.gz")
                
                with open(funcSmoothFileName, "a") as funcSmoothFile:
                    funcSmoothFile.write("fslmaths {inFunc} -s {s} {outFunc}\n".format(inFunc=inFunc, outFunc=outFunc, s=args.smoothing))
            
            # Record functional
            funcLocs[runID] = outFunc
            
            # Register mask to standard space
            inMask = os.path.join(fixDir, "mask.nii.gz")
            outMask = os.path.join(maskDir, runPrefix + "_mask.nii.gz")
            
            maskRegFile.write("applywarp --in={inMask} --out={outMask} --ref={ref} --warp={warp}\n".format(inMask=inMask, outMask=outMask, ref=ref, warp=warp))
            
        # Save all run locations
        functionalLocations[subjID] = funcLocs

print("Done.")
print()

################################################################################  
# And submit!
print("*" * 80)
print("Submitting preprocessing jobs...")
print()

# If relative path, prepend ./ so scripts are callable
def makeCallable(fileName):
    if os.path.isabs(fileName):
        return fileName
    else:
        return os.path.join(".", fileName)

# Register functionals
funcRegJob = subprocess.run(["fsl_sub", "-q", "veryshort.q", "-l", queueLogDir, "-t", makeCallable(funcRegFileName)], check=True, stdout=subprocess.PIPE)
funcRegJobID = funcRegJob.stdout.strip()
print("Functional registrations submitted to the queue (job {j}).".format(j=funcRegJobID))

# Once done, smooth functionals
if args.smoothing is not None:
    funcSmoothJob = subprocess.run(["fsl_sub", "-q", "veryshort.q", "-j", funcRegJobID, "-l", queueLogDir, "-t", makeCallable(funcSmoothFileName)], check=True, stdout=subprocess.PIPE)
    funcSmoothJobID = funcSmoothJob.stdout.strip()
    print("Functional smoothing submitted to the queue (job {j}).".format(j=funcSmoothJobID))

# Register masks
maskRegJob = subprocess.run(["fsl_sub", "-q", "veryshort.q", "-l", queueLogDir, "-t", makeCallable(maskRegFileName)], check=True, stdout=subprocess.PIPE)
maskRegJobID = maskRegJob.stdout.strip()
print("Mask registrations submitted to the queue (job {j}).".format(j=maskRegJobID))

# Once done, make the group mask
groupMaskFileName = os.path.join(os.path.dirname(os.path.realpath(__file__)), "makeGroupMask.sh")

groupMaskJob = subprocess.run(["fsl_sub", "-q", "veryshort.q", "-j", maskRegJobID, "-l", queueLogDir, makeCallable(groupMaskFileName), maskDir, "{:.3f}".format(args.threshold)], check=True, stdout=subprocess.PIPE)
groupMaskJobID = groupMaskJob.stdout.strip()
print("Group mask generation submitted to the queue (job {j}).".format(j=groupMaskJobID))

print("Done.")
print()

################################################################################
print("*" * 80)
print("Generating PROFUMO spec file...")
print()

# Spec file for analysis - where the functionals are
profumoDataLocations = os.path.join(analysisLogDir, "DataLocations.json")
dumpJSON(functionalLocations, profumoDataLocations)

print("Done.")
print()

################################################################################
# Bye!
print("*" * 80)
raise SystemExit()
