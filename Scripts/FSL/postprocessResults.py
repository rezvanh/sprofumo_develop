#!/usr/bin/env python
# -*- coding: utf-8 -*-

# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

# Given a set of results from PROFUMO:
#   Saves out the group maps
#   Saves out the group spatial standard deviations
#   Saves out the group spatial normalisation parameters
#   Saves out the spatial basis
#   Saves out collections of subject maps
#   Saves out the subject/run weights
#   Saves out the subject/run time courses
#   Saves out the subject/run netmats

################################################################################
print("*" * 80)
print("Loading libraries...")
print()

import argparse
import shutil
import pathlib
import subprocess
import json
import warnings

# Data manipulation
import nibabel
import numpy
import math

# Import PROFUMO helper functions
import os, sys
sys.path.append(os.path.expanduser( "~samh/PROFUMO/Visualisation" ))
import PROFUMO

from PROFUMO import addExtension as addExt

print("Done.")
print()

################################################################################
print("*" * 80)
print("Parsing arguments...")
print()

parser = argparse.ArgumentParser(description="Saves results from PROFUMO run for visualisation and inference.")

# Input .prof directory
parser.add_argument("profDir", type=PROFUMO.validDirectory, metavar="<profDirectory>", help="The .prof results directory to use.")


### Optional inputs ###

# Output directory
parser.add_argument("--outputDir", type=PROFUMO.validBaseDirectory, metavar="<outputDirectory>", help="Where to save the processed data.")

# Mask
parser.add_argument("--mask", dest="maskFileName", type=PROFUMO.validFile, metavar="<maskFile>", help="File used to mask results.")

# Subject order
parser.add_argument("--subjectOrder", dest="subjectOrderFileName", type=PROFUMO.validFile, metavar="<subjectOrder.json>", help="Location of a JSON file containing an ordered list of subject IDs (or a subset thereof).")

# Run order
parser.add_argument("--runOrder", dest="runOrderFileName", type=PROFUMO.validFile, metavar="<runOrder.json>", help="Location of a JSON file containing an ordered list of subject ID and run ID pairs (or a subset thereof).")

# Mode order
parser.add_argument("--modeOrder", dest="modeOrderFileName", type=PROFUMO.validFile, metavar="<modeOrder.txt>", help="Location of a text file containing an ordered list of modes (or a subset thereof).")

# Whether to save subject maps to file
parser.add_argument("--saveSubjectMaps", action="store_true", default=False)

# Whether to save subject-specific maps with different normalisations
parser.add_argument("--saveUnnormalisedMaps", action="store_true", default=False)
parser.add_argument("--savePercentSignalChangeMaps", action="store_true", default=False)

# Whether to generate nice pictures
parser.add_argument("--saveSummaryImages", dest="backgroundImageFileName", type=PROFUMO.validFile, metavar="<backgroundFile>", help="Whether or not to save summary images of the group maps. A background image needs to be supplied.")

# Where to store logss
parser.add_argument("--logDir", type=PROFUMO.validBaseDirectory, metavar="<logDirectory>", help="Directory to store logs in.")

# Design for statistics
# Define what we need to be passed
def validDesign(designBaseName):
    return PROFUMO.checkArgument(designBaseName, lambda x: pathlib.Path(x).expanduser().resolve(), lambda x: addExt(x,".mat").is_file() and addExt(x,".con").is_file(), "Design (.mat and/or .con)")
# And parse
parser.add_argument("-rd", "--runDesign", dest="runDesignBaseName", type=validDesign, metavar="<runDesignBaseName>", help="Base name for the run design files (i.e. full design path without .mat/.con extensions).")
parser.add_argument("-sd", "--subjectDesign", dest="subjectDesignBaseName", type=validDesign, metavar="<subjectDesignBaseName>", help="Base name for the subject design files (i.e. full design path without .mat/.con extensions).")

# Analysis directory (i.e. the output from the C++ code)
parser.add_argument("--pfmDir", type=PROFUMO.validDirectory, metavar="<pfmDirectory>", help="The .pfm directory output by the PROFUMO algorithm.")


# And parse!
args = parser.parse_args()


# Add + to output directory name if it already exists
outputExists = args.outputDir.exists()
while args.outputDir.exists():
    args.outputDir = addExt(args.outputDir, "+")
if outputExists:
    warnings.warn("Output directory already exists! Results will now be saved to {d}".format(d=args.outputDir))


# Now we need to set the defaults!
if args.pfmDir is None:
    args.pfmDir = args.profDir / "Analysis.pfm"
    if not args.pfmDir.is_dir():
        raise argparse.ArgumentTypeError("Could not find the analysis directory: {fileName}".format(fileName=args.pfmDir))
    
if args.outputDir is None:
    args.outputDir = args.profDir / "PostprocessedResults"
    
if args.maskFileName is None:
    args.maskFileName = args.pfmDir / "Preprocessing" / "Mask.nii.gz"
    if not args.maskFileName.is_file():
        raise argparse.ArgumentTypeError("Could not find the mask file: {fileName}".format(fileName=args.maskFileName))
    
if args.subjectOrderFileName is None:
    args.subjectOrderFileName = args.profDir / "Logs" / "Preprocessing" / "SubjectOrder.txt"
    if not args.subjectOrderFileName.is_file():
        raise argparse.ArgumentTypeError("Could not find the subject order: {fileName}".format(fileName=args.subjectOrderFileName))
    
if args.runOrderFileName is None:
    args.runOrderFileName = args.profDir / "Logs" / "Preprocessing" / "RunOrder.txt"
    if not args.runOrderFileName.is_file():
        raise argparse.ArgumentTypeError("Could not find the run order: {fileName}".format(fileName=args.runOrderFileName))
    
if args.logDir is None:
    args.logDir = args.outputDir / "Logs"
    #args.logDir = args.profDir / "Logs" / "Postprocessing"
    #if not (args.profDir / "Logs").is_dir():
    #    raise argparse.ArgumentTypeError("Could not find the log directory: {fileName}".format(fileName=(args.profDir / "Logs")))


print("Done.")
print()

################################################################################
print("*" * 80)
print("Initialising directory structure...")
print()

print("Making output directories...")

# Make the directory
args.outputDir.mkdir()

# Other useful directories
mapDir = args.outputDir / "Maps"; mapDir.mkdir()
timeCourseDir = args.outputDir / "TimeCourses"; timeCourseDir.mkdir()
netMatsDir = args.outputDir / "NetMats"; netMatsDir.mkdir()
weightsDir = args.outputDir / "Weights"; weightsDir.mkdir()
# If log directory doesn't exist, make it!
if not args.logDir.is_dir():
    args.logDir.mkdir()
queueLogDir = args.logDir / "Queues"; queueLogDir.mkdir()
print("Done.") # Say where!
print()

# Take a copy of the subject / mode / design files
# Subjects
print("Taking a copy of the subject list...")
shutil.copy(args.subjectOrderFileName, args.logDir / "SubjectOrder.txt")
print("Done.") # Say where!
print()

# Runs
print("Taking a copy of the run list...")
shutil.copy(args.runOrderFileName, args.logDir / "RunOrder.txt")
print("Done.") # Say where!
print()

# Designs
if args.runDesignBaseName is not None:
    print("Taking a copy of the run design files...")
    shutil.copy(addExt(args.runDesignBaseName, ".mat"), args.logDir / "RunDesign.mat")
    shutil.copy(addExt(args.runDesignBaseName, ".con"), args.logDir / "RunDesign.con")
    print("Done.") # Say where!
    print()
if args.subjectDesignBaseName is not None:
    print("Taking a copy of the subject design files...")
    shutil.copy(addExt(args.subjectDesignBaseName, ".mat"), args.logDir / "SubjectDesign.mat")
    shutil.copy(addExt(args.subjectDesignBaseName, ".con"), args.logDir / "SubjectDesign.con")
    print("Done.") # Say where!
    print()

################################################################################
print("*" * 80)
print("Initialising...")
print()

# Load subjects
with open(args.subjectOrderFileName) as subjectOrderFile:
    subjectOrder = json.load(subjectOrderFile)

S = len(subjectOrder)
#print(subjectOrder)


# Load runs
with open(args.runOrderFileName) as runOrderFile:
    runOrder = json.load(runOrderFile)

# Flip into paired list format: [[subjectA,runB],[subjectC,runD]]
# subject = runOrder[10][0]; run = runOrder[10][1]
runOrder = [[run["Subject"],run["Run"]] for run in runOrder]
R = len(runOrder)
#print(runOrder)

# Turn into {subject : [run1, run2]} form for PROFUMO class
runList = {subject: [] for subject in set([run[0] for run in runOrder])}
for run in runOrder:
    runList[run[0]].append(run[1])
#print(runList)


# Initialise PFMs
pfms = PROFUMO.PFMs(args.pfmDir)


# Load modes if requested, otherwise select all
if args.modeOrderFileName is not None:
    modes = numpy.loadtxt(args.modeOrderFileName, dtype=int)
    M = len(modes)
    # CHECK THIS IS NOT EMPTY
else:
    M = pfms.M
    modes = range(M)

# Make all group maps 'positive'
groupMaps = pfms.loadGroupSpatialMaps(modes)
signs = numpy.sign(numpy.mean(groupMaps**3, axis=0)) # Third moment (i.e. skewness around zero)
# numpy.mean(groupMaps**3, axis=0) / numpy.mean(groupMaps**2, axis=0)**1.5
#signs = numpy.loadtxt(os.path.join(resultsDir, "Signs.txt"), dtype=int)
#print(zip(modes,signs))

print("{:d} subjects and {:d} modes to be analysed.".format(S, M))
print()

# Save modes and signs
if args.modeOrderFileName is not None:
    print("Taking a copy of the mode list...")
    shutil.copy(args.modeOrderFileName, args.logDir / "ModeOrder.txt")
else:
    print("Saving the mode list...")
    numpy.savetxt(args.logDir / "ModeOrder.txt", modes, fmt="%d")
print("Done.") # Say where!
print()

print("Saving the signs list...")
numpy.savetxt(args.logDir / "ModeSigns.txt", signs, fmt="%d")
print("Done.") # Say where!
print()


# Load mask
print("Loading mask...")
#print("Mask file: {m}".format(m=args.maskFileName))

maskImage = nibabel.load(str(args.maskFileName))
maskData = maskImage.get_data()

# Armadillo is column-major, numpy is row-major - ouch!
# Loop through the mask in column-major order, and record the mask indices for 
# each element extracted by Armadillo.
maskInds = numpy.zeros((numpy.sum(maskData != 0.0), 3))
n = 0
for k in range(maskImage.shape[2]):
    for j in range(maskImage.shape[1]):
        for i in range(maskImage.shape[0]):
            if maskData[i,j,k] != 0.0:
                maskInds[n,:] = [i,j,k]
                n = n + 1
maskInds = numpy.intp(maskInds)

print("Done.")
print()

del groupMaps, maskData

#-------------------------------------------------------------------------------
def saveMaps(data, fileName):
    # Unmask
    if data.ndim == 1:
        unmaskedData = numpy.zeros(maskImage.shape)
        unmaskedData[maskInds[:,0],maskInds[:,1],maskInds[:,2]] = data
    elif data.ndim == 2:
        unmaskedData = numpy.zeros(maskImage.shape + (data.shape[1],))
        unmaskedData[maskInds[:,0],maskInds[:,1],maskInds[:,2],:] = data
    else:
        raise ValueError("Could not save data as a NIFTI file: expecting vector or matrix!")
    
    
    # Save
    niftiImage = nibabel.Nifti1Image(unmaskedData, affine=maskImage.affine, header=maskImage.header)
    nibabel.save(niftiImage, str(fileName))
    
    return
#-------------------------------------------------------------------------------

################################################################################
# Time courses
print("*" * 80)
print("Processing time courses...")
print()

# Load
timeCourses = pfms.loadRunTimeCourses(runList, modes, signs, cleanTCs=True)

# Save!
print("Saving time courses...")
n = 0; nZeros = int(math.log10(R)) + 1
for subject,run in runOrder:
    timeCourseFileName = timeCourseDir / "{n:0{nZ}d}_{subject}_{run}_TCs.txt".format(n=n, nZ=nZeros, subject=subject, run=run)
    # Save in transposed orientation (time x modes)
    numpy.savetxt(timeCourseFileName, timeCourses[subject][run].T)
    n = n + 1

print("Done.")
print()

# And clean up
del timeCourses, timeCourseFileName

################################################################################
# Temporal corrs
print("*" * 80)
print("Processing netmats...")
print()

# Load subject precision matrices
# Don't know if we want to load for subjectDesign or runDesign initially
# But loading is cheap, so just load them all...
modelType, subjectPrecMats = pfms.loadSubjectPrecisionMatrices(modes=modes, signs=signs)


# Convert to netmats
print("Converting precision matrices to netmats...")
if modelType == "Subject":
    
    #netMats = numpy.zeros((S, int(M*(M+1)/2)))
    netMats = numpy.zeros((S, int(M*(M-1)/2)))
    for s,subject in enumerate(subjectOrder):
        netMatElems, pVars = PROFUMO.convertPrecMatToNetMatElems( subjectPrecMats[subject] )
        #netMats[s,:] = numpy.concatenate( (pVars, netMatElems) )
        netMats[s,:] = netMatElems

elif modelType == "Run" or modelType == "Categorised Runs":
    
    #netMats = numpy.zeros((R, int(M*(M+1)/2)))
    netMats = numpy.zeros((R, int(M*(M-1)/2)))
    for r,run in enumerate(runOrder):
        subject,run = run
        netMatElems, pVars = PROFUMO.convertPrecMatToNetMatElems( subjectPrecMats[subject][run] )
        #netMats[r,:] = numpy.concatenate( (pVars, netMatElems) )
        netMats[r,:] = netMatElems

else:
    raise ValueError("Unrecognised model for temporal precision matrices.")

print("Done.")
print()


# Save
print("Saving netmats...")
numpy.savetxt(netMatsDir / "NetMats.csv", netMats, delimiter=", ")

print("Done.")
print()


# Work out which design to use
if modelType == "Subject":
    design = args.subjectDesignBaseName
elif modelType == "Run" or modelType == "Categorised Runs":
    design = args.runDesignBaseName

# If requested, run statistics using PALM
if design is not None:
    print("Running netmat statistics...")
    netMatsStatsDir = netMatsDir / "Statistics"; netMatsStatsDir.mkdir()
    
    # Submit to queue
    job = subprocess.run(["fsl_sub", "-q", "veryshort.q", "-l", queueLogDir,
        pathlib.Path("~winkler/bin/palm").expanduser(), "-i", (netMatsDir / "NetMats.csv"),
        "-d", addExt(design,".mat"), "-t", addExt(design,".con"),
        "-n", str(1000), "-o", (netMatsStatsDir / "Netmats")], 
        check=True, stdout=subprocess.PIPE)
    # "-logp" for nicer visualisations of spatial maps
    # "-vg" may reduce power (slightly for small groups) and makes permutations a nightmare
    jobID = job.stdout.strip()
    
    print("Netmat statistics submitted to the queue (job {j}).".format(j=jobID))
    print()


# And clean up
del subjectPrecMats, pVars, netMatElems, netMats, design

################################################################################
# Weights
print("*" * 80)
print("Processing weights...")
print()

# Load
weights = pfms.loadRunWeights(runList, modes)


# Put together
print("Collating weights...")
weightMat = numpy.zeros((R, M))
for r,run in enumerate(runOrder):
    subject,run = run
    weightMat[r,:] = weights[subject][run]

print("Done.")
print()


# Save
print("Saving weights...")
numpy.savetxt(weightsDir / "Weights.csv", weightMat, delimiter=", ")

print("Done.")
print()


# If requested, run statistics using PALM
if args.runDesignBaseName is not None:
    print("Running weight statistics...")
    weightsStatsDir = weightsDir / "Statistics"; weightsStatsDir.mkdir()
    
    # Submit to queue
    job = subprocess.run(["fsl_sub", "-q", "veryshort.q", "-l", queueLogDir,
        pathlib.Path("~winkler/bin/palm").expanduser(), "-i", (weightsDir / "Weights.csv"),
        "-d", addExt(args.runDesignBaseName,".mat"), "-t", addExt(args.runDesignBaseName,".con"),
        "-n", str(1000), "-o", (weightsStatsDir / "Weights")], 
        check=True, stdout=subprocess.PIPE)
    jobID = job.stdout.strip()
    
    print("Weight statistics submitted to the queue (job {j}).".format(j=jobID))
    print()


# And clean up
del weights, weightMat

################################################################################
# Group maps
print("*" * 80)
print("Processing group maps...")
print()

groupMaps = pfms.loadGroupSpatialMaps(modes, signs)

print("Saving group maps...")
saveMaps(groupMaps, (mapDir / "GroupMaps.nii.gz"))

print("Done.")
print()

# Generate summary images
if args.backgroundImageFileName is not None:
    print("Generating summary images...")
    
    # Submit to queue
    mapThreshold = 0.25
    job = subprocess.run(["fsl_sub", "-q", "veryshort.q", "-l", queueLogDir, "slices_summary", (mapDir / "GroupMaps.nii.gz"), "{t:.02f}".format(t=mapThreshold), args.backgroundImageFileName, (mapDir / "GroupMaps.sum")], 
        check=True, stdout=subprocess.PIPE)
    jobID = job.stdout.strip()
    
    print("Summary image generation submitted to queue (job {j}).".format(j=jobID))
    print()

# And clean up
del groupMaps

################################################################################
# Group spatial posteriors
print("*" * 80)
print("Processing group-level spatial posteriors...")
print()

# Load
posteriors = {}
posteriors["GroupMeans"] = pfms.loadGroupSpatialMapMeans(modes)
posteriors["GroupMemberships"] = pfms.loadGroupSpatialMapMemberships(modes)
posteriors["GroupStandardDeviations"] = 1.0 / numpy.sqrt( pfms.loadGroupSpatialMapPrecisions(modes) )
if pfms.config["Spatial model"] == "Modes":
    posteriors["GroupNoiseStandardDeviations"] = 1.0 / numpy.sqrt( pfms.loadGroupSpatialMapNoisePrecisions() )

print("Saving group spatial posteriors...")

# Make directory
posteriorDir = mapDir / "GroupPosteriors"; posteriorDir.mkdir()

# Save!
for name,posterior in posteriors.items():
    saveMaps(posterior, (posteriorDir / (name + ".nii.gz")))

print("Done.")
print()

# And clean up
del posterior, posteriors

################################################################################
# Normalisation
if pfms.config["Data normalisation"]:
    print("*" * 80)
    print("Processing group-level data normalisation parameters...")
    print()
    
    
    # Load the normalisations
    normalisations = pfms.loadDataNormalisations()
    
    
    # Find the median
    print("Calculating the group-level data normalisation parameters...")
    
    R = sum(len(runNorms) for runNorms in normalisations.values())
    groupNorm = numpy.zeros((maskInds.shape[0], R))
    r = 0
    for subject,runNorms in normalisations.items():
        for run,runNorm in runNorms.items():
            groupNorm[:,r] = runNorm; r += 1
    
    groupNorm = numpy.median(groupNorm, axis=1)
    
    print("Done.")
    print()
    
    
    # Save!
    print("Saving group-level data normalisation parameters...")
    
    saveMaps(groupNorm, (mapDir / "GroupNormalisation.nii.gz"))
    
    print("Done.")
    print()
    
    
    # And clean up
    del normalisations, groupNorm

################################################################################
# Spatial bases
if "Fixed group parameters" not in pfms.config:
    print("*" * 80)
    print("Processing spatial basis...")
    print()
    
    
    # Load basis
    print("Loading spatial basis...")
    SB = PROFUMO.loadHDF5(args.pfmDir / "Preprocessing/NormalisedSpatialBasis.hdf5")
    IM = PROFUMO.loadHDF5(args.pfmDir / "Preprocessing/InitialMaps.hdf5")
    print("Done.")
    print()
    
    
    # Save!
    print("Saving spatial basis...")
    
    saveMaps(SB, (mapDir / "SpatialBasis.nii.gz"))
    saveMaps(IM, (mapDir / "InitialMaps.nii.gz"))
    
    print("Done.")
    print()
    
    
    # And clean up
    del SB, IM

################################################################################
# Subject maps
print("*" * 80)
print("Processing subject maps...")
print()

# Load
subjectMaps = pfms.loadSubjectSpatialMaps(subjectOrder, modes, signs)
# Subject maps are now in the order given in "modes"!
if args.saveUnnormalisedMaps:
    subjectMaps_UN = pfms.loadSubjectSpatialMaps(subjectOrder, modes, signs, normalisation="RAW_DATA")
if args.savePercentSignalChangeMaps:
    subjectMaps_PSC = pfms.loadSubjectSpatialMaps(subjectOrder, modes, signs, normalisation="PERCENT_SIGNAL_CHANGE")

# Save on a per mode basis
print("Saving subject maps, grouped by mode...")

if args.subjectDesignBaseName is not None:
    print("Also running spatial map statistics...")
    mapStatsDir = mapDir / "Statistics"; mapStatsDir.mkdir()

print()

for m,mode in enumerate(modes):
    print("Saving subject maps for mode {m}...".format(m=m))
    
    modeMapsBaseName = mapDir / "Mode_{m:02d}_SubjectMaps".format(m=m)
    modeMaps = numpy.stack([subjectMaps[subject][:,m] for subject in subjectOrder], axis=1)
    saveMaps(modeMaps, addExt(modeMapsBaseName,".nii.gz"))
    
    # If requested, run statistics using Randomise
    if args.subjectDesignBaseName is not None:
        
        # Submit to queue
        job = subprocess.run(["fsl_sub", "-q", "short.q", "-l", queueLogDir,
            "randomise", "-i", addExt(modeMapsBaseName,".nii.gz"), "-m", args.maskFileName,
            "-d", addExt(args.subjectDesignBaseName,".mat"), "-t", addExt(args.subjectDesignBaseName,".con"),
            "-T", "-n", str(1000), "-o", (mapStatsDir / "Mode_{m:02d}".format(m=m))], 
            check=True, stdout=subprocess.PIPE)
        jobID = job.stdout.strip()
        
        print("Spatial statistics for mode {m} submitted to the queue (job {j}).".format(m=m, j=jobID))
    
    # And other normalisations (don't do stats)
    if args.saveUnnormalisedMaps:
        modeMaps = numpy.stack([subjectMaps_UN[subject][:,m] for subject in subjectOrder], axis=1)
        saveMaps(modeMaps, addExt(modeMapsBaseName, "_UN.nii.gz"))
    if args.savePercentSignalChangeMaps:
        modeMaps = numpy.stack([subjectMaps_PSC[subject][:,m] for subject in subjectOrder], axis=1)
        saveMaps(modeMaps, addExt(modeMapsBaseName, "_PSC.nii.gz"))
    
    print("Done.")
    print()

# And clean up
del modeMaps, modeMapsBaseName

# Save on a per subject basis, if requested
if args.saveSubjectMaps:
    print("Saving subject maps, grouped by subject...")
    print()
    
    for (s,subject) in enumerate(subjectOrder):
        print("Saving maps for subject {s}...".format(s=subject))
        
        subjectMapsBaseName = mapDir / "{s:02d}_{subject}_SubjectMaps".format(s=s, subject=subject)
        saveMaps(subjectMaps[subject], addExt(subjectMapsBaseName, ".nii.gz"))
    
        # And other normalisations (don't do stats)
        if args.saveUnnormalisedMaps:
            saveMaps(subjectMaps_UN[subject], addExt(subjectMapsBaseName, "_UN.nii.gz"))
        if args.savePercentSignalChangeMaps:
            saveMaps(subjectMaps_PSC[subject], addExt(subjectMapsBaseName, "_PSC.nii.gz"))

        print("Done.")
        print()

    # And clean up
    del subjectMaps, subjectMapsBaseName
    if args.saveUnnormalisedMaps:
        del subjectMaps_UN
    if args.savePercentSignalChangeMaps:
        del subjectMaps_PSC

################################################################################
# Bye!

print("*" * 80)
raise SystemExit()

################################################################################
# Some useful commands:


# Check for significance in spatial maps:
# cd <spatialStatsDir>
# for i in Mode_??_tfce_corrp_tstat[12].nii.gz; do
#   echo $i `fslstats $i -R`;
# done
# for i in Mode_??_tfce_corrp_tstat[12].nii.gz; do echo $i `fslstats $i -R`; done | grep 0[.]9

# Nicer display of netmat stats - display as a square
import numpy
N = "<numberOfModes>"
pVals = numpy.loadtxt("???????_tstat_fwep_c1.csv", delimiter=",")
pSquare = numpy.diag(pVals[0:N])
pInds = numpy.triu_indices(N,1)
pSquare[pInds] = pVals[N:]
print(numpy.array_str(pSquare, max_line_width=1000000))
# numpy.savetxt("temp.txt", pSquare)

