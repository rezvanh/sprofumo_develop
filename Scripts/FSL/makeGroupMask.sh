#!/bin/bash -e
shopt -s nullglob

# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

# makeGroupmask.sh <maskDirectory> <threshold>

maskDir=$1
threshold=$2

groupMean=${maskDir}/groupMean
groupMask=${maskDir}/groupMask

# Remove existing group masks
#rm -f "${groupMean}" "${groupMask}"

# Get masks
masks=( "${maskDir}"/*_*_mask.nii.gz )
# Number of masks
N=${#masks[@]}

if [ $N -eq 0 ]; then
    echo "No masks found in ${maskDir}! Exiting..."
    exit 1
fi

# Make image of zeros
fslmaths "${masks[0]}" -mul 0.0 "${groupMean}"

# Loop over all masks
for mask in "${masks[@]}"; do
    
    # Add subject mask to group mask
    fslmaths "${groupMean}" -add "${mask}" "${groupMean}"
    
done

# Turn into a group mean
fslmaths "${groupMean}" -div ${N} "${groupMean}"

# And binarise to get group mask
fslmaths "${groupMean}" -thr "${threshold}" -bin "${groupMask}"
