#!/usr/bin/env python
# -*- coding: utf-8 -*-

# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

################################################################################
print("*" * 80)
print("Generating HRF...")
print()

import os, os.path
import sys

import numpy

import matplotlib as mpl
import matplotlib.pyplot as plt

sys.path.append(os.path.expanduser( "~samh/PROFUMO/Visualisation" ))
import samplots as splt

################################################################################

hrfFile = os.path.expandvars(os.path.join("$FSLDIR","etc", "default_flobs.flobs", "hrfbasisfns.txt"))
hrfBasis = numpy.loadtxt(hrfFile)

# FLOBS dt = 0.05
t = numpy.linspace(0, 0.05 * (hrfBasis.shape[0] - 1), hrfBasis.shape[0])

hrf = hrfBasis[:,0]; hrf = hrf / numpy.max(numpy.abs( hrf ))

HRF = numpy.zeros((hrf.size, 2))
HRF[:,0] = t
HRF[:,1] = hrf

# Plot examples
plt.figure()
plt.plot(t, hrfBasis)

plt.figure()
plt.plot(t, hrf)

plt.show()

# Save!
numpy.savetxt("DefaultHRF.phrf", HRF)

print("Done.")
print()

################################################################################
# Bye!

print("*" * 80)
raise SystemExit()

