# Stochastic PROFUMO sPROFUMO

A method for inferring for [PRObabilistic FUnctional
MOdes] from _big_ fMRI data.
Developed by the FMRIB Analysis Group at the [Wellcome Centre for Integrative
Neuroimaging](https://www.win.ox.ac.uk/), University of Oxford, to integrate
with FSL.

Original PROFUMO paper: https://doi.org/10.1016/j.neuroimage.2015.01.013

Second PROFUMO paper: https://doi.org/10.1016/j.neuroimage.2020.117226

Stochastic PROFUMO paper: https://doi.org/10.1016/j.neuroimage.2021.118513 

This repository includes the latest changes in stochastic PROFUMO (sPROFUMO), to be integrated within [PROFUMO](https://git.fmrib.ox.ac.uk/profumo/profumo) and [FSL](https://fsl.fmrib.ox.ac.uk).
----------

Version stochastic beta; 2021-11-06

!! Note that this repository is actively under development. If you are considering using it, please get in touch with Rezvan Farahibozorg:
rezvan.farahibozorg@ndcn.ox.ac.uk

[Repository](https://git.fmrib.ox.ac.uk/rezvanh/sprofumo_develop)
&mdash;
[License](LICENSE)
&mdash;
[Contributors](CONTRIBUTORS.md)


Running the code after installation
------------

add HOME/lib to your LD_LIBRARY_PATH

export OMP_NUM_THREADS=20

N_MODES=50
TR=0.72 #e.g. for HCP

$HOME/bin/sPROFUMO_develop /YOUR_PATH_TO/DataLocations.json $N_MODES \
/YOUR_PATH_TO/sprofumo_output_dir.pfm \
--bigData --stochasticBeta 0.6 --stochasticBatchSize 50 --stochInitBatchUpdates 10 --groupIteration 4000 --subjectIteration 2.5 \
-m /YOUR_PATH_TO/brain_mask.nii.gz \
--useHRF $TR --hrfFile /YOUR_PATH_TO/sprofumo_develop/Scripts/DefaultHRF.phrf --multiStartIterations 10 -d 0.075 > /YOUR_PATH_TO/sprofumo_log.txt

#run $HOME/bin/sPROFUMO_develop --help for info on different arguments. 

#--bigData flag enables sPROFUMO usage 

# Detailed notes 

A method for inferring for [PRObabilistic FUnctional
MOdes](https://doi.org/10.1016/j.neuroimage.2015.01.013) from fMRI data.
Developed by the FMRIB Analysis Group at the [Wellcome Centre for Integrative
Neuroimaging](https://www.win.ox.ac.uk/), University of Oxford, to integrate
with [FSL](https://fsl.fmrib.ox.ac.uk).

Note: This is the stable version of PROFUMO, for under-development version, including stochastic PROFUMO see:
https://git.fmrib.ox.ac.uk/rezvanh/sprofumo_develop 
The two repositories will be merged in due course.

----------


For those looking to run the code, please get in touch! If you have questions
about the suitability of PROFUMO for your dataset and/or research question,
then feel free to [contact us directly](CONTRIBUTORS.md). For technical
questions or issues running the code, please use the [FSL email list](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Support)
such that the solutions are available to others as part of the searchable
archive. Finally, bug reports and contributions are welcomed and can be
submitted directly to this [repository](https://git.fmrib.ox.ac.uk/profumo/profumo).

----------

### Contents

 + [Overview](#overview)
 + [Dependencies](#dependencies)
   + [C++](#dependencies-cpp)
   + [Python](#dependencies-python)
 + [Installation](#installation)
   + [Containers](#installation-containers)
   + [C++](#installation-cpp)
   + [Python](#installation-python)
   + [Tests](#installation-tests)
 + [User Guide](#userguide)
   + [Data Requirements](#userguide-data)
   + [Computational Demands](#userguide-computation)
   + [Analysis](#userguide-analysis)
   + [Postprocessing](#userguide-postprocessing)
   + [Python API](#userguide-pythonapi)
 + [References](#references)
   + [John Profumo](#references-profumo)

----------

### <a name="overview"></a> Overview

PROFUMO is the algorithm and codebase that extracts Probabilistic Functional
Modes (PFMs) from neuroimaging data. PFMs are essentially resting-state
networks, but which are specified via a formal probabilistic model.

The model is hierarchical over subjects, and as such there is a particular
emphasis on accurately inferring subject-specific information. At the
subject level, PFMs are defined by four key sets of parameters:
 + Spatial maps, specifying the location of the PFMs within the brain.
 + Time courses, describing how the brain activity fluctuated over the course
   of the scan.
 + Amplitudes, capturing the overall activity level of each PFM.
 + Netmats (or functional connectomes), capturing the temporal relationships
   between different PFMs.

These are all inferred at the group and subject level simultaneously, which
allows us to pool information across subject but without averaging over
features that are unique to specific subjects.

Finally, while the approach is tailored to fMRI data, it is possible to run on
other types of data. For example, it is possible to turn off the haemodynamic
modelling such that it is possible to run the algorithm on the amplitude
envelopes of MEG data.

In terms of the interface with FSL, PROFUMO can be thought of as an alternative
to the [MELODIC](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/MELODIC),
[Dual Regression](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/DualRegression), and
[FSLNets](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FSLNets) pipeline.

For more information, and details of how to cite the code, please see the
[References section](#references).

----------

### <a name="dependencies"></a> Dependencies

PROFUMO is built using [C++14](https://isocpp.org) and
[Python 3](https://www.python.org), and, in addition to
[FSL 6](https://fsl.fmrib.ox.ac.uk), requires the following:

##### <a name="dependencies-cpp"></a> C++

 + [Armadillo](http://arma.sourceforge.net) [≥ 7.700]:
   C++ linear algebra library.
 + [Boost](https://boost.org):
   In particular, the *Special Functions* section of the *Math* library.
 + [HDF5](https://www.hdfgroup.org/downloads/hdf5/):
   A data model, library, and file format for storing and managing data.

Optional dependencies:
 + [OpenBLAS](http://www.openblas.net):
   An optimized BLAS library.

Included submodules:
 + [JSON for Modern C++](https://github.com/nlohmann/json):
   A C++11 header-only [JSON](https://json.org) library.
 + [spdlog](https://github.com/gabime/spdlog):
   A fast C++ logging library.
 + [TCLAP](http://tclap.sourceforge.net):
   Templatized C++ Command Line Parser Library.
 + [tk spline](https://github.com/ttk592/spline/):
   C++ cubic spline library.

##### <a name="dependencies-python"></a> Python

An [environment.yml](Python/environment.yml) file is provided for managing the
following dependencies via [conda](https://conda.io/docs/). That file also
contains information about the version requirements.

 + [NumPy](https://numpy.org):
   The fundamental package for scientific computing with Python.
 + [SciPy](https://scipy.org):
   A collection of numerical algorithms and domain-specific toolboxes,
   including signal processing, optimization and statistics.
 + [Matplotlib](https://matplotlib.org):
   A Python 2D plotting library.
 + [NiBabel](https://nipy.org/nibabel/):
   Provides read / write access to some common neuroimaging file formats.
 + [cifti](https://github.com/MichielCottaar/cifti):
   Module for creating, reading and manipulating CIFTI files.
 + [h5py](https://www.h5py.org):
   A Pythonic interface to the HDF5 binary data format.
 + [Jinja2](https://palletsprojects.com/p/jinja/):
   A full featured template engine for Python.

----------

### <a name="installation"></a> Installation

The code can be built from source, 

Start by cloning the repository:
```shell
git clone --branch=master --recurse-submodules \
    https://git.fmrib.ox.ac.uk/rezvanh/sprofumo_develop.git
```


##### <a name="installation-cpp"></a> C++

A [Makefile](https://www.gnu.org/software/make/) is provided, so&mdash;given
that the dependencies are installed and a C++14 compatible compiler is
available&mdash;building should just consist of:
```shell
cd profumo/C++/  # Also add to `$PATH`
make [all|PROFUMO|Test]  # N.B. default build does not include tests
```

We *strongly* recommend building with [OpenMP](https://openmp.org/) where
possible, as the parallel version of the code will be much quicker if multiple
threads are available. This just requires passing some extra arguments to the
`make` command. For example, to build with OpenMP and link to OpenBLAS use the
following command:
```shell
make CXXEXTRAS="-fopenmp" LDEXTRAS="-fopenmp -lopenblas"
```

If you get linker errors about the NIfTI library, it may be that you need to
rebuild it using the same compiler as the PROFUMO source. You can do this using
the following commands:
```shell
cp -r "${FSLDIR}/src/NewNifti" Dependencies/
cd Dependencies/NewNifti/
make
# N.B. The FSLMACHTYPE environment variable may need adjusting
```
And then try and rebuild the rest of the C++ code as before.

Finally, compiling on macOS may well require Clang rather than the default of
GCC (see e.g. issue #4). This is straightforward to specify during the build
process via `make CXX=clang++ ...`.

Once built, you can run a few basic tests using:
```shell
./Test  # Requires `make all` or `make Test`
```

##### <a name="installation-python"></a> Python

For the Python code, one just needs to install the dependencies. Using conda,
the process is simply:
```shell
cd profumo/Python/  # Also add to `$PATH`
conda env create -f environment.yml
conda activate profumo
```

Finally, one should add the `C++/` and `Python/` directories to `$PATH`.

##### <a name="installation-tests"></a> Tests

Once installed, you can run a few basic tests using:
```shell
cd profumo/Scripts/
generate_test_data.py [--data_type <format>] </path/to/create>
PROFUMO ...  # See output of above
postprocess_results.py ...  # See output of above
```
If the above commands run, then the installation was a success.

----------

### <a name="userguide"></a> User Guide

Running PROFUMO consists of two main phases: the data is analysed using the C++
portion of the code, and then the results are postprocessed using the Python
routines.

A typical analysis will result in the following directories being created:

 + `Analysis.pfm`: The raw output from the C++ analysis. This contains all the
   results, but the format is somewhat obtuse. As such, the Python routines
   give a simple way of summarising and querying these results.

 + `Results.ppp`: The postprocessed output from the Python code. This contains
   the key quantities of interest (spatial maps, time courses, etc.) in simple
   to use data formats (NIfTI, CSV, etc.).

   + `Report.pwr`: The postprocessing code can also generate a web report
     summarising some of the key results.

Note that code snippets use the following syntax: `<arg>` indicates a
value to be replaced, and `[arg]` indicates an optional argument. A `|`
character indicates a choice between two options.
```shell
PROFUMO [--option1|--option2 <value>] <output>
# Choosing option2, this becomes:
PROFUMO --option2 50 /path/to/output_directory
```
Furthermore, we assume that `$PATH` has been set such that the executables can
be found (this will be done automatically if using a container).

##### <a name="userguide-data"></a> Data Requirements

The hierarchical modelling inherent in PROFUMO means that more subjects
improves the quality of the inference, often drastically. We recommend at least
20 subjects (unless the data is of particularly high quality).

Furthermore, PROFUMO works much more effectively when there is more data per
subject. In an ideal world that would be at least 10 minutes of fMRI data per
subject (which can be split across multiple runs).

PROFUMO analyses preprocessed functional data, and it can analyse either NIfTI
or CIFTI files. The only strict data requirement is that all data is in the
same space (i.e. every scan has the same number of voxels), but better
preprocessing will drastically improve the quality of the results!

The standard [FEAT](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FEAT) or
[MELODIC](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/MELODIC) pipelines will
produce data in the right format, and it can be instructive to run e.g. PROFUMO
and MELODIC on the same data.  The following additional preprocessing steps are
*strongly* recommended if possible.

Spatial preprocessing:
 + B0 unwarping / fieldmap correction.
 + Surface-based registration, ideally using functional features
   \[[MSM](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/MSM)\].
 + Relatively light spatial smoothing (typically FWHM 1&times; to 2&times;
   voxel size).

Temporal preprocessing:
 + Physiological noise modelling
   \[[PNM](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/PNM)\].
 + ICA-based artefact removal
   \[[FIX](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FIX),
   [AROMA](https://github.com/maartenmennes/ICA-AROMA)\].

Note that we do not typically recommend either scrubbing or global signal
regression. Scrubbing can impact the HRF model PROFUMO uses, which assumes
volumes are acquired sequentially and evenly spaced in time. Therefore, we
recommend ICA-based methods instead. Similarly, we recommend focused
physiological noise models (or temporal ICA for large data sets) instead of
global signal regression as these are typically more conservative in the sense
that they are less likely to remove 'neural' signal.

That being said, pragmatically, these are effective forms of artefact removal
and may be necessary if the above are not successful. However, in that
instance, the results would need to be interpreted with a degree of caution.


##### <a name="userguide-analysis"></a> Analysis

The analysis is performed by the `PROFUMO` executable.
```shell
PROFUMO --version
PROFUMO --help
PROFUMO [<options>] <DataLocations.json> <M> <Analysis.pfm>
```

The three required parameters are:

 + `DataLocations.json`: the path to the file specifying where all the data is
   stored. See below for a description of its contents.

 + `M`: the number of PFMs to infer. For typical volumetric data we recommend
   starting with `M=30`. For HCP-style data, this can be pushed to `M=50`. The
   inherent Bayesian complexity penalties will eliminate PFMs that are not
   supported by the data, so you may need to increase this to prevent
   oversplitting if all `M` PFMs are present and strong. Conversely, if the
   number of PFMs is substantially less than `M` then you can reduce it for
   subsequent analyses to reduce the computational burden.

 + `Analysis.pfm`: the path to the output directory. `.pfm` will be appended if
   not present.

`DataLocations.json` should contain the paths to the data, as per the following
example:
```json
{
    "sub-01": {
        "run-01": "/path/to/data.nii.gz",
        "run-02": "/path/to/data.nii.gz"
    },
    "sub-02": {
        "run-01": "/path/to/data.nii.gz",
        "run-02": "/path/to/data.nii.gz"
    }
}
```
Note that, unlike [BIDS](https://bids.neuroimaging.io/), PROFUMO does not
distinguish between sessions and runs&mdash;the hierarchy can only be two
levels deep. However, you are free to combine or examine the inferred
parameters from different runs/sessions as you see fit later.

The following are the recommended options for (volumetric) fMRI data:
```shell
PROFUMO <DataLocations.json> <M> <Analysis.pfm> \
    --useHRF <TR> [--hrfFile "/path/to/profumo/HRFs/Default.phrf"] \
    --covModel <model> --mask <mask.nii.gz> \
    --dofCorrection <float> [--lowRankData <int>] \
    [--nThreads <int>] [--loadSequentially]
```
The key arguments do the following:

 + `--useHRF <TR>`: turn on the HRF modelling. The specific form of the HRF can
   be modified using `--hrfFile <file>`. This can be omitted if running in a
   container.

 + `--covModel <model>`: change the hierarchical temporal network matrix model.
   The different forms are:
   + `Subject`: each subject has a single temporal network matrix, shared
     across all runs.
   + `Run`: each run has its own temporal network matrix.
   + `Condition`: each run has its own temporal network matrix, but with
     condition specific regularisation. That is, there is a different
     group-level prior for every unique run name in `DataLocations.json`. In
     the example above, `run-01` and `run-02` would be treated as two different
     conditions.

 + `--dofCorrection <float>`: apply a correction for the spatial smoothness of
   the data. For volumetric data, this can be estimated by running the
   `estimate_smoothness.py` script (in the `Python/` directory) on one of the
   functionals that will be fed into PROFUMO. For CIFTI data preprocessed with
   HCP-style pipelines and minimally smoothed, `--dofCorrection 0.5` is usually
   sufficient.

 + `--lowRankData <int>`: apply data reduction to reduce computational demands.
   This is particularly useful when there are multiple runs per subject. The
   number represents the rank of the SVD-based approximation to the full data.
   If you have more than 500 time points per subject (summing over all runs)
   then it is advisable to set this to something close to `5 * M`. It should
   not be less than 100, and not more than half the number of time points per
   subject.

 + `--nThreads <int>`: If the code has been compiled with OpenMP then this
   sets the number of threads PROFUMO will use. The alternative is to set the
   `$OMP_NUM_THREADS` environment variable.

 + `--loadSequentially`: If the code has been compiled with OpenMP then by
   default both the algorithm and the data loading are parallelised. However,
   when memory is tight it and `--lowRankData` has been selected, then it is
   possible that the data loading may be the most memory intensive part of the
   analysis (i.e. during loading each of the `nThreads` will have their own
   temporary copy of one subject's *full* data in memory, which may well be too
   much). This option disables the parallelisation of *only* the data loading
   to reduce memory demands (at the expense of a slightly longer runtime).

The remaining parameters can either be left at their defaults, or, in the case
of `--spatialModel Parcellation`, the `--fixedSpatial...` options, and
`--spatialNeighbours`, should be treated as experimental and potentially
unstable.

PROFUMO will print some information on progress to screen as it runs, but more
detailed information can be found in the log files. Of these, `output.txt` is
designed to be readable, such that it is possible to monitor progress more
closely with e.g. `tail -f <Analysis.pfm>/output.txt`.

##### <a name="userguide-postprocessing"></a> Postprocessing

The `postprocess_results.py` script runs the postprocessing pipeline.
```shell
postprocess_results.py --help
postprocess_results.py [--web-report] \
    <Analysis.pfm> <Results.ppp> <ReferenceImage.nii.gz>
```

The three required parameters are:

 + `Analysis.pfm`: the path to the directory containing the output from the C++
   analysis.

 + `Results.ppp`: the path to the output directory. `.ppp` will be appended if
   not present.

 + `ReferenceImage.nii.gz`: The image from which header information should be
   extracted when saving the spatial maps. For NIfTI data, this will also be
   the background image when generating the web report. Therefore, this is
   typically the structural image in standard space to which all subjects were
   registered. It could however be any image with the appropriate metadata.

We also strongly recommend that you generate a web report. As PROFUMO has
inbuilt Bayesian complexity penalties, it will eliminate any modes that do not
fit the modelling assumptions. As such, the first step of a typically analysis
is to triage the modes by removing any PFMs that are either empty or that
represent artefacts. The web report helps in this regard.

For *volumetric* analyses, we also provide a simple wrapper script that loads
the key group parameter maps into
[FSLeyes](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FSLeyes):
``` shell
cd profumo/Scripts/
./pfmeyes <Results.ppp>
```

You will need to generate a list of 'good' modes and store them as a CSV file
(note that numbering starts from 0, like in FSLeyes and the report). This
should be a single column as per the example below.
``` shell
0
3
4
...
```
The postprocessing script automatically generates files of this form, so see
`Results.ppp/Modes.csv` and `Results.ppp/Signs.csv` for examples.

We can now re-run the postprocessing pipeline on only those modes such that
the 'bad' modes do not contaminate any future analyses. This is done by
providing the `--modes <Modes.csv>` flag to `postprocess_results.py`. In the
unlikely event that any modes are the wrong sign (i.e. are predominantly
negative) then this can also be corrected at this stage using
`--signs <Signs.csv>`.

##### <a name="userguide-pythonapi"></a> Python API

The `profumo` Python package provides a very flexible way of extracting
information from an analysis. Almost all the parameters inferred as part of the
model, as well as the preprocessing parameters, are available via the
`profumo.PFMs` class.
```shell
cd profumo/Python/
python
```

The example below is very brief, but the general structure of the code should
be clear.
```python
import profumo
help(profumo)
help(profumo.PFMs)

# The PFMs class is the core part of the API
pfms = profumo.PFMs("Analysis.pfm")

# All operations can be performed on subsets of subjects, runs or modes
subjects = pfms.subjects[:3]
runs = {subject: pfms.runs[subject] for subject in subjects}
modes = range(5,pfms.M)

# This is just a subset of the parameters accessible through the PFMs class
group_maps = pfms.load_group_maps()
subject_maps = pfms.load_subject_maps(subjects, modes)
amplitudes = pfms.load_run_amplitudes()
time_courses = pfms.load_run_time_courses(runs)

# Several convenience plotting functions are also available
import matplotlib.pyplot as plt
import profumo.plotting as pplt
fig, ax = plt.subplots()
pplt.plot_matrix(fig, ax, group_maps, title="Group Maps")
plt.show()

# As are several useful utilities
import profumo.utilities as utils
precmat = pfms.load_group_amplitude_precmat()
partial_corrs = - utils.normalise_covmat(precmat)
fig, ax = plt.subplots()
pplt.plot_correlation_matrix(
        fig, ax, partial_corrs,
        title="Relationship between PFM amplitudes",
        clabel="Partial correlation")
plt.show()
```

----------

### <a name="references"></a> References

Please use something like the following text if citing the method. The full
citations can be found in [References.bib](References.bib).

> Probabilistic Functional Modes (PFMs) [Harrison-Smith-2015;
> Harrison-Woolrich-2020] were inferred using PROFUMO (version 0.11.3;
> https://git.fmrib.ox.ac.uk/samh/profumo).

When using stochastic PROFUMO (for big data) please use something like the following text if citing the method. The full
citations can be found in [References.bib](References.bib).

> stochastic Probabilistic Functional Modes (PFMs) [Farahibozorg-Smith-Harrison-Woolrich-2021;
> were inferred using sPROFUMO (currently at:
> https://git.fmrib.ox.ac.uk/rezvanh/sprofumo_develop).

##### <a name="references-profumo"></a> John Profumo

The name of the code comes from the British politician
[John Profumo](https://en.wikipedia.org/wiki/John_Profumo).  He is perhaps best
known for the "[Profumo affair](https://en.wikipedia.org/wiki/Profumo_affair)"
which ended his political career (unfortunately, that was one of the better
outcomes for the involved parties). After politics, he spent the last forty
years of his life working as a volunteer at
[Toynbee Hall](https://toynbeehall.org.uk/), a charitable organisation
based in the East End of London.

----------

Written in [CommonMark-flavoured Markdown](https://commonmark.org/) using the
associated [live testing tool](https://spec.commonmark.org/dingus/).

