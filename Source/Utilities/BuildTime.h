// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2017
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Wrapper files that get rebuilt in order to include build time in the 
// binaries. Separate file so quick to recompile.

#ifndef UTILITIES_BUILD_TIME
#define UTILITIES_BUILD_TIME

#include <string>

namespace PROFUMO
{
    namespace Utilities
    {
        std::string getBuildTime();
        
        std::string getBuildDate();
    }
}
#endif
