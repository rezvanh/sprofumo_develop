// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Computes the SVD using an efficient randomised algorithm.

#ifndef UTILITIES_RANDOM_SVD_H
#define UTILITIES_RANDOM_SVD_H

#include <vector>
#include <armadillo>
#include <memory>

namespace PROFUMO
{
    ////////////////////////////////////////////////////////////////////////////
    
    // Singular Value Decomposition
    // X = U * diagmat(s) * V.t()
    struct SVD
    {
    public:
        arma::fmat U;
        arma::fvec s;
        arma::fmat V;
    };
    
    // Singular Value Decomposition for multiple data
    // X[i] = U * diagmat(s) * V[i].t()
    struct ConcatenatedSVD
    {
    public:
        arma::fmat U;
        arma::fvec s;
        std::vector<arma::fmat> V;
    };
    
    struct MIGPConcatenatedSVD
    {
    public:
        arma::fmat Wmat;
        arma::fvec Eigvals;
        arma::fmat Eigvecs;
    };
    
    ////////////////////////////////////////////////////////////////////////////
    
    namespace Utilities
    {
        // Compute a randomised version of the SVD
        // Algorithm from Halko, Martinson & Tropp, SIAM Review 53(2) pp.217-288, 2011
        // DOI: 10.1137/090771806
        // Parameters:
        // D: Data
        // K: Target number of components to estimate
        // k: Number of extra components during algorithm to improve accuracy
        // q: Rounds of power iteration to perform
        SVD computeRandomSVD(const arma::fmat& D, const unsigned int K, const unsigned int k=10, const unsigned int q=2);
        
        // Extension of the random SVD to work on multiple data matrices
        // Data will be concatenated in the second dimension
        // e.g. D[0] is (M x N1), D[1] is (M x N2) etc
        ConcatenatedSVD computeRandomConcatenatedSVD(const std::vector<const arma::fmat*>& D, unsigned int K, const unsigned int k=10, const unsigned int q=2);
        
        MIGPConcatenatedSVD computeMIGPConcatenatedSVD(const arma::fmat subjectData, unsigned int K, const arma::fmat previousWMAT, const bool lastSub = false);
    }
    
    ////////////////////////////////////////////////////////////////////////////
}
#endif
