// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Assorted functions for fast approximations to some mathematical functions.

#ifndef UTILITIES_FAST_APPROXIMATE_MATHS
#define UTILITIES_FAST_APPROXIMATE_MATHS

#include <cmath>

namespace PROFUMO
{
    namespace Utilities
    {
        namespace FastApprox
        {
            ////////////////////////////////////////////////////////////////////
            // Inspired by:
            // "A Fast, Compact Approximation of the Exponential Function", Nicol N. Schraudolph, Neural Computation, 1999
            // "On a Fast, Compact Approximation of the Exponential Function", Gavin C. Cawley, Neural Computation, 2000
            // fastapprox by Paul Mineiro:
            // https://code.google.com/archive/p/fastapprox/
            // http://www.machinedlearnings.com/2011/06/fast-approximate-logarithm-exponential.html
            // Martin Ankerl:
            // http://martin.ankerl.com/2012/01/25/optimized-approximative-pow-in-c-and-cpp/
            
            // The problem with the originals is they rely on bitwise manipulations
            // of floating point numbers, which makes a lot of assumptions about 
            // what is going on "under the hood".
            
            // This takes a slightly different approach, which means that it should 
            // be platform agnostic. Basically, the exponent is just the floor of 
            // the power, and we take a quick and dirty approximation to the mantissa
            // using a Padé approximation.
            
            static inline float pow2(const float x)
            {
                // 2^x = 2^floor(x) * 2^(x - floor(x))
                
                // Exponent is just floor(x)
                const int exponent = static_cast<int>( std::floor(x) );
                
                // Find the remainder
                const float z = x - static_cast<float>( exponent );
                
                // Approximate the mantissa (2^z)
                // 3rd order polynomial
                //const float mantissa = 0.9998120990f + 0.6968370983f * z + 0.2241303147f * z * z + 0.0790171483f * z * z * z;
                
                // Pade 1,1 approximation
                //const float mantissa = (0.9977492582f + 0.4207806548f * z) / (1.0f + -0.2916840000f * z);
                
                // Pade 2,2 approximation
                const float mantissa = (1.0000014591f + 0.3669827097f * z + 0.0479880820f * z * z) / (1.0f + -0.3261313174f * z + 0.0336181033f * z * z);
                
                // Convert to full representation
                return std::ldexp(mantissa, exponent);
            }
            
            ////////////////////////////////////////////////////////////////////
            
            static inline float exp(const float x)
            {
              return pow2(1.4426950409f * x);
            }
            
            ////////////////////////////////////////////////////////////////////
        }
    }
}
#endif
