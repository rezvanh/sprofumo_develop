// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Functions to generate HRF-based models

#ifndef UTILITIES_HRF_MODELLING_H
#define UTILITIES_HRF_MODELLING_H

#include <string>
#include <armadillo>

#include "MFModels/A/KroneckerHRF.h"

namespace PROFUMO
{
    namespace Utilities
    {
        
        // Generates an HRF from the FLOBS basis set
        arma::fvec generateHRF(const std::string hrfFile, const unsigned int T, const float TR);
        
        // Generates a prior for the KroneckerHRF model from the FLOBS basis
        MFModels::A::KroneckerHRF::Parameters generateHRFPrior(const std::string hrfFile, const unsigned int T, const float TR, const unsigned int M, const float priorRelaxation=0.0, const float posteriorRelaxation=0.0);
        // Prior relaxation adds a white noise process on top of the HRF (by 
        // adding a small amount of the identity matrix). The parameter itself 
        // is the proportion of the variance this 'noise' describes.
        // Posterior relaxation regularises the deconvolution, by adding a 
        // small amount of the identity matrix when inverting the covariance.
        // Both between 0.0 and 1.0
        
    }
}
#endif
