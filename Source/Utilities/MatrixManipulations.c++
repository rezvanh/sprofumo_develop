// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "MatrixManipulations.h"

#include <memory>
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <ios>      // std::scientific
#include <iomanip>  // std::setprecision
#include <limits>   // std::numeric_limits<float>::digits10

#include "MFModel.h"
#include "MFVariableModels.h"
#include "MFModels/FullRankMFModel.h"
#include "MFModels/P/IndependentMixtureModels.h"
#include "MFModels/P/Parcellation.h"
#include "MFModels/A/MultivariateNormal.h"
#include "MFModels/Psi/GammaPrecision.h"

#include "ModuleList.h"
#include "VBModules/Constant.h"
#include "VBModules/CopyParent.h"
#include "VBModules/PrecisionMatrices/ComponentwiseGammaPrecision.h"

#include "Utilities/Timer.h"

namespace MM = PROFUMO::Utilities::MatrixManipulations;

////////////////////////////////////////////////////////////////////////////////

void MM::randomiseColumns(arma::fmat& X)
{
    // Apply a rotation via an orthogonal matrix
    arma::fmat Q,R,Z;
    Z = arma::randn<arma::fmat>(X.n_cols, X.n_cols);
    arma::qr(Q,R,Z);
    
    X *= Q;
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MM::flipColumnMeans(arma::fmat& X)
{
    X.each_row() %= arma::sign( arma::mean(X, 0) );
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MM::flipColumnSkews(arma::fmat& X)
{
    // Calculate skewness
    // https://en.wikipedia.org/wiki/Skewness#Pearson.27s_moment_coefficient_of_skewness
    // Just do that here: we should probably do it in MiscMaths.h, but then 
    // there's the issue of making it general across vectors / rows / columns, 
    // and it seems overkill to create a new Armadillo operation.
    const arma::frowvec means = arma::mean(X, 0);
    const arma::frowvec vars = arma::var(X, 0);
    const arma::frowvec skews = 
        (arma::mean(arma::pow(X, 3), 0) - 3.0 * means % vars - arma::pow(means, 3)); // / arma::pow(vars, 1.5);
    // Can ignore divisor as we are only interested in the sign
    
    X.each_row() %= arma::sign( skews );
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MM::flipColumnSigns(arma::fmat& X)
{
    // Essentially work with the central third moment (i.e. we are interested 
    // in the shape of the distribution around zero, as that is more useful 
    // than the observed mean for sparse maps)
    const arma::frowvec skews = arma::mean(arma::pow(X, 3), 0); // / arma::pow(arma::mean(arma::pow(X, 2), 0), 1.5);
    // Can ignore divisor as we are only interested in the sign
    
    X.each_row() %= arma::sign( skews );
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
// Cosine similarity
// https://en.wikipedia.org/wiki/Cosine_similarity

arma::fmat MM::calculateCosineSimilarity(const arma::fmat& X)
{
    return calculateCosineSimilarity(X, X);
}

arma::fmat MM::calculateCosineSimilarity(const arma::fmat& X, const arma::fmat& Y)
{
    // Check for size issues
    if (X.n_rows != Y.n_rows) {
        throw std::invalid_argument("Utilities::MiscMaths::calculateCosineSimilarity(X,Y): X & Y must have the same number of rows.");
    }
    
    // Calculate scores and component magnitudes
    const arma::fmat XY = X.t() * Y;
    arma::frowvec x = arma::sum(X % X); x.elem( arma::find(x < 1.0e-10) ).ones(); x = arma::sqrt(x);
    arma::frowvec y = arma::sum(Y % Y); y.elem( arma::find(y < 1.0e-10) ).ones(); y = arma::sqrt(y);
    
    // Combine to form similarity
    arma::fmat similarity = XY;
    similarity.each_col() /= x.t(); similarity.each_row() /= y;
    
    return similarity;
}

////////////////////////////////////////////////////////////////////////////////
// Pair matrix columns based on cosine similarity
// Uses a greedy matching

MM::Pairing MM::pairMatrixColumns(const arma::fmat& X, const arma::fmat& Y)
{
    // Check for size issues
    if (X.n_rows != Y.n_rows) {
        throw std::invalid_argument("Utilities::MiscMaths::calculateCosineSimilarity(X,Y): X & Y must have the same number of rows.");
    }
    
    // Intialise
    arma::fmat similarity = calculateCosineSimilarity(X, Y);
    const unsigned int N = std::min(similarity.n_rows, similarity.n_cols);
    Pairing pairing;
    pairing.inds1 = arma::zeros<arma::uvec>(N);
    pairing.inds2 = arma::zeros<arma::uvec>(N);
    pairing.scores = arma::zeros<arma::fvec>(N);
    
    // Iteratively find the best matches (greedy approach)
    for (unsigned int n = 0; n < N; ++n) {
        // Find the max
        const unsigned int index = arma::abs(similarity).index_max();
        const arma::uvec indices = ind2sub(arma::size(similarity), index);
        
        // Record
        pairing.inds1(n) = indices(0);
        pairing.inds2(n) = indices(1);
        pairing.scores(n) = similarity(index);
        
        // Null the similarities to remove these components for the next iteration
        similarity.row(indices(0)).fill( arma::datum::nan );
        similarity.col(indices(1)).fill( arma::datum::nan );
    }
    
    return pairing;
}

////////////////////////////////////////////////////////////////////////////////

MM::Decomposition MM::calculateVBMatrixFactorisation(const DataTypes::FullRankData data, const unsigned int rank, const FactorisationType factorisationType, const arma::fmat& initialisation, const unsigned int iterations)
{
    Utilities::Timer timer;
    std::cout << std::scientific << std::setprecision(std::numeric_limits<float>::digits10);
    
    ////////////////////////////////////////////////////////////////////////////
    std::cout << "Initialising decomposition..." << std::endl;
    timer.reset();
    
    // Key sizes
    const unsigned int V = data.V;
    const unsigned int T = data.T;
    const unsigned int M = rank;
    
    // Storage for hyperpriors (if we need them)
    std::unique_ptr<Modules::MatrixMean_VBPosterior> tau;           // Means
    std::unique_ptr<Modules::MatrixPrecision_VBPosterior> beta;     // Precisions
    std::unique_ptr<Modules::MembershipProbability_VBPosterior> pi; // Memberships
    
    // First, make a constant spatial model
    MFModels::P::P2C Einit; Einit.P = initialisation;
    Einit.PtP = Einit.P.t() * Einit.P + 0.01 * V * arma::eye<arma::fmat>(M, M);
    std::unique_ptr<MFModels::P_VBPosterior> Pconst = std::make_unique<VBModules::Constant<MFModels::P::C2P, MFModels::P::P2C>>(Einit);
    
    // Make the actual spatial model
    const float dofCorrectionFactor = 1.0;
    std::unique_ptr<MFModels::P_VBPosterior> P;
    //--------------------------------------------------------------------------
    if (factorisationType == FactorisationType::MODES) {
        MFModels::P::IndependentMixtureModels::Parameters PPrior;
        PPrior.V = V; PPrior.M = M; PPrior.randomInitialisation = false;
        // Positive Gaussian for the bulk of the weights, and a 'slab' for large weights
        // Mean of around 1.0 fits with the overall scaling / normalisation goals (i.e. E[p^2] ≈ 1.0, weights account for sparsity)
        // Making things sparser helps reproducibity, but we don't want to move too far from the full model
        // Variance needs to be quite high too, maps seem to have a pretty high dynamic range -> hence slab
        // Somewhat surprisingly, the sparsity doesn't seem to make any material difference to the decomposition
        VBDistributions::GaussianMixtureModel::GaussianParameters posGaussianPrior;
        posGaussianPrior.parameters.mu = 1.0; posGaussianPrior.parameters.sigma2 = std::pow(0.75, 2); posGaussianPrior.probability = 0.125;
        // High variance 'slab'
        VBDistributions::GaussianMixtureModel::GaussianParameters slabPrior;
        slabPrior.parameters.mu = 1.0; slabPrior.parameters.sigma2 = std::pow(2.0, 2); slabPrior.probability = 0.025;
        PPrior.gaussianPriors = {posGaussianPrior, slabPrior};
        // And a spike at zero
        VBDistributions::GaussianMixtureModel::DeltaParameters spikePrior;
        spikePrior.parameters.mu = 0.0; spikePrior.probability = 0.850;
        PPrior.deltaPriors = {spikePrior};
        
        P = std::make_unique<MFModels::P::IndependentMixtureModels>(PPrior, dofCorrectionFactor);
    }
    //--------------------------------------------------------------------------
    else if (factorisationType == FactorisationType::PARCELS) {
        // Mean hyperprior
        Modules::MatrixMeans::P2C Etau; Etau.X = 1.0 * arma::ones<arma::fmat>(V,M); Etau.X2 = 1.0 * arma::ones<arma::fmat>(V,M);
        tau = std::make_unique<VBModules::Constant<Modules::MatrixMeans::C2P, Modules::MatrixMeans::P2C>>(Etau);
        
        // Precison hyperprior
        Modules::MatrixPrecisions::P2C Ebeta; Ebeta.X = 1.0 * arma::ones<arma::fmat>(V,M); Ebeta.logX = arma::log(Ebeta.X);
        beta = std::make_unique<VBModules::Constant<Modules::MatrixPrecisions::C2P, Modules::MatrixPrecisions::P2C>>(Ebeta);
        
        // Membership hyperprior
        Modules::MembershipProbabilities::P2C Epi;
        Epi.p = std::vector<arma::fmat>(M, (1.0 / M) * arma::ones<arma::fmat>(V,1));
        Epi.logP = std::vector<arma::fmat>(M, arma::log((1.0 / M) * arma::ones<arma::fmat>(V,1)));
        pi = std::make_unique<VBModules::Constant<Modules::MembershipProbabilities::C2P, Modules::MembershipProbabilities::P2C>>(Epi);
        
        // Actual model
        MFModels::P::Parcellation::Parameters PPrior;
        PPrior.V = V; PPrior.M = M;
        
        P = std::make_unique<MFModels::P::Parcellation>(tau.get(), beta.get(), pi.get(), PPrior, dofCorrectionFactor);
    }
    //--------------------------------------------------------------------------
    
    // Weights
    MFModels::H::P2C EH; EH.h = arma::ones<arma::fvec>(M); EH.hht = arma::ones<arma::fmat>(M, M);
    std::unique_ptr<MFModels::H_VBPosterior> H = std::make_unique<VBModules::Constant<MFModels::H::C2P, MFModels::H::P2C>>(EH);
    
    // Time courses
    // Precision matrix
    VBModules::PrecisionMatrices::ComponentwiseGammaPrecision::Parameters alphaAPrior;
    alphaAPrior.gammaPrior.a = 1.0e-3; alphaAPrior.gammaPrior.b = 1.0e-3; alphaAPrior.size = M;
    std::unique_ptr<Modules::PrecisionMatrix_VBPosterior> alphaA = std::make_unique<VBModules::PrecisionMatrices::ComponentwiseGammaPrecision>(alphaAPrior);
    // And the time courses themselves    
    MFModels::A::MultivariateNormal::Parameters APrior; APrior.M = M; APrior.T = T;
    std::unique_ptr<MFModels::A_VBPosterior> A = std::make_unique<MFModels::A::MultivariateNormal>(alphaA.get(), APrior);

    //MFModels::A::MultivariateNormal::Parameters APrior; APrior.M = M; APrior.T = T;
    //arma::fmat previousA = arma::zeros<arma::fmat>(APrior.M,APrior.T);
    //std::shared_ptr<Modules::MatrixMean_Parent> sTM_Means;
    //Modules::MatrixMeans::P2C ETimeCourses;
    //ETimeCourses.X = previousA; ETimeCourses.X2 = arma::square(previousA);            
    //sTM_Means = std::make_shared<VBModules::CopyParent<Modules::MatrixMeans::C2P, Modules::MatrixMeans::P2C>>(ETimeCourses); 
    //std::unique_ptr<MFModels::A_VBPosterior> A = std::make_unique<MFModels::A::MultivariateNormal>(alphaA.get(), sTM_Means.get(), APrior);
    
    // Noise
    VBDistributions::Gamma::Parameters psiPrior; psiPrior.a = 1.0; psiPrior.b = 1.0;
    std::unique_ptr<MFModels::Psi_VBPosterior> Psi = std::make_unique<MFModels::Psi::GammaPrecision>(psiPrior);
    
    // And make the MFModel
    std::unique_ptr<MFModel> MFM = std::make_unique<MFModels::FullRankMFModel>(data, Pconst.get(), H.get(), A.get(), Psi.get(), dofCorrectionFactor);
    
    // Convenience lambda to calculate free energy
    auto calculateFreeEnergy = [&] () -> float {
        float F = MFM->getLogLikelihood();
        F -= P->getKL();
        F -= H->getKL();
        F -= A->getKL();
        F -= alphaA->getKL();
        F -= Psi->getKL();
        return F;
    };
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Few updates to make sure things start sensibly
    for (unsigned int z = 0; z < 3; ++z) {
        A->update();
        H->update();
        Psi->update();
    }
    
    // Switch to true spatial model
    MFM->setPModel( P.get() );
    for (unsigned int z = 0; z < 3; ++z) {
        P->update();
        H->update();
        Psi->update();
    }
    
    std::cout << std::endl;
    timer.printTimeElapsed();
    std::cout << std::endl;
    
    ////////////////////////////////////////////////////////////////////////////
    std::cout << "Calculating decomposition..." << std::endl << std::endl;
    timer.reset();
    
    std::cout << "Free energy: " << calculateFreeEnergy() << std::endl << std::endl;
    
    // And get cracking with the updates!
    for (unsigned int z = 0; z < iterations; ++z) {
        Utilities::Timer loopTimer;
        
        // Update!
        P->update();
        H->update();
        A->update();
        alphaA->update();
        Psi->update();
        
        loopTimer.printTimeElapsed("Iteration " + std::to_string(z+1));
        if ( ((z+1) % 25) == 0 ) {
            std::cout << std::endl;
            std::cout << "Summary at end of iteration " << z+1 << std::endl;
            
            std::cout << "P: " << arma::mean(arma::diagvec(P->getExpectations().PtP)) / V << std::endl;
            std::cout << "H: " << arma::mean(H->getExpectations().h) << std::endl;
            std::cout << "A: " << arma::mean(arma::diagvec(A->getExpectations().AAt)) / T << std::endl;
            std::cout << "Ψ: " << 1.0 / std::sqrt(Psi->getExpectations().psi) << std::endl;
            
            std::cout << "F: " << calculateFreeEnergy() << std::endl;
            
            std::cout << std::endl;
        }
    }
    
    // Collate results
    Decomposition decomposition;
    decomposition.P = P->getExpectations().P;
    decomposition.h = H->getExpectations().h;
    decomposition.A = A->getExpectations().A;
    decomposition.Alpha = alphaA->getExpectations().X;
    decomposition.F = calculateFreeEnergy();
    
    
    std::cout << std::endl;
    timer.printTimeElapsed();
    std::cout << std::endl;
    
    // And return the final map means
    return decomposition;
}

////////////////////////////////////////////////////////////////////////////////
