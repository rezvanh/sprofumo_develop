// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "HRFModelling.h"

#include <stdlib.h>
#include <stdexcept>
#include <vector>

#include "spline/spline.h"

#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;

namespace Utils = PROFUMO::Utilities;

////////////////////////////////////////////////////////////////////////////////

arma::fvec Utils::generateHRF(const std::string hrfFile, const unsigned int T, const float TR)
{
    if (TR <= 0.0) { throw std::invalid_argument("Utilities::generateHRF(): TR must be >= 0.0"); }
    
    // Load HRF
    arma::fmat fileContents; const bool loaded = fileContents.load(hrfFile);
    if (!loaded) { throw std::invalid_argument("Utilities::generateHRF(): Unable to load HRF file! " + hrfFile); }
    arma::fvec baseTimePoints = fileContents.col(0);
    arma::fvec baseHRF = fileContents.col(1);
    
    // Now interpolate!
    // Time points need to be sorted and strictly increasing
    arma::uvec inds = arma::find_unique( baseTimePoints );
    baseTimePoints = baseTimePoints(inds); baseHRF = baseHRF(inds);
    inds = arma::sort_index( baseTimePoints, "ascend" );
    baseTimePoints = baseTimePoints(inds); baseHRF = baseHRF(inds);
    // Generate the spline
    tk::spline hrfSpline;
    hrfSpline.set_points(
            arma::conv_to< std::vector<double> >::from( baseTimePoints ),
            arma::conv_to< std::vector<double> >::from( baseHRF )
        );
    
    // Generate the new set of time points
    const arma::fvec timePoints = arma::linspace<arma::fvec>(0.0, TR * (T - 1), T);
    
    // And find the HRF at those time points
    arma::fvec hrf = arma::zeros<arma::fvec>(T);
    for (unsigned int t = 0; t < T; ++t) {
        // If we are within the bounds of the base HRF, interpolate
        if (timePoints(t) >= arma::min(baseTimePoints) && timePoints(t) <= arma::max(baseTimePoints)) {
            hrf(t) = hrfSpline(timePoints(t));
        }
    }
    
    return hrf;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::MFModels::A::KroneckerHRF::Parameters Utils::generateHRFPrior(const std::string hrfFile, const unsigned int T, const float TR, const unsigned int M, const float priorRelaxation, const float posteriorRelaxation)
{
    MFModels::A::KroneckerHRF::Parameters hrfPrior;
    hrfPrior.T = T;
    hrfPrior.M = M;
    
    // Get an HRF
    *hrfPrior.hrf = generateHRF(hrfFile, T, TR);
    const arma::fvec& hrf = *hrfPrior.hrf;
    
    // Calculate the autocorrelation
    const arma::fvec autocorrelation = arma::conv(hrf, arma::flipud(hrf), "full");
    
    // Make the autocorrelation into a *correlation* matrix
    *hrfPrior.K = arma::toeplitz( autocorrelation.tail(hrf.n_elem) / arma::max(autocorrelation) );
    // And add some 'noise' if requested
    *hrfPrior.K *= 1.0 - priorRelaxation;
    *hrfPrior.K += priorRelaxation * arma::eye<arma::fmat>( arma::size(*hrfPrior.K) );
    
    // And get the (well conditioned) eigendecomposition of the covariance
    linalg::eig_sympd(*hrfPrior.L_K, *hrfPrior.U_K, *hrfPrior.K);
    
    // Record the posterior relaxation
    hrfPrior.relaxation = posteriorRelaxation;
    
    return hrfPrior;
}

////////////////////////////////////////////////////////////////////////////////
