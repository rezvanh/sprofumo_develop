// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Some numerically stable implementations of a handful of useful mathematical 
// functions:
//  + sigmoid / logistic
//  + softmax

#ifndef UTILITIES_MISC_MATHS
#define UTILITIES_MISC_MATHS

#include <cmath>
#include <armadillo>
#include <stdexcept>

#include <boost/math/special_functions/gamma.hpp>
#include <boost/math/special_functions/digamma.hpp>
namespace bmath = boost::math;

namespace PROFUMO
{
    namespace Utilities
    {
        namespace MiscMaths
        {
            ////////////////////////////////////////////////////////////////////
            
            // Square: apes arma::square() by replacing std::pow(x,2)
            
            static inline float square(const float x)
            {
                return x * x;
            }
            
            ////////////////////////////////////////////////////////////////////
            
            // Sigmoid: x = 1 / (1 + exp(-x))
            // Special case of the logistic function
            
            static inline float sigmoid(const float x)
            {
                // Truncate particularly small or large values
                // Gives a max error of ~1.0e-10
                if ( x < -25.0 ) {
                    return 0.0;
                }
                else if ( x > 25.0 ) {
                    return 1.0;
                }
                else {
                    // Sigmoid itself!
                    return 1.0 / ( 1.0 + std::exp( -x ) );
                }
            }
            
            // See also:
            // http://www.musicdsp.org/showone.php?id=238
            ////////////////////////////////////////////////////////////////////
            
            // Softmax functions: yi = exp(xi) / sum(exp(xj))
            // Computationally stable version, based on the log(sum(exp())) trick
            // https://en.wikipedia.org/wiki/LogSumExp
            // https://hips.seas.harvard.edu/blog/2013/01/09/computing-log-sum-exp/
                
            static inline arma::fvec softmax(const arma::fvec& x)
            {
                // Calculate exp()
                // Subtract the max - this prevents overflow, which happens for x ~ 1000
                arma::fvec y = arma::exp(x - arma::max(x));
                // Renormalise
                y /= arma::sum(y);
                
                return y;
            }
            
            static inline arma::frowvec softmax(const arma::frowvec& x)
            {
                arma::frowvec y = arma::exp(x - arma::max(x));
                y /= arma::sum(y);
                
                return y;
            }
            
            static inline arma::fmat softmax(const arma::fmat& X, const unsigned int dim = 0)
            {
                if (dim == 0) {
                    // Subtract max and exponentiate
                    arma::fmat Y = X.each_row() - arma::max(X, 0);
                    Y = arma::exp(Y);
                    // Renormalise
                    Y.each_row() /= arma::sum(Y, 0);
                    
                    return Y;
                    // This is equivalent to
                    //return softmax(X.t(), 1).t();
                }
                else if (dim == 1) {
                    // Subtract max and exponentiate
                    arma::fmat Y = X.each_col() - arma::max(X, 1);
                    Y = arma::exp(Y);
                    // Renormalise
                    Y.each_col() /= arma::sum(Y, 1);
                    
                    return Y;
                }
                else {
                    throw std::invalid_argument("Utilities::MiscMaths::softmax(arma::fmat X, uint dim): 'dim' must be 0 or 1.");
                }
            }
            
            ////////////////////////////////////////////////////////////////////
            
            // Multivariate gamma and digamma functions
            // https://en.wikipedia.org/wiki/Multivariate_gamma_function
            
            // z = log(Gamma_N(x))
            static inline float mlgamma(const float x, const unsigned int N)
            {
                float z = N * (N-1) * std::log(M_PI) / 4.0;
                for (unsigned int n = 0; n < N; ++n) {
                    z += bmath::lgamma(x - 0.5 * n);
                }
                
                return z;
            }
            
            // z = digamma_N(x))
            static inline float mdigamma(const float x, const unsigned int N)
            {
                float z = 0.0;
                for (unsigned int n = 0; n < N; ++n) {
                    z += bmath::digamma(x - 0.5 * n);
                }
                
                return z;
            }
            
            ////////////////////////////////////////////////////////////////////
        }
    }
}
#endif
