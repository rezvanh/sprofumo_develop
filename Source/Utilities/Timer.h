// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Simple class which measures CPU and wall time

// http://en.cppreference.com/w/cpp/chrono

#ifndef UTILITIES_TIMER_H
#define UTILITIES_TIMER_H

#include <chrono>
// <chrono> also includes <ctime>
#include <string>

namespace PROFUMO
{
    namespace Utilities
    {
        
        class Timer
        {
        public:
            // Initialises, and starts timing
            Timer();
            
            // Prints out time taken (precision sets decimal places)
            void printTimeElapsed(const unsigned int precision = 2) const;
            void printTimeElapsed(const std::string qualifier, const unsigned int precision = 2) const;
            
            // Restarts timer
            void reset();
            
        protected:
            typedef std::chrono::time_point<std::chrono::high_resolution_clock> WallTime;
            
            // Stores start points
            std::clock_t cpuStart_;
            WallTime wallStart_;
        };
        
    }
}
#endif
