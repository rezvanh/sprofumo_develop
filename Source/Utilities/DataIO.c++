// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "DataIO.h"

#include <vector>
#include <stdexcept>

////////////////////////////////////////////////////////////////////////////////

// Loads some common neuroimaging file formats
arma::fmat PROFUMO::Utilities::loadDataMatrix(const std::string fileName)
{
    // Files that can be loaded:
    // *.nii.gz, *.dtseries.nii: NewNifti
    // *.txt, *.hdf5: Armadillo
    
    ////////////////////////////////////////////////////////////////////////////
    if (hasExtension(fileName, ".nii.gz") || hasExtension(fileName, ".dtseries.nii")) {
        // Set up NIFTI/CIFTI reader
        NiftiIO Reader;
        char* buffer;
        std::vector<NiftiExtension> extensions;
        bool allocateBuffer = true;
        NiftiHeader header;
        
        // Load data
        #pragma omp critical(loadData)
        {
        header = Reader.loadImage(fileName, buffer, extensions, allocateBuffer);
        }
        
        // Now work out how to transform to matrix
        const bool copyAuxMem = false, strict = true;
        arma::fmat tempData;
        
        // NIFTI
        if (hasExtension(fileName, ".nii.gz")) {
            tempData = arma::fmat((float*) buffer, header.dim[1]*header.dim[2]*header.dim[3], header.dim[4], copyAuxMem, strict);
        // CIFTI
        } else if (hasExtension(fileName, ".dtseries.nii")) {
            tempData = arma::fmat((float*) buffer, header.dim[5], header.dim[6], copyAuxMem, strict);
            //arma::fmat tempData = arma::fmat((float*) buffer, header.dim[6], header.dim[5], copyAuxMem, strict);
            tempData = tempData.t();
        }
        // Free the buffer
        delete[] buffer;
        
        // Convert to normal arma::fmat and return
        return arma::conv_to<arma::fmat>::from( tempData );
    }
    ////////////////////////////////////////////////////////////////////////////
    else if (hasExtension(fileName, ".txt") || hasExtension(fileName, ".hdf5") || hasExtension(fileName, ".bin")) {
        // Check for files that Armadillo can load
        arma::fmat dataMatrix;
        #pragma omp critical(loadData)
        {
        #pragma omp critical(HDF5)
        {
        dataMatrix.load( fileName );
        }
        }
        return dataMatrix;
    }
    ////////////////////////////////////////////////////////////////////////////
    else {
        throw std::invalid_argument("Utilities::DataIO: Unrecognised data type!\n    File: " + fileName);
    }
    ////////////////////////////////////////////////////////////////////////////
}

////////////////////////////////////////////////////////////////////////////////

// Checks to see whether or not a file name has a given extension
bool PROFUMO::Utilities::hasExtension(const std::string fileName, const std::string extension)
{
    // Algorithm from http://stackoverflow.com/a/874160 and http://stackoverflow.com/q/9158894
    
    // Have to have a file name longer than the given extension
    if (fileName.length() >= extension.length()) {
        // Extract the ending
        const unsigned int startPos = fileName.length() - extension.length();
        const unsigned int length = extension.length();
        std::string ending = fileName.substr(startPos, length);
        
        // Check for equality
        return (ending == extension);
    } else {
        // If string is too short, can't contain the full extension!
        return false;
    }
}

////////////////////////////////////////////////////////////////////////////////
