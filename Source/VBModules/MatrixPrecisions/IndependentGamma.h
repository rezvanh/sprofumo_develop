// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a matrix of independent gamma distributions.

#ifndef VB_MODULES_MATRIX_PRECISIONS_INDEPENDENT_GAMMA_H
#define VB_MODULES_MATRIX_PRECISIONS_INDEPENDENT_GAMMA_H

#include <string>
#include <vector>
#include <memory>
#include <armadillo>

#include "Module.h"
#include "Posterior.h"
#include "ModuleList.h"
#include "VBDistributions/Gamma.h"

#include "Utilities/DataIO.h"

namespace PROFUMO
{
    namespace VBModules
    {
        namespace MatrixPrecisions
        {
            
            class IndependentGamma :
                public Modules::MatrixPrecision_VBPosterior,
                protected CachedModule<Modules::MatrixPrecisions::P2C>
            {
            public:
                //--------------------------------------------------------------
                struct Parameters {
                public:
                    VBDistributions::Gamma::Parameters gammaPrior;
                    unsigned int nRows, nCols; // Sizes
                };
                //--------------------------------------------------------------
                IndependentGamma(const Parameters prior);
                
                void update(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                float getKL(const bool bigData = false) const;
                
                void save(const std::string directory) const;

                void load(const std::string directory);
                
                arma::fmat signFlip(const arma::fmat spatialSigns);
                
            private:
                // Storage for posteriors
                // These will ape Armadillo's column-major ordering
                std::vector<VBDistributions::Gamma> posteriors_;
                const unsigned int nRows_, nCols_, nElem_;
                
                // Helper functions to collect expectations / set caches
                Modules::MatrixPrecisions::C2P collectExpectations(const bool bigData = false, const int subjectNum = 0) const;
                void updateExpectations();
            };
            
        }
    }
}
#endif
