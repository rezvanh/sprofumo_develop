// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// A generic VBModule that just holds a set of constant expectations.

#ifndef VB_MODULES_CONSTANT_H
#define VB_MODULES_CONSTANT_H

#include <string>
#include <armadillo>

#include "Module.h"
#include "Posterior.h"

namespace PROFUMO
{
    namespace VBModules
    {
        ////////////////////////////////////////////////////////////////////////
        
        template <class C2P, class P2C>
        class Constant :
            public VBPosterior<C2P, P2C>,
            protected CachedModule<P2C>
        {
        public:
            // Construct from a supplied set of expectations
            Constant(const P2C expectations);
            
            // Required functionality
            void update(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
            float getKL(const bool bigData = false) const;
            void save(const std::string directory) const;
            void load(const std::string directory);
            arma::fmat signFlip(const arma::fmat spatialSigns); 
        };
    
        ////////////////////////////////////////////////////////////////////////
        // Constructor

        template <class C2P, class P2C>
        Constant<C2P,P2C>::Constant(const P2C expectations)
        {
            CachedModule<P2C>::expectations_ = expectations;
            return;
        }
    
        ////////////////////////////////////////////////////////////////////////

        template <class C2P, class P2C>
        void Constant<C2P,P2C>::update(const bool bigData, const float posteriorRho, const int subjectNum)
        {
            return;
        }
    
        ////////////////////////////////////////////////////////////////////////

        template <class C2P, class P2C>
        float Constant<C2P,P2C>::getKL(const bool bigData) const
        {
            // Prior = posterior, so KL divergence between the two is zero
            return 0.0;
        }
        
        ////////////////////////////////////////////////////////////////////////

        template <class C2P, class P2C>
        arma::fmat Constant<C2P,P2C>::signFlip(const arma::fmat spatialSigns)
        {
            
            return arma::zeros<arma::fmat>(1,1);
        }
    
        ////////////////////////////////////////////////////////////////////////

        template <class C2P, class P2C>
        void Constant<C2P,P2C>::save(const std::string /*directory*/) const
        {
            // No generic way of saving expectations so can't do anything here
            return;
        }

        ////////////////////////////////////////////////////////////////////////

        template <class C2P, class P2C>
        void Constant<C2P,P2C>::load(const std::string /*directory*/)
        {
            // No generic way of load expectations so can't do anything here
            return;
        }
    
        ////////////////////////////////////////////////////////////////////////
    }
}
#endif
