// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines an arbitrary matrix of independent Dirichlet distributions, useful 
// for modelling membership probabilities in mixture models.

#ifndef VB_MODULES_MEMBERSHIP_PROBABILITIES_INDEPENDENT_DIRICHLET_H
#define VB_MODULES_MEMBERSHIP_PROBABILITIES_INDEPENDENT_DIRICHLET_H

#include <string>
#include <vector>
#include <memory>
#include <armadillo>

#include "Module.h"
#include "Posterior.h"
#include "ModuleList.h"
#include "VBDistributions/Dirichlet.h"

#include "Utilities/DataIO.h"

namespace PROFUMO
{
    namespace VBModules
    {
        namespace MembershipProbabilities
        {
            
            class IndependentDirichlet :
                public Modules::MembershipProbability_VBPosterior,
                protected CachedModule<Modules::MembershipProbabilities::P2C>
            {
            public:
                //--------------------------------------------------------------
                struct Parameters {
                public:
                    VBDistributions::Dirichlet::Parameters dirichletPrior;
                    unsigned int nRows, nCols; // Sizes
                };
                //--------------------------------------------------------------
                IndependentDirichlet(const Parameters prior);
                
                void update(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                float getKL(const bool bigData = false) const;
                
                void save(const std::string directory) const;

                void load(const std::string directory);
                
                arma::fmat signFlip(const arma::fmat spatialSigns);
                
            private:
                // Storage for posteriors
                // These will ape Armadillo's column-major ordering
                std::vector<VBDistributions::Dirichlet> posteriors_;
                const unsigned int nRows_, nCols_, nElem_, nClasses_;
                
                // Helper functions to collect expectations / set caches
                Modules::MembershipProbabilities::C2P collectExpectations(const bool bigData = false, const int subjectNum = 0) const;
                void updateExpectations();
            };
            
        }
    }
}
#endif
