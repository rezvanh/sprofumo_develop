// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "IndependentDirichlet.h"

#include <armadillo>
#include <sys/stat.h>
#include <iomanip>

namespace VBDists = PROFUMO::VBDistributions;
namespace MemberProbs = PROFUMO::Modules::MembershipProbabilities;
namespace VBMemberProbs = PROFUMO::VBModules::MembershipProbabilities;

////////////////////////////////////////////////////////////////////////////////

VBMemberProbs::IndependentDirichlet::IndependentDirichlet(const Parameters prior)
: nRows_(prior.nRows), nCols_(prior.nCols), nElem_(prior.nRows * prior.nCols), nClasses_(prior.dirichletPrior.counts.size())
{
    // Initialise all the posteriors
    for (unsigned int i = 0; i < nElem_; ++i) {
        posteriors_.emplace_back( VBDists::Dirichlet(prior.dirichletPrior) );
    }
    
    updateExpectations();
    
    return;
}


////////////////////////////////////////////////////////////////////////////////

void VBMemberProbs::IndependentDirichlet::update(const bool bigData, const float posteriorRho, const int subjectNum)
{
    // Collect the data from all the children
    const MemberProbs::C2P D = collectExpectations(bigData, subjectNum);
    
    // Pass to the Dirichlet posteriors
    #pragma omp parallel for schedule(dynamic,512)
    for (unsigned int i = 0; i < nElem_; ++i) {
        VBDists::DataExpectations::Dirichlet d;
        d.counts = arma::zeros<arma::fvec>(nClasses_);
        for (unsigned int c = 0; c < nClasses_; ++c) {
            d.counts[c] = D.counts[c][i];
        }
        
        posteriors_[i].update(d,bigData, posteriorRho);
    }
    
    //Gather expectations
    updateExpectations();
}

////////////////////////////////////////////////////////////////////////////////

float VBMemberProbs::IndependentDirichlet::getKL(const bool bigData) const
{
    float KL = 0.0;
    #pragma omp parallel for reduction( + : KL ) schedule(dynamic,512)
    for (unsigned int i = 0; i < nElem_; ++i) {
        KL += posteriors_[i].getKL(bigData);
    }
    
    return KL;
}
////////////////////////////////////////////////////////////////////////////////

arma::fmat VBMemberProbs::IndependentDirichlet::signFlip(const arma::fmat spatialSigns)
{
    return arma::zeros<arma::fmat>(1,1);
}
////////////////////////////////////////////////////////////////////////////////

void VBMemberProbs::IndependentDirichlet::save(const std::string directory) const
{
    // Save cached expectations for each component in turn
    const unsigned int padWidth = ((unsigned int) std::floor(std::log10(nClasses_))) + 1;
    for (unsigned int c = 0; c < nClasses_; ++c) {
        
        //Get zero padded class number
        std::ostringstream classNumber;
        classNumber << std::setw( padWidth ) << std::setfill( '0' ) << c+1;
        
        // Make directory
        const std::string classDir = directory + "Class_" + classNumber.str() + "/";
        mkdir( classDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        
        // Save!
        std::string fileName;
        
        // P
        fileName = classDir + "Probabilities.hdf5";
        expectations_.p[c].save(fileName, arma::hdf5_binary);
        
        // logP
        fileName = classDir + "LogProbabilities.hdf5";
        expectations_.logP[c].save(fileName, arma::hdf5_binary);
        
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBMemberProbs::IndependentDirichlet::load(const std::string directory)
{
    // load expectations for each component in turn
    const unsigned int padWidth = ((unsigned int) std::floor(std::log10(nClasses_))) + 1;
    for (unsigned int c = 0; c < nClasses_; ++c) {
        
        //Get zero padded class number
        std::ostringstream classNumber;
        classNumber << std::setw( padWidth ) << std::setfill( '0' ) << c+1;
        
        // directory name
        const std::string classDir = directory + "Class_" + classNumber.str() + "/";
        
        // Load!
        std::string fileName;
        
        // P
        fileName = classDir + "Probabilities.hdf5";
        expectations_.p[c] = Utilities::loadDataMatrix(fileName);
        
        // logP
        fileName = classDir + "LogProbabilities.hdf5";
        expectations_.logP[c] = Utilities::loadDataMatrix(fileName);
        
    }
    
    return;
}


////////////////////////////////////////////////////////////////////////////////

MemberProbs::C2P VBMemberProbs::IndependentDirichlet::collectExpectations(const bool bigData, const int subjectNum) const
{
    // Group-level model so parallelise collection
    
    // Intialise expectations
    MemberProbs::C2P D;
    D.counts = std::vector<arma::fmat> (nClasses_, arma::zeros<arma::fmat>(nRows_,nCols_));
    
    // Switch based on number of children
    if (childModules_.size() >= 100) {
        // If we have a lot of children, such that we can expect each thread 
        // to be used lots of times, recreate a reduction block.
        // Each thread has a set of local expectations, and these are all added 
        // together at the end. This parallelises both the expectation 
        // gathering and the accumulation into one set of expectations. 
        
        #pragma omp parallel
        {
        // Give each thread a local set of expectations
        MemberProbs::C2P D_local;
        D_local.counts = std::vector<arma::fmat> (nClasses_, arma::zeros<arma::fmat>(nRows_,nCols_));
        
        // Loop over all children, collecting expectations
        for (listType::const_iterator child = childModules_.begin(); child != childModules_.end(); ++child) {
            #pragma omp single nowait
            {
            const MemberProbs::C2P d = (*child)->getExpectations();
            for (unsigned int c = 0; c < nClasses_; ++c) {
                D_local.counts[c] += d.counts[c];
            }
            }
        }
        
        // Now collect all local expectations
        #pragma omp critical
        {
        for (unsigned int c = 0; c < nClasses_; ++c) {
            D.counts[c] += D_local.counts[c];
        }
        }
        }
        
    }
    else {
        // If we don't have many children then the overhead of assigning each 
        // thread its own expectations is proably too high (accumulation is 
        // relatively cheap compared to instantiation). This therefore is a 
        // more conventional method for parallelisation where the expectation 
        // gathering is sped up, but the accumulation takes the same amount 
        // of time.  
        
        #pragma omp parallel
        {
        for (listType::const_iterator child = childModules_.begin(); child != childModules_.end(); ++child) {
            #pragma omp single nowait
            {
            const MemberProbs::C2P d = (*child)->getExpectations();
            #pragma omp critical
            {
            for (unsigned int c = 0; c < nClasses_; ++c) {
                D.counts[c] += d.counts[c];
            }
            }
            }
        }
        }
        
    }
    
    
    if (bigData) {
        for (unsigned int c = 0; c < nClasses_; ++c) {
            D.counts[c] = ((float)subjectNum / (float)childModules_.size()) * D.counts[c];                
        }
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////

void VBMemberProbs::IndependentDirichlet::updateExpectations()
{
    // Intialise expectations
    expectations_.p = std::vector<arma::fmat>(nClasses_, arma::zeros<arma::fmat>(nRows_,nCols_));
    expectations_.logP = std::vector<arma::fmat>(nClasses_, arma::zeros<arma::fmat>(nRows_,nCols_));
    
    // And collect from posteriors
    #pragma omp parallel for schedule(dynamic,512)
    for (unsigned int i = 0; i < nElem_; ++i) {
        const VBDists::Expectations::Dirichlet e = posteriors_[i].getExpectations();
        
        for (unsigned int c = 0; c < nClasses_; ++c) {
            expectations_.p[c][i]    = e.p[c];
            expectations_.logP[c][i] = e.log_p[c];
        }
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
