// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "IndependentMeans.h"

#include <armadillo>

namespace VBDists = PROFUMO::VBDistributions;
namespace MatMeans = PROFUMO::Modules::MatrixMeans;
namespace VBMatMeans = PROFUMO::VBModules::MatrixMeans;
namespace GMMComps = PROFUMO::VBDistributions::GaussianMixtureModelComponents;

/////////////////////////////////////////////////////////////////////////////////
// Gaussian version

VBMatMeans::IndependentMeans::IndependentMeans(const GaussianParameters prior)
: nRows_(prior.nRows), nCols_(prior.nCols), nElem_(prior.nRows * prior.nCols)
{
    // Initialise all the posteriors
    for (unsigned int i = 0; i < nElem_; ++i) {
        posteriors_.emplace_back( std::make_unique<VBDists::Gaussian>(prior.gaussianPrior, prior.randomInitialisation) );
    }
    
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
// Mixture model version

VBMatMeans::IndependentMeans::IndependentMeans(const MixtureModelParameters prior)
: nRows_(prior.nRows), nCols_(prior.nCols), nElem_(prior.nRows * prior.nCols)
{
    // Initialise all the posteriors
    for (unsigned int i = 0; i < nElem_; ++i) {
        posteriors_.emplace_back( std::make_unique<VBDists::GaussianMixtureModel>(prior.gaussianPriors, prior.deltaPriors, prior.randomInitialisation) );
    }
    
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBMatMeans::IndependentMeans::update(const bool bigData, const float posteriorRho, const int subjectNum)
{
    // Collect the data from all the children
    const MatMeans::C2P D = collectExpectations(bigData, subjectNum);
    // Pass to the Gaussian posteriors
    #pragma omp parallel for schedule(dynamic,512)
    for (unsigned int i = 0; i < nElem_; ++i) {
        VBDists::DataExpectations::Gaussian d;
        d.psi = D.Psi[i]; d.psi_d = D.PsiD[i];
        
        posteriors_[i]->update(d, bigData, posteriorRho);
    }
    
    // Gather expectations
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

float VBMatMeans::IndependentMeans::getKL(const bool bigData) const
{
    float KL = 0.0;
    #pragma omp parallel for reduction( + : KL ) schedule(dynamic,512)
    for (unsigned int i = 0; i < nElem_; ++i) {
        KL += posteriors_[i]->getKL(bigData);
    }
    
    return KL;
}
////////////////////////////////////////////////////////////////////////////////

arma::fmat VBMatMeans::IndependentMeans::signFlip(const arma::fmat spatialSigns)
{
    if (expectations_.X.n_cols==1){
        expectations_.X %= arma::sign( spatialSigns.t() );
    } else {
        expectations_.X.each_row() %= arma::sign( spatialSigns );
    }
    
    return spatialSigns;
}
////////////////////////////////////////////////////////////////////////////////

void VBMatMeans::IndependentMeans::save(const std::string directory) const
{
    // Just save cached expectations
    std::string fileName;
    
    // Means
    fileName = directory + "Means.hdf5";
    expectations_.X.save(fileName, arma::hdf5_binary);
    
    // Variances
    fileName = directory + "Variances.hdf5";
    expectations_.X2.save(fileName, arma::hdf5_binary);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBMatMeans::IndependentMeans::load(const std::string directory)
{
    // Just save cached expectations
    std::string fileName;
    
    // Means
    fileName = directory + "Means.hdf5";
    arma::fmat expectations_X;
    expectations_X = Utilities::loadDataMatrix(fileName);
    expectations_.X = expectations_X;
    
    
    // Variances
    fileName = directory + "Variances.hdf5";
    arma::fmat expectations_X2;
    expectations_X2 =  Utilities::loadDataMatrix(fileName);
    expectations_.X2 = expectations_X2;
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

MatMeans::C2P VBMatMeans::IndependentMeans::collectExpectations(const bool bigData, const int subjectNum) const
{
    // Group-level model so parallelise collection
    
    // Intialise expectations
    MatMeans::C2P D;
    D.Psi = arma::zeros<arma::fmat>(nRows_,nCols_);
    D.PsiD = arma::zeros<arma::fmat>(nRows_,nCols_);
    // Switch based on number of children
    if (childModules_.size() >= 100) {
        // If we have a lot of children, such that we can expect each thread 
        // to be used lots of times, recreate a reduction block.
        // Each thread has a set of local expectations, and these are all added 
        // together at the end. This parallelises both the expectation 
        // gathering and the accumulation into one set of expectations. 
        
        #pragma omp parallel
        {
        // Give each thread a local set of expectations
        MatMeans::C2P D_local;
        D_local.Psi = arma::zeros<arma::fmat>(nRows_,nCols_);
        D_local.PsiD = arma::zeros<arma::fmat>(nRows_,nCols_);
        
        // Loop over all children, collecting expectations
        for (listType::const_iterator child = childModules_.begin(); child != childModules_.end(); ++child) {
            #pragma omp single nowait
            {
            const MatMeans::C2P d = (*child)->getExpectations();
            D_local.Psi += d.Psi;
            D_local.PsiD += d.PsiD;
            }
        }
        
        // Now collect all local expectations
        #pragma omp critical
        {
        D.Psi += D_local.Psi;
        D.PsiD += D_local.PsiD;
        }
        }
    }
    else {
        // If we don't have many children then the overhead of assigning each 
        // thread its own expectations is proably too high (accumulation is 
        // relatively cheap compared to instantiation). This therefore is a 
        // more conventional method for parallelisation where the expectation 
        // gathering is sped up, but the accumulation takes the same amount 
        // of time.  
        
        #pragma omp parallel
        {
        for (listType::const_iterator child = childModules_.begin(); child != childModules_.end(); ++child) {
            #pragma omp single nowait
            {            
            const MatMeans::C2P d = (*child)->getExpectations();
            #pragma omp critical
            {
            D.Psi += d.Psi;
            D.PsiD += d.PsiD;
            }
            }
        }
        }
    }
    //if stochastic PFMs, scale D as if expectations were calculated based on N subjects (entire population)
    //rather than M subjects (batch size)
    if (bigData) {
        D.Psi = ((float)subjectNum / (float)childModules_.size()) * D.Psi;
        D.PsiD = ((float)subjectNum / (float)childModules_.size()) * D.PsiD;    
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////

void VBMatMeans::IndependentMeans::updateExpectations()
{
    // Intialise expectations
    expectations_.X = arma::zeros<arma::fmat>(nRows_,nCols_);
    expectations_.X2 = arma::zeros<arma::fmat>(nRows_,nCols_);
    
    // And collect from posteriors
    #pragma omp parallel for schedule(dynamic,512)
    for (unsigned int i = 0; i < nElem_; ++i) {
        const VBDists::Expectations::Gaussian e = posteriors_[i]->getExpectations();
        
        expectations_.X[i] = e.x;
        expectations_.X2[i] = e.x2;
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
