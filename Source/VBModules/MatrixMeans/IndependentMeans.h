// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// A matrix of independent distributions - either univariate Gaussians or 
// mixture models.

#ifndef VB_MODULES_MATRIX_MEANS_INDEPENDENT_MEANS_H
#define VB_MODULES_MATRIX_MEANS_INDEPENDENT_MEANS_H

#include <string>
#include <vector>
#include <memory>
#include <armadillo>


#include "Module.h"
#include "Posterior.h"
#include "ModuleList.h"
#include "VBDistributions/Gaussian.h"
#include "VBDistributions/GaussianMixtureModel.h"

#include "Utilities/DataIO.h"

namespace PROFUMO
{
    namespace VBModules
    {
        namespace MatrixMeans
        {
            
            class IndependentMeans :
                public Modules::MatrixMean_VBPosterior,
                protected CachedModule<Modules::MatrixMeans::P2C>
            {
            public:
                // Makes a set of Gaussians
                //--------------------------------------------------------------
                struct GaussianParameters {
                public:
                    VBDistributions::Gaussian::Parameters gaussianPrior;
                    unsigned int nRows, nCols; // Sizes
                    bool randomInitialisation = true; // Initialisation
                };
                //--------------------------------------------------------------
                IndependentMeans(const GaussianParameters prior);
                
                // Makes a set of mixture models
                //--------------------------------------------------------------
                struct MixtureModelParameters {
                public:
                    std::vector<VBDistributions::GaussianMixtureModel::GaussianParameters> gaussianPriors;
                    std::vector<VBDistributions::GaussianMixtureModel::DeltaParameters> deltaPriors;
                    unsigned int nRows, nCols; // Sizes
                    bool randomInitialisation = true; // Initialisation
                };
                //--------------------------------------------------------------
                IndependentMeans(const MixtureModelParameters prior);
                
                void update(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                float getKL(const bool bigData = false) const;
                
                void save(const std::string directory) const;

                void load(const std::string directory);
                
                arma::fmat signFlip(const arma::fmat spatialSigns);
            private:
                // Storage for posteriors
                // These will ape Armadillo's column-major ordering
                std::vector<std::unique_ptr< VBDistribution<
                    VBDistributions::DataExpectations::Gaussian, 
                    VBDistributions::Expectations::Gaussian>
                >> posteriors_;
                const unsigned int nRows_, nCols_, nElem_;
                
                // Helper functions to collect expectations / set caches
                Modules::MatrixMeans::C2P collectExpectations(const bool bigData = false, const int subjectNum = 0) const;
                void updateExpectations();
                
            };
            
            typedef IndependentMeans IndependentGaussian;
            typedef IndependentMeans IndependentMixtureModels;
            
        }
    }
}
#endif
