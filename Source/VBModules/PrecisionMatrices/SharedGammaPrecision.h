// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Precision matrix model - just the identity scaled by a gamma distribution.

#ifndef VB_MODULES_PRECISION_MATRICES_SHARED_GAMMA_PRECISION_H
#define VB_MODULES_PRECISION_MATRICES_SHARED_GAMMA_PRECISION_H

#include <iostream>
#include <string>
#include <armadillo>
#include <memory>
#include <vector>

#include "Module.h"
#include "Posterior.h"
#include "ModuleList.h"

#include "VBDistributions/Gamma.h"

#include "Utilities/DataIO.h"

namespace PROFUMO
{
    namespace VBModules
    {
        namespace PrecisionMatrices
        {
            
            class SharedGammaPrecision :
                public Modules::PrecisionMatrix_VBPosterior,
                protected CachedModule<Modules::PrecisionMatrices::P2C>
            {
            public:
                //--------------------------------------------------------------
                struct Parameters {
                public:
                    VBDistributions::Gamma::Parameters gammaPrior;
                    unsigned int size;
                };
                //--------------------------------------------------------------
                SharedGammaPrecision(const Parameters prior, const bool preInitialised = false, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = nullptr);
                
                void update(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                float getKL(const bool bigData = false) const;
                
                void save(const std::string directory) const;

                void load(const std::string directory);
                
                arma::fmat signFlip(const arma::fmat spatialSigns);
                
            private:
                VBDistributions::Gamma posterior_;
                const unsigned int size_; // size_ x size_ precision matrix
                
                // Helper functions to collect expectations / set caches
                Modules::PrecisionMatrices::C2P collectExpectations(const bool bigData = false, const int subjectNum = 0) const;
                void updateExpectations(const bool preInitialised = false, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = nullptr);
            };
            
        }
    }
}
#endif
