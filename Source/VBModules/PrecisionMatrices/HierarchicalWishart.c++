// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "HierarchicalWishart.h"

#include <cmath>
#include <fstream>
#include <memory>

#include "Utilities/MiscMaths.h"
namespace miscmaths = PROFUMO::Utilities::MiscMaths;
#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;

namespace Mods = PROFUMO::Modules;
namespace PrecMats = PROFUMO::Modules::PrecisionMatrices;
namespace VBPrecMats = PROFUMO::VBModules::PrecisionMatrices;

// Resolve calls to internally cached expectations
typedef PROFUMO::CachedModule<PrecMats::C2P> C2PCache;
typedef PROFUMO::CachedModule<PrecMats::P2C> P2CCache;

////////////////////////////////////////////////////////////////////////////////

VBPrecMats::HierarchicalWishart::HierarchicalWishart(Mods::PrecisionMatrix_Parent* B, const Parameters prior, const bool preInitialised, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation)
: Mods::PrecisionMatrix_Child(B), prior_(prior), size_(prior.size)
{
    // Set initial params to prior  
    if (preInitialised){
        std::vector<arma::fmat> initialisationMatrices = *expectationInitialisation;
        posterior_.a = initialisationMatrices[2].at(0);
        posterior_.B = initialisationMatrices[3];
    } else {
        posterior_.a = prior_.a;
        posterior_.B = parentModule_->getExpectations().X;
    }
        
    updateExpectations(preInitialised, expectationInitialisation);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBPrecMats::HierarchicalWishart::update(const bool bigData, const float posteriorRho, const int subjectNum)
{
    // Collect expectations from all linked models
    const PrecMats::C2P D = collectExpectations(bigData, subjectNum);
    
    // Update posterior parameters
    if (bigData) {        
        interimPosterior_.a = prior_.a + D.N;
        interimPosterior_.B = parentModule_->getExpectations().X + D.XXt;
        float previousA = posterior_.a;     // Shape
        arma::fmat previousB = posterior_.B;  // Rate
        // posterior_.a = prior_.a + (posteriorRho * D.N) + ((1.0 - posteriorRho) * previousA);
        posterior_.a = (posteriorRho * (prior_.a + D.N)) + ((1.0 - posteriorRho) * previousA);
        // posterior_.B = parentModule_->getExpectations().X + (posteriorRho * D.XXt) + ((1.0 - posteriorRho) * previousB); 
        posterior_.B = (posteriorRho * (parentModule_->getExpectations().X + D.XXt)) + ((1.0 - posteriorRho) * previousB);   
    } else {
        posterior_.a = prior_.a + D.N;
        posterior_.B = parentModule_->getExpectations().X + D.XXt;
    
    }
    // Gather expectations
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

float VBPrecMats::HierarchicalWishart::getKL(const bool bigData) const
{
    float KL;
    if (bigData) {
        // Get expectations from parent
        const PrecMats::P2C prior_B = parentModule_->getExpectations();        
        // KL divergence between prior and posterior
        KL = 0.5 * prior_.a * (linalg::log_det_sympd(interimPosterior_.B) - prior_B.logDetX);
        KL += 0.5 * interimPosterior_.a * (arma::trace(prior_B.X * linalg::inv_sympd(interimPosterior_.B)) - size_);
        KL += 0.5 * (interimPosterior_.a - prior_.a) * miscmaths::mdigamma(interimPosterior_.a / 2.0, size_);
        KL -= miscmaths::mlgamma(interimPosterior_.a / 2.0, size_);
        KL += miscmaths::mlgamma(prior_.a / 2.0, size_);
        
    } else {
        // Get expectations from parent
        const PrecMats::P2C prior_B = parentModule_->getExpectations();        
        // KL divergence between prior and posterior
        KL = 0.5 * prior_.a * (linalg::log_det_sympd(posterior_.B) - prior_B.logDetX);
        KL += 0.5 * posterior_.a * (arma::trace(prior_B.X * linalg::inv_sympd(posterior_.B)) - size_);
        KL += 0.5 * (posterior_.a - prior_.a) * miscmaths::mdigamma(posterior_.a / 2.0, size_);
        KL -= miscmaths::mlgamma(posterior_.a / 2.0, size_);
        KL += miscmaths::mlgamma(prior_.a / 2.0, size_);
    }
    
    
    return KL;
}
////////////////////////////////////////////////////////////////////////////////

arma::fmat VBPrecMats::HierarchicalWishart::signFlip(const arma::fmat spatialSigns)
{
    //expectations_.X.each_row() %= arma::sign( spatialSigns );    
    P2CCache::expectations_.X.each_row() %= arma::sign( spatialSigns );
    P2CCache::expectations_.X.each_col() %= arma::sign( spatialSigns.t() );
    
    return spatialSigns;
}
////////////////////////////////////////////////////////////////////////////////

void VBPrecMats::HierarchicalWishart::save(const std::string directory) const
{
    // Just save cached expectations
    std::string fileName;
    
    // Mean
    fileName = directory + "Mean.hdf5";
    P2CCache::expectations_.X.save(fileName, arma::hdf5_binary);
    
    // logDet
    fileName = directory + "logDeterminant.txt";
    std::ofstream file; file.open( fileName.c_str() );
    file << P2CCache::expectations_.logDetX;
    file.close();
    
    // posterior a (RF: I added this here to use for subject initialisation later on)
    fileName = directory + "posteriorA.txt";
    std::ofstream fileA; fileA.open( fileName.c_str() );    
    fileA << "a: " << posterior_.a << std::endl;    
    fileA.close();
    
    // Mean
    fileName = directory + "posteriorB.hdf5";
    posterior_.B.save(fileName, arma::hdf5_binary);
    
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBPrecMats::HierarchicalWishart::load(const std::string directory)
{
    // Load expectations
    std::string fileName;
    
    // Mean
    fileName = directory + "Mean.hdf5";
    arma::fmat expectations_X;
    expectations_X = Utilities::loadDataMatrix(fileName);
    P2CCache::expectations_.X = expectations_X;
    
    // logDet
    fileName = directory + "logDeterminant.txt";
    std::ifstream file(fileName.c_str());
    float prior_ld;    
    file >> prior_ld;
    // arma::fvec ldPrior(1);
    // ldPrior.at(0) = prior_ld;
    P2CCache::expectations_.logDetX = prior_ld;
    file.close();

    // posterior a (RF: I added this here to use for subject initialisation later on)
    fileName = directory + "posteriorA.txt"; 
    std::ifstream fileA(fileName.c_str());
    std::string astr;
    float prior_a;    
    fileA >> astr >> prior_a;
    // arma::fvec agPrior(1);
    // agPrior.at(0) = prior_a;
    posterior_.a = prior_a; 
    fileA.close();   
    
    // Mean
    fileName = directory + "posteriorB.hdf5";
    arma::fmat posterior_B;
    posterior_B = Utilities::loadDataMatrix(fileName);
    posterior_.B = posterior_B;
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

PrecMats::C2P VBPrecMats::HierarchicalWishart::collectExpectations(const bool bigData, const int subjectNum) const
{
    // Can be either group or subject level, and super quick anyway, so not
    // worth parallelising as yet
    
    PrecMats::C2P D;
    if (childModules_.empty()) {
        // All zero if no children
        D.N = 0.0;
        D.XXt = arma::zeros<arma::fmat>(size_,size_);
    }
    else {
        // Initialise store from first MF model
        listType::const_iterator child = childModules_.begin();
        D = (*child)->getExpectations();
        
        // Add other models if necessary
        for (++child; child != childModules_.end(); ++child) {
            const PrecMats::C2P d = (*child)->getExpectations();
            D.N += d.N;
            D.XXt += d.XXt;
        }
    }
    
    //if stochastic PFMs, scale D as if expectations were calculated based on N subjects (entire population)
    //rather than M subjects (batch size)
    if (bigData) {
        D.N = ((float)subjectNum / (float)childModules_.size()) * D.N;
        D.XXt = ((float)subjectNum / (float)childModules_.size()) * D.XXt;    
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////

void VBPrecMats::HierarchicalWishart::updateExpectations(const bool preInitialised, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation)
{
    // Expectations as a parent
    arma::fmat& X = P2CCache::expectations_.X;
    float& logDetX = P2CCache::expectations_.logDetX;
    if (preInitialised) {
        std::vector<arma::fmat> initialisationMatrices = *expectationInitialisation;
        logDetX = initialisationMatrices[1].at(0);
        X = initialisationMatrices[0];
    } else {        
        // Mean
        X = posterior_.a * linalg::inv_sympd(posterior_.B);
        
        // logDet
        logDetX  = miscmaths::mdigamma(posterior_.a / 2.0, size_);
        logDetX -= linalg::log_det_sympd(posterior_.B);
        logDetX += size_ * std::log(2.0);
    }
    
    // Expectations as a child
    arma::fmat& XXt = C2PCache::expectations_.XXt;
    float& N = C2PCache::expectations_.N;
    
    XXt = X;
    N = prior_.a;
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
/*
import math, numpy, matplotlib, matplotlib.pyplot as plt

# Use gamma distributions for this example, but trivial to extend to Wishart
# x ~ N(0, alpha^-1)
# alpha ~ G(ak, beta)
# beta ~ G(a, b)

N = 5      # Observations
xxt = 2.0  # Sum of squares of observations
K = 3      # Classes / subjects / components etc
# Priors
ak = 2.0   # Equivalent to 'ak' observations
a = 2.0    # Equivalent to 'a' classes
b = 1.0    # Prior prec ≈ ak / (a / b) ≈ (ak / a) * b

# Update rules
print("Update rules: alpha, beta")
alpha = beta = 5.0
print(alpha, beta)
for n in range(10):
    alpha = (ak + N) / (beta + xxt)
    beta = (a + K*ak) / (b + K*alpha)
    print(alpha, beta)
print()

print("Observation std: {:.3f}".format(math.sqrt(xxt / N)))
print("Prior std: {:.3f}".format(math.sqrt(a / (ak * b))))
print("Inferred std: {:.3f}".format(math.sqrt(1.0 / alpha)))


# We can also solve these equations exactly, which gives
def solve(N, xxt, K, ak, a, b):
    # We get a quadratic expression for alpha
    a1 = K * xxt
    b1 = a - K * N + b * xxt
    c1 = - b * (ak + N)
    alpha = (-b1 + numpy.sqrt(b1**2 - 4 * a1 * c1)) / (2 * a1)
    
    # And then the standard update rule for beta
    beta = (a + K*ak) / (b + K*alpha)
    
    return alpha, beta

alpha, beta = solve(N, xxt, K, ak, a, b)
print(alpha, beta)
print("Exact std: {:.3f}".format(math.sqrt(1.0 / alpha)))


# Note that the update rules also represent the natural gradient of the system
A, B = numpy.meshgrid(numpy.linspace(0.0,5.0,20), numpy.linspace(0.0,5.0,20))
dA = (ak + N) / (B + xxt) - A
dB = (a + K*ak) / (b + K*A) - B
plt.quiver(A,B,dA,dB)
plt.xlabel("alpha"); plt.ylabel("beta"); plt.title("Natural gradient")
plt.show()
*/
////////////////////////////////////////////////////////////////////////////////
