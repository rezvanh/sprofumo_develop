// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "ComponentwiseGammaPrecision.h"

#include <armadillo>
#include <fstream>

namespace VBDists = PROFUMO::VBDistributions;
namespace PrecMats = PROFUMO::Modules::PrecisionMatrices;
namespace VBPrecMats = PROFUMO::VBModules::PrecisionMatrices;

////////////////////////////////////////////////////////////////////////////////

VBPrecMats::ComponentwiseGammaPrecision::ComponentwiseGammaPrecision(const Parameters prior, const bool preInitialised, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation)
: size_(prior.size)
{
    posterior_ = std::vector<VBDists::Gamma>(size_, VBDists::Gamma(prior.gammaPrior));
    
    updateExpectations(preInitialised, expectationInitialisation);
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBPrecMats::ComponentwiseGammaPrecision::update(const bool bigData, const float posteriorRho, const int subjectNum)
{
    // Collect expectations from all linked models
    const PrecMats::C2P D = collectExpectations(bigData, subjectNum);
    
    // Pass to the gamma posteriors
    for (unsigned int i = 0; i < size_; ++i) {
        VBDists::DataExpectations::Gamma d;
        d.n = D.N; d.d2 = D.XXt(i,i);
        posterior_[i].update(d, bigData, posteriorRho);
    }
    
    // Gather expectations
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

float VBPrecMats::ComponentwiseGammaPrecision::getKL(const bool bigData) const
{
    float KL = 0.0;
    for (unsigned int i = 0; i < size_; ++i) {
        KL += posterior_[i].getKL(bigData);
    }
    
    return KL;
}
////////////////////////////////////////////////////////////////////////////////

arma::fmat VBPrecMats::ComponentwiseGammaPrecision::signFlip(const arma::fmat spatialSigns)
{
    //expectations_.X.each_row() %= arma::sign( spatialSigns );    
    expectations_.X.each_row() %= arma::sign( spatialSigns );
    expectations_.X.each_col() %= arma::sign( spatialSigns.t() );
    
    return spatialSigns;
}
////////////////////////////////////////////////////////////////////////////////

void VBPrecMats::ComponentwiseGammaPrecision::save(const std::string directory) const
{
    // Just save cached expectations
    std::string fileName;
    
    // Mean
    fileName = directory + "Mean.hdf5";
    expectations_.X.save(fileName, arma::hdf5_binary);
    
    // logDet
    fileName = directory + "logDeterminant.txt";
    std::ofstream file; file.open( fileName.c_str() );
    file << expectations_.logDetX;
    file.close();
    
    // And just save the gamma distribution too, why the hell not
    // for (unsigned int i = 0; i < size_; ++i) {
    //     posterior_[i].save(directory + "GammaPosterior_component"+std::to_string(i)+".txt");
    // }
    
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBPrecMats::ComponentwiseGammaPrecision::load(const std::string directory)
{
    // Just save cached expectations
    std::string fileName;
    
    // Mean
    fileName = directory + "Mean.hdf5";
    arma::fmat expectations_X;
    expectations_X = Utilities::loadDataMatrix(fileName);
    expectations_.X = expectations_X;
    
    // logDet
    fileName = directory + "logDeterminant.txt";
    std::ifstream file(fileName.c_str());
    float prior_ld;    
    file >> prior_ld;
    // arma::fvec ldPrior(1);
    // ldPrior.at(0) = prior_ld;
    expectations_.logDetX = prior_ld;
    file.close();
    
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

PrecMats::C2P VBPrecMats::ComponentwiseGammaPrecision::collectExpectations(const bool bigData, const int subjectNum) const
{
    // Can be either group or subject level, and super quick anyway, so not
    // worth parallelising as yet
    
    PrecMats::C2P D;
    if (childModules_.empty()) {
        // All zero if no children
        D.N = 0.0;
        D.XXt = arma::zeros<arma::fmat>(size_,size_);
    }
    else {
        // Initialise store from first MF model
        listType::const_iterator child = childModules_.begin();
        D = (*child)->getExpectations();
        
        // Add other models if necessary
        for (++child; child != childModules_.end(); ++child) {
            const PrecMats::C2P d = (*child)->getExpectations();
            D.N += d.N;
            D.XXt += d.XXt;
        }
    }
    
    //if stochastic PFMs, scale D as if expectations were calculated based on N subjects (entire population)
    //rather than M subjects (batch size)
    if (bigData) {
        D.N = ((float)subjectNum / (float)childModules_.size()) * D.N;
        D.XXt = ((float)subjectNum / (float)childModules_.size()) * D.XXt;    
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////

void VBPrecMats::ComponentwiseGammaPrecision::updateExpectations(const bool preInitialised, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation)
{
    if (preInitialised) {
        std::vector<arma::fmat> initialisationMatrices = *expectationInitialisation;
        expectations_.logDetX = initialisationMatrices[1].at(0);
        expectations_.X = initialisationMatrices[0];
    } else {
        expectations_.X = arma::zeros<arma::fmat>(size_, size_);
        expectations_.logDetX = 0.0;        
    
        for (unsigned int i = 0; i < size_; ++i) {
            const VBDists::Expectations::Gamma e = posterior_[i].getExpectations();            
            expectations_.X(i,i) = e.x;
            expectations_.logDetX += e.log_x;
        }
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
