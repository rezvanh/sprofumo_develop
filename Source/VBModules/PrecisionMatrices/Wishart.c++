// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "Wishart.h"

#include <cmath>
#include <fstream>

#include "Utilities/MiscMaths.h"
namespace miscmaths = PROFUMO::Utilities::MiscMaths;
#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;

namespace PrecMats = PROFUMO::Modules::PrecisionMatrices;
namespace VBPrecMats = PROFUMO::VBModules::PrecisionMatrices;

////////////////////////////////////////////////////////////////////////////////

VBPrecMats::Wishart::Wishart(const Parameters prior, const bool preInitialised, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation)
: prior_(prior), size_(prior.B.n_rows)
{
    // Set initial params to prior
    
    posterior_ = prior_;
        
    updateExpectations(preInitialised, expectationInitialisation);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBPrecMats::Wishart::update(const bool bigData, const float posteriorRho, const int subjectNum)
{
    // Collect expectations from all linked models
    const PrecMats::C2P D = collectExpectations(bigData, subjectNum);
    
    // Update posterior parameters
    if (bigData) {
        interimPosterior_.a = prior_.a + D.N;
        interimPosterior_.B = prior_.B + D.XXt;
        float previousA = posterior_.a;     // Shape
        arma::fmat previousB = posterior_.B;  // Rate
        // posterior_.a = prior_.a + (posteriorRho * D.N) + ((1.0 - posteriorRho) * previousA);
        posterior_.a = (posteriorRho * (prior_.a + D.N)) + ((1.0 - posteriorRho) * previousA);
        // posterior_.B = prior_.B + (posteriorRho * D.XXt) + ((1.0 - posteriorRho) * previousB);
        posterior_.B = (posteriorRho * (prior_.B + D.XXt)) + ((1.0 - posteriorRho) * previousB);  
    
    } else {
        posterior_.a = prior_.a + D.N;
        posterior_.B = prior_.B + D.XXt;
    
    }
    
    // Gather expectations
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

float VBPrecMats::Wishart::getKL(const bool bigData) const
{
    float KL;
    if (bigData) {
        // KL divergence between prior and posterior
        KL = 0.5 * prior_.a * (linalg::log_det_sympd(interimPosterior_.B) - linalg::log_det_sympd(prior_.B));
        KL += 0.5 * interimPosterior_.a * (arma::trace(prior_.B * linalg::inv_sympd(interimPosterior_.B)) - size_);
        KL += 0.5 * (interimPosterior_.a - prior_.a) * miscmaths::mdigamma(interimPosterior_.a / 2.0, size_);
        KL -= miscmaths::mlgamma(interimPosterior_.a / 2.0, size_);
        KL += miscmaths::mlgamma(prior_.a / 2.0, size_);
        
    } else {
        // KL divergence between prior and posterior
        KL = 0.5 * prior_.a * (linalg::log_det_sympd(posterior_.B) - linalg::log_det_sympd(prior_.B));
        KL += 0.5 * posterior_.a * (arma::trace(prior_.B * linalg::inv_sympd(posterior_.B)) - size_);
        KL += 0.5 * (posterior_.a - prior_.a) * miscmaths::mdigamma(posterior_.a / 2.0, size_);
        KL -= miscmaths::mlgamma(posterior_.a / 2.0, size_);
        KL += miscmaths::mlgamma(prior_.a / 2.0, size_);
    }
    
    
    return KL;
}
////////////////////////////////////////////////////////////////////////////////

arma::fmat VBPrecMats::Wishart::signFlip(const arma::fmat spatialSigns)
{
    //expectations_.X.each_row() %= arma::sign( spatialSigns );    
    expectations_.X.each_row() %= arma::sign( spatialSigns );
    expectations_.X.each_col() %= arma::sign( spatialSigns.t() );
    
    return spatialSigns;
}
////////////////////////////////////////////////////////////////////////////////

void VBPrecMats::Wishart::save(const std::string directory) const
{
    // Just save cached expectations
    std::string fileName;
    
    // Mean
    fileName = directory + "Mean.hdf5";
    expectations_.X.save(fileName, arma::hdf5_binary);
    
    // logDet
    fileName = directory + "logDeterminant.txt";
    std::ofstream file; file.open( fileName.c_str() );
    file << expectations_.logDetX;
    file.close();
    
    // posterior a (RF: I added this here to use for subject initialisation later on, in case of big Data)
    fileName = directory + "posteriorA.txt";
    std::ofstream fileA; fileA.open( fileName.c_str() );    
    fileA << "a: " << posterior_.a << std::endl;    
    fileA.close();
    
    // Mean
    arma::fmat posteriorB = posterior_.B;
    fileName = directory + "posteriorB.hdf5";
    posteriorB.save(fileName, arma::hdf5_binary);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBPrecMats::Wishart::load(const std::string directory)
{
    // Load expectations
    std::string fileName;
    
    // Mean
    fileName = directory + "Mean.hdf5";
    arma::fmat expectations_X;
    expectations_X = Utilities::loadDataMatrix(fileName);
    expectations_.X = expectations_X;
    
    // logDet
    fileName = directory + "logDeterminant.txt";
    std::ifstream file(fileName.c_str());
    float prior_ld;    
    file >> prior_ld;
    // arma::fvec ldPrior(1);
    // ldPrior.at(0) = prior_ld;
    expectations_.logDetX = prior_ld;
    file.close();
    
    // posterior a (RF: I added this here to use for subject initialisation later on, in case of big Data)
    fileName = directory + "posteriorA.txt";
    std::ifstream fileA(fileName.c_str());
    std::string astr;
    float prior_a;    
    fileA >> astr >> prior_a;
    // arma::fvec agPrior(1);
    // agPrior.at(0) = prior_a;
    posterior_.a = prior_a; 
    fileA.close(); 
    
    // Mean
    fileName = directory + "posteriorB.hdf5";
    arma::fmat posterior_B;
    posterior_B = Utilities::loadDataMatrix(fileName);
    posterior_.B = posterior_B;
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

PrecMats::C2P VBPrecMats::Wishart::collectExpectations(const bool bigData, const int subjectNum) const
{
    // Can be either group or subject level, and super quick anyway, so not
    // worth parallelising as yet
    
    PrecMats::C2P D;
    if (childModules_.empty()) {
        // All zero if no children
        D.N = 0.0;
        D.XXt = arma::zeros<arma::fmat>(size_,size_);
    }
    else {
        // Initialise store from first MF model
        listType::const_iterator child = childModules_.begin();
        D = (*child)->getExpectations();
        
        // Add other models if necessary
        for (++child; child != childModules_.end(); ++child) {
            const PrecMats::C2P d = (*child)->getExpectations();
            D.N += d.N;
            D.XXt += d.XXt;
        }
    }
    
    //if stochastic PFMs, scale D as if expectations were calculated based on N subjects (entire population)
    //rather than M subjects (batch size)
    if (bigData) {
        D.N = ((float)subjectNum / (float)childModules_.size()) * D.N;
        D.XXt = ((float)subjectNum / (float)childModules_.size()) * D.XXt;    
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////

void VBPrecMats::Wishart::updateExpectations(const bool preInitialised, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation)
{
    if (preInitialised) {
        std::vector<arma::fmat> initialisationMatrices = *expectationInitialisation;
        expectations_.logDetX = initialisationMatrices[1].at(0);
        expectations_.X = initialisationMatrices[0];
    } else {
        expectations_.X = posterior_.a * linalg::inv_sympd(posterior_.B);
    
        expectations_.logDetX  = miscmaths::mdigamma(posterior_.a / 2.0, size_);
        expectations_.logDetX -= linalg::log_det_sympd(posterior_.B);
        expectations_.logDetX += size_ * std::log(2.0);  
    }
      
    return;
}

////////////////////////////////////////////////////////////////////////////////
