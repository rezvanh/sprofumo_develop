// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Definitions of different reprentations of data

////////////////////////////////////////////////////////////////////////////////
// DATA IS IMMUTABLE!

// These data classes try and mimic Armadillo matrices, but in a way that 
// means the data can't be modified when it is out in the wild.

// See the end of the file for a discussion of this approach

// Immutability:
// http://benlast.livejournal.com/30085.htmlhttp://benlast.livejournal.com/30085.html
// http://stackoverflow.com/q/4136156
// http://forums.codeguru.com/showthread.php?499183-Why-does-std-vector-require-its-elements-to-be-assignable

// Operator overloading
// http://courses.cms.caltech.edu/cs11/material/cpp/donnie/cpp-ops.html
// http://stackoverflow.com/q/4421706
// http://stackoverflow.com/q/4172722
// http://stackoverflow.com/q/3279543
// http://pages.cs.wisc.edu/~hasti/cs368/CppTutorial/NOTES/CLASSES-PTRS.html
// http://www.gotw.ca/gotw/023.htm
// http://www.cplusplus.com/articles/y8hv0pDG/
// http://stackoverflow.com/q/5608556
////////////////////////////////////////////////////////////////////////////////

#ifndef DATA_TYPES_H
#define DATA_TYPES_H

#include <armadillo>
#include <memory>

namespace PROFUMO
{
    namespace DataTypes
    {
        ////////////////////////////////////////////////////////////////////////
        
        // Full rank data: just a const pointer to an Armadillo matrix
        class FullRankData
        {
        public:
            // Data itself
            const std::shared_ptr<const arma::fmat> D;
            // Sizes: D is [voxels (V) x time points (T)]
            const unsigned int V, T;
            
            // Constructors
            FullRankData(const std::shared_ptr<const arma::fmat> D);
            FullRankData(const arma::fmat& D);
            
            // Overload assignment operator
            FullRankData& operator=(const FullRankData &rhs);
        };
        
        // Note this can't really be a unique_ptr(), though perhaps it should be
        //  + Makes it a pain in containers
        //  + Would break the assigment operator
        
        ////////////////////////////////////////////////////////////////////////
        
        // Low rank data: D = Pd * Ad
        class LowRankData
        {
        public:
            // Smaller matrices
            const std::shared_ptr<const arma::fmat> Pd;
            const std::shared_ptr<const arma::fmat> Ad;
            // Rank of approximation
            const unsigned int N;
            // Sizes: D is [voxels (V) x time points (T)]
            const unsigned int V, T;
            // trace(Dt * D): computed for full D, not low rank version 
            const float TrDtD;
            
            // Constructor
            LowRankData(const std::shared_ptr<const arma::fmat> Pd, const std::shared_ptr<const arma::fmat> Ad, const unsigned int N, const float TrDtD);
            
            // Destructor - not needed with shared pointers managing everything
            //~LowRankData();
            
            // Copy constructor - default is fine with shared pointers
            //LowRankData(const LowRankData &L);
            
            // Overload assignment operator
            LowRankData& operator=(const LowRankData &rhs);
        };
        
        ////////////////////////////////////////////////////////////////////////
    }
}
#endif



#if 0
////////////////////////////////////////////////////////////////////////////////
// In C++, a variable is something (i.e. a type) and has a state

int i = 5; // "i" is an int. Its state is "5"
i = 6; // Now its state is "6" (it's still an int ;-p)


const int j = 7; // "j" is also an int. Its state is "6". But good luck changing it...
/* j = 8; */ // Fuck off

////////////////////////////////////////////////////////////////////////////////
// How does this extend to classes?

class Foo
{
public:
    int i;
    Foo(int i) : i(i) {};
}

Foo foo(5); // "foo" is a Foo. Its state is captured by "foo.i = 5"
foo.i = 6; // Now "foo.i = 6"

const Foo foo_c(7); // "foo_c" is a Foo. Its state is captured by "foo_c.i = 7"
/* foo_c.i = 8; */ // Fuck off
// At this level, we can't change anything about foo_c

////////////////////////////////////////////////////////////////////////////////
// And const classes

class Bar
{
public:
    int i;
    const int j;
    Bar(int i, int j) : i(i), j(j) {};
}

Bar bar(1,2); // "bar" is a Bar. Its state is captured by "bar.i = 1; bar.j = 2"
bar.i = 3;
/* bar.j = 4; */ // Fuck off
// So the difference between a Foo and a Bar is the way we can modify the state
bar = Bar(3,4); // But at this level (i.e. above the scope of the class) there's no
// reason why we can't change the state of bar - we'll just have to do it differently

const Bar bar_c(5,6); // "bar_c" is a Bar. Its state is captured by "bar_c.i = 5; bar_c.j = 6"
/* bar_c.i = 7; */ // Fuck off
/* bar_c.j = 8; */ // Fuck off
/* bar_c = Bar(7,8); */ // Fuck off
// In this case, const means we can't change the state, however clever we are
// because the const-ness at this level doesn't allow it

// Essentially, classes have an internal const-ness (what can change in their 
// state) and an external const-ness (whether as used the program expects them 
// to change).

// The last complication is copy constructors / assignment operators
// Because Bar has const members C++ doesn't know how to copy assign it
// We have to do that!

////////////////////////////////////////////////////////////////////////////////
// So what does this mean for us?

// Note that it's more standard C++ to have private members and get/set functions
// But as armadillo doesn't do that, in this case we'll stick with their format
// They even provide arma::access::rw() for the nitty-gritty

// What the above declarations do is provide interfaces to data, but in a way 
// that never lets it be changed! Once a data set is in the wild, you can fuck 
// off if you think you're going to mess with it.

// But that doesn't mean code can't switch between different data sets (i.e. 
// we're happy with data being fixed but copy assignable) e.g.

// Implicit const
Data d = getSomeData()
// do something
d = getSomeNewData()
d.member += 1  // ERROR!!!
// etc

// Explicit const
const Data d = getSomeData()
// do something
d = getSomeNewData() // ERROR!!!
// etc

// In the first case, the data is read-only but at this level isn't explicitly 
// const, there's nothing telling us that it can't change. In the latter, the 
// data is explicitly const *at this level* and a such we know we can't change 
// it from the outset. However, we implement the assignment operator so that 
// the internal const-ness doesn't impinge on the external declaration.

////////////////////////////////////////////////////////////////////////////////
#endif
