// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Class that loads the full set of subject data.

#ifndef DATA_LOADER_H
#define DATA_LOADER_H

#include <vector>
#include <map>
#include <omp.h>
#include <algorithm>
#include <armadillo>
#include <memory>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>    // std::ostringstream
#include <sys/stat.h>
#include <stdexcept>
#include <nlohmann/json.hpp>
using json = nlohmann::json;

#include "Utilities/DataIO.h"
#include "Utilities/DataNormalisation.h"
#include "Utilities/RandomSVD.h"

#include "SubjectModelling/Run.h"
#include "SubjectModelling/Subject.h"

namespace PROFUMO
{
    ////////////////////////////////////////////////////////////////////////////

    // Class which contains a method that transforms the data
    // Need this as a separate class because we can't call virtual functions
    // in the constructor
    // See https://isocpp.org/wiki/faq/strange-inheritance#calling-virtuals-from-ctor-idiom
    template <class D>
    class DataTransformer
    {
    public:
        // Turns a set of data matrices into the requested form
        virtual std::map<RunID, D> transformSubjectData(const std::map<RunID, std::shared_ptr<const arma::fmat>>& subjectDataMatrices, const SubjectID subjectID, const bool BD, const std::string bigDataDir) const = 0;
        //virtual std::map<RunID, D> transformBatchSubjectData(const std::map<RunID, std::string>& runInfo) const = 0; //, const SubjectID subjectID, const std::string bigDataDir
        virtual ~DataTransformer() = default;

    };

    template <class D>
    class SaveTransformedData
    {
    public:
        // Turns a set of data matrices into the requested form
        virtual void saveTransformedSubjectData(const std::map<RunID, D> transformedSubjectData, const SubjectID subjectID, const bool BD, const std::string bigDataDir) const = 0;
        //virtual std::map<RunID, D> transformBatchSubjectData(const std::map<RunID, std::string>& runInfo) const = 0; //, const SubjectID subjectID, const std::string bigDataDir
        virtual ~SaveTransformedData() = default;

    };

    template <class D>
    class LoadTransformedBatch
    {
    public:
        // Turns a set of data matrices into the requested form
        //virtual std::map<RunID, D> transformSubjectData(const std::map<RunID, std::shared_ptr<const arma::fmat>>& subjectDataMatrices, const SubjectID subjectID, const bool BD, const std::string bigDataDir) const = 0;
        virtual std::map<RunID, D> transformBatchSubjectData(const std::map<RunID, std::string>& runInfo) const = 0; //, const SubjectID subjectID, const std::string bigDataDir
        virtual ~LoadTransformedBatch() = default;

    };

    ////////////////////////////////////////////////////////////////////////////

    template <class D>
    class DataLoader
    {
    public:
        // Constructor: loads all the data
        DataLoader(const std::string dataLocationsFile, const std::string outputDirectory, const unsigned int M, std::unique_ptr<DataTransformer<D>> dataTransformer, std::unique_ptr<SaveTransformedData<D>> saveTransformed, std::unique_ptr<LoadTransformedBatch<D>> transformedBatch, std::shared_ptr<const arma::uvec> maskInds = nullptr, const bool normaliseData = true, const bool BD = false, const bool batchLoader = false, const std::string bigDataDir= "", const std::vector<std::string> subNamesList= {""});

        // Computes the spatial basis for all the data (i.e. U * diag(s) from an SVD)
        virtual arma::fmat computeSpatialBasis(const unsigned int K, const bool BD, std::string bigDataDir) const = 0;

        // Get the data!
        std::map<SubjectID, std::map<RunID, D>> getData() const;
        arma::fmat getSpatialBasisMIGP() const;
        std::map<SubjectID, std::map<RunID, std::string>> getDataLoc() const {return dataLocations;}
        // Loads the subject information from a file
        std::map<SubjectID, std::map<RunID, std::string>> loadDataLocations(const std::string dataLocationsFile);
        std::map<SubjectID, std::map<RunID, std::string>> loadBatchDataLocations(const std::string dataLocationsFile, const std::vector<std::string> subNamesList, const std::string bigDataDir);

        // Get key info
        unsigned int getS() const {return S_;}
        unsigned int getV() const {return V_;}
        arma::uvec getT() const {return T_;}
        std::vector<SubjectID> getSubjectList() const {return subjectList;}

        virtual ~DataLoader() = default;



    protected:
        // Converts data matrices to new representation
        const std::unique_ptr<DataTransformer<D>> dataTransformer_;
        const std::unique_ptr<LoadTransformedBatch<D>> transformedBatch_;
        const std::unique_ptr<SaveTransformedData<D>> saveTransformed_;

        // Data store
        const std::string dataLocationsFile_;
        std::map<SubjectID, std::map<RunID, D>> dataStore_;
        std::map<SubjectID, std::map<RunID, std::string>> dataLocations;
        std::vector<SubjectID> subjectList;
        //std::map<RunID, std::shared_ptr<const arma::fmat>> subjectDataMatrices;
        //const std::map<RunID, D> subjectData;


        // Key sizes
        unsigned int S_, V_ = 0; // Subjects, voxels
        arma::uvec T_; // Time points (can be different across runs)


        // Size of subspace for noise correction
        const unsigned int M_;

        // Mask
        const bool useMask_;
        const std::shared_ptr<const arma::uvec> maskInds_;

        // for big data loading
        arma::fmat wMAT_MIGP;

        // Loads data, records key info, masks etc
        arma::fmat loadDataMatrix(const std::string dataLocation);
        arma::fmat computeSpatialBasisMIGP(const arma::fmat concatSubData, const unsigned int reducedDimension, const arma::fmat previousWMAT, const bool lastSub);


        // Few basic pre-processing steps
        void preprocessDataMatrix(arma::fmat& data, const bool normaliseData, const std::string outputBaseName);
    };

    ////////////////////////////////////////////////////////////////////////////
    // Constructor

    template <class D>
    DataLoader<D>::DataLoader(const std::string dataLocationsFile, const std::string outputDirectory, const unsigned int M, std::unique_ptr<DataTransformer<D>> dataTransformer, std::unique_ptr<SaveTransformedData<D>> saveTransformed, std::unique_ptr<LoadTransformedBatch<D>> transformedBatch, std::shared_ptr<const arma::uvec> maskInds, const bool normaliseData, const bool BD, const bool batchLoader, const std::string bigDataDir, const std::vector<std::string> subNamesList)
    : dataTransformer_(std::move(dataTransformer)), saveTransformed_(std::move(saveTransformed)), transformedBatch_(std::move(transformedBatch)), dataLocationsFile_(dataLocationsFile), M_(M), useMask_(maskInds != nullptr), maskInds_(maskInds)
    {
        // Get IDs and data locations from spec file
        if (batchLoader) {
            //std::cout << "batchloader!" <<std::endl;
            dataLocations = loadBatchDataLocations(dataLocationsFile_, subNamesList, bigDataDir);

        } else {
            //std::cout << "No batchloader!" <<std::endl;
            dataLocations = loadDataLocations(dataLocationsFile_);//loadBatchDataLocations(dataLocationsFile_, v, outputDirectory);
        }

        // Collect SubjectIDs (easier for parallelising than a map)
        //std::vector<SubjectID> subjectList;
        for (const auto& subject : dataLocations) {
            subjectList.emplace_back(subject.first);
        }
        //arma::fmat wMAT_MIGP;

        // Container for raw subject data
        if (!batchLoader) {
            // Loop over subjects
            #pragma omp parallel for schedule(dynamic)
            for (unsigned int s = 0; s < subjectList.size(); ++s) { //add random subject order for migp?
                const SubjectID subjectID = subjectList[s];

                // Make directory for preprocessing output
                const std::string subjectDirectory = outputDirectory + subjectID + "/";
                mkdir( subjectDirectory.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

                std::map<RunID, std::shared_ptr<const arma::fmat>> subjectDataMatrices;

                // Load each run
                for (const auto& run : dataLocations.at(subjectID)) {
                    const RunID runID = run.first;
                    const std::string dataLocation = run.second;

                    // Get base filename for everything we save
                    const std::string baseName = subjectDirectory + runID;

                    // Get the data

                    //if (!batchLoader) {
                    // Save the data location

                    std::ofstream fileDataLoc;
                    fileDataLoc.open( (baseName + "_DataLocation.txt").c_str() );
                    fileDataLoc << dataLocation << std::endl;
                    fileDataLoc.close();


                    std::shared_ptr<arma::fmat> runDataMatrix =
                        std::make_shared<arma::fmat>( loadDataMatrix(dataLocation) );

                    // Pre-process
                    preprocessDataMatrix( *runDataMatrix, normaliseData, baseName );

                    //} else {
                    //    arma::fmat runDataMatrixPre;
                    //    runDataMatrixPre.load(dataLocation, arma::hdf5_binary);
                    //    runDataMatrix = std::make_shared<arma::fmat>( runDataMatrixPre);
                        //V_ = dataMatrix.n_rows;

                    //}
                    // Store
                    subjectDataMatrices.emplace(runID, runDataMatrix);

                }


                // Turn into the form we want
                const std::map<RunID, D> subjectData = dataTransformer_->transformSubjectData( subjectDataMatrices, subjectID, BD, bigDataDir );

                #pragma omp critical(HDF5)
                {
                if (!BD) {

                    dataStore_.emplace(subjectID, subjectData);

                } else {
                    const std::map<RunID, D> subjectData1=subjectData;
                    saveTransformed_->saveTransformedSubjectData( subjectData1, subjectID, BD, bigDataDir );
                }
                }
            }
        } else {
            dataStore_.clear();
            #pragma omp parallel for schedule(dynamic)
            for (unsigned int s = 0; s < subjectList.size(); ++s) { //add random subject order for migp?
                const SubjectID subjectID = subjectList[s];
                std::map<RunID, std::string> subjectDataPaths;
                for (const auto& run : dataLocations.at(subjectID)) {
                    const RunID runID = run.first;
                    const std::string dataLocation = run.second;
                    subjectDataPaths.emplace(runID, dataLocation);
                }
                const std::map<RunID, D> subjectData = transformedBatch_->transformBatchSubjectData( subjectDataPaths ); //, subjectID, bigDataDir
                #pragma omp critical(HDF5)
                {
                dataStore_.emplace(subjectID, subjectData);

                }

            }
        }

        // Set key sizes
        S_ = subjectList.size();
        unsigned int R_ = 0;
        for (const auto& subject : dataLocations) {
            R_ += subject.second.size();
        }
        T_ = arma::sort(T_);

        // If we are usng a mask, update the number of voxels to reflect this
        if (useMask_) {
            V_ = maskInds_->n_elem;
        }
        if (BD && !batchLoader) {
            arma::fvec V_S_R = { static_cast<float>(V_),
                                 static_cast<float>(S_),
                                 static_cast<float>(R_) };

            V_S_R.save(bigDataDir + "N_VSR.hdf5", arma::hdf5_binary);
            T_.save(bigDataDir + "N_T.hdf5", arma::hdf5_binary);
        }

        if (BD && batchLoader) {
            arma::fvec V_S_R;
            V_S_R.load(bigDataDir + "N_VSR.hdf5", arma::hdf5_binary);
            V_ = V_S_R.at(0); //arma::conv_to<int>::  //arma::from(N_TrDtD[0]);
            T_.load(bigDataDir + "N_T.hdf5", arma::hdf5_binary);
        }
        // And print a summary
        std::cout << std::endl;
        std::cout << "## Data summary ##" << std::endl;
        std::cout << "Subjects:   " << S_ << std::endl;
        std::cout << "Runs:       " << R_ << std::endl;
        std::cout << "Voxels:     " << V_ << std::endl;
        std::cout << "Timepoints: ";
        for (unsigned int i = 0; i < T_.size(); ++i) {
            std::cout << T_[i];
            if (i != T_.size() - 1) {std::cout << ", ";}
        }

        std::cout << std::endl;

        return;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Get the data!

    template <class D>
    std::map<SubjectID, std::map<RunID, D>> DataLoader<D>::getData() const
    {
        return dataStore_;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Get spatial basis!

    template <class D>
    arma::fmat DataLoader<D>::getSpatialBasisMIGP() const
    {
        return wMAT_MIGP;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Get data locations from spec file

    template <class D>
    std::map<SubjectID, std::map<RunID, std::string>> DataLoader<D>::loadDataLocations(const std::string dataLocationsFile)
    {
        std::map<SubjectID, std::map<RunID, std::string>> dataLocationStore;

        try {
            // Parse file into JSON object
            json dataLocations2;
            std::ifstream dataLocationsFile2; dataLocationsFile2.open(dataLocationsFile_);
            dataLocations2 = json::parse(dataLocationsFile2);
            dataLocationsFile2.close();


            // Check we've actually got an object we can work with...
            if ( !dataLocations2.is_object() || dataLocations2.empty() ) {
                throw std::invalid_argument("Could not find base JSON object in data locations file! Note the file cannot be set up as an array (i.e. it must use { } instead of [ ]).");
            }

            // Loop over subjects
            for (json::const_iterator subject = dataLocations2.begin(); subject != dataLocations2.end(); ++subject) {
                const SubjectID subjectID = subject.key();
                // Sanity checks
                if ( !subject.value().is_object() || subject.value().empty() ) {
                    std::ostringstream message; message << "Could not find run locations for subject \"" << subjectID << "\"! Note the runs cannot be set up as an array (i.e. they must use { } instead of [ ]).";
                    throw std::invalid_argument( message.str() );
                }
                // Duplicate keys - I think the parser silently ignores these for now...
                else if (dataLocationStore.count(subjectID) != 0) {
                    std::ostringstream message; message << "Found two entries for subject \"" << subjectID << "\"!";
                    throw std::invalid_argument( message.str() );
                }

                // And then process the runs
                std::map<RunID, std::string> subjectLocations;
                for (json::const_iterator run = subject.value().begin(); run != subject.value().end(); ++run) {
                    const RunID runID = run.key();
                    // More sanity checks
                    if ( !run.value().is_string() ) {
                        std::ostringstream message; message << "Could not find valid file name for run \"" << runID << "\" in subject \"" << subjectID << "\"!";
                        throw std::invalid_argument( message.str() );
                    }
                    else if (subjectLocations.count(runID) != 0) {
                        std::ostringstream message; message << "Found two entries for run \"" << runID << "\" in subject \"" << subjectID << "\"!";
                        throw std::invalid_argument( message.str() );
                    }
                    // Yay!
                    const std::string fileName = run.value();
                    subjectLocations.emplace(runID, fileName);
                }
                // Record everything
                dataLocationStore.emplace(subjectID, subjectLocations);
            }
        }
        catch (std::invalid_argument error) {
            std::ostringstream message; message << "Error while parsing file \"" << dataLocationsFile_ << "\".\n    " << error.what();
            throw std::invalid_argument( message.str() );
        }

        return dataLocationStore;
    }

////////////////////////////////////////////////////////////////////////////
    // Get batch data locations from spec file

    template <class D>
    std::map<SubjectID, std::map<RunID, std::string>> DataLoader<D>::loadBatchDataLocations(const std::string dataLocationsFile, const std::vector<std::string> subNamesList, const std::string bigDataDir)
    {
        std::map<SubjectID, std::map<RunID, std::string>> dataLocationStore;

        try {
            // Parse file into JSON object
            json dataLocations2;

            std::ifstream dataLocationsFile2; dataLocationsFile2.open(dataLocationsFile_);
            dataLocations2 = json::parse(dataLocationsFile2);
            dataLocationsFile2.close();


            // Check we've actually got an object we can work with...
            if ( !dataLocations2.is_object() || dataLocations2.empty() ) {
                throw std::invalid_argument("Could not find base JSON object in data locations file! Note the file cannot be set up as an array (i.e. it must use { } instead of [ ]).");
            }

            // Loop over subjects
            for (json::const_iterator subject = dataLocations2.begin(); subject != dataLocations2.end(); ++subject) {
                const SubjectID subjectID = subject.key();
                std::string subjectName = subjectID;
                if (std::find(subNamesList.begin(), subNamesList.end(), subjectName) != subNamesList.end())
                {
                    // Sanity checks
                    if ( !subject.value().is_object() || subject.value().empty() ) {
                        std::ostringstream message; message << "Could not find run locations for subject \"" << subjectID << "\"! Note the runs cannot be set up as an array (i.e. they must use { } instead of [ ]).";
                        throw std::invalid_argument( message.str() );
                    }
                    // Duplicate keys - I think the parser silently ignores these for now...
                    else if (dataLocationStore.count(subjectID) != 0) {
                        std::ostringstream message; message << "Found two entries for subject \"" << subjectID << "\"!";
                        throw std::invalid_argument( message.str() );
                    }

                    // And then process the runs
                    std::map<RunID, std::string> subjectLocations;
                    for (json::const_iterator run = subject.value().begin(); run != subject.value().end(); ++run) {
                        const RunID runID = run.key();
                        // More sanity checks
                        if ( !run.value().is_string() ) {
                            std::ostringstream message; message << "Could not find valid file name for run \"" << runID << "\" in subject \"" << subjectID << "\"!";
                            throw std::invalid_argument( message.str() );
                        }
                        else if (subjectLocations.count(runID) != 0) {
                            std::ostringstream message; message << "Found two entries for run \"" << runID << "\" in subject \"" << subjectID << "\"!";
                            throw std::invalid_argument( message.str() );
                        }
                        // Yay!
                        const std::string fileName = bigDataDir + "Subjects" + "/" + subjectID + "/" + "Runs" + "/" + runID + "/"; // + "RunPCA.hdf5";//run.value();
                        subjectLocations.emplace(runID, fileName);
                    }
                    // Record everything
                    dataLocationStore.emplace(subjectID, subjectLocations);
                }
            }
        }
        catch (std::invalid_argument error) {
            std::ostringstream message; message << "Error while parsing file \"" << dataLocationsFile_ << "\".\n    " << error.what();
            throw std::invalid_argument( message.str() );
        }

        return dataLocationStore;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Pre-process data

    template <class D>
    void DataLoader<D>::preprocessDataMatrix(arma::fmat& data, const bool normaliseData, const std::string outputBaseName)
    {
        namespace DN = PROFUMO::Utilities::DataNormalisation;
        // Demean
        arma::fvec means = DN::demeanRows(data);
        #pragma omp critical(HDF5)
        {
            means.save(outputBaseName + "_Means.hdf5", arma::hdf5_binary);
        }

        // Normalise variances, if requested
        if (normaliseData) {
            // Generative model assumes noise variance is the same in all voxels
            // And we probably want each scan to have a roughly similar amount of signal
            arma::fvec norm = arma::ones<arma::fvec>(data.n_rows);
            norm *= DN::normaliseGlobally(data);     // Just gets things in the right ballpark
            norm %= DN::normaliseRows(data, 1.0e-5); // If the voxelwise std is less than 1.0e-5, safer to ignore (probably e.g. mask issues)
            norm %= DN::normaliseNoiseSubspaceRows(data, M_, 1.0e-5); // Likewise (things are v. unlikely to be pure signal)
            norm *= DN::normaliseSignalSubspaceGlobally(data, M_);
            //DN::normaliseRowsAB(data);
            // Finally, apply a correction for the number of modes.
            data *= std::sqrt(M_); norm *= std::sqrt(M_);
            // The penultimate step sets the subspace of the top M SVD
            // components to  be unit variance. However, the generative model
            // is D = P * A, so D_ij = sum_m P_im A_km. Therefore, assuming
            // independence between P and A, the elementwise relationship is:
            //   E[d^2] = M * E[p^2] * E[a^2]
            // If, for example, we have p,a ~ N(0,1) then E[d^2] = M. In
            // essence, by applying this correction, we set each modes subspace
            // to be unit variance, rather than the combination thereof.
            arma::fvec means2 = DN::demeanRows(data);
            // Save normalisation to file
            #pragma omp critical(HDF5)
            {
                norm.save(outputBaseName + "_Normalisation.hdf5", arma::hdf5_binary);
            }
        }

        return;
    }

    ////////////////////////////////////////////////////////////////////////////

    template <class D>
    arma::fmat DataLoader<D>::loadDataMatrix(const std::string dataLocation)
    {
        // Get the data!
        const arma::fmat dataMatrix = Utilities::loadDataMatrix( dataLocation );

        // Check size
        #pragma omp critical(checkSize)
        {
        // Check voxels
        if ( V_ != 0 ) {
            if ( dataMatrix.n_rows != V_ ) {
                std::stringstream errorMessage;
                errorMessage << "All data must have the same number of voxels.\n";
                errorMessage << dataLocation << "\n";
                errorMessage << "New: " << dataMatrix.n_rows << "; Old: " << V_ << "\n";
                throw std::runtime_error( errorMessage.str() );
            }
        }
        else { V_ = dataMatrix.n_rows; }

        // And time points
        if ( not arma::any(T_ == dataMatrix.n_cols) ) {
            T_ = arma::join_vert(T_, arma::uvec{dataMatrix.n_cols});
        }
        }

        // Mask, if required
        if (useMask_) {
            return dataMatrix.rows(*maskInds_);
        }
        else {
            return dataMatrix;
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    template <class D>
    arma::fmat DataLoader<D>::computeSpatialBasisMIGP(const arma::fmat concatSubData, const unsigned int reducedDimension, const arma::fmat previousWMAT, const bool lastSub)
    {
        // Make sure we were given some data!

        if (concatSubData.is_empty()) {
            throw std::invalid_argument("DataLoader<D>::computeSpatialBasisMIGP: No data provided!");
        }

        if (!previousWMAT.is_empty()){
            const unsigned int M = concatSubData.n_rows; //num voxels
            if (previousWMAT.n_cols != M) {
                throw std::invalid_argument("DataLoader<D>::computeSpatialBasisMIGP: Matrices have different numbers of rows- i.e. voxels!");
            }
        }

        ////////////////////////////////////////////////////////////////////////////
        // Now implement the code below for MIGP:
        // r = randperm(s); randomise the subject order
        // W = Y(r(1)); first random subject- W is t by v
        // for i=2:ss
        //  W=[W;Y{r(i))]; accumulate two datasets along the columns (i.e. time points)
        //  [U,D] = eigs(W*W', t*2-1);
        //  W = U'*W;
        // end
        // W = W(1:n,:)

        // NOTE! transpose D[i] so it'll be t x v

        arma::fmat Wmat = previousWMAT;//D[Rperm[0]]->t(); // first random subject- W is t by v
        unsigned int thist;
        arma::fvec Thiseigval;
        arma::fmat Thiseigvec;
        arma::uvec eig_indices;
        arma::uvec maxeig_indices;

        Wmat = arma::join_cols(Wmat,concatSubData.t());
        thist = concatSubData.n_cols;

        arma::eig_sym( Thiseigval, Thiseigvec, Wmat*Wmat.t() );
        eig_indices = arma::sort_index(Thiseigval, "descend"); // Belt & braces
        maxeig_indices = eig_indices.head(2*thist-1);
        Thiseigvec = Thiseigvec.cols(maxeig_indices);
        Wmat = Thiseigvec.t()*Wmat;

        if (lastSub){
            arma::fvec Finaleigval;
            arma::fmat Finaleigvec;
            arma::eig_sym( Finaleigval, Finaleigvec, Wmat*Wmat.t());
            Finaleigvec = Finaleigvec.tail_cols(reducedDimension);
            Finaleigval = Finaleigval.tail(reducedDimension);
            Wmat = Wmat.rows(0,reducedDimension);
        }
        return Wmat;
    }

    ////////////////////////////////////////////////////////////////////////////
}
#endif

// Python to establish variance normalisation thresholds
/*
mask = nibabel.load(maskFile)
data = nibabel.load(dataFile)

data = data.get_data()[mask.get_data() != 0.0]
data = (data.T - numpy.mean(data, axis=1)).T
PFMplots.plotMaps(data, "Raw data")

sD = numpy.std(data, axis=1)
sD[sD < 1.0e-10] = 1.0

plt.figure(); plt.plot(sD, '.')
plt.figure(); plt.plot(numpy.log10(sD), '.')
plt.figure(); plt.hist(numpy.log10(sD), bins=250)
*/

// Python to look at norms post-hoc
/*
pfms = PROFUMO.PFMs(resultsDir)
norms = pfms.loadDataNormalisations()
norms = [norms[s][r] for s in norms for r in norms[s]]
norms = numpy.vstack(norms).T
plt.figure(); plt.hist(numpy.log10(norms.flatten()), bins=250)
*/
