// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Class that manages the whole inference process: loads the data, sets up the 
// cohort, manages the updates etc

#ifndef MODEL_MANAGER_H
#define MODEL_MANAGER_H

#include <memory>
#include <vector>
#include <string>
#include <cmath>

#include "CohortModelling/Cohort.h"
#include "CohortModelling/GroupModel.h"

namespace PROFUMO
{
    
    class ModelManager
    {
    public:
        // Define the potential models
        //----------------------------------------------------------------------
        enum class SpatialModel {
            MODES,
            PARCELLATION
        };
        
        // Extra spatial parameters
        struct SpatialParameters {
        public:
            std::shared_ptr<arma::fmat> GroupMeans = nullptr; //fixedGroupMeans -> GroupMeans
            std::shared_ptr<arma::fmat> GroupPrecisions = nullptr; // fixedGroupPrecisions -> GroupPrecisions
            std::shared_ptr<arma::fmat> GroupMemberships = nullptr; // fixedGroupMemberships -> GroupMemberships
            std::shared_ptr<arma::fmat> predefinedInitialMaps = nullptr; // fixedGroupMemberships -> GroupMemberships
            std::shared_ptr<arma::fmat> predefinedSpatialBasis = nullptr; // fixedGroupMemberships -> GroupMemberships
            bool fixedParams = false;
            bool randomInitialisation = true;
            // bool fixedParamsPrecisions = false; useful to define three separate fixed params for mean, precision and memberships?
            // bool fixedParamsMemberships = false;
            
            std::shared_ptr<std::vector<arma::uvec>> spatialNeighbours = nullptr;
        };
        //----------------------------------------------------------------------
        enum class WeightModel {
            CONSTANT,
            GROUP,
            RECTIFIED_MULTIVARIATE_NORMAL,
            RECTIFIED_SPIKE_SLAB
        };
        //----------------------------------------------------------------------
        enum class TimeCourseModel {
            MULTIVARIATE_NORMAL,
            CLEAN_HRF,
            NOISY_HRF
        };
        //----------------------------------------------------------------------
        enum class TemporalPrecisionModel {
            CONSTANT,
            GROUP_PRECISION_MATRIX,
            CONSTANT_GROUP,
            SUBJECT_PRECISION_MATRICES,
            RUN_PRECISION_MATRICES,
            CATEGORISED_RUN_PRECISION_MATRICES
        };
        //----------------------------------------------------------------------
        enum class NoiseModel {
            INDEPENDENT_RUNS
        };
        //----------------------------------------------------------------------
        
        
        // Options for the constructor
        //----------------------------------------------------------------------
        struct Options {
        public:
            unsigned int K = 0;
            bool normaliseData = true;
            bool bigData = false;
            bool fullPredefined = false;
            bool MIGP = false;
            float dofCorrectionFactor = 1.0;
            float TR = NAN;
            std::string hrfFile;
            std::string bigDataDir;
            std::string fullPredefinedInPath;
            std::string fullPredefinedOutPath;
            std::shared_ptr<const arma::uvec> maskInds = nullptr;
            unsigned int multiStartIterations = 5;
            unsigned int batchNum = 60;
            unsigned int batchCnt = 0;
            unsigned int batchUpdates = 20;
            unsigned int initialBatchUpdates = 20;
            unsigned int fullPredefinedIter = 50;
            
        };
        //----------------------------------------------------------------------
        
        
        // Constructor
        ModelManager( const std::string dataLocationsFile, const unsigned int M, const SpatialModel spatialModel, SpatialParameters spatialParameters, const WeightModel weightModel, const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel, const NoiseModel noiseModel, const Options options, const std::string preprocessingDirectory );
        
        // load batch and initialise
        void initialiseBatchData(const std::string dataLocationsFile, const std::vector<std::string> subjectNamesList, const arma::fmat initialMaps, const SpatialModel spatialModel, SpatialParameters spatialParameters, const WeightModel weightModel, const TimeCourseModel timeCourseModel, const TimeCourseModel initialTimeCourseModel, const TemporalPrecisionModel temporalPrecisionModel, const NoiseModel noiseModel, const Options options, unsigned int multiUpdates=0, unsigned int subjectNum=0);
        
        // reinitialises each batch- for bigData
        void reinitialiseBatch(const std::string dataLocationsFile, const std::vector<std::string> subNamesList, const Options options);
        
        // load data and initialise fully-predefined model
        // void initialiseFullPredefinedData(const std::string dataLocationsFile, const std::vector<std::string> subjectNamesList, const arma::fmat initialMaps, const SpatialModel spatialModel, SpatialParameters spatialParameters, const WeightModel weightModel, const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel, const NoiseModel noiseModel , const Options options, unsigned int multiUpdates=0, unsigned int subjectNum=0);

        // Updates the model
        void update(const unsigned int iterations, const std::string intermediatesDir, const bool normaliseTimeCourses=false, const bool bigData = false, const bool fullPredefined = false, const float posteriorRho = 1.0);
        void alignModelSigns(const bool flipGroup = true, const bool flipSubjects = true);
        // Functions to dynamically switch models
        void switchSpatialModel(const SpatialModel spatialModel, SpatialParameters spatialParameters, const bool batchCohort = false);
        void switchWeightModel(const WeightModel weightModel);
        void switchTemporalModel(const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel);
        void switchNoiseModel(const NoiseModel noiseModel);
        
        // Reset the model, with continuity coming from the current group maps
        void reinitialiseModel(const SpatialModel spatialModel, SpatialParameters spatialParameters, const WeightModel weightModel, const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel);
        
        // Save everything
        void save(const std::string directory, const bool saveSubjects = true, const bool saveGroup = true) const;
        
        // Get F
        arma::frowvec getFreeEnergy(const bool bigData = false, const int subjectNum = 0, const float posteriorRho = 1.0, const std::string bigDataDirBatch = {""}) const;
        
        // Saves the group maps to file to examine convergence post hoc
        void saveIntermediates(const std::string intermediatesDir, const unsigned int iteration, const unsigned int maxIteration, const bool bigData = false, const int subjectNum = 0, const unsigned int batchUpdateIteration= 0, const unsigned int maxBatchUpdateIteration = 0, std::string bigDataDirBatch = {""});
        
        //Get Sub list
        //std::vector<std::string> subjectNamesList;
        //std::vector<std::string> getSubjectNamesList(); //const {return subjectNamesList;}
        //std::vector<PROFUMO::SubjectID> subjectNamesPreList;
        //unsigned int s;
        //std::string subjectDirectory1;
        
    private:
        // The cohort itself
        std::unique_ptr<CohortModelling::Cohort> cohort_;
        
        
        // Key sizes
        unsigned int S_;        // Subjects
        //unsigned int R_;        // Runs
        const unsigned int M_;  // Modes       [FIXED]
        unsigned int V_;        // Voxels      [FIXED] - all scans are in the same space
        //std::string dLoc;
        //arma::uvec T_;          // Time points [VARIABLE]
        const float TR_;        // TR          [FIXED]
        const std::string hrfFile_;
        
        // Spatial DOF correction
        const float dofCorrectionFactor_;
        
        // Helper function to put Cohort together - useful as we have different types of data
        template<class D>
        void initialiseCohort(const std::map<SubjectID, std::map<RunID, D>> subjectData, const arma::fmat initialMaps, const SpatialModel spatialModel, SpatialParameters spatialParameters, const WeightModel weightModel, const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel, const NoiseModel noiseModel , const bool batchCohort=false, const std::string bigDataDir= {""});
        
        template<class D>
        void initialiseInterimCohort(const std::map<SubjectID, std::map<RunID, D>> subjectData, const arma::fmat initialMaps, const SpatialModel spatialModel, SpatialParameters spatialParameters, const WeightModel weightModel, const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel, const NoiseModel noiseModel, const std::string bigDataDirBatch = {""});

        // Helper function to initialise batch cohort
        template<class D>
        void initialiseNthBatchCohort(const std::map<SubjectID, std::map<RunID, D>> subjectData, const arma::fmat initialMaps, const SpatialModel spatialModel, SpatialParameters spatialParameters, const WeightModel weightModel, const TimeCourseModel timeCourseModel, const TimeCourseModel initialTimeCourseModel, const TemporalPrecisionModel temporalPrecisionModel, const NoiseModel noiseModel, const Options options, const std::string bigDataDir= {""}, unsigned int multiUpdates = 0, unsigned int subjectNum=0);

        // Helper function to initialise fully predefined cohort
        template<class D>
        void initialiseFullPredefinedCohort(const std::map<SubjectID, std::map<RunID, D>> subjectData, const arma::fmat initialMaps, const SpatialModel spatialModel, SpatialParameters spatialParameters, const WeightModel weightModel, const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel, const NoiseModel noiseModel, const std::string fullPredefinedInPath = {""});

        
        // Helper functions to generate models
        std::shared_ptr<CohortModelling::GroupSpatialModel> getSpatialModel(const SpatialModel spatialModel, SpatialParameters spatialParameters) const;
        std::shared_ptr<CohortModelling::GroupWeightModel> getWeightModel(const WeightModel weightModel) const;
        std::shared_ptr<CohortModelling::GroupTemporalPrecisionModel> getTemporalPrecisionModel(const TemporalPrecisionModel temporalPrecisionModel) const;
        std::shared_ptr<CohortModelling::GroupTemporalModel> getTemporalModel(const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel) const;
        std::shared_ptr<CohortModelling::GroupNoiseModel> getNoiseModel(const NoiseModel noiseModel) const;
        
        // Get a set of maps to intialise the full algorithm with 
        // Based on multiple decompositions for reliability
        arma::fmat generateInitialMaps(const arma::fmat& spatialBasis, const SpatialModel spatialModel, const unsigned int multiStartIterations, const std::string outputDirectory);
        
        
    };
    
}
#endif
