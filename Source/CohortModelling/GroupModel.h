// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a generic class that contains the group level parameters for a set 
// of subjects.

#ifndef COHORT_MODELLING_GROUP_MODEL_H
#define COHORT_MODELLING_GROUP_MODEL_H

#include <memory>
#include <string>
#include <armadillo>

#include "SubjectModelling/Subject.h"

namespace PROFUMO
{
    namespace CohortModelling
    {
        ////////////////////////////////////////////////////////////////////////
        
        template<class SubjectModel>
        class GroupModel
        {
        public:
            virtual SubjectModel getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick = {true, true}, const std::string bigDataDirBatch = {""}) = 0;
            
            virtual void resetSubjects() = 0;
            
            void update(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
            
            void save(const std::string directory) const;

            void load(const std::string directory);
            
            float getKL(const bool bigData = false) const;
            
            virtual ~GroupModel() = default;
            
        private:
            virtual void updateModel(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0) = 0;
            
            virtual std::vector<arma::fmat> loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick = true, const std::string bigDataDirBatch = {""}, const std::string runIDDir = {""}) = 0;
            
            virtual void saveModel(const std::string directory) const = 0;

            virtual void loadModel(const std::string directory) = 0;
            
            virtual float getModelKL(const bool bigData = false) const = 0;
        };
        
        ////////////////////////////////////////////////////////////////////////
        
        // Spatial model
        class GroupSpatialModel :
            public GroupModel<SubjectModelling::Subject::SpatialModel>
        {
        public:
            virtual arma::fmat getGroupMaps() const = 0;
            virtual arma::fmat getGroupMeans() const = 0;
            virtual arma::frowvec signFlip() = 0;
        };
        
        ////////////////////////////////////////////////////////////////////////
        
        // Weight model
        //typedef GroupModel<SubjectModelling::Subject::WeightModel> GroupWeightModel;
        // Spatial model
        class GroupWeightModel :
            public GroupModel<SubjectModelling::Subject::WeightModel>
        {
        public:
            virtual void signFlip(const arma::frowvec spatialSigns) = 0;
        };
        
        ////////////////////////////////////////////////////////////////////////
        
        // Precision matrix model
        class GroupTemporalPrecisionModel:
            public GroupModel<SubjectModelling::Subject::TemporalModel::Precisions>
        {
        public:
            virtual arma::fmat getGroupPrecisionMatrix() const = 0;
            virtual void signFlip(const arma::frowvec spatialSigns) = 0;
        };
        
        
        // Full temporal model
        class GroupTemporalModel :
            public GroupModel<SubjectModelling::Subject::TemporalModel>
        {
        public:
            GroupTemporalModel(std::shared_ptr<GroupTemporalPrecisionModel> precisionModel);
            
            arma::fmat getGroupPrecisionMatrix() const;
            
            virtual void signFlip(const arma::frowvec spatialSigns) = 0;
            
        protected:
            std::shared_ptr<GroupTemporalPrecisionModel> precisionModel_;
            
        private:
            // Override group functions to deal with precision
            void updateModel(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0) final;
            
            void saveModel(const std::string directory) const final;

            void loadModel(const std::string directory) final;
            
            float getModelKL(const bool bigData = false) const final;
            
            // And declare functions that let time course model play
            virtual void updateTimeCourseModel(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0) = 0;
            
            virtual void saveTimeCourseModel(const std::string directory) const = 0;

            virtual void loadTimeCourseModel(const std::string directory) = 0;
            
            virtual float getTimeCourseModelKL(const bool bigData = false) const = 0;
        };
        
        ////////////////////////////////////////////////////////////////////////
        
        // Noise model
        typedef GroupModel<SubjectModelling::Subject::NoiseModel> GroupNoiseModel;
        
        ////////////////////////////////////////////////////////////////////////
    }
}

////////////////////////////////////////////////////////////////////////////////
//                    Implementation of template functions                    //
////////////////////////////////////////////////////////////////////////////////

template<class SubjectModel>
void PROFUMO::CohortModelling::GroupModel<SubjectModel>::update(const bool bigData, const float posteriorRho, const int subjectNum)
{
    // Logging?
    updateModel(bigData, posteriorRho, subjectNum);
    return;
}

////////////////////////////////////////////////////////////////////////////////

template<class SubjectModel>
void PROFUMO::CohortModelling::GroupModel<SubjectModel>::save(const std::string directory) const
{
    // Logging?
    saveModel(directory);
    return;
}

///////////////////////////////////////////////////////////////////////////////

template<class SubjectModel>
void PROFUMO::CohortModelling::GroupModel<SubjectModel>::load(const std::string directory)
{
    // Logging?
    loadModel(directory);
    return;
}
////////////////////////////////////////////////////////////////////////////////

template<class SubjectModel>
float PROFUMO::CohortModelling::GroupModel<SubjectModel>::getKL(const bool bigData) const
{
    return getModelKL(bigData);
}

////////////////////////////////////////////////////////////////////////////////
#endif
