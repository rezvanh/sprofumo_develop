// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "GroupPrecisionMatrix.h"

////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupTemporalPrecisionModels::GroupPrecisionMatrix::GroupPrecisionMatrix(std::shared_ptr<SubjectModelling::Alpha_Post> groupPrecision)
: groupPrecision_(groupPrecision)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SubjectModelling::Subject::TemporalModel::Precisions PROFUMO::CohortModelling::GroupTemporalPrecisionModels::GroupPrecisionMatrix::getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick, const std::string bigDataDirBatch)
{
    // Make the subject model
    SubjectModelling::Subject::TemporalModel::Precisions subjectModel;
    // Nothing at the subject level
    subjectModel.subjectLevel.model = nullptr;
    subjectModel.subjectLevel.isInternalModel = false;
    // Populate with run models
    for (const RunInformation& runInformation : subjectInformation.runs) {
        // Make a Run model
        SubjectModelling::Run::Model<SubjectModelling::Alpha_Post> runModel;
        runModel.model = groupPrecision_;
        runModel.isInternalModel = false;
        // And record
        subjectModel.runLevel.models.emplace(runInformation.runID, runModel);
    }
    
    return subjectModel;
}
////////////////////////////////////////////////////////////////////////////////
std::vector<arma::fmat> PROFUMO::CohortModelling::GroupTemporalPrecisionModels::GroupPrecisionMatrix::loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick, const std::string bigDataDirBatch, const std::string runIDDir)
{
    std::vector<arma::fmat> initialisationMatrices;
    
    return initialisationMatrices;
}
///////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::GroupPrecisionMatrix::resetSubjects()
{
    groupPrecision_->resetParentModule();
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::CohortModelling::GroupTemporalPrecisionModels::GroupPrecisionMatrix::getGroupPrecisionMatrix() const
{
    return groupPrecision_->getExpectations().X;
}
////////////////////////////////////////////////////////////////////////////////
void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::GroupPrecisionMatrix::signFlip(const arma::frowvec spatialSigns)
{
    
    groupPrecision_->signFlip(spatialSigns);
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::GroupPrecisionMatrix::updateModel(const bool bigData, const float posteriorRho, const int subjectNum)
{
    groupPrecision_->update(bigData, posteriorRho, subjectNum);
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::GroupPrecisionMatrix::saveModel(const std::string directory) const
{
    groupPrecision_->save(directory);
    return;
}
////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::GroupPrecisionMatrix::loadModel(const std::string directory)
{
    groupPrecision_->load(directory);
    return;
}
////////////////////////////////////////////////////////////////////////////////

float PROFUMO::CohortModelling::GroupTemporalPrecisionModels::GroupPrecisionMatrix::getModelKL(const bool bigData) const
{
    return groupPrecision_->getKL(bigData);
}

////////////////////////////////////////////////////////////////////////////////
