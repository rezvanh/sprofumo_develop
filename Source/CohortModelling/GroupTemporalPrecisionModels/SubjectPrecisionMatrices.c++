// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "SubjectPrecisionMatrices.h"

#include <fstream>

#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;

////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupTemporalPrecisionModels::SubjectPrecisionMatrices::SubjectPrecisionMatrices(std::shared_ptr<SubjectModelling::Alpha_Post> groupPrecision, const VBModules::PrecisionMatrices::HierarchicalWishart::Parameters subjectPrior)
: groupPrecision_(groupPrecision), subjectPrior_(subjectPrior)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SubjectModelling::Subject::TemporalModel::Precisions PROFUMO::CohortModelling::GroupTemporalPrecisionModels::SubjectPrecisionMatrices::getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick, const std::string bigDataDirBatch)
{
    // Make the subject model
    SubjectModelling::Subject::TemporalModel::Precisions subjectModel;
    if (firstBatchfirstPick[0]){
        // Make subject level precision matrix
        subjectModel.subjectLevel.model = std::make_shared<VBModules::PrecisionMatrices::HierarchicalWishart>(groupPrecision_.get(), subjectPrior_);
    } else {
        std::vector<arma::fmat> initialisationMatrices = loadInitialisation(subjectInformation, firstBatchfirstPick[1], bigDataDirBatch);
        std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = std::make_shared<std::vector<arma::fmat>>(initialisationMatrices);
        subjectModel.subjectLevel.model = std::make_shared<VBModules::PrecisionMatrices::HierarchicalWishart>(groupPrecision_.get(), subjectPrior_, true, expectationInitialisation);
    
    }
    subjectModel.subjectLevel.isInternalModel = true;
    // And just copy into run models
    for (const RunInformation& runInformation : subjectInformation.runs) {
        // Make a Run model
        SubjectModelling::Run::Model<SubjectModelling::Alpha_Post> runModel;
        runModel.model = subjectModel.subjectLevel.model;
        runModel.isInternalModel = false;
        // And record
        subjectModel.runLevel.models.emplace(runInformation.runID, runModel);
    }
    
    return subjectModel;
}
///////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::SubjectPrecisionMatrices::resetSubjects()
{
    groupPrecision_->resetParentModule();    
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
std::vector<arma::fmat> PROFUMO::CohortModelling::GroupTemporalPrecisionModels::SubjectPrecisionMatrices::loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick, const std::string bigDataDirBatch, const std::string runIDDir)
{
    std::vector<arma::fmat> initialisationMatrices;
    if (firstPick){
        //initialise based on the initialise group model
        const std::string dirMean = bigDataDirBatch + "GroupInitialisation" + "/" + "GroupTemporalModel" + "/" + "PrecisionMatrixModel" + "/" + "Mean.hdf5"; //membership
        std::ifstream fileMean(dirMean.c_str());
        
        const std::string dirLogDet = bigDataDirBatch + "GroupInitialisation" + "/" + "GroupTemporalModel" + "/" + "PrecisionMatrixModel" + "/"+ "logDeterminant.txt"; //membership
        std::ifstream fileLogDet(dirLogDet.c_str());
        
        
        if (fileMean.good() && fileLogDet.good()) {
            arma::fmat groupPrecMat = Utilities::loadDataMatrix(dirMean);
            
            float prior_ld;    
            fileLogDet >> prior_ld;
            arma::fvec ldPrior(1);
            ldPrior.at(0) = prior_ld;
            
            initialisationMatrices.emplace_back(groupPrecMat);
            initialisationMatrices.emplace_back(ldPrior);
        
        } else {
            throw std::runtime_error("initial group temporal precision matrix model wasn't saved!") ; 
        }
        fileMean.close();
        fileLogDet.close();
        
    } else {
        //initialise based on the last time the subject was picked in 
        const std::string dirMean = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "TemporalPrecisionMatrix.post" +"/" + "Mean.hdf5"; //membership
        std::ifstream fileMean(dirMean.c_str());
        
        const std::string dirLogDet = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "TemporalPrecisionMatrix.post" +"/" + "logDeterminant.txt"; //membership
        std::ifstream fileLogDet(dirLogDet.c_str());
        
        const std::string dirPosteriorB = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "TemporalPrecisionMatrix.post" +"/" + "posteriorB.hdf5"; //membership
        std::ifstream filePosteriorB(dirPosteriorB.c_str());
        
        const std::string dirPosteriorA = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "TemporalPrecisionMatrix.post" +"/" + "posteriorA.txt"; //membership
        std::ifstream filePosteriorA(dirPosteriorA.c_str());
        
        if (fileMean.good() && fileLogDet.good() && filePosteriorB.good() && filePosteriorA.good()) {
            arma::fmat subjectPrecMat = Utilities::loadDataMatrix(dirMean);
            arma::fmat subjectPosteriorB = Utilities::loadDataMatrix(dirPosteriorB);
            float prior_ld;    
            fileLogDet >> prior_ld;
            arma::fvec ldPrior(1);
            ldPrior.at(0) = prior_ld;
            
            std::string astr;
            float prior_a;    
            filePosteriorA >> astr >> prior_a;
            arma::fvec agPrior(1);
            agPrior.at(0) = prior_a;
            
            
            initialisationMatrices.emplace_back(subjectPrecMat);
            initialisationMatrices.emplace_back(ldPrior);
            initialisationMatrices.emplace_back(agPrior);
            initialisationMatrices.emplace_back(subjectPosteriorB);
            
        } else {
            throw std::runtime_error("Subject's temporal precision matrix model wasn't saved the first time it was picked in a batch! Subject: " + subjectInformation.subjectID); 
        }
        fileMean.close();
        fileLogDet.close();
        filePosteriorB.close();
        filePosteriorA.close();
    
    }
    
    
    return initialisationMatrices;
}
////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::CohortModelling::GroupTemporalPrecisionModels::SubjectPrecisionMatrices::getGroupPrecisionMatrix() const
{
    // Transform hyperprior on rate matrix to precision matrix
    const arma::fmat X = groupPrecision_->getExpectations().X;
    return subjectPrior_.a * linalg::inv_sympd(X);
}
////////////////////////////////////////////////////////////////////////////////
void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::SubjectPrecisionMatrices::signFlip(const arma::frowvec spatialSigns)
{
    
    groupPrecision_->signFlip(spatialSigns);
    
    return;
} 
////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::SubjectPrecisionMatrices::updateModel(const bool bigData, const float posteriorRho, const int subjectNum)
{
    groupPrecision_->update(bigData, posteriorRho, subjectNum);
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::SubjectPrecisionMatrices::saveModel(const std::string directory) const
{
    // Group posterior
    groupPrecision_->save(directory);
    
    // And save prior (needed to calculate subject expectations)
    const std::string fileName = directory + "PriorDegreesOfFreedom.txt";
    std::ofstream file;
    file.open( fileName.c_str() );
    file << subjectPrior_.a << std::endl;
    file.close();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::SubjectPrecisionMatrices::loadModel(const std::string directory)
{
    // Group posterior
    //std::cout << "loading existing GroupTemporalPrecisionModels" << std::endl;
    groupPrecision_->load(directory);
    
    // // And load prior (needed to calculate subject expectations)
    // const std::string fileName = directory + "PriorDegreesOfFreedom.txt";
    // std::ifstream file(fileName.c_str());
    // float prior_a;
    // file >> prior_a;
    // subjectPrior_.a = prior_a;
    // file.close();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

float PROFUMO::CohortModelling::GroupTemporalPrecisionModels::SubjectPrecisionMatrices::getModelKL(const bool bigData) const
{
    return groupPrecision_->getKL(bigData);
}

////////////////////////////////////////////////////////////////////////////////
