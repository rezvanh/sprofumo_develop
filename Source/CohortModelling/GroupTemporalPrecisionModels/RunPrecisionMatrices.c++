// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "RunPrecisionMatrices.h"

#include <fstream>

#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;

////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupTemporalPrecisionModels::RunPrecisionMatrices::RunPrecisionMatrices(std::shared_ptr<SubjectModelling::Alpha_Post> groupPrecision, const VBModules::PrecisionMatrices::HierarchicalWishart::Parameters runPrior)
: groupPrecision_(groupPrecision), runPrior_(runPrior)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SubjectModelling::Subject::TemporalModel::Precisions PROFUMO::CohortModelling::GroupTemporalPrecisionModels::RunPrecisionMatrices::getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick, const std::string bigDataDirBatch)
{
    // Make the subject model
    SubjectModelling::Subject::TemporalModel::Precisions subjectModel;
    // No subject level precision matrix
    subjectModel.subjectLevel.model = nullptr;
    subjectModel.subjectLevel.isInternalModel = false;
    // Make the independent run models
    for (const RunInformation& runInformation : subjectInformation.runs) {
        // Make a Run model
        SubjectModelling::Run::Model<SubjectModelling::Alpha_Post> runModel;
        if (firstBatchfirstPick[0]){
            runModel.model = std::make_shared<VBModules::PrecisionMatrices::HierarchicalWishart>(groupPrecision_.get(), runPrior_);
        } else {            
            std::vector<arma::fmat> initialisationMatrices = loadInitialisation(subjectInformation, firstBatchfirstPick[1], bigDataDirBatch, runInformation.runID);
            std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = std::make_shared<std::vector<arma::fmat>>(initialisationMatrices);
            runModel.model = std::make_shared<VBModules::PrecisionMatrices::HierarchicalWishart>(groupPrecision_.get(), runPrior_, true, expectationInitialisation);        
        }
        runModel.isInternalModel = true;
        // And record
        subjectModel.runLevel.models.emplace(runInformation.runID, runModel);
    }
    
    return subjectModel;
}

////////////////////////////////////////////////////////////////////////////////
std::vector<arma::fmat> PROFUMO::CohortModelling::GroupTemporalPrecisionModels::RunPrecisionMatrices::loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick, const std::string bigDataDirBatch, const std::string runIDDir)
{
    std::vector<arma::fmat> initialisationMatrices;
    if (firstPick){
        const std::string dirMean = bigDataDirBatch + "GroupInitialisation" + "/" + "GroupTemporalModel" + "/" + "PrecisionMatrixModel" + "/" + "Mean.hdf5"; //membership
        std::ifstream fileMean(dirMean.c_str());
        
        const std::string dirLogDet = bigDataDirBatch + "GroupInitialisation" + "/" + "GroupTemporalModel" + "/" + "PrecisionMatrixModel" + "/"+ "logDeterminant.txt"; //membership
        std::ifstream fileLogDet(dirLogDet.c_str());
        
        
        if (fileMean.good() && fileLogDet.good()) {
            arma::fmat groupPrecMat = Utilities::loadDataMatrix(dirMean);
            
            float prior_ld;    
            fileLogDet >> prior_ld;
            arma::fvec ldPrior(1);
            ldPrior.at(0) = prior_ld;
            
            initialisationMatrices.emplace_back(groupPrecMat);
            initialisationMatrices.emplace_back(ldPrior);
        
        } else {
            throw std::runtime_error("initial group temporal precision matrix model wasn't saved!") ; 
        }
        fileMean.close();
        fileLogDet.close();
        
    } else {
        const std::string dirMean = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "Runs" + "/" + runIDDir + "/" + "TemporalPrecisionMatrix.post" +"/" + "Mean.hdf5"; //membership
        std::ifstream fileMean(dirMean.c_str());
        
        const std::string dirLogDet = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "Runs" + "/" + runIDDir + "/" + "TemporalPrecisionMatrix.post" +"/" + "logDeterminant.txt";; //membership
        std::ifstream fileLogDet(dirLogDet.c_str());
        
        const std::string dirPosteriorB = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" +"Runs" + "/" + runIDDir + "/" + "TemporalPrecisionMatrix.post" +"/" + "posteriorB.hdf5"; //membership
        std::ifstream filePosteriorB(dirPosteriorB.c_str());
        
        const std::string dirPosteriorA = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "Runs" + "/" + runIDDir + "/" + "TemporalPrecisionMatrix.post" +"/" + "posteriorA.txt"; //membership
        std::ifstream filePosteriorA(dirPosteriorA.c_str());
        
        if (fileMean.good() && fileLogDet.good() && filePosteriorB.good() && filePosteriorA.good()) {
            arma::fmat runPrecMat = Utilities::loadDataMatrix(dirMean);
            arma::fmat runPosteriorB = Utilities::loadDataMatrix(dirPosteriorB);
            float prior_ld;    
            fileLogDet >> prior_ld;
            arma::fvec ldPrior(1);
            ldPrior.at(0) = prior_ld;
            
            std::string astr;
            float prior_a;    
            filePosteriorA >> astr >> prior_a;
            arma::fvec agPrior(1);
            agPrior.at(0) = prior_a;
            
            std::vector<arma::fmat> initialisationMatrices;
            initialisationMatrices.emplace_back(runPrecMat);
            initialisationMatrices.emplace_back(ldPrior);
            initialisationMatrices.emplace_back(agPrior);
            initialisationMatrices.emplace_back(runPosteriorB);
            
        } else {
            throw std::runtime_error("Run's temporal precision matrix model wasn't saved the first time it was picked in a batch! Subject: " + subjectInformation.subjectID); 
        }
        fileMean.close();
        fileLogDet.close();
        filePosteriorB.close();
        filePosteriorA.close();
        
    }
    
    
    return initialisationMatrices;
}
///////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::RunPrecisionMatrices::resetSubjects()
{
    groupPrecision_->resetParentModule();
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::CohortModelling::GroupTemporalPrecisionModels::RunPrecisionMatrices::getGroupPrecisionMatrix() const
{
    // Transform hyperprior on rate matrix to precision matrix
    const arma::fmat X = groupPrecision_->getExpectations().X;
    return runPrior_.a * linalg::inv_sympd(X);
}
////////////////////////////////////////////////////////////////////////////////
void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::RunPrecisionMatrices::signFlip(const arma::frowvec spatialSigns)
{
    
    groupPrecision_->signFlip(spatialSigns);
    
    return;
} 
////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::RunPrecisionMatrices::updateModel(const bool bigData, const float posteriorRho, const int subjectNum)
{
    groupPrecision_->update(bigData, posteriorRho, subjectNum);
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::RunPrecisionMatrices::saveModel(const std::string directory) const
{
    // Group posterior
    groupPrecision_->save(directory);
    
    // And save prior (needed to calculate run expectations)
    const std::string fileName = directory + "PriorDegreesOfFreedom.txt";
    std::ofstream file;
    file.open( fileName.c_str() );
    file << runPrior_.a << std::endl;
    file.close();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::RunPrecisionMatrices::loadModel(const std::string directory)
{
    // Group posterior
    groupPrecision_->load(directory);
    
    // // And load prior (needed to calculate run expectations)
    // const std::string fileName = directory + "PriorDegreesOfFreedom.txt";
    // std::ifstream file(fileName.c_str());
    // float prior_a;
    // file >> prior_a;
    // runPrior_.a = prior_a;
    // file.close();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

float PROFUMO::CohortModelling::GroupTemporalPrecisionModels::RunPrecisionMatrices::getModelKL(const bool bigData) const
{
    return groupPrecision_->getKL(bigData);
}

////////////////////////////////////////////////////////////////////////////////
