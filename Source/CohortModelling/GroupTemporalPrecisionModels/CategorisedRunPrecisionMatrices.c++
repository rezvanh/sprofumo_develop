// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "CategorisedRunPrecisionMatrices.h"

#include <sys/stat.h>
#include <fstream>

#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;

////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupTemporalPrecisionModels::CategorisedRunPrecisionMatrices::CategorisedRunPrecisionMatrices(const VBModules::PrecisionMatrices::Wishart::Parameters groupPrior, const VBModules::PrecisionMatrices::HierarchicalWishart::Parameters runPrior)
: groupPrior_(groupPrior), runPrior_(runPrior)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SubjectModelling::Subject::TemporalModel::Precisions PROFUMO::CohortModelling::GroupTemporalPrecisionModels::CategorisedRunPrecisionMatrices::getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick, const std::string bigDataDirBatch)
{
    // Make the subject model
    SubjectModelling::Subject::TemporalModel::Precisions subjectModel;
    // No subject level precision matrix
    subjectModel.subjectLevel.model = nullptr;
    subjectModel.subjectLevel.isInternalModel = false;
    // Either make, or assign from pool of, run models
    for (const RunInformation& runInformation : subjectInformation.runs) {
        const RunID runID = runInformation.runID;
        // Make a Run model
        SubjectModelling::Run::Model<SubjectModelling::Alpha_Post> runModel;
        // See if we have this in the pool already, and if not make it
        if (groupPrecisions_.count(runID) == 0) {
            groupPrecisions_.emplace(runID, std::make_shared<VBModules::PrecisionMatrices::Wishart>(groupPrior_));
        }
        if (firstBatchfirstPick[0]){            
            // Make the run level model
            runModel.model = std::make_shared<VBModules::PrecisionMatrices::HierarchicalWishart>(groupPrecisions_.at(runID).get(), runPrior_);
        } else {
            std::vector<arma::fmat> initialisationMatrices = loadInitialisation(subjectInformation, firstBatchfirstPick[1], bigDataDirBatch, runInformation.runID);
            std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = std::make_shared<std::vector<arma::fmat>>(initialisationMatrices);
            runModel.model = std::make_shared<VBModules::PrecisionMatrices::HierarchicalWishart>(groupPrecisions_.at(runID).get(), runPrior_, true, expectationInitialisation);        
        }
        
        runModel.isInternalModel = true;
        // And record
        subjectModel.runLevel.models.emplace(runID, runModel);
    }
    
    return subjectModel;
}

////////////////////////////////////////////////////////////////////////////////
std::vector<arma::fmat> PROFUMO::CohortModelling::GroupTemporalPrecisionModels::CategorisedRunPrecisionMatrices::loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick, const std::string bigDataDirBatch, const std::string runIDDir)
{
    std::vector<arma::fmat> initialisationMatrices;
    if (firstPick){
        const std::string dirMean = bigDataDirBatch + "GroupInitialisation" + "/" + "GroupTemporalModel" + "/" + "PrecisionMatrixModel" + "/" + "Mean.hdf5"; //membership
        std::ifstream fileMean(dirMean.c_str());
        
        const std::string dirLogDet = bigDataDirBatch + "GroupInitialisation" + "/" + "GroupTemporalModel" + "/" + "PrecisionMatrixModel" + "/"+ "logDeterminant.txt"; //membership
        std::ifstream fileLogDet(dirLogDet.c_str());
        
        
        if (fileMean.good() && fileLogDet.good()) {
            arma::fmat groupPrecMat = Utilities::loadDataMatrix(dirMean);
            
            float prior_ld;    
            fileLogDet >> prior_ld;
            arma::fvec ldPrior(1);
            ldPrior.at(0) = prior_ld;
            
            initialisationMatrices.emplace_back(groupPrecMat);
            initialisationMatrices.emplace_back(ldPrior);
        
        } else {
            throw std::runtime_error("initial group temporal precision matrix model wasn't saved!") ; 
        }  
        fileMean.close();
        fileLogDet.close();
        
    } else {
        const std::string dirMean = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "Runs" + "/" + runIDDir + "/" + "TemporalPrecisionMatrix.post" +"/" + "Mean.hdf5"; //membership
        std::ifstream fileMean(dirMean.c_str());
        
        const std::string dirLogDet = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "Runs" + "/" + runIDDir + "/" + "TemporalPrecisionMatrix.post" +"/" + "logDeterminant.txt";; //membership
        std::ifstream fileLogDet(dirLogDet.c_str());
        
        const std::string dirPosteriorB = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" +"Runs" + "/" + runIDDir + "/" + "TemporalPrecisionMatrix.post" +"/" + "posteriorB.hdf5"; //membership
        std::ifstream filePosteriorB(dirPosteriorB.c_str());
        
        const std::string dirPosteriorA = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "Runs" + "/" + runIDDir + "/" + "TemporalPrecisionMatrix.post" +"/" + "posteriorA.txt"; //membership
        std::ifstream filePosteriorA(dirPosteriorA.c_str());
        
        
        if (fileMean.good() && fileLogDet.good() && filePosteriorB.good() && filePosteriorA.good()) {
            arma::fmat subjectPrecMat = Utilities::loadDataMatrix(dirMean);
            arma::fmat runPosteriorB = Utilities::loadDataMatrix(dirPosteriorB);
            
            float prior_ld;    
            fileLogDet >> prior_ld;
            arma::fvec ldPrior(1);
            ldPrior.at(0) = prior_ld;
            
            std::string astr;
            float prior_a;    
            filePosteriorA >> astr >> prior_a;
            arma::fvec agPrior(1);
            agPrior.at(0) = prior_a;
            
            std::vector<arma::fmat> initialisationMatrices;
            initialisationMatrices.emplace_back(subjectPrecMat);
            initialisationMatrices.emplace_back(ldPrior);
            initialisationMatrices.emplace_back(agPrior);
            initialisationMatrices.emplace_back(runPosteriorB);
            
        } else {
            throw std::runtime_error("Run's temporal precision matrix model wasn't saved the first time it was picked in a batch! Subject: " + subjectInformation.subjectID); 
        }
        fileMean.close();
        fileLogDet.close();
        filePosteriorB.close();
        filePosteriorA.close();
        
    }
    
    
    return initialisationMatrices;
}
///////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::CategorisedRunPrecisionMatrices::resetSubjects()
{
    //std::cout << "resetting subjects in TPM" <<std::endl;
    //for (const RunInformation& runInformation : subjectInformation.runs) {
    //    const RunID runID = runInformation.runID;
    //    groupPrecisions_.at(runID)->resetParentModule();
    //}
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::CohortModelling::GroupTemporalPrecisionModels::CategorisedRunPrecisionMatrices::getGroupPrecisionMatrix() const
{
    // Use the mean precision matrix
    // Initialise with precision matrix from first category
    auto gPM = groupPrecisions_.cbegin();
    arma::fmat X = gPM->second->getExpectations().X;
    // Add other models if necessary
    for (++gPM; gPM != groupPrecisions_.cend(); ++gPM) {
        X += gPM->second->getExpectations().X;
    }
    // And normalise
    X /= groupPrecisions_.size();
    
    // Transform hyperprior on rate matrix to precision matrix
    return runPrior_.a * linalg::inv_sympd(X);
}
////////////////////////////////////////////////////////////////////////////////
void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::CategorisedRunPrecisionMatrices::signFlip(const arma::frowvec spatialSigns)
{
    for (auto& groupPrecision : groupPrecisions_) {
        groupPrecision.second->signFlip(spatialSigns);
    }
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::CategorisedRunPrecisionMatrices::updateModel(const bool bigData, const float posteriorRho , const int subjectNum )
{
    for (auto& groupPrecision : groupPrecisions_) {
        groupPrecision.second->update(bigData, posteriorRho, subjectNum);
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::CategorisedRunPrecisionMatrices::saveModel(const std::string directory) const
{
    // Group posteriors
    for (const auto& groupPrecision : groupPrecisions_) {
        RunID runID = groupPrecision.first;
        const std::string runDir = directory + runID + "/";
        mkdir( runDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        groupPrecision.second->save(runDir);
    }
    
    // And save prior (needed to calculate run expectations)
    const std::string fileName = directory + "PriorDegreesOfFreedom.txt";
    std::ofstream file;
    file.open( fileName.c_str() );
    file << runPrior_.a << std::endl;
    file.close();
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::CategorisedRunPrecisionMatrices::loadModel(const std::string directory)
{
    // Group posteriors
    for (const auto& groupPrecision : groupPrecisions_) {
        RunID runID = groupPrecision.first;
        const std::string runDir = directory + runID + "/";
        groupPrecision.second->load(runDir);
    }
    
    // // And load prior (needed to calculate run expectations)
    // const std::string fileName = directory + "PriorDegreesOfFreedom.txt";
    // std::ifstream file(fileName.c_str());
    // float prior_a;
    // file >> prior_a;
    // runPrior_.a = prior_a;
    // file.close();

    return;
}
////////////////////////////////////////////////////////////////////////////////

float PROFUMO::CohortModelling::GroupTemporalPrecisionModels::CategorisedRunPrecisionMatrices::getModelKL(const bool bigData) const
{
    float KL = 0.0;
    
    for (const auto& groupPrecision : groupPrecisions_) {
        KL += groupPrecision.second->getKL(bigData);
    }
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////
