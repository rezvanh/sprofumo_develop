// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a group-level time course model based on an MVN.

#ifndef COHORT_MODELLING_GROUP_TEMPORAL_MODELS_MULTIVARIATE_NORMAL_H
#define COHORT_MODELLING_GROUP_TEMPORAL_MODELS_MULTIVARIATE_NORMAL_H

#include <memory>
#include <string>

#include "SubjectModelling/Subject.h"
#include "CohortModelling/GroupModel.h"

#include "ModuleList.h"
#include "VBModules/Constant.h"
#include "VBModules/CopyParent.h"
#include "MFModels/A/MultivariateNormal.h"

#include "Utilities/Timer.h"
#include "Utilities/DataIO.h"


namespace PROFUMO
{
    namespace CohortModelling
    {
        namespace GroupTemporalModels
        {
            
            class MultivariateNormal :
                public GroupTemporalModel
            {
            public:
                // Constructor
                MultivariateNormal(std::shared_ptr<GroupTemporalPrecisionModel> temporalPrecisionModel, const unsigned int M);
                
                virtual SubjectModelling::Subject::TemporalModel getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick = {true, true}, const std::string bigDataDirBatch = {""});
                
                virtual void resetSubjects();
                
                virtual void signFlip(const arma::frowvec spatialSigns);
                
            private:
                virtual void updateTimeCourseModel(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                virtual std::vector<arma::fmat> loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick = true, const std::string bigDataDirBatch = {""}, const std::string runIDDir = {""});
                
                virtual void saveTimeCourseModel(const std::string directory) const;

                virtual void loadTimeCourseModel(const std::string directory);
                
                virtual float getTimeCourseModelKL(const bool bigData = false) const;
                
                // Size
                const unsigned int M_;
            };
            
        }
    }
}
#endif
