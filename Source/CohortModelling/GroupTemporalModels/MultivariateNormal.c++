// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */
#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;
#include "MultivariateNormal.h"

#include <armadillo>

////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupTemporalModels::MultivariateNormal::MultivariateNormal(std::shared_ptr<GroupTemporalPrecisionModel> temporalPrecisionModel, const unsigned int M)
: GroupTemporalModel(temporalPrecisionModel), M_(M)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SubjectModelling::Subject::TemporalModel PROFUMO::CohortModelling::GroupTemporalModels::MultivariateNormal::getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick, const std::string bigDataDirBatch)
{
    // Make the subject model
    SubjectModelling::Subject::TemporalModel subjectModel;
    if (firstBatchfirstPick[0]){
        // Get the precision matrix model
        subjectModel.precisions = precisionModel_->getSubjectModel(subjectInformation);
    } else {
        subjectModel.precisions = precisionModel_->getSubjectModel(subjectInformation, {false, firstBatchfirstPick[1]}, bigDataDirBatch);
    }
    // Populate with run models
    for (const RunInformation& runInformation : subjectInformation.runs) {
        const RunID runID = runInformation.runID;
        // Make a Run model
        SubjectModelling::Run::Model<SubjectModelling::A_Post> runModel;
        MFModels::A::MultivariateNormal::Parameters prior;
        prior.M = M_; prior.T = runInformation.T;
        auto runPrecision = subjectModel.precisions.runLevel.models.at(runID).model.get();
        std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation;
        if (firstBatchfirstPick[0]){
            runModel.model = std::make_shared<MFModels::A::MultivariateNormal>(runPrecision, prior);
            //previousA = arma::zeros<arma::fmat>(prior.M,prior.T); //runModel.model = std::make_shared<MFModels::A::MultivariateNormal>(runPrecision, prior);
        } else {
            std::vector<arma::fmat> initialisationMatrices = loadInitialisation(subjectInformation, firstBatchfirstPick[1], bigDataDirBatch, runInformation.runID);
            if (firstBatchfirstPick[1]){
                arma::fmat TSigma = linalg::inv_sympd( initialisationMatrices[0] );
                arma::fmat subjectTCMean = arma::zeros<arma::fmat>(prior.M, prior.T);
                arma::fmat subjectTCAAt = prior.T * TSigma;
                
                initialisationMatrices.emplace_back(subjectTCMean);
                initialisationMatrices.emplace_back(subjectTCAAt);
                
            } 
            std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = std::make_shared<std::vector<arma::fmat>>(initialisationMatrices);
            runModel.model = std::make_shared<MFModels::A::MultivariateNormal>(runPrecision, prior, true, expectationInitialisation);
                   
        }
        
        //std::shared_ptr<Modules::MatrixMean_Parent> sTM_Means;
        //Modules::MatrixMeans::P2C ETimeCourses;
        //ETimeCourses.X = previousA; ETimeCourses.X2 = arma::square(previousA);            
        //sTM_Means = std::make_shared<VBModules::CopyParent<Modules::MatrixMeans::C2P, Modules::MatrixMeans::P2C>>(ETimeCourses);        
        
        runModel.isInternalModel = true;
        // And record
        subjectModel.timeCourses.models.emplace(runID, runModel);
    }
    
    return subjectModel;
}
////////////////////////////////////////////////////////////////////////////////
std::vector<arma::fmat> PROFUMO::CohortModelling::GroupTemporalModels::MultivariateNormal::loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick, const std::string bigDataDirBatch, const std::string runIDDir)
{
    std::vector<arma::fmat> initialisationMatrices;
    if (firstPick){
        const std::string dirMean = bigDataDirBatch + "GroupInitialisation" + "/" + "GroupTemporalModel" + "/" + "PrecisionMatrixModel" + "/" + "Mean.hdf5"; //membership
        std::ifstream fileMean(dirMean.c_str());
        
        const std::string dirLogDet = bigDataDirBatch + "GroupInitialisation" + "/" + "GroupTemporalModel" + "/" + "PrecisionMatrixModel" + "/"+ "logDeterminant.txt"; //membership
        std::ifstream fileLogDet(dirLogDet.c_str());
        
        if (fileMean.good() && fileLogDet.good()) {
            arma::fmat groupPrecMat = Utilities::loadDataMatrix(dirMean);
            
            float prior_ld;    
            fileLogDet >> prior_ld;
            arma::fvec ldPrior(1);
            ldPrior.at(0) = prior_ld;
            
            initialisationMatrices.emplace_back(groupPrecMat);
            initialisationMatrices.emplace_back(ldPrior);
        
        } else {
            throw std::runtime_error("initial group temporal precision matrix model wasn't saved!") ; 
        }        
        
    } else {
        const std::string dirPrecMean = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "TemporalPrecisionMatrix.post" +"/" + "Mean.hdf5"; //membership
        std::ifstream filePrecMean(dirPrecMean.c_str());
        
        const std::string dirLogDet = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/"   + "TemporalPrecisionMatrix.post" +"/" + "logDeterminant.txt";//membership
        std::ifstream fileLogDet(dirLogDet.c_str());
        
        const std::string dirMean = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "Runs" + "/" + runIDDir + "/" + "TimeCourses.post" + "/" + "Means.hdf5"; //membership
        std::ifstream fileMean(dirMean.c_str());
        
        const std::string dirInnerProd = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "Runs" + "/" + runIDDir + "/" + "TimeCourses.post" + "/" + "InnerProduct.hdf5"; //membership
        std::ifstream fileInnerProd(dirInnerProd.c_str());            
        
        if (filePrecMean.good() && fileLogDet.good() && fileMean.good() && fileInnerProd.good()) {
        
            arma::fmat subjectPrecMat = Utilities::loadDataMatrix(dirPrecMean);
            
            float prior_ld;    
            fileLogDet >> prior_ld;
            arma::fvec ldPrior(1);
            ldPrior.at(0) = prior_ld;
            
            arma::fmat subjectTCMean = Utilities::loadDataMatrix(dirMean);
            arma::fmat subjectTCAAt = Utilities::loadDataMatrix(dirInnerProd);
            
            initialisationMatrices.emplace_back(subjectPrecMat);
            initialisationMatrices.emplace_back(ldPrior);
            initialisationMatrices.emplace_back(subjectTCMean);
            initialisationMatrices.emplace_back(subjectTCAAt);
            
                   
        } else {
            throw std::runtime_error("Run's time courses weren't saved the first time it was picked in a batch! Subject: " + subjectInformation.subjectID); 
        }           
    }
    
    
    return initialisationMatrices;
}
///////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalModels::MultivariateNormal::resetSubjects()
{
    precisionModel_->resetSubjects();
    
    return;
}
////////////////////////////////////////////////////////////////////////////////
void PROFUMO::CohortModelling::GroupTemporalModels::MultivariateNormal::signFlip(const arma::frowvec spatialSigns)
{
    
    precisionModel_->signFlip(spatialSigns);
    
    return;
} 
////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalModels::MultivariateNormal::updateTimeCourseModel(const bool bigData , const float posteriorRho, const int subjectNum)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalModels::MultivariateNormal::saveTimeCourseModel(const std::string) const
{;
    return;
}
////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalModels::MultivariateNormal::loadTimeCourseModel(const std::string)
{;
    return;
}
////////////////////////////////////////////////////////////////////////////////

float PROFUMO::CohortModelling::GroupTemporalModels::MultivariateNormal::getTimeCourseModelKL(const bool bigData) const
{
    return 0.0;
}

////////////////////////////////////////////////////////////////////////////////
