// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "CleanHRF.h"

#include <armadillo>
#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;

#include "Utilities/HRFModelling.h"

////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupTemporalModels::CleanHRF::CleanHRF(std::shared_ptr<GroupTemporalPrecisionModel> temporalPrecisionModel, const unsigned int M, const float TR, const std::string hrfFile, const float priorRelaxation, const float posteriorRelaxation)
: GroupTemporalModel(temporalPrecisionModel), M_(M), TR_(TR), priorRelaxation_(priorRelaxation), posteriorRelaxation_(posteriorRelaxation), hrfFile_(hrfFile)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SubjectModelling::Subject::TemporalModel PROFUMO::CohortModelling::GroupTemporalModels::CleanHRF::getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick, const std::string bigDataDirBatch)
{
    // Make the subject model
    SubjectModelling::Subject::TemporalModel subjectModel;
    if (firstBatchfirstPick[0]){
        // Get the precision matrix model
        subjectModel.precisions = precisionModel_->getSubjectModel(subjectInformation);
    } else {
        subjectModel.precisions = precisionModel_->getSubjectModel(subjectInformation, {false, firstBatchfirstPick[1]}, bigDataDirBatch);
    }
    // Populate with run models
    for (const RunInformation& runInformation : subjectInformation.runs) {
        const RunID runID = runInformation.runID;
        const unsigned int T = runInformation.T;
        // Make a Run model
        SubjectModelling::Run::Model<SubjectModelling::A_Post> runModel;
        // Check to see if we have made an HRF prior before
        if (hrfPriors_.count(T) == 0) {
            hrfPriors_.emplace(T, Utilities::generateHRFPrior(hrfFile_, T, TR_, M_, priorRelaxation_, posteriorRelaxation_));
        }
        // Make the HRF-based TCs
        Modules::PrecisionMatrix_Parent* alpha = subjectModel.precisions.runLevel.models.at(runID).model.get();
        std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation;
        if (firstBatchfirstPick[0]){
            runModel.model = std::make_shared<MFModels::A::KroneckerHRF>(alpha, hrfPriors_.at(T));
        } else {
            std::vector<arma::fmat> initialisationMatrices = loadInitialisation(subjectInformation, firstBatchfirstPick[1], bigDataDirBatch, runInformation.runID);
            if (firstBatchfirstPick[1]){
                
                arma::fmat subjectTCMean = arma::zeros<arma::fmat>(M_,T);
                arma::fmat subjectTCAAt = arma::trace( *hrfPriors_.at(T).K ) * linalg::inv_sympd( initialisationMatrices[0] );
                arma::fmat subjectTCXXt = T * linalg::inv_sympd( initialisationMatrices[0] );
                
                initialisationMatrices.emplace_back(subjectTCMean);
                initialisationMatrices.emplace_back(subjectTCAAt);
                initialisationMatrices.emplace_back(subjectTCXXt);
                
            } 
            std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = std::make_shared<std::vector<arma::fmat>>(initialisationMatrices);
            runModel.model = std::make_shared<MFModels::A::KroneckerHRF>(alpha, hrfPriors_.at(T), true, expectationInitialisation);
                   
        }
        runModel.isInternalModel = true;
        // And record
        subjectModel.timeCourses.models.emplace(runID, runModel);
    }
    
    return subjectModel;
}
////////////////////////////////////////////////////////////////////////////////
std::vector<arma::fmat> PROFUMO::CohortModelling::GroupTemporalModels::CleanHRF::loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick, const std::string bigDataDirBatch, const std::string runIDDir)
{
    std::vector<arma::fmat> initialisationMatrices;
    if (firstPick){
        const std::string dirMean = bigDataDirBatch + "GroupInitialisation" + "/" + "GroupTemporalModel" + "/" + "PrecisionMatrixModel" + "/" + "Mean.hdf5"; //membership
        std::ifstream fileMean(dirMean.c_str());
        
        const std::string dirLogDet = bigDataDirBatch + "GroupInitialisation" + "/" + "GroupTemporalModel" + "/" + "PrecisionMatrixModel" + "/"+ "logDeterminant.txt"; //membership
        std::ifstream fileLogDet(dirLogDet.c_str());
        
        
        if (fileMean.good() && fileLogDet.good()) {
            arma::fmat groupPrecMat = Utilities::loadDataMatrix(dirMean);
            
            float prior_ld;    
            fileLogDet >> prior_ld;
            arma::fvec ldPrior(1);
            ldPrior.at(0) = prior_ld;
            
            initialisationMatrices.emplace_back(groupPrecMat);
            initialisationMatrices.emplace_back(ldPrior);
        
        } else {
            throw std::runtime_error("initial group temporal precision matrix model wasn't saved!") ; 
        }        
        
    } else {
        const std::string dirPrecMean = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "TemporalPrecisionMatrix.post" +"/" + "Mean.hdf5"; //membership
        std::ifstream filePrecMean(dirPrecMean.c_str());
        
        const std::string dirLogDet = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "TemporalPrecisionMatrix.post" +"/" + "logDeterminant.txt";//membership
        std::ifstream fileLogDet(dirLogDet.c_str());
        
        const std::string dirMean = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "Runs" + "/" + runIDDir + "/" + "TimeCourses.post" + "/" + "Means.hdf5"; //membership
        std::ifstream fileMean(dirMean.c_str());
        
        const std::string dirInnerProd = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "Runs" + "/" + runIDDir + "/" + "TimeCourses.post" + "/" + "InnerProduct.hdf5"; //membership
        std::ifstream fileInnerProd(dirInnerProd.c_str());    
        
        const std::string dirInnerProdInvCov = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "Runs" + "/" + runIDDir + "/" + "TimeCourses.post" + "/" + "InnerProduct_InverseCovariance.hdf5"; //membership
        std::ifstream fileInnerProdInvCov(dirInnerProdInvCov.c_str());
        
        // load covariance
        if (filePrecMean.good() && fileLogDet.good() && fileMean.good() && fileInnerProd.good() && fileInnerProdInvCov.good()) {
            arma::fmat subjectPrecMat = Utilities::loadDataMatrix(dirPrecMean);
            
            float prior_ld;    
            fileLogDet >> prior_ld;
            arma::fvec ldPrior(1);
            ldPrior.at(0) = prior_ld;
            
            arma::fmat subjectTCMean = Utilities::loadDataMatrix(dirMean);
            arma::fmat subjectTCAAt = Utilities::loadDataMatrix(dirInnerProd);
            arma::fmat subjectTCXXt = Utilities::loadDataMatrix(dirInnerProdInvCov);
            
            initialisationMatrices.emplace_back(subjectPrecMat);
            initialisationMatrices.emplace_back(ldPrior);
            initialisationMatrices.emplace_back(subjectTCMean);
            initialisationMatrices.emplace_back(subjectTCAAt);
            initialisationMatrices.emplace_back(subjectTCXXt);
            
        } else {
            throw std::runtime_error("Run's time courses weren't saved the first time it was picked in a batch! Subject: " + subjectInformation.subjectID); 
        }     
    }
    
    return initialisationMatrices;
}
///////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalModels::CleanHRF::resetSubjects()
{
    precisionModel_->resetSubjects();
    
    return;
}
////////////////////////////////////////////////////////////////////////////////
void PROFUMO::CohortModelling::GroupTemporalModels::CleanHRF::signFlip(const arma::frowvec spatialSigns)
{
    
    precisionModel_->signFlip(spatialSigns);
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalModels::CleanHRF::updateTimeCourseModel(const bool bigData, const float posteriorRho, const int subjectNum)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalModels::CleanHRF::saveTimeCourseModel(const std::string directory) const
{
    // Save HRF
    for (const auto& hrfPrior : hrfPriors_) {
        const unsigned int T = hrfPrior.first;
        hrfPrior.second.hrf->save(directory + "HRF_T" + std::to_string(T) + ".hdf5", arma::hdf5_binary);
        hrfPrior.second.K->save(directory + "HRFCovariance_T" + std::to_string(T) + ".hdf5", arma::hdf5_binary);
    }
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalModels::CleanHRF::loadTimeCourseModel(const std::string directory)
{
    // Load HRF
    for (const auto& hrfPrior : hrfPriors_) {
        const unsigned int T = hrfPrior.first;
        hrfPrior.second.hrf->load(directory + "HRF_T" + std::to_string(T) + ".hdf5", arma::hdf5_binary);
        hrfPrior.second.K->load(directory + "HRFCovariance_T" + std::to_string(T) + ".hdf5", arma::hdf5_binary);
    }
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

float PROFUMO::CohortModelling::GroupTemporalModels::CleanHRF::getTimeCourseModelKL(const bool bigData) const
{
    return 0.0;
}

////////////////////////////////////////////////////////////////////////////////
