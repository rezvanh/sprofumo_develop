// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "NoisyHRF.h"

#include <armadillo>

#include "Utilities/HRFModelling.h"

#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;

////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupTemporalModels::NoisyHRF::NoisyHRF(std::shared_ptr<GroupTemporalPrecisionModel> temporalPrecisionModel, const unsigned int M, const float TR, const std::string hrfFile, const VBModules::PrecisionMatrices::ComponentwiseGammaPrecision::Parameters noisePrior, const float priorRelaxation, const float posteriorRelaxation)
: GroupTemporalModel(temporalPrecisionModel), M_(M), TR_(TR), priorRelaxation_(priorRelaxation), posteriorRelaxation_(posteriorRelaxation), hrfFile_(hrfFile), noisePrior_(noisePrior)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SubjectModelling::Subject::TemporalModel PROFUMO::CohortModelling::GroupTemporalModels::NoisyHRF::getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick, const std::string bigDataDirBatch)
{
    // Make the subject model
    SubjectModelling::Subject::TemporalModel subjectModel;
    
    if (firstBatchfirstPick[0]){
        // Get the precision matrix model
        subjectModel.precisions = precisionModel_->getSubjectModel(subjectInformation);
    } else {
        subjectModel.precisions = precisionModel_->getSubjectModel(subjectInformation, {false, firstBatchfirstPick[1]}, bigDataDirBatch);
    }
    
    // Populate with run models
    for (const RunInformation& runInformation : subjectInformation.runs) {
        const RunID runID = runInformation.runID;
        const unsigned int T = runInformation.T;
        // Make a Run model
        SubjectModelling::Run::Model<SubjectModelling::A_Post> runModel;
        // Check to see if we have made an HRF prior before
        if (hrfPriors_.count(T) == 0) {
            hrfPriors_.emplace(T, Utilities::generateHRFPrior(hrfFile_, T, TR_, M_, priorRelaxation_, posteriorRelaxation_));
        }
        // Make the clean HRF-based TCs
        Modules::PrecisionMatrix_Parent* cleanAlpha = subjectModel.precisions.runLevel.models.at(runID).model.get();
        std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation;
        std::shared_ptr<std::vector<arma::fmat>> InitialisationCleanSignal;
        std::shared_ptr<std::vector<arma::fmat>> InitialisationNoise;
        
        if (firstBatchfirstPick[0]){
            std::unique_ptr<MFModels::A_VBPosterior> cleanA = std::make_unique<MFModels::A::KroneckerHRF>(cleanAlpha, hrfPriors_.at(T));
            // Noise precision
            std::unique_ptr<Modules::PrecisionMatrix_VBPosterior> noiseAlpha = std::make_unique<VBModules::PrecisionMatrices::ComponentwiseGammaPrecision>(noisePrior_);
            // And put together
            MFModels::A::AdditiveMultivariateNormal::Parameters APrior; APrior.M = M_; APrior.T = T;
            runModel.model = std::make_shared<MFModels::A::AdditiveMultivariateNormal>(std::move(cleanA), std::move(noiseAlpha), APrior);

        } else {
            std::vector<arma::fmat> initialisationMatrices = loadInitialisation(subjectInformation, firstBatchfirstPick[1], bigDataDirBatch, runInformation.runID);
            if (firstBatchfirstPick[1]){
                
                std::unique_ptr<Modules::PrecisionMatrix_VBPosterior> noiseAlphaPrior = std::make_unique<VBModules::PrecisionMatrices::ComponentwiseGammaPrecision>(noisePrior_);
                arma::fmat groupTCMean = arma::zeros<arma::fmat>(M_,T);
                arma::fmat groupTCAAt = arma::trace( *hrfPriors_.at(T).K ) * linalg::inv_sympd( initialisationMatrices[0] );
                arma::fmat groupTCCleanMean = arma::zeros<arma::fmat>(M_,T);
                arma::fmat groupTCCleanAAt = arma::trace( *hrfPriors_.at(T).K ) * linalg::inv_sympd( initialisationMatrices[0] );
                arma::fmat groupTCCleanXXt = T * linalg::inv_sympd( initialisationMatrices[0] );
                
                arma::fmat groupNoisePrecMat = noiseAlphaPrior->getExpectations().X;
                float prior_ldn = noiseAlphaPrior->getExpectations().logDetX;    
                arma::fvec ldnPrior(1);
                ldnPrior.at(0) = prior_ldn;
                
                arma::fmat groupNoiseTCMean = arma::zeros<arma::fmat>(M_,T);
                arma::fmat groupNoiseTCCovar = linalg::inv_sympd( noiseAlphaPrior->getExpectations().X );
                
                initialisationMatrices.emplace_back(groupTCMean);
                initialisationMatrices.emplace_back(groupTCAAt);
                initialisationMatrices.emplace_back(groupTCCleanMean);
                initialisationMatrices.emplace_back(groupTCCleanAAt);
                initialisationMatrices.emplace_back(groupTCCleanXXt);
                initialisationMatrices.emplace_back(groupNoisePrecMat);
                initialisationMatrices.emplace_back(ldnPrior);
                initialisationMatrices.emplace_back(groupNoiseTCMean);
                initialisationMatrices.emplace_back(groupNoiseTCCovar);
                
                std::vector<arma::fmat> cleanSignalMatrices;
                cleanSignalMatrices.emplace_back(initialisationMatrices[0]);
                cleanSignalMatrices.emplace_back(initialisationMatrices[1]);
                cleanSignalMatrices.emplace_back(initialisationMatrices[4]);//mean
                cleanSignalMatrices.emplace_back(initialisationMatrices[5]);//aat
                cleanSignalMatrices.emplace_back(initialisationMatrices[6]);//xxt
                
                std::vector<arma::fmat> noiseMatrices;
                noiseMatrices.emplace_back(initialisationMatrices[7]);//n precmat
                noiseMatrices.emplace_back(initialisationMatrices[8]);// n logdet
                noiseMatrices.emplace_back(initialisationMatrices[9]);// n mean
                noiseMatrices.emplace_back(initialisationMatrices[10]);// n cov
                
                expectationInitialisation = std::make_shared<std::vector<arma::fmat>>(initialisationMatrices);
                InitialisationCleanSignal = std::make_shared<std::vector<arma::fmat>>(cleanSignalMatrices);
                InitialisationNoise= std::make_shared<std::vector<arma::fmat>>(noiseMatrices);
                
                std::unique_ptr<MFModels::A_VBPosterior> cleanA = std::make_unique<MFModels::A::KroneckerHRF>(cleanAlpha, hrfPriors_.at(T), true, InitialisationCleanSignal);
                // Noise precision
                std::unique_ptr<Modules::PrecisionMatrix_VBPosterior> noiseAlpha = std::make_unique<VBModules::PrecisionMatrices::ComponentwiseGammaPrecision>(noisePrior_, true, InitialisationNoise);
                // And put together
                MFModels::A::AdditiveMultivariateNormal::Parameters APrior; APrior.M = M_; APrior.T = T;
                runModel.model = std::make_shared<MFModels::A::AdditiveMultivariateNormal>(std::move(cleanA), std::move(noiseAlpha), APrior, true, expectationInitialisation);
                
            } else {
            
                std::vector<arma::fmat> cleanSignalMatrices;
                cleanSignalMatrices.emplace_back(initialisationMatrices[0]);
                cleanSignalMatrices.emplace_back(initialisationMatrices[1]);
                cleanSignalMatrices.emplace_back(initialisationMatrices[4]);//mean
                cleanSignalMatrices.emplace_back(initialisationMatrices[5]);//aat
                cleanSignalMatrices.emplace_back(initialisationMatrices[6]);//xxt
                
                std::vector<arma::fmat> noiseMatrices;
                noiseMatrices.emplace_back(initialisationMatrices[7]);//n precmat
                noiseMatrices.emplace_back(initialisationMatrices[8]);// n logdet
                noiseMatrices.emplace_back(initialisationMatrices[9]);// n mean
                noiseMatrices.emplace_back(initialisationMatrices[10]);// n cov
                
                expectationInitialisation = std::make_shared<std::vector<arma::fmat>>(initialisationMatrices);
                InitialisationCleanSignal = std::make_shared<std::vector<arma::fmat>>(cleanSignalMatrices);
                InitialisationNoise= std::make_shared<std::vector<arma::fmat>>(noiseMatrices);
                
                
                //std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = std::make_shared<std::vector<arma::fmat>>(initialisationMatrices);
                std::unique_ptr<MFModels::A_VBPosterior> cleanA = std::make_unique<MFModels::A::KroneckerHRF>(cleanAlpha, hrfPriors_.at(T), true, InitialisationCleanSignal);
                // Noise precision
                std::unique_ptr<Modules::PrecisionMatrix_VBPosterior> noiseAlpha = std::make_unique<VBModules::PrecisionMatrices::ComponentwiseGammaPrecision>(noisePrior_, true, InitialisationNoise);
                // And put together
                MFModels::A::AdditiveMultivariateNormal::Parameters APrior; APrior.M = M_; APrior.T = T;
                runModel.model = std::make_shared<MFModels::A::AdditiveMultivariateNormal>(std::move(cleanA), std::move(noiseAlpha), APrior, true, expectationInitialisation);
            }
        }          
        //std::shared_ptr<Modules::MatrixMean_Parent> sTM_MeansClean;
        //Modules::MatrixMeans::P2C ETimeCourses;
        //ETimeCourses.X = previousA; ETimeCourses.X2 = arma::square(previousA);            
        //sTM_MeansClean = std::make_shared<VBModules::CopyParent<Modules::MatrixMeans::C2P, Modules::MatrixMeans::P2C>>(ETimeCourses);
        runModel.isInternalModel = true;
        // And record
        subjectModel.timeCourses.models.emplace(runID, runModel);
    }
    
    return subjectModel;
}

////////////////////////////////////////////////////////////////////////////////
std::vector<arma::fmat> PROFUMO::CohortModelling::GroupTemporalModels::NoisyHRF::loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick, const std::string bigDataDirBatch, const std::string runIDDir)
{
    std::vector<arma::fmat> initialisationMatrices;
    if (firstPick){
        const std::string dirMean = bigDataDirBatch + "GroupInitialisation" + "/" + "GroupTemporalModel" + "/" + "PrecisionMatrixModel" + "/" + "Mean.hdf5"; //membership
        std::ifstream fileMean(dirMean.c_str());
        
        const std::string dirLogDet = bigDataDirBatch + "GroupInitialisation" + "/" + "GroupTemporalModel" + "/" + "PrecisionMatrixModel" + "/"+ "logDeterminant.txt"; //membership
        std::ifstream fileLogDet(dirLogDet.c_str());
        
        
        if (fileMean.good() && fileLogDet.good()) {
            arma::fmat groupPrecMat = Utilities::loadDataMatrix(dirMean);
            
            float prior_ld;    
            fileLogDet >> prior_ld;
            arma::fvec ldPrior(1);
            ldPrior.at(0) = prior_ld;
            
            initialisationMatrices.emplace_back(groupPrecMat);
            initialisationMatrices.emplace_back(ldPrior);
        
        } else {
            throw std::runtime_error("initial group temporal precision matrix model wasn't saved!") ; 
        }
        fileMean.close();
        fileLogDet.close();
        
    } else {
        const std::string dirPrecMean = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "TemporalPrecisionMatrix.post" +"/" + "Mean.hdf5"; //membership
        std::ifstream filePrecMean(dirPrecMean.c_str());
                    
        const std::string dirLogDet = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "TemporalPrecisionMatrix.post" +"/" + "logDeterminant.txt";//membership
        std::ifstream fileLogDet(dirLogDet.c_str());
        
        const std::string dirMean = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "Runs" + "/" + runIDDir + "/" + "TimeCourses.post" + "/" + "Means.hdf5"; //membership
        std::ifstream fileMean(dirMean.c_str());
                    
        const std::string dirInnerProd = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "Runs" + "/" + runIDDir + "/" + "TimeCourses.post" + "/" + "InnerProduct.hdf5"; //membership
        std::ifstream fileInnerProd(dirInnerProd.c_str()); 
        
        
        const std::string dirCleanMean = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "Runs" + "/" + runIDDir + "/" + "TimeCourses.post" + "/" +"CleanTimeCourses"+ "/"+ "Means.hdf5"; //membership
        std::ifstream fileCleanMean(dirCleanMean.c_str());
        
        const std::string dirCleanInnerProd = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "Runs" + "/" + runIDDir + "/" + "TimeCourses.post" + "/" +"CleanTimeCourses"+ "/"+ "InnerProduct.hdf5"; //membership
        std::ifstream fileCleanInnerProd(dirCleanInnerProd.c_str());
        
        const std::string dirCleanInnerProdInvCov = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "Runs" + "/" + runIDDir + "/" + "TimeCourses.post" + "/" + "CleanTimeCourses"+ "/" + "InnerProduct_InverseCovariance.hdf5"; //membership
        std::ifstream fileCleanInnerProdInvCov(dirCleanInnerProdInvCov.c_str()); 
        
        const std::string dirNoisePrecMean = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "Runs" + "/" + runIDDir + "/" + "TimeCourses.post" + "/" +"Noise"+ "/" + "PrecisionMatrix" + "/"+ "Mean.hdf5"; //membership
        std::ifstream fileNoisePrecMean(dirNoisePrecMean.c_str());
        
        const std::string dirNoiseLogDet = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "Runs" + "/" + runIDDir + "/" + "TimeCourses.post" + "/" +"Noise"+ "/" + "PrecisionMatrix" + "/"+ "logDeterminant.txt"; //membership
        std::ifstream fileNoiseLogDet(dirNoiseLogDet.c_str());          
        
        const std::string dirNoiseTCMean = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "Runs" + "/" + runIDDir + "/" + "TimeCourses.post" + "/" +"Noise"+ "/" + "TimeCourses" + "/"+ "Means.hdf5"; //membership
        std::ifstream fileNoiseTCMean(dirNoiseTCMean.c_str());
        
        const std::string dirNoiseTCCovar = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "Runs" + "/" + runIDDir + "/" + "TimeCourses.post" + "/" +"Noise"+ "/" + "TimeCourses" + "/"+ "Covariance.hdf5"; //membership
        std::ifstream fileNoiseTCCovar(dirNoiseTCCovar.c_str());

        // load covariance
        //const std::string dirPrecMat = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "Runs" +"/" +runInformation.runID + "/"+"TimeCourses.post"+"/"+ "InnerProduct";
        //std::ifstream filePrec(dirPrecMat.c_str());
        if (filePrecMean.good() && fileLogDet.good() && fileMean.good() && fileInnerProd.good() && fileCleanMean.good() && fileCleanInnerProd.good() && fileCleanInnerProdInvCov.good() && fileNoisePrecMean.good() && fileNoiseLogDet.good() && fileNoiseTCMean.good() && fileNoiseTCCovar.good()) {
            arma::fmat subjectPrecMat = Utilities::loadDataMatrix(dirPrecMean);
            
            float prior_ld;    
            fileLogDet >> prior_ld;
            arma::fvec ldPrior(1);
            ldPrior.at(0) = prior_ld;
            
            arma::fmat subjectTCMean = Utilities::loadDataMatrix(dirMean);
            arma::fmat subjectTCAAt = Utilities::loadDataMatrix(dirInnerProd);
            arma::fmat subjectCleanTCMean = Utilities::loadDataMatrix(dirCleanMean);
            arma::fmat subjectCleanTCAAt = Utilities::loadDataMatrix(dirCleanInnerProd);
            arma::fmat subjectCleanTCXXt = Utilities::loadDataMatrix(dirCleanInnerProdInvCov);
            
            arma::fmat subjectNoisePrecMat = Utilities::loadDataMatrix(dirNoisePrecMean);
            
            float prior_ldn;    
            fileNoiseLogDet >> prior_ldn;
            arma::fvec ldnPrior(1);
            ldnPrior.at(0) = prior_ldn;
            
            arma::fmat subjectNoiseTCMean = Utilities::loadDataMatrix(dirNoiseTCMean);
            arma::fmat subjectNoiseTCCovar = Utilities::loadDataMatrix(dirNoiseTCCovar);
            
            initialisationMatrices.emplace_back(subjectPrecMat);
            initialisationMatrices.emplace_back(ldPrior);
            initialisationMatrices.emplace_back(subjectTCMean);
            initialisationMatrices.emplace_back(subjectTCAAt);
            initialisationMatrices.emplace_back(subjectCleanTCMean);
            initialisationMatrices.emplace_back(subjectCleanTCAAt);
            initialisationMatrices.emplace_back(subjectCleanTCXXt);
            
            initialisationMatrices.emplace_back(subjectNoisePrecMat);
            initialisationMatrices.emplace_back(ldnPrior);
            initialisationMatrices.emplace_back(subjectNoiseTCMean);
            initialisationMatrices.emplace_back(subjectNoiseTCCovar);
            
        } else {
            throw std::runtime_error("Run's time courses weren't saved the first time it was picked in a batch! Subject: " + subjectInformation.subjectID); 
        }
        filePrecMean.close();
        fileLogDet.close();
        fileMean.close();
        fileInnerProd.close();
        fileCleanMean.close();
        fileCleanInnerProd.close();
        fileCleanInnerProdInvCov.close();
        fileNoisePrecMean.close();
        fileNoiseLogDet.close();
        fileNoiseTCMean.close();
        fileNoiseTCCovar.close();            
    }
    
    
    return initialisationMatrices;
}
///////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalModels::NoisyHRF::resetSubjects()
{
    precisionModel_->resetSubjects();
    
    return;
}
////////////////////////////////////////////////////////////////////////////////
void PROFUMO::CohortModelling::GroupTemporalModels::NoisyHRF::signFlip(const arma::frowvec spatialSigns)
{
    
    precisionModel_->signFlip(spatialSigns);
    
    return;
} 
////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalModels::NoisyHRF::updateTimeCourseModel(const bool bigData, const float posteriorRho, const int subjectNum)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalModels::NoisyHRF::saveTimeCourseModel(const std::string directory) const
{
    // Save HRF
    for (const auto& hrfPrior : hrfPriors_) {
        const unsigned int T = hrfPrior.first;
        hrfPrior.second.hrf->save(directory + "HRF_T" + std::to_string(T) + ".hdf5", arma::hdf5_binary);
        hrfPrior.second.K->save(directory + "HRFCovariance_T" + std::to_string(T) + ".hdf5", arma::hdf5_binary);
    }
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalModels::NoisyHRF::loadTimeCourseModel(const std::string directory)
{
    // Load HRF
    for (const auto& hrfPrior : hrfPriors_) {
        const unsigned int T = hrfPrior.first;
        hrfPrior.second.hrf->load(directory + "HRF_T" + std::to_string(T) + ".hdf5", arma::hdf5_binary);
        hrfPrior.second.K->load(directory + "HRFCovariance_T" + std::to_string(T) + ".hdf5", arma::hdf5_binary);
    }
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

float PROFUMO::CohortModelling::GroupTemporalModels::NoisyHRF::getTimeCourseModelKL(const bool bigData) const
{
    return 0.0;
}

////////////////////////////////////////////////////////////////////////////////
