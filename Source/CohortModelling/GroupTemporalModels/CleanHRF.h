// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a group-level time course model based on an HRF.

#ifndef COHORT_MODELLING_GROUP_TEMPORAL_MODELS_CLEAN_HRF_H
#define COHORT_MODELLING_GROUP_TEMPORAL_MODELS_CLEAN_HRF_H

#include <memory>
#include <string>
#include <map>

#include "VBModules/Constant.h"
#include "VBModules/CopyParent.h"

#include "SubjectModelling/Subject.h"
#include "CohortModelling/GroupModel.h"

#include "MFModels/A/KroneckerHRF.h"

#include "Utilities/Timer.h"
#include "Utilities/DataIO.h"

namespace PROFUMO
{
    namespace CohortModelling
    {
        namespace GroupTemporalModels
        {
            
            class CleanHRF :
                public GroupTemporalModel
            {
            public:
                // Constructor
                CleanHRF(std::shared_ptr<GroupTemporalPrecisionModel> temporalPrecisionModel, const unsigned int M, const float TR, const std::string hrfFile, const float priorRelaxation=0.0, const float posteriorRelaxation=0.0);
                
                virtual SubjectModelling::Subject::TemporalModel getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick = {true, true}, const std::string bigDataDirBatch = {""});
                
                virtual void resetSubjects();
                
                virtual void signFlip(const arma::frowvec spatialSigns);
                
            private:
                virtual void updateTimeCourseModel(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                virtual std::vector<arma::fmat> loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick = true, const std::string bigDataDirBatch = {""}, const std::string runIDDir = {""});
                
                virtual void saveTimeCourseModel(const std::string directory) const;

                virtual void loadTimeCourseModel(const std::string directory);
                
                virtual float getTimeCourseModelKL(const bool bigData = false) const;
                
                // Parameters, hyperpriors etc
                const unsigned int M_;
                const float TR_, priorRelaxation_, posteriorRelaxation_;
                const std::string hrfFile_;
                std::map<unsigned int, MFModels::A::KroneckerHRF::Parameters> hrfPriors_;
            };
            
        }
    }
}
#endif
