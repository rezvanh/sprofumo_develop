// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2017, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "AdditiveGaussian.h"

#include <sys/stat.h>

////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupSpatialModels::AdditiveGaussian::AdditiveGaussian(std::shared_ptr<Modules::MatrixMean_VBPosterior> groupMeans, const MFModels::P::AdditiveGaussian::Parameters prior, const float dofCorrectionFactor)
: groupMeans_(groupMeans), prior_(prior), dofCorrectionFactor_(dofCorrectionFactor)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SubjectModelling::Subject::SpatialModel PROFUMO::CohortModelling::GroupSpatialModels::AdditiveGaussian::getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick, const std::string bigDataDirBatch)
{
    SubjectModelling::Subject::SpatialModel subjectModel;
    if (firstBatchfirstPick[0]){
        subjectModel.model = std::make_shared<MFModels::P::AdditiveGaussian>(groupMeans_.get(), prior_, dofCorrectionFactor_);
    } else {
        std::vector<arma::fmat> initialisationMatrices = loadInitialisation(subjectInformation, firstBatchfirstPick[1], bigDataDirBatch);
        std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = std::make_shared<std::vector<arma::fmat>>(initialisationMatrices);
        subjectModel.model = std::make_shared<MFModels::P::AdditiveGaussian>(groupMeans_.get(), prior_, dofCorrectionFactor_, true, expectationInitialisation);            
    }
    subjectModel.isInternalModel = true;
    
    return subjectModel;
}

////////////////////////////////////////////////////////////////////////////////
std::vector<arma::fmat> PROFUMO::CohortModelling::GroupSpatialModels::AdditiveGaussian::loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick, const std::string bigDataDirBatch, const std::string runIDDir)
{
    std::vector<arma::fmat> initialisationMatrices;
    if (firstPick){
        const std::string dirMean = bigDataDirBatch + "GroupInitialisation" + "/" + "GroupSpatialModel" + "/"+ "InitialMaps.hdf5";//"Means.hdf5";//membership
        std::ifstream fileMean(dirMean.c_str());     
        
        if (fileMean.good()) {
            arma::fmat groupSpatialMean = Utilities::loadDataMatrix(dirMean);
            
            float prior_a = 20.0;
            float prior_b = 10.0;   
            arma::fmat groupCovariance = (1.0 / std::sqrt(prior_a/prior_b)) * arma::eye<arma::fmat>(prior_.M,prior_.M);;
                        
            arma::fvec agPrior(2);
            agPrior.at(0) = prior_a;
            agPrior.at(1) = prior_b;            
            
            initialisationMatrices.emplace_back(groupSpatialMean);
            initialisationMatrices.emplace_back(groupCovariance);
            initialisationMatrices.emplace_back(agPrior);
            
                                
        } else {
            throw std::runtime_error("initial group spatial model wasn't saved!");
        }  
        fileMean.close();
        
    } else {
        const std::string dirMean = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "SpatialMaps.post" + "/"+ "Means.hdf5"; //membership
        std::ifstream fileMean(dirMean.c_str());
        
        const std::string dirSigma = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "SpatialMaps.post" + "/"+ "Covariance.hdf5"; //membership
        std::ifstream fileSigma(dirSigma.c_str());
        
        const std::string dirPrec = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "SpatialMaps.post" + "/"+ "AdditivePrecision.txt";
        std::ifstream filePrec(dirPrec.c_str(), std::ios_base::in);        
        
        if (fileMean.good() && fileSigma.good() && filePrec.good()) {
            arma::fmat subjectSpatialMean = Utilities::loadDataMatrix(dirMean);
            
            arma::fmat subjectCovariance = Utilities::loadDataMatrix(dirSigma);
            
            std::string astr, bstr;
            float prior_a, prior_b;    
            filePrec >> astr >> prior_a >> bstr >> prior_b;
            
            arma::fvec agPrior(2);
            agPrior.at(0) = prior_a;
            agPrior.at(1) = prior_b;
            
            
            initialisationMatrices.emplace_back(subjectSpatialMean);
            initialisationMatrices.emplace_back(subjectCovariance);
            initialisationMatrices.emplace_back(agPrior);
            
                                
        } else {
            throw std::runtime_error("Subject's spatial model wasn't saved the first time it was picked in a batch!" + subjectInformation.subjectID); 
        }  
        fileMean.close();
    
    }
    
    
    return initialisationMatrices;
}
///////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupSpatialModels::AdditiveGaussian::resetSubjects()
{
    groupMeans_->resetParentModule();
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

arma::frowvec PROFUMO::CohortModelling::GroupSpatialModels::AdditiveGaussian::signFlip()
{
    arma::fmat interimGroupMap = getGroupMaps(); 
    const arma::frowvec skews = arma::mean(arma::pow(interimGroupMap, 3), 0);
    groupMeans_->signFlip(skews);
    //groupMeans_->getExpectations().X.each_row() %= arma::sign( skews );
    
    
    return skews;
}
////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::CohortModelling::GroupSpatialModels::AdditiveGaussian::getGroupMaps() const
{
    return groupMeans_->getExpectations().X;

}

////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::CohortModelling::GroupSpatialModels::AdditiveGaussian::getGroupMeans() const
{
    return groupMeans_->getExpectations().X;

}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupSpatialModels::AdditiveGaussian::updateModel(const bool bigData, const float posteriorRho, const int subjectNum)
{
    Utilities::Timer timer;
    
    groupMeans_->update(bigData, posteriorRho, subjectNum);
    timer.printTimeElapsed("Group maps"); timer.reset();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupSpatialModels::AdditiveGaussian::saveModel(const std::string directory) const
{
    // Voxelwise means
    const std::string meansDir = directory + "Means.post" + "/";
    mkdir( meansDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    groupMeans_->save(meansDir);
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupSpatialModels::AdditiveGaussian::loadModel(const std::string directory)
{
    // Voxelwise means
    const std::string meansDir = directory + "Means.post" + "/";
    groupMeans_->load(meansDir);
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

float PROFUMO::CohortModelling::GroupSpatialModels::AdditiveGaussian::getModelKL(const bool bigData) const
{
    // Have to correct for D.O.F.!
    return dofCorrectionFactor_ * groupMeans_->getKL(bigData);
}

////////////////////////////////////////////////////////////////////////////////
