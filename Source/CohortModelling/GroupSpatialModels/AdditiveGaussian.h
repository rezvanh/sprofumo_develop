// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2017, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a group-level spatial model based around the AdditiveGaussian 
// subject map model.

#ifndef COHORT_MODELLING_GROUP_SPATIAL_MODELS_ADDITIVE_GAUSSIAN_H
#define COHORT_MODELLING_GROUP_SPATIAL_MODELS_ADDITIVE_GAUSSIAN_H

#include <memory>
#include <string>
#include <armadillo>

#include "SubjectModelling/Subject.h"
#include "CohortModelling/GroupModel.h"

#include "MFModels/P/AdditiveGaussian.h"
#include "MFModels/P/Parcellation.h"
#include "MFModels/P/PottsParcellation.h"


#include "ModuleList.h"
#include "VBModules/Constant.h"
#include "VBModules/CopyParent.h"

#include "VBModules/PrecisionMatrices/Wishart.h"
#include "VBModules/PrecisionMatrices/HierarchicalWishart.h"
#include "VBModules/PrecisionMatrices/ComponentwiseGammaPrecision.h"
#include "VBModules/MatrixMeans/IndependentMeans.h"
#include "VBModules/MatrixPrecisions/IndependentGamma.h"
#include "VBModules/MembershipProbabilities/IndependentDirichlet.h"

#include "ModuleList.h"
#include "MFModels/P/AdditiveGaussian.h"

#include "Utilities/DataIO.h"
#include "Utilities/Timer.h"


namespace PROFUMO
{
    namespace CohortModelling
    {
        namespace GroupSpatialModels
        {
            
            class AdditiveGaussian :
                public GroupSpatialModel
            {
            public:
                AdditiveGaussian(std::shared_ptr<Modules::MatrixMean_VBPosterior> groupMeans, const MFModels::P::AdditiveGaussian::Parameters prior, const float dofCorrectionFactor);
                
                virtual SubjectModelling::Subject::SpatialModel getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick = {true, true}, const std::string bigDataDirBatch = {""});
                
                virtual void resetSubjects();
                
                virtual arma::fmat getGroupMaps() const;
                
                virtual arma::fmat getGroupMeans() const;
                
                virtual arma::frowvec signFlip();
                
            private:
                virtual void updateModel(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                virtual std::vector<arma::fmat> loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick = true, const std::string bigDataDirBatch = {""}, const std::string runIDDir = {""});
                
                virtual void saveModel(const std::string directory) const;

                virtual void loadModel(const std::string directory);
                
                virtual float getModelKL(const bool bigData = false) const;
                
                // Hyperpriors
                std::shared_ptr<Modules::MatrixMean_VBPosterior> groupMeans_;
                
                // Parameters
                const MFModels::P::AdditiveGaussian::Parameters prior_;
                const float dofCorrectionFactor_;
            };
            
        }
    }
}
#endif
