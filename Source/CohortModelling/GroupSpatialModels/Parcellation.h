// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a group-level spatial model based around subject-level parcellations.

#ifndef COHORT_MODELLING_GROUP_SPATIAL_MODELS_PARCELLATION_H
#define COHORT_MODELLING_GROUP_SPATIAL_MODELS_PARCELLATION_H

#include <memory>
#include <string>
#include <sys/stat.h>
#include <armadillo>
#include <vector>

#include "SubjectModelling/Subject.h"
#include "CohortModelling/GroupModel.h"

#include "ModuleList.h"
#include "VBModules/Constant.h"
#include "VBModules/CopyParent.h"
#include "VBModules/PrecisionMatrices/Wishart.h"
#include "VBModules/PrecisionMatrices/HierarchicalWishart.h"
#include "VBModules/PrecisionMatrices/ComponentwiseGammaPrecision.h"
#include "VBModules/MatrixMeans/IndependentMeans.h"
#include "VBModules/MatrixPrecisions/IndependentGamma.h"
#include "VBModules/MembershipProbabilities/IndependentDirichlet.h"


#include "ModuleList.h"
#include "MFModels/P/DGMM.h"
#include "MFModels/P/AdditiveGaussian.h"
#include "MFModels/P/Parcellation.h"
#include "MFModels/P/PottsParcellation.h"

#include "Utilities/Timer.h"
#include "Utilities/DataIO.h"


namespace PROFUMO
{
    namespace CohortModelling
    {
        namespace GroupSpatialModels
        {
            
            template<class Implementation>
            class Parcellation :
                public GroupSpatialModel
            {
            public:
                Parcellation(std::shared_ptr<Modules::MatrixMean_VBPosterior> means, std::shared_ptr<Modules::MatrixPrecision_VBPosterior> precisions, std::shared_ptr<Modules::MembershipProbability_VBPosterior> memberships, const typename Implementation::Parameters prior, const float dofCorrectionFactor = 1.0);
                
                virtual SubjectModelling::Subject::SpatialModel getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick = {true, true}, const std::string bigDataDirBatch = {""});
                
                virtual void resetSubjects();
                
                virtual arma::fmat getGroupMaps() const;
                
                virtual arma::fmat getGroupMeans() const;
                
                virtual arma::frowvec signFlip();
                
            private:
                virtual void updateModel(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                virtual std::vector<arma::fmat> loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick = true, const std::string bigDataDirBatch = {""}, const std::string runIDDir = {""});
                
                virtual void saveModel(const std::string directory) const;

                virtual void loadModel(const std::string directory);
                
                virtual float getModelKL(const bool bigData = false) const;
                
                
                // Hyperpriors
                std::shared_ptr<Modules::MatrixMean_VBPosterior> means_;
                std::shared_ptr<Modules::MatrixPrecision_VBPosterior> precisions_;
                std::shared_ptr<Modules::MembershipProbability_VBPosterior> memberships_;
                
                // Parameters
                const typename Implementation::Parameters prior_;
                const float dofCorrectionFactor_;
            };
            
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
//                    Implementation of template functions                    //
////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
PROFUMO::CohortModelling::GroupSpatialModels::Parcellation<Implementation>::Parcellation(std::shared_ptr<Modules::MatrixMean_VBPosterior> means, std::shared_ptr<Modules::MatrixPrecision_VBPosterior> precisions, std::shared_ptr<Modules::MembershipProbability_VBPosterior> memberships, const typename Implementation::Parameters prior, const float dofCorrectionFactor)
: means_(means), precisions_(precisions), memberships_(memberships), prior_(prior), dofCorrectionFactor_(dofCorrectionFactor)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
PROFUMO::SubjectModelling::Subject::SpatialModel PROFUMO::CohortModelling::GroupSpatialModels::Parcellation<Implementation>::getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick, const std::string bigDataDirBatch)
{
    SubjectModelling::Subject::SpatialModel subjectModel;
    if (firstBatchfirstPick[0]){
        subjectModel.model = std::make_shared<Implementation>(means_.get(), precisions_.get(), memberships_.get(), prior_, dofCorrectionFactor_);
    } else {
        std::vector<arma::fmat> initialisationMatrices = loadInitialisation(subjectInformation, firstBatchfirstPick[1], bigDataDirBatch);        
        std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = std::make_shared<std::vector<arma::fmat>>(initialisationMatrices);
        subjectModel.model = std::make_shared<Implementation>(means_.get(), precisions_.get(), memberships_.get(), prior_, dofCorrectionFactor_, true, expectationInitialisation);    
    }
    subjectModel.isInternalModel = true;
    
    return subjectModel;
}

///////////////////////////////////////////////////////////////////////////////
template<class Implementation>
void PROFUMO::CohortModelling::GroupSpatialModels::Parcellation<Implementation>::resetSubjects()
{
    means_->resetParentModule();
    precisions_->resetParentModule();
    memberships_->resetParentModule();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
template<class Implementation>
std::vector<arma::fmat> PROFUMO::CohortModelling::GroupSpatialModels::Parcellation<Implementation>::loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick, const std::string bigDataDirBatch, const std::string runIDDir)
{
    std::vector<arma::fmat> initialisationMatrices;
    if (firstPick){
        //initialise based on the initialise group model
        const std::string dirMean = bigDataDirBatch + "GroupInitialisation" + "/" + "GroupSpatialModel" + "/" + "InitialMaps.hdf5";//"Means.post" +"/" + "Means.hdf5"; //membership
        std::ifstream fileMean(dirMean.c_str());
        
        const std::string dirPrec = bigDataDirBatch + "GroupInitialisation" + "/" + "GroupSpatialModel" + "/" + "Precisions.post" + "/"+ "Means.hdf5"; //membership
        std::ifstream filePrec(dirPrec.c_str());
        
        const std::string dirMemProb = bigDataDirBatch + "GroupInitialisation" + "/" + "GroupSpatialModel" + "/" + "Memberships.post" + "/" + "Class_2" + "/"+ "Probabilities.hdf5";
        std::ifstream fileMemProb(dirMemProb.c_str());        
        
        
        if (fileMean.good() && filePrec.good() && fileMemProb.good() ) {
            arma::fmat groupSpatialMean = Utilities::loadDataMatrix(dirMean);
                        
            arma::fmat groupSpatialPrec = 1.0 / (Utilities::loadDataMatrix(dirPrec));                
                        
            arma::fmat groupMemProb = Utilities::loadDataMatrix(dirMemProb);            
                        
            initialisationMatrices.emplace_back(groupSpatialMean);
            initialisationMatrices.emplace_back(groupSpatialPrec);
            initialisationMatrices.emplace_back(groupMemProb);
                 
        
        } else {
            throw std::runtime_error("initial group spatial model wasn't saved!") ; 
        }
        fileMean.close();
        filePrec.close();
        fileMemProb.close();
    } else {
        //initialise based on the last time the subject was picked in 
        const std::string dirMean = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "SpatialMaps.post" +"/" + "Means.hdf5"; //membership
        std::ifstream fileMean(dirMean.c_str());
        
        const std::string dirPrec = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "SpatialMaps.post" +"/" + "Variances.hdf5"; //membership
        std::ifstream filePrec(dirPrec.c_str());
        
        const std::string dirMemProb = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "SpatialMaps.post" + "/" + "MembershipProbabilities.hdf5";
        std::ifstream fileMemProb(dirMemProb.c_str());
                
        if (fileMean.good() && filePrec.good() && fileMemProb.good()) {
            arma::fmat subjectSpatialMean = Utilities::loadDataMatrix(dirMean);
                        
            arma::fmat subjectSpatialPrec = Utilities::loadDataMatrix(dirPrec);
                        
            arma::fmat subjectMemProb = Utilities::loadDataMatrix(dirMemProb);
            
            initialisationMatrices.emplace_back(subjectSpatialMean);
            initialisationMatrices.emplace_back(subjectSpatialPrec);
            initialisationMatrices.emplace_back(subjectMemProb);
                    
        } else {
            throw std::runtime_error("Subject's spatial model wasn't saved the first time it was picked in a batch!" + subjectInformation.subjectID); 
        }
        fileMean.close();
        filePrec.close();
        fileMemProb.close();
    
    }
    
    
    return initialisationMatrices;
}

////////////////////////////////////////////////////////////////////////////////
template<class Implementation>
arma::frowvec PROFUMO::CohortModelling::GroupSpatialModels::Parcellation<Implementation>::signFlip()
{
    arma::fmat interimGroupMap = getGroupMaps(); 
    const arma::frowvec skews = arma::mean(arma::pow(interimGroupMap, 3), 0);
    means_->signFlip(skews);
    //means_->getExpectations().X.each_row() %= arma::sign( skews );
    
    
    return skews;
}
////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
arma::fmat PROFUMO::CohortModelling::GroupSpatialModels::Parcellation<Implementation>::getGroupMaps() const
{
    // Rescale memberships (useful if prior is relatively strong)
    Modules::MembershipProbabilities::P2C pi = memberships_->getExpectations();
    arma::fmat memberships = arma::zeros<arma::fmat>(prior_.V, prior_.M);
    for (unsigned int m = 0; m < prior_.M; ++m) {
        memberships.col(m) = pi.p[m];
    } 
    memberships -= arma::min(arma::vectorise( memberships ));
    memberships /= arma::max(arma::vectorise( memberships ));
    
    // And combine with means
    arma::fmat means = means_->getExpectations().X;
    
    return means % memberships;
}

////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
arma::fmat PROFUMO::CohortModelling::GroupSpatialModels::Parcellation<Implementation>::getGroupMeans() const
{
    // And combine with means
    arma::fmat means = means_->getExpectations().X;
    
    return means;
}
////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
void PROFUMO::CohortModelling::GroupSpatialModels::Parcellation<Implementation>::updateModel(const bool bigData, const float posteriorRho, const int subjectNum)
{
    Utilities::Timer timer;
    
    means_->update(bigData, posteriorRho, subjectNum);
    timer.printTimeElapsed("Group map means"); timer.reset();
    
    precisions_->update(bigData, posteriorRho, subjectNum);
    timer.printTimeElapsed("Group map precisions"); timer.reset();
    
    memberships_->update(bigData, posteriorRho, subjectNum);
    timer.printTimeElapsed("Group map memberships"); timer.reset();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
void PROFUMO::CohortModelling::GroupSpatialModels::Parcellation<Implementation>::saveModel(const std::string directory) const
{
    // Voxelwise means
    const std::string meansDir = directory + "Means.post" + "/";
    mkdir( meansDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    means_->save(meansDir);
    
    // Voxelwise precisions
    const std::string precisionsDir = directory + "Precisions.post" + "/";
    mkdir( precisionsDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    precisions_->save(precisionsDir);
    
    // Voxelwise memberships
    const std::string membershipsDir = directory + "Memberships.post" + "/";
    mkdir( membershipsDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    memberships_->save(membershipsDir);
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
void PROFUMO::CohortModelling::GroupSpatialModels::Parcellation<Implementation>::loadModel(const std::string directory)
{
    // Voxelwise means
    const std::string meansDir = directory + "Means.post" + "/";
    means_->load(meansDir);
    
    // Voxelwise precisions
    const std::string precisionsDir = directory + "Precisions.post" + "/";
    precisions_->load(precisionsDir);
    
    // Voxelwise memberships
    const std::string membershipsDir = directory + "Memberships.post" + "/";
    memberships_->load(membershipsDir);
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
float PROFUMO::CohortModelling::GroupSpatialModels::Parcellation<Implementation>::getModelKL(const bool bigData) const
{
    float KL = 0.0;
    
    // Have to correct for D.O.F.!
    KL += dofCorrectionFactor_ * means_->getKL(bigData);
    KL += dofCorrectionFactor_ * precisions_->getKL(bigData);
    KL += dofCorrectionFactor_ * memberships_->getKL(bigData);
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////
#endif
