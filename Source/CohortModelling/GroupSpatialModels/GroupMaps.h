// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a generic class that contains a group level set of spatial maps that 
// are consistent across a set of subjects.

#ifndef COHORT_MODELLING_GROUP_SPATIAL_MODELS_GROUP_MAPS_H
#define COHORT_MODELLING_GROUP_SPATIAL_MODELS_GROUP_MAPS_H

#include <memory>
#include <string>
#include <armadillo>

#include "SubjectModelling/Subject.h"
#include "CohortModelling/GroupModel.h"

namespace PROFUMO
{
    namespace CohortModelling
    {
        namespace GroupSpatialModels
        {
            
            class GroupMaps :
                public GroupSpatialModel
            {
            public:
                GroupMaps(std::shared_ptr<SubjectModelling::P_Post> groupMaps);
                
                virtual SubjectModelling::Subject::SpatialModel getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick = {true, true}, const std::string bigDataDirBatch = {""});
                
                virtual void resetSubjects();
                
                virtual arma::fmat getGroupMaps() const;
                
                virtual arma::fmat getGroupMeans() const;
                
                virtual arma::frowvec signFlip();
                
            private:
                std::shared_ptr<SubjectModelling::P_Post> groupMaps_;
                
                virtual void updateModel(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                virtual std::vector<arma::fmat> loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick = true, const std::string bigDataDirBatch = {""}, const std::string runIDDir = {""});
                
                virtual void saveModel(const std::string directory) const;

                virtual void loadModel(const std::string directory);
                
                virtual float getModelKL(const bool bigData = false) const;
                
            };
            
        }
    }
}
#endif
