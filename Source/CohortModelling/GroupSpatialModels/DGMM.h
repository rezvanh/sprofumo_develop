// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a group-level spatial model based around the DGMM subject map model.

#ifndef COHORT_MODELLING_GROUP_SPATIAL_MODELS_DGMM_H
#define COHORT_MODELLING_GROUP_SPATIAL_MODELS_DGMM_H

#include <memory>
#include <string>
#include <armadillo>
#include <vector>
#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <dirent.h>

#include "SubjectModelling/Subject.h"
#include "CohortModelling/GroupModel.h"


#include "ModuleList.h"
#include "VBModules/Constant.h"
#include "VBModules/CopyParent.h"

#include "VBModules/PrecisionMatrices/Wishart.h"
#include "VBModules/PrecisionMatrices/HierarchicalWishart.h"
#include "VBModules/PrecisionMatrices/ComponentwiseGammaPrecision.h"
#include "VBModules/MatrixMeans/IndependentMeans.h"
#include "VBModules/MatrixPrecisions/IndependentGamma.h"
#include "VBModules/MembershipProbabilities/IndependentDirichlet.h"


#include "MFModels/P/DGMM.h"
#include "MFModels/P/AdditiveGaussian.h"
#include "MFModels/P/Parcellation.h"
#include "MFModels/P/PottsParcellation.h"

#include "Utilities/Timer.h"
#include "Utilities/DataIO.h"

namespace PROFUMO
{
    namespace CohortModelling
    {
        namespace GroupSpatialModels
        {
            
            class DGMM :
                public GroupSpatialModel
            {
            public:
                DGMM(std::shared_ptr<Modules::MatrixMean_VBPosterior> signalMeans, std::shared_ptr<Modules::MatrixPrecision_VBPosterior> signalPrecisions, std::shared_ptr<Modules::MatrixPrecision_VBPosterior> noisePrecisions, std::shared_ptr<Modules::MembershipProbability_VBPosterior> memberships, const MFModels::P::DGMM::Parameters prior, const float dofCorrectionFactor);
                
                virtual SubjectModelling::Subject::SpatialModel getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick = {true, true}, const std::string bigDataDirBatch = {""});
                
                virtual void resetSubjects();
                
                virtual arma::fmat getGroupMaps() const;
                
                virtual arma::fmat getGroupMeans() const;
                
                virtual arma::frowvec signFlip();
                
                
            private:
                virtual void updateModel(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                virtual std::vector<arma::fmat> loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick = true, const std::string bigDataDirBatch = {""}, const std::string runIDDir = {""});
                
                virtual void saveModel(const std::string directory) const;

                virtual void loadModel(const std::string directory);
                
                virtual float getModelKL(const bool bigData = false) const;
                
                // Hyperpriors
                std::shared_ptr<Modules::MatrixMean_VBPosterior> signalMeans_;
                std::shared_ptr<Modules::MatrixPrecision_VBPosterior> signalPrecisions_;
                std::shared_ptr<Modules::MatrixPrecision_VBPosterior> noisePrecisions_;
                std::shared_ptr<Modules::MembershipProbability_VBPosterior> memberships_;
                
                // Parameters
                const MFModels::P::DGMM::Parameters prior_;
                const float dofCorrectionFactor_;
            };
            
        }
    }
}
#endif
