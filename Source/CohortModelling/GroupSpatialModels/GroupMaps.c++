// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "GroupMaps.h"

////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupSpatialModels::GroupMaps::GroupMaps(std::shared_ptr<SubjectModelling::P_Post> groupMaps)
: groupMaps_(groupMaps)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SubjectModelling::Subject::SpatialModel PROFUMO::CohortModelling::GroupSpatialModels::GroupMaps::getSubjectModel(const SubjectInformation, const std::vector<bool> firstBatchfirstPick, const std::string bigDataDirBatch)
{
    SubjectModelling::Subject::SpatialModel subjectModel;
    subjectModel.model = groupMaps_;
    subjectModel.isInternalModel = false;
    
    return subjectModel;
}
///////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupSpatialModels::GroupMaps::resetSubjects()
{
    groupMaps_->resetParentModule();
    
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
std::vector<arma::fmat> PROFUMO::CohortModelling::GroupSpatialModels::GroupMaps::loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick, const std::string bigDataDirBatch, const std::string runIDDir)
{
    std::vector<arma::fmat> initialisationMatrices;    
    return initialisationMatrices;
}
////////////////////////////////////////////////////////////////////////////////

arma::frowvec PROFUMO::CohortModelling::GroupSpatialModels::GroupMaps::signFlip()
{
    arma::fmat interimGroupMap = getGroupMaps(); 
    const arma::frowvec skews = arma::mean(arma::pow(interimGroupMap, 3), 0);
    groupMaps_->signFlip( skews );
    
    
    return skews;
}
////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::CohortModelling::GroupSpatialModels::GroupMaps::getGroupMaps() const
{
    return groupMaps_->getExpectations().P;
}

////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::CohortModelling::GroupSpatialModels::GroupMaps::getGroupMeans() const
{
    return groupMaps_->getExpectations().P;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupSpatialModels::GroupMaps::updateModel(const bool bigData, const float posteriorRho, const int subjectNum)
{
    groupMaps_->update(bigData, posteriorRho, subjectNum);
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupSpatialModels::GroupMaps::saveModel(const std::string directory) const
{
    groupMaps_->save(directory);
    return;
}
////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupSpatialModels::GroupMaps::loadModel(const std::string directory)
{
    groupMaps_->load(directory);
    return;
}
////////////////////////////////////////////////////////////////////////////////

float PROFUMO::CohortModelling::GroupSpatialModels::GroupMaps::getModelKL(const bool bigData) const
{
    return groupMaps_->getKL(bigData);
}

////////////////////////////////////////////////////////////////////////////////
