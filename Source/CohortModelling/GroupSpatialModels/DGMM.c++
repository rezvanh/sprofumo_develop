// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include <memory>
#include <vector>
#include <string>
#include <armadillo>
#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <dirent.h>

#include "DGMM.h"

#include <sys/stat.h>

#include <cmath>

#include "Utilities/LinearAlgebra.h"

////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupSpatialModels::DGMM::DGMM(std::shared_ptr<Modules::MatrixMean_VBPosterior> signalMeans, std::shared_ptr<Modules::MatrixPrecision_VBPosterior> signalPrecisions, std::shared_ptr<Modules::MatrixPrecision_VBPosterior> noisePrecisions, std::shared_ptr<Modules::MembershipProbability_VBPosterior> memberships, const MFModels::P::DGMM::Parameters prior, const float dofCorrectionFactor)
: signalMeans_(signalMeans), signalPrecisions_(signalPrecisions), noisePrecisions_(noisePrecisions), memberships_(memberships), prior_(prior), dofCorrectionFactor_(dofCorrectionFactor)
{
    return;
}

///////////////////////////////////////////////////////////////////////////////

PROFUMO::SubjectModelling::Subject::SpatialModel PROFUMO::CohortModelling::GroupSpatialModels::DGMM::getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick, const std::string bigDataDirBatch)
{
    SubjectModelling::Subject::SpatialModel subjectModel;
    if (firstBatchfirstPick[0]){
        subjectModel.model = std::make_shared<MFModels::P::DGMM>(signalMeans_.get(), signalPrecisions_.get(), noisePrecisions_.get(), memberships_.get(), prior_, dofCorrectionFactor_);
    } else { 
        
        std::vector<arma::fmat> initialisationMatrices = loadInitialisation(subjectInformation, firstBatchfirstPick[1], bigDataDirBatch);
        
        std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = std::make_shared<std::vector<arma::fmat>>(initialisationMatrices);
        subjectModel.model = std::make_shared<MFModels::P::DGMM>(signalMeans_.get(), signalPrecisions_.get(), noisePrecisions_.get(), memberships_.get(), prior_, dofCorrectionFactor_, true, expectationInitialisation);        
    }
    subjectModel.isInternalModel = true;
    
    return subjectModel;
}

///////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupSpatialModels::DGMM::resetSubjects()
{
    signalMeans_->resetParentModule();
    signalPrecisions_->resetParentModule();
    noisePrecisions_->resetParentModule();
    memberships_->resetParentModule();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
std::vector<arma::fmat> PROFUMO::CohortModelling::GroupSpatialModels::DGMM::loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick, const std::string bigDataDirBatch, const std::string runIDDir)
{
    std::vector<arma::fmat> initialisationMatrices;
    if (firstPick){
        //initialise based on the initialise group model
        const std::string dirMean = bigDataDirBatch + "GroupInitialisation" + "/" + "GroupSpatialModel" + "/" + "InitialMaps.hdf5";//SignalMeans.post" +"/" + "Means.hdf5"; //membership
        std::ifstream fileMean(dirMean.c_str());
        
        const std::string dirPrec = bigDataDirBatch + "GroupInitialisation" + "/" + "GroupSpatialModel" + "/" + "SignalPrecisions.post" + "/" + "Means.hdf5"; //membership
        std::ifstream filePrec(dirPrec.c_str());
        
        const std::string dirMemProb = bigDataDirBatch + "GroupInitialisation" + "/" + "GroupSpatialModel" + "/" + "Memberships.post" + "/" + "Class_2" + "/"+ "Probabilities.hdf5";
        std::ifstream fileMemProb(dirMemProb.c_str());        
               
        const std::string dirNoisePrec = bigDataDirBatch + "GroupInitialisation" + "/" + "GroupSpatialModel" + "/" + "NoisePrecisions.post" +"/" + "Means.hdf5"; //membership
        std::ifstream fileNoisePrec(dirNoisePrec.c_str());
        
        if (fileMean.good() && filePrec.good() && fileMemProb.good() && fileNoisePrec.good() ) {
            arma::fmat groupSpatialMean = Utilities::loadDataMatrix(dirMean);
                        
            arma::fmat groupSpatialPrec = 1.0 / (Utilities::loadDataMatrix(dirPrec));
                
            arma::fmat groupNoiseMean = arma::zeros<arma::fmat>(prior_.V, prior_.M);
                
            arma::fmat groupNoisePrec = arma::repmat(1.0 / (Utilities::loadDataMatrix(dirNoisePrec)), 1, prior_.M);//1.0 / (Utilities::loadDataMatrix(dirNoisePrec));
                                    
            arma::fmat groupMemProb = Utilities::loadDataMatrix(dirMemProb);
            
            
            float prior_hMu = 1.0, prior_hSigma2 = 0.25;
            arma::frowvec groupNoiseScaling = prior_hMu * arma::ones<arma::frowvec>(prior_.M);
            arma::frowvec groupNoiseScalingVar = prior_hSigma2 * arma::ones<arma::frowvec>(prior_.M);
                        
            initialisationMatrices.emplace_back(groupSpatialMean);
            initialisationMatrices.emplace_back(groupSpatialPrec);
            initialisationMatrices.emplace_back(groupMemProb);
            
            initialisationMatrices.emplace_back(groupNoiseMean);
            initialisationMatrices.emplace_back(groupNoisePrec);
            initialisationMatrices.emplace_back(groupNoiseScaling);
            initialisationMatrices.emplace_back(groupNoiseScalingVar);        
        
        } else {
            throw std::runtime_error("initial group spatial model wasn't saved!") ; 
        }
        fileMean.close();
        filePrec.close();
        fileMemProb.close();
        fileNoisePrec.close();
    } else {
        //initialise based on the last time the subject was picked in 
        const std::string dirMean = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "SpatialMaps.post" +"/" +"Signal" + "/"+ "Means.hdf5"; //membership
        std::ifstream fileMean(dirMean.c_str());
        
        const std::string dirPrec = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "SpatialMaps.post" +"/" +"Signal" + "/"+ "Variances.hdf5"; //membership
        std::ifstream filePrec(dirPrec.c_str());
        
        const std::string dirMemProb = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "SpatialMaps.post" + "/" + "Signal" + "/"+ "MembershipProbabilities.hdf5";
        std::ifstream fileMemProb(dirMemProb.c_str());
        
        const std::string dirNoiseMean = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "SpatialMaps.post" +"/" +"Noise" + "/"+ "Means.hdf5"; //membership
        std::ifstream fileNoiseMean(dirNoiseMean.c_str());
        
        const std::string dirNoisePrec = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "SpatialMaps.post" +"/" +"Noise" + "/"+ "Variances.hdf5"; //membership
        std::ifstream fileNoisePrec(dirNoisePrec.c_str());
        
        const std::string dirNoiseScaling = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "SpatialMaps.post" +"/" +"Noise" + "/"+ "Scalings.hdf5"; //membership
        std::ifstream fileNoiseScaling(dirNoiseScaling.c_str());
        
        const std::string dirNoiseScalingVar = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "SpatialMaps.post" +"/" +"Noise" + "/"+ "ScalingVariances.hdf5"; //membership
        std::ifstream fileNoiseScalingVar(dirNoiseScalingVar.c_str());
        
        if (fileMean.good() && filePrec.good() && fileMemProb.good() && fileNoiseMean.good() && fileNoisePrec.good() && fileNoiseScaling.good() && fileNoiseScalingVar.good()) {
            arma::fmat subjectSpatialMean = Utilities::loadDataMatrix(dirMean);
                        
            arma::fmat subjectSpatialPrec = Utilities::loadDataMatrix(dirPrec);
                
            arma::fmat subjectNoiseMean = Utilities::loadDataMatrix(dirNoiseMean);
                
            arma::fmat subjectNoisePrec = Utilities::loadDataMatrix(dirNoisePrec);
                        
            arma::fmat subjectMemProb = Utilities::loadDataMatrix(dirMemProb);
                
            arma::frowvec subjectNoiseScaling = Utilities::loadDataMatrix(dirNoiseScaling);
            arma::frowvec subjectNoiseScalingVar = Utilities::loadDataMatrix(dirNoiseScalingVar);
            
            //arma::fmat memberships = subjectMemProb;
            //memberships -= arma::min(arma::vectorise( memberships ));
            //memberships /= arma::max(arma::vectorise( memberships ));
            // And combine with means
            //arma::fmat subjectSpatialMaps = subjectSpatialMean % memberships;
            //arma::fmat interimGroupMap = getGroupMaps();
            
            //const arma::frowvec subjectSkews = arma::mean(arma::pow(subjectSpatialMaps, 3), 0);
            //const arma::frowvec groupSkews = arma::mean(arma::pow(interimGroupMap, 3), 0); // / arma::pow(arma::mean(arma::pow(X, 2), 0), 1.5);
            // Can ignore divisor as we are only interested in the sign
    
            //subjectSpatialMean.each_row() %= arma::sign( subjectSkews ) % arma::sign( groupSkews );
                        
            initialisationMatrices.emplace_back(subjectSpatialMean);
            initialisationMatrices.emplace_back(subjectSpatialPrec);
            initialisationMatrices.emplace_back(subjectMemProb);
            
            initialisationMatrices.emplace_back(subjectNoiseMean);
            initialisationMatrices.emplace_back(subjectNoisePrec);
            initialisationMatrices.emplace_back(subjectNoiseScaling);
            initialisationMatrices.emplace_back(subjectNoiseScalingVar);
            
            
        } else {
            throw std::runtime_error("Subject's spatial model wasn't saved the first time it was picked in a batch!" + subjectInformation.subjectID); 
        }
        fileMean.close();
        filePrec.close();
        fileMemProb.close();
        fileNoiseMean.close();
        fileNoisePrec.close();
        fileNoiseScaling.close();
        fileNoiseScalingVar.close();
    
    }
    
    
    return initialisationMatrices;
} 
////////////////////////////////////////////////////////////////////////////////

arma::frowvec PROFUMO::CohortModelling::GroupSpatialModels::DGMM::signFlip()
{
    arma::fmat interimGroupMean = getGroupMeans(); //signalMeans_->getExpectations().X;
    //getGroupMaps(); 
    const arma::frowvec skews = arma::mean(arma::pow(interimGroupMean, 3), 0);
    signalMeans_->signFlip(skews);
    //signalMeans_->getExpectations().X.each_row() %= arma::sign( skews );
    
    
    return skews;
}              
////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::CohortModelling::GroupSpatialModels::DGMM::getGroupMaps() const
{
    // Rescale memberships (useful if prior is relatively strong)
    arma::fmat memberships = memberships_->getExpectations().p[1];
    //memberships -= arma::min(arma::vectorise( memberships ));
    //memberships /= arma::max(arma::vectorise( memberships ));
    
    // And combine with means
    arma::fmat means = signalMeans_->getExpectations().X;
    
    return means % memberships;
}

////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::CohortModelling::GroupSpatialModels::DGMM::getGroupMeans() const
{
    // And combine with means
    arma::fmat means = signalMeans_->getExpectations().X;
    
    return means;
}


////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupSpatialModels::DGMM::updateModel(const bool bigData, const float posteriorRho, const int subjectNum)
{
    Utilities::Timer timer;
    signalMeans_->update(bigData, posteriorRho, subjectNum);
    timer.printTimeElapsed("Group map signal means"); timer.reset();
    
    signalPrecisions_->update(bigData, posteriorRho, subjectNum);
    timer.printTimeElapsed("Group map signal precisions"); timer.reset();
    
    noisePrecisions_->update(bigData, posteriorRho, subjectNum);
    timer.printTimeElapsed("Group map noise precisions"); timer.reset();
    
    memberships_->update(bigData, posteriorRho, subjectNum);
    timer.printTimeElapsed("Group map memberships"); timer.reset();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupSpatialModels::DGMM::saveModel(const std::string directory) const
{
    // Voxelwise means
    const std::string meansDir = directory + "SignalMeans.post" + "/";
    mkdir( meansDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    signalMeans_->save(meansDir);
    
    // Voxelwise precisions
    const std::string precisionsDir = directory + "SignalPrecisions.post" + "/";
    mkdir( precisionsDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    signalPrecisions_->save(precisionsDir);
    
    // Noise
    const std::string noiseDir = directory + "NoisePrecisions.post" + "/";
    mkdir( noiseDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    noisePrecisions_->save(noiseDir);
    
    // Voxelwise memberships
    const std::string membershipsDir = directory + "Memberships.post" + "/";
    mkdir( membershipsDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    memberships_->save(membershipsDir);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
void PROFUMO::CohortModelling::GroupSpatialModels::DGMM::loadModel(const std::string directory)
{
    //std::cout << "loading existing GroupSpatialModels" << std::endl;
    // Voxelwise means
    const std::string meansDir = directory + "SignalMeans.post" + "/";
    signalMeans_->load(meansDir);
    
    // Voxelwise precisions
    const std::string precisionsDir = directory + "SignalPrecisions.post" + "/";
    signalPrecisions_->load(precisionsDir);
    
    // Noise
    const std::string noiseDir = directory + "NoisePrecisions.post" + "/";
    noisePrecisions_->load(noiseDir);
    
    // Voxelwise memberships
    const std::string membershipsDir = directory + "Memberships.post" + "/";
    memberships_->load(membershipsDir);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

float PROFUMO::CohortModelling::GroupSpatialModels::DGMM::getModelKL(const bool bigData) const
{
    float KL = 0.0;
    
    // Have to correct for D.O.F.!
    float smKL = signalMeans_->getKL(bigData);
    KL += dofCorrectionFactor_ * smKL;
    
    float spKL = signalPrecisions_->getKL(bigData);
    KL += dofCorrectionFactor_ * spKL;
    
    float npKL = noisePrecisions_->getKL(bigData);
    KL += dofCorrectionFactor_ * npKL;
    
    float memKL = memberships_->getKL(bigData);
    KL += dofCorrectionFactor_ * memKL;
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////
