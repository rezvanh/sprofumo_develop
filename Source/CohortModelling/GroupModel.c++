// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "GroupModel.h"

#include <sys/stat.h>

////////////////////////////////////////////////////////////////////////////////
// GroupTemporalModel
////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupTemporalModel::GroupTemporalModel(std::shared_ptr<GroupTemporalPrecisionModel> precisionModel)
: precisionModel_(precisionModel)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::CohortModelling::GroupTemporalModel::getGroupPrecisionMatrix() const
{
    return precisionModel_->getGroupPrecisionMatrix();
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalModel::updateModel(const bool bigData, const float posteriorRho, const int subjectNum)
{
    // Time courses
    updateTimeCourseModel(bigData, posteriorRho, subjectNum);
    // Then precision
    precisionModel_->update(bigData, posteriorRho, subjectNum);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalModel::saveModel(const std::string directory) const
{
    // Time courses
    saveTimeCourseModel(directory);
    
    // Then precision
    // Make directory
    const std::string precisionDir = directory + "PrecisionMatrixModel" + "/";
    mkdir( precisionDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    // Save
    precisionModel_->save(precisionDir);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalModel::loadModel(const std::string directory)
{
    // Time courses
    loadTimeCourseModel(directory);
    
    // Then precision
    // Make directory
    const std::string precisionDir = directory + "PrecisionMatrixModel" + "/";
    // Load
    precisionModel_->load(precisionDir);
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

float PROFUMO::CohortModelling::GroupTemporalModel::getModelKL(const bool bigData) const
{
    float KL = 0.0;
    
    // Time courses
    KL += getTimeCourseModelKL(bigData);
    // Then precision
    KL += precisionModel_->getKL(bigData);
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////
