// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2017
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a generic class that makes a set of weights with a consistent 
// correlation structure across a set of subjects.

#ifndef COHORT_MODELLING_GROUP_WEIGHT_MODELS_INDEPENDENT_WEIGHTS_H
#define COHORT_MODELLING_GROUP_WEIGHT_MODELS_INDEPENDENT_WEIGHTS_H

#include <memory>
#include <string>
#include <sys/stat.h>
#include <armadillo>
#include <vector>

#include "SubjectModelling/Subject.h"
#include "CohortModelling/GroupModel.h"

#include "ModuleList.h"

namespace PROFUMO
{
    namespace CohortModelling
    {
        namespace GroupWeightModels
        {
            
            template<class Implementation>
            class IndependentWeights :
                public GroupWeightModel
            {
            public:
                IndependentWeights(const typename Implementation::Parameters prior);
                
                virtual SubjectModelling::Subject::WeightModel getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick = {true, true}, const std::string bigDataDirBatch = {""});
                
                virtual void resetSubjects();
                
                virtual void signFlip(const arma::frowvec spatialSigns);
                
            private:
                // Prior
                const typename Implementation::Parameters prior_;
                
                virtual void updateModel(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                virtual std::vector<arma::fmat> loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick = true, const std::string bigDataDirBatch = {""}, const std::string runIDDir = {""});
                
                virtual void saveModel(const std::string directory) const;

                virtual void loadModel(const std::string directory);
                
                virtual float getModelKL(const bool bigData = false) const;
                
                
            };
            
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
//                    Implementation of template functions                    //
////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
PROFUMO::CohortModelling::GroupWeightModels::IndependentWeights<Implementation>::IndependentWeights(const typename Implementation::Parameters prior)
: prior_(prior)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
PROFUMO::SubjectModelling::Subject::WeightModel PROFUMO::CohortModelling::GroupWeightModels::IndependentWeights<Implementation>::getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick, const std::string bigDataDirBatch)
{
    // Make the subject model
    SubjectModelling::Subject::WeightModel subjectModel;
    // Populate with run models
    for (const RunInformation& runInformation : subjectInformation.runs) {
        // Make a Run model
        SubjectModelling::Run::Model<SubjectModelling::H_Post> runModel;
        if (firstBatchfirstPick[0]) {
            runModel.model = std::make_shared<Implementation>(prior_);
        } else {
            if (firstBatchfirstPick[1]){
                runModel.model = std::make_shared<Implementation>(prior_);
            } else {
                const std::string bigDataDirBatchRun = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "Runs" +"/" +runInformation.runID;
                std::vector<arma::fmat> initialisationMatrices = loadInitialisation(subjectInformation, firstBatchfirstPick[1], bigDataDirBatchRun);
                
                std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = std::make_shared<std::vector<arma::fmat>>(initialisationMatrices);
                    
                runModel.model = std::make_shared<Implementation>(prior_, true, expectationInitialisation);
            }
        }        
        runModel.isInternalModel = true;
        // And record
        subjectModel.models.emplace(runInformation.runID, runModel);
    }
    
    return subjectModel;
}
///////////////////////////////////////////////////////////////////////////////
template<class Implementation>
void PROFUMO::CohortModelling::GroupWeightModels::IndependentWeights<Implementation>::resetSubjects()
{
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
template<class Implementation>
std::vector<arma::fmat> PROFUMO::CohortModelling::GroupWeightModels::IndependentWeights<Implementation>::loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick, const std::string bigDataDirBatch, const std::string runIDDir)
{
    std::vector<arma::fmat> initialisationMatrices; 
    
    const std::string dirMean = bigDataDirBatch + "/"+"ComponentWeightings.post"+"/"+ "Means.hdf5";
    std::ifstream fileMean(dirMean.c_str());                
    if (fileMean.good()) {
        arma::fmat subjectWeightMean = Utilities::loadDataMatrix(dirMean);
        initialisationMatrices.emplace_back(subjectWeightMean);
        //initialisationMatrices.emplace_back(subjectWPrecMat);                            
    } else {
        throw std::runtime_error("Run's weight model wasn't saved the first time it was picked in a batch! Subject: " + subjectInformation.subjectID); 
    }
    fileMean.close();
    return initialisationMatrices;
} 
////////////////////////////////////////////////////////////////////////////////
template<class Implementation>
void PROFUMO::CohortModelling::GroupWeightModels::IndependentWeights<Implementation>::signFlip(const arma::frowvec spatialSigns)
{
    return;
} 
////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
void PROFUMO::CohortModelling::GroupWeightModels::IndependentWeights<Implementation>::updateModel(const bool bigData, const float posteriorRho, const int subjectNum)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
void PROFUMO::CohortModelling::GroupWeightModels::IndependentWeights<Implementation>::saveModel(const std::string /*directory*/) const
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
void PROFUMO::CohortModelling::GroupWeightModels::IndependentWeights<Implementation>::loadModel(const std::string /*directory*/)
{
    return;
}
////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
float PROFUMO::CohortModelling::GroupWeightModels::IndependentWeights<Implementation>::getModelKL(const bool bigData) const
{
    return 0.0;
}

////////////////////////////////////////////////////////////////////////////////
#endif
