// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a generic class that contains a group level set of weights that 
// are consistent across a set of subjects.

#ifndef COHORT_MODELLING_GROUP_WEIGHT_MODELS_GROUP_WEIGHTS_H
#define COHORT_MODELLING_GROUP_WEIGHT_MODELS_GROUP_WEIGHTS_H

#include <memory>
#include <string>

#include "Utilities/DataIO.h"

#include "SubjectModelling/Subject.h"
#include "CohortModelling/GroupModel.h"

#include "ModuleList.h"
#include "Module.h"
#include "Posterior.h"
#include "VBModules/Constant.h"


namespace PROFUMO
{
    namespace CohortModelling
    {
        namespace GroupWeightModels
        {
            
            class GroupWeights :
                public GroupWeightModel
            {
            public:
                GroupWeights(std::shared_ptr<SubjectModelling::H_Post> groupWeights);
                
                virtual SubjectModelling::Subject::WeightModel getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick = {true, true}, const std::string bigDataDirBatch = {""});
                
                virtual void resetSubjects();
                
                virtual void signFlip(const arma::frowvec spatialSigns);
            private:
                std::shared_ptr<SubjectModelling::H_Post> groupWeights_;
                
                virtual void updateModel(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                virtual std::vector<arma::fmat> loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick = true, const std::string bigDataDirBatch = {""}, const std::string runIDDir = {""});
                
                virtual void saveModel(const std::string directory) const;

                virtual void loadModel(const std::string directory);
                
                virtual float getModelKL(const bool bigData = false) const;
                
                
            };
            
        }
    }
}
#endif
