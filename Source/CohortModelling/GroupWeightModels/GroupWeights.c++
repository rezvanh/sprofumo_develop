// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "GroupWeights.h"

////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupWeightModels::GroupWeights::GroupWeights(std::shared_ptr<SubjectModelling::H_Post> groupWeights)
: groupWeights_(groupWeights)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SubjectModelling::Subject::WeightModel PROFUMO::CohortModelling::GroupWeightModels::GroupWeights::getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick, const std::string bigDataDirBatch)
{
    // Make the subject model
    SubjectModelling::Subject::WeightModel subjectModel;
    // Populate with run models
    for (const RunInformation& runInformation : subjectInformation.runs) {
        // Make a Run model
        SubjectModelling::Run::Model<SubjectModelling::H_Post> runModel;        
        runModel.model = groupWeights_;
        runModel.isInternalModel = false;
        // And record
        subjectModel.models.emplace(runInformation.runID, runModel);
    }
    
    return subjectModel;
}
///////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupWeightModels::GroupWeights::resetSubjects()
{
    groupWeights_->resetParentModule();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
std::vector<arma::fmat> PROFUMO::CohortModelling::GroupWeightModels::GroupWeights::loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick, const std::string bigDataDirBatch, const std::string runIDDir)
{
    std::vector<arma::fmat> initialisationMatrices;  
    return initialisationMatrices;
} 
////////////////////////////////////////////////////////////////////////////////
void PROFUMO::CohortModelling::GroupWeightModels::GroupWeights::signFlip(const arma::frowvec spatialSigns)
{
    groupWeights_->getExpectations().h %= arma::sign( spatialSigns );
    groupWeights_->getExpectations().hht.each_row() %= arma::sign( spatialSigns );
    groupWeights_->getExpectations().hht.each_col() %= arma::sign( spatialSigns.t() );
    return;
} 
////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupWeightModels::GroupWeights::updateModel(const bool bigData, const float posteriorRho, const int subjectNum)
{
    groupWeights_->update(bigData, posteriorRho, subjectNum);
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupWeightModels::GroupWeights::saveModel(const std::string directory) const
{
    groupWeights_->save(directory);
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupWeightModels::GroupWeights::loadModel(const std::string directory)
{
    groupWeights_->load(directory);
    return;
}

////////////////////////////////////////////////////////////////////////////////

float PROFUMO::CohortModelling::GroupWeightModels::GroupWeights::getModelKL(const bool bigData) const
{
    return groupWeights_->getKL(bigData);
}

////////////////////////////////////////////////////////////////////////////////
