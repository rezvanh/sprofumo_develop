// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a generic class that makes a set of weights with a consistent 
// set of means and correlation structure across a set of subjects.

#ifndef COHORT_MODELLING_GROUP_WEIGHT_MODELS_PARAMETERISED_WEIGHTS_H
#define COHORT_MODELLING_GROUP_WEIGHT_MODELS_PARAMETERISED_WEIGHTS_H

#include <memory>
#include <string>
#include <sys/stat.h>
#include <armadillo>
#include <vector>

#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;


#include "SubjectModelling/Subject.h"
#include "CohortModelling/GroupModel.h"

#include "ModuleList.h"
#include "VBModules/Constant.h"
#include "VBModules/CopyParent.h"

#include "VBModules/PrecisionMatrices/Wishart.h"
#include "VBModules/PrecisionMatrices/HierarchicalWishart.h"
#include "VBModules/PrecisionMatrices/ComponentwiseGammaPrecision.h"
#include "VBModules/MatrixMeans/IndependentMeans.h"
#include "VBModules/MatrixPrecisions/IndependentGamma.h"
#include "VBModules/MembershipProbabilities/IndependentDirichlet.h"

#include "Module.h"
#include "Posterior.h"
#include "ModuleList.h"
#include "MFModel.h"
#include "MFVariableModels.h"

#include "MFModels/H/MultivariateNormal.h"


#include "Utilities/DataIO.h"

namespace PROFUMO
{
    namespace CohortModelling
    {
        namespace GroupWeightModels
        {
            
            template<class Implementation>
            class ParameterisedWeights :
                public GroupWeightModel
            {
            public:
                ParameterisedWeights(std::shared_ptr<Modules::MatrixMean_VBPosterior> means, std::shared_ptr<Modules::PrecisionMatrix_VBPosterior> precisionMatrix, const typename Implementation::Parameters prior);
                
                virtual SubjectModelling::Subject::WeightModel getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick = {true, true}, const std::string bigDataDirBatch = {""});
                
                virtual void resetSubjects();
                
                virtual void signFlip(const arma::frowvec spatialSigns);
                
            private:
                // Group means
                std::shared_ptr<Modules::MatrixMean_VBPosterior> means_;
                // Group precision matrix - captures the between weight correlations
                std::shared_ptr<Modules::PrecisionMatrix_VBPosterior> precisionMatrix_;
                
                // Prior
                const typename Implementation::Parameters prior_;
                
                virtual void updateModel(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                virtual std::vector<arma::fmat> loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick = true, const std::string bigDataDirBatch = {""}, const std::string runIDDir = {""});
                
                virtual void saveModel(const std::string directory) const;

                virtual void loadModel(const std::string directory);
                
                virtual float getModelKL(const bool bigData = false) const;
                
                
            };
            
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
//                    Implementation of template functions                    //
////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
PROFUMO::CohortModelling::GroupWeightModels::ParameterisedWeights<Implementation>::ParameterisedWeights(std::shared_ptr<Modules::MatrixMean_VBPosterior> means, std::shared_ptr<Modules::PrecisionMatrix_VBPosterior> precisionMatrix, const typename Implementation::Parameters prior)
: means_(means), precisionMatrix_(precisionMatrix), prior_(prior)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
PROFUMO::SubjectModelling::Subject::WeightModel PROFUMO::CohortModelling::GroupWeightModels::ParameterisedWeights<Implementation>::getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick, const std::string bigDataDirBatch)
{
    // Make the subject model
    SubjectModelling::Subject::WeightModel subjectModel;
    
        
    if (firstBatchfirstPick[0]){
        // Populate with run models
        for (const RunInformation& runInformation : subjectInformation.runs) {
            // Make a Run model
            SubjectModelling::Run::Model<SubjectModelling::H_Post> runModel;
            runModel.model = std::make_shared<Implementation>(means_.get(), precisionMatrix_.get(), prior_);
            runModel.isInternalModel = true;
            // And record
            subjectModel.models.emplace(runInformation.runID, runModel);
        }
    } else {
        // Populate with run models
        for (const RunInformation& runInformation : subjectInformation.runs) {
            // Make a Run model            
            SubjectModelling::Run::Model<SubjectModelling::H_Post> runModel;
            //load mean
            std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation;
            
            if (firstBatchfirstPick[1]) {
                //from initialisation
                std::vector<arma::fmat> initialisationMatrices = loadInitialisation(subjectInformation, firstBatchfirstPick[1], bigDataDirBatch);
                expectationInitialisation = std::make_shared<std::vector<arma::fmat>>(initialisationMatrices);            
            } else {
                //from last subject run
                const std::string bigDataDirBatchRun = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "Runs" +"/" +runInformation.runID;
                std::vector<arma::fmat> initialisationMatrices = loadInitialisation(subjectInformation, firstBatchfirstPick[1], bigDataDirBatchRun);
                expectationInitialisation = std::make_shared<std::vector<arma::fmat>>(initialisationMatrices);
            }
            runModel.model = std::make_shared<Implementation>(means_.get(), precisionMatrix_.get(), prior_, true, expectationInitialisation);
            runModel.isInternalModel = true;
            // And record
            subjectModel.models.emplace(runInformation.runID, runModel);
        }
    
    }
        
    
    
    return subjectModel;
}
///////////////////////////////////////////////////////////////////////////////

template<class Implementation>
void PROFUMO::CohortModelling::GroupWeightModels::ParameterisedWeights<Implementation>::resetSubjects()
{
    means_->resetParentModule();
    precisionMatrix_->resetParentModule();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////]template<class Implementation>
template<class Implementation>
std::vector<arma::fmat> PROFUMO::CohortModelling::GroupWeightModels::ParameterisedWeights<Implementation>::loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick, const std::string bigDataDirBatch, const std::string runIDDir)
{
    std::vector<arma::fmat> initialisationMatrices;
    if (firstPick){
        //initialise based on the initialise group model
        const std::string dirMean = bigDataDirBatch + "GroupInitialisation" + "/" + "GroupWeightModel" + "/" + "Means.post" +"/" + "Means.hdf5"; //membership
        std::ifstream fileMean(dirMean.c_str());
        
        const std::string dirPrec = bigDataDirBatch + "GroupInitialisation" + "/" + "GroupWeightModel" + "/" + "PrecisionMatrix.post" + "/"+ "Mean.hdf5"; //membership
        std::ifstream filePrec(dirPrec.c_str());
                        
        if (fileMean.good() && filePrec.good()) {
            arma::fmat groupSpatialMean = Utilities::loadDataMatrix(dirMean);                        
            arma::fmat groupSpatialPrec = linalg::inv_sympd(Utilities::loadDataMatrix(dirPrec));
                                        
            initialisationMatrices.emplace_back(groupSpatialMean);
            initialisationMatrices.emplace_back(groupSpatialPrec);                           
        } else {
            throw std::runtime_error("initial group weight model wasn't saved!") ; 
        }
        fileMean.close();
        filePrec.close();
        
    } else {
        //initialise based on the last time the subject was picked 
        const std::string dirMean = bigDataDirBatch + "/"+"ComponentWeightings.post"+"/"+ "Means.hdf5";
        std::ifstream fileMean(dirMean.c_str());
        // load covariance
        const std::string dirPrecMat = bigDataDirBatch + "/"+"ComponentWeightings.post"+"/"+ "OuterProduct.hdf5";
        std::ifstream filePrec(dirPrecMat.c_str());
        
        if (fileMean.good() && filePrec.good()) {
            arma::fmat subjectWeightMean = Utilities::loadDataMatrix(dirMean);                                
            arma::fmat subjectWPrecMat = Utilities::loadDataMatrix(dirPrecMat);                                                
            initialisationMatrices.emplace_back(subjectWeightMean);
            initialisationMatrices.emplace_back(subjectWPrecMat);
                    
        } else {
            throw std::runtime_error("Run's weight model wasn't saved the first time it was picked in a batch! Subject: " + subjectInformation.subjectID); 
        }
        fileMean.close();
        filePrec.close();
 
    }
    
    
    return initialisationMatrices;
}
////////////////////////////////////////////////////////////////////////////////
template<class Implementation>
void PROFUMO::CohortModelling::GroupWeightModels::ParameterisedWeights<Implementation>::signFlip(const arma::frowvec spatialSigns)
{
    
    //means_->getExpectations().X %= arma::sign( spatialSigns.t() );
    means_->signFlip( spatialSigns );
    precisionMatrix_->signFlip(spatialSigns);
    //precisionMatrix_->getExpectations().X.each_row() %= arma::sign( spatialSigns );
    //precisionMatrix_->getExpectations().X.each_col() %= arma::sign( spatialSigns.t() );
    
    
    return;
} 
////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
void PROFUMO::CohortModelling::GroupWeightModels::ParameterisedWeights<Implementation>::updateModel(const bool bigData, const float posteriorRho, const int subjectNum)
{
    means_->update(bigData, posteriorRho, subjectNum);
    precisionMatrix_->update(bigData, posteriorRho, subjectNum);
    return;
}

////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
void PROFUMO::CohortModelling::GroupWeightModels::ParameterisedWeights<Implementation>::saveModel(const std::string directory) const
{
    const std::string meansDir = directory + "Means.post" + "/";
    mkdir( meansDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    means_->save(meansDir);
    
    const std::string precMatDir = directory + "PrecisionMatrix.post" + "/";
    mkdir( precMatDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    precisionMatrix_->save(precMatDir);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
void PROFUMO::CohortModelling::GroupWeightModels::ParameterisedWeights<Implementation>::loadModel(const std::string directory)
{
    const std::string meansDir = directory + "Means.post" + "/";
    means_->load(meansDir);
    
    const std::string precMatDir = directory + "PrecisionMatrix.post" + "/";
    precisionMatrix_->load(precMatDir);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
float PROFUMO::CohortModelling::GroupWeightModels::ParameterisedWeights<Implementation>::getModelKL(const bool bigData) const
{
    return means_->getKL(bigData) + precisionMatrix_->getKL(bigData);
}

////////////////////////////////////////////////////////////////////////////////
#endif
