// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "Cohort.h"

#include <sys/stat.h>

//////////////////////////////////////////////////////////////////////////////
// Updates

void PROFUMO::CohortModelling::Cohort::update(const bool bigData, const float posteriorRho, const int subjectNum, const bool updateGroup)
{
    updateSpatialModel(bigData, posteriorRho, subjectNum, updateGroup);
    updateWeightModel(bigData, posteriorRho, subjectNum, updateGroup);
    updateTemporalModel(bigData, posteriorRho, subjectNum, updateGroup);
    updateNoiseModel(bigData, posteriorRho, subjectNum, updateGroup);
    return;
}

void PROFUMO::CohortModelling::Cohort::updateSpatialModel(const bool bigData, const float posteriorRho, const int subjectNum, const bool updateGroup)
{
    Utilities::Timer timer;
    
    // Update subjects
    //for (auto& subject : subjects_) {
    #pragma omp parallel for schedule(dynamic)
    for (unsigned int s = 0; s < S_; ++s) {
        subjects_[s]->updateSpatialModel();
    }
    timer.printTimeElapsed("Subject spatial models"); timer.reset();
    
    // Update group model
    if (updateGroup) {
        groupSpatialModel_->update(bigData, posteriorRho, subjectNum);
    }
    
    
    timer.printTimeElapsed("Group spatial model"); timer.reset();
    
    return;
}

void PROFUMO::CohortModelling::Cohort::signFlipSpatialModel(const bool flipGroup, const bool flipSubjects)
{
    
    Utilities::Timer timer;
    // make sure groupmaps are positively skewed and subject maps are aligned 
    if (flipGroup) {
        arma::fmat spatialSignsG = groupSpatialModel_->signFlip();
        groupTemporalModel_->signFlip(spatialSignsG);
    }
    if (flipSubjects) {
        // Flipping subject signs to match group
        groupMeans = groupSpatialModel_->getGroupMeans();
        #pragma omp parallel for schedule(dynamic)
        for (unsigned int s = 0; s < S_; ++s) {
            arma::fmat spatialSigns = subjects_[s]->signFlipSubjectMap(groupMeans);
            //subjects_[s]->signFlipSubjectWeight(spatialSigns);
            subjects_[s]->signFlipSubjectTimecourse(spatialSigns);
            
            
        }
    }
    timer.printTimeElapsed("Spatial model sign flipping"); timer.reset();
    
    
    return;
    
}

void PROFUMO::CohortModelling::Cohort::updateWeightModel(const bool bigData, const float posteriorRho, const int subjectNum, const bool updateGroup)
{
    Utilities::Timer timer;
    
    // Update subjects
    #pragma omp parallel for schedule(dynamic)
    for (unsigned int s = 0; s < S_; ++s) {
        subjects_[s]->updateWeightModel();
    }
    timer.printTimeElapsed("Subject weight models"); timer.reset();
    
    // Update group model
    if (updateGroup) {
        groupWeightModel_->update(bigData, posteriorRho, subjectNum);
    }
    
    //we need to do some sign flips for big data to keep subjects and group aligned
    //if (bigData){ 
    //   groupWeightModel_->signFlipGroupWeight(spatialSigns);
    //    #pragma omp parallel for schedule(dynamic)
    //    for (unsigned int s = 0; s < S_; ++s) {
    //        subjects_[s]->signFlipSubjectWeight(spatialSigns);
    //    }
    //}
    timer.printTimeElapsed("Group weight model"); timer.reset();
    
    return;
}

void PROFUMO::CohortModelling::Cohort::updateTemporalModel(const bool bigData, const float posteriorRho, const int subjectNum, const bool updateGroup)
{
    Utilities::Timer timer;
    
    // Update subjects
    #pragma omp parallel for schedule(dynamic)
    for (unsigned int s = 0; s < S_; ++s) {
        subjects_[s]->updateTemporalModel();
    }
    timer.printTimeElapsed("Subject temporal models"); timer.reset();
    
    // Update group model
    if (updateGroup) {
        groupTemporalModel_->update(bigData, posteriorRho, subjectNum);
    }
    timer.printTimeElapsed("Group temporal model"); timer.reset();
    
    return;
}

void PROFUMO::CohortModelling::Cohort::updateNoiseModel(const bool bigData, const float posteriorRho, const int subjectNum, const bool updateGroup)
{
    Utilities::Timer timer;
    
    // Update subjects
    #pragma omp parallel for schedule(dynamic)
    for (unsigned int s = 0; s < S_; ++s) {
        subjects_[s]->updateNoiseModel();
    }
    timer.printTimeElapsed("Subject noise models"); timer.reset();
    
    // Update group model
    if (updateGroup) {
        groupNoiseModel_->update(bigData, posteriorRho, subjectNum);
    }
    timer.printTimeElapsed("Group noise model"); timer.reset();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
// Model switching

void PROFUMO::CohortModelling::Cohort::setSpatialModel(std::shared_ptr<GroupSpatialModel> newGroupSpatialModel)
{
    // Change the Subjects
    for (auto& subject : subjects_) {
        // Make the models
        auto sSM = newGroupSpatialModel->getSubjectModel( subject->getSubjectInformation() );
        // And then update the subject
        subject->setSpatialModel(sSM);
    }
    
    // Update the internal model
    groupSpatialModel_ = newGroupSpatialModel;
    
    return;
}

void PROFUMO::CohortModelling::Cohort::setWeightModel(std::shared_ptr<GroupWeightModel> newGroupWeightModel)
{
    // Change the Subjects
    for (auto& subject : subjects_) {
        // Make the models
        auto sWM = newGroupWeightModel->getSubjectModel( subject->getSubjectInformation() );
        // And then update the subject
        subject->setWeightModel(sWM);
    }
    
    // Update the internal model
    groupWeightModel_ = newGroupWeightModel;
    
    return;
}

void PROFUMO::CohortModelling::Cohort::setTemporalModel(std::shared_ptr<GroupTemporalModel> newGroupTemporalModel)
{
    // Change the Subjects
    for (auto& subject : subjects_) {
        // Make the models
        auto sTM = newGroupTemporalModel->getSubjectModel( subject->getSubjectInformation() );
        // And then update the subject
        subject->setTemporalModel(sTM);
    }
    
    // Update the internal model
    groupTemporalModel_ = newGroupTemporalModel;
    
    return;
}

void PROFUMO::CohortModelling::Cohort::setNoiseModel(std::shared_ptr<GroupNoiseModel> newGroupNoiseModel)
{
    // Change the Subjects
    for (auto& subject : subjects_) {
        // Make the models
        auto sNM = newGroupNoiseModel->getSubjectModel( subject->getSubjectInformation() );
        // And then update the subject
        subject->setNoiseModel(sNM);
    }
    
    // Update the internal model
    groupNoiseModel_ = newGroupNoiseModel;
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
// Save

void PROFUMO::CohortModelling::Cohort::save(const std::string directory, const bool saveSubjects, const bool saveGroup, const bool replaceSubjects) const
{
    // Save Subjects
    // Make subjects directory
    if (saveSubjects) {
        const std::string subjectsDir = directory + "Subjects" + "/";
        mkdir( subjectsDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        // And then save subjects in that
        for (const auto& subject : subjects_) {
            // Make directory
            const std::string subjectDir = subjectsDir + subject->getSubjectID() + "/";
            if (replaceSubjects) {
                mkdir( subjectDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
                // Save subject
                subject->save(subjectDir);
            } else {
                DIR* thisDir = opendir(subjectDir.c_str());
                if (thisDir) {
                    closedir(thisDir);
                } else {
                    mkdir( subjectDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
                    // Save subject
                    subject->save(subjectDir);
                }
                
            }
            
        }
    
    }
    
    if (saveGroup){
        // And group models
        const std::string GSMDir = directory + "GroupSpatialModel" + "/";
        mkdir( GSMDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        groupSpatialModel_->save(GSMDir);
        
        const std::string GWMDir = directory + "GroupWeightModel" + "/";
        mkdir( GWMDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        groupWeightModel_->save(GWMDir);
        
        const std::string GTMDir = directory + "GroupTemporalModel" + "/";
        mkdir( GTMDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        groupTemporalModel_->save(GTMDir);
        
        const std::string GNMDir = directory + "GroupNoiseModel" + "/";
        mkdir( GNMDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        groupNoiseModel_->save(GNMDir);
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
// Load existing Cohort model

void PROFUMO::CohortModelling::Cohort::load(const std::string directory)
{
    
    
    // load group models
    const std::string GSMDir = directory + "GroupSpatialModel" + "/";
    groupSpatialModel_->load(GSMDir);
    
    const std::string GWMDir = directory + "GroupWeightModel" + "/";
    groupWeightModel_->load(GWMDir);
    
    const std::string GTMDir = directory + "GroupTemporalModel" + "/";
    groupTemporalModel_->load(GTMDir);
    
    const std::string GNMDir = directory + "GroupNoiseModel" + "/";
    groupNoiseModel_->load(GNMDir);
    
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
// Clear

void PROFUMO::CohortModelling::Cohort::clear(const bool groupMapSignFlip)
{
    subjects_.clear();
    allSubjectInformations_.clear();
    groupSpatialModel_->resetSubjects();
    groupWeightModel_->resetSubjects();
    groupTemporalModel_->resetSubjects();
    groupNoiseModel_->resetSubjects();
    
    //if (groupMapSignFlip) {
    //    spatialSigns = groupSpatialModel_->signFlipGroupMap();
        
    //}
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

arma::frowvec PROFUMO::CohortModelling::Cohort::getFreeEnergy(const bool bigData, const int subjectNum, const float posteriorRho, const std::string bigDataDirBatch ) 
{
    //float Ftotal = 0.0;
    //float Fsub = 0.0;
    float Fgroup = 0.0;
    arma::frowvec Fsub = arma::zeros<arma::frowvec>(9); 
    arma::frowvec F = arma::zeros<arma::frowvec>(10);  
    //const std::string subjectsDir = bigDataDirBatch + "Subjects" + "/";
    //mkdir( subjectsDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    
    // Collect from Subjects
    //#pragma omp parallel for schedule(dynamic) reduction(+:Fsub)
    for (unsigned int s = 0; s < S_; ++s) {
        //Fsub += subjects_[s]->getFreeEnergy();
        //Fsub = posteriorRho*(subjects_[s]->getFreeEnergy())+(1-posteriorRho)*Fsub;
        //NOTE! the if condition below is only true when we are getting the offset free energy for batches
        if (posteriorRho < 1.0) {
            //arma::frowvec tempFsub = subjects_[s]->getFreeEnergy(true);
            Fsub += subjects_[s]->getFreeEnergy();
            //const std::string subjectDir = subjectsDir + subjects_[s]->getSubjectID() + "/";            
            //mkdir( subjectDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        } else {
            Fsub += subjects_[s]->getFreeEnergy();
        }
    }
    //upscale F as though the entire subject population (subjectNum) was used to compute FreeEnergy
    if (bigData){
        Fsub = ((float)subjectNum / (float)S_) * Fsub;    
    }
    
    // Then adjust with group Models
    float sKL = groupSpatialModel_->getKL();//bigData
    Fgroup -= sKL;//groupSpatialModel_->getKL(bigData);
    float wKL = groupWeightModel_->getKL();
    Fgroup -= wKL;//groupWeightModel_->getKL(bigData);
    float tKL = groupTemporalModel_->getKL();
    Fgroup -= tKL;//groupTemporalModel_->getKL(bigData);
    float nKL = groupNoiseModel_->getKL();
    Fgroup -= nKL;//groupNoiseModel_->getKL(bigData);
    
    //Ftotal = Fsub + Fgroup;
    F.head_cols(9)=Fsub;
    F.at(9)=Fgroup;
    
    
    //arma::frowvec F = { Fsub, Fgroup, Ftotal };
    
    return F;
}

/////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::CohortModelling::Cohort::getGroupMaps() const
{
    return groupSpatialModel_->getGroupMaps();
}
/////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::CohortModelling::Cohort::getGroupPrecisionMatrix() const
{
    return groupTemporalModel_->getGroupPrecisionMatrix();
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::Cohort::setTimeCourseNormalisation(const bool normalisation)
{
    for (auto& subject : subjects_) {
        subject->setTimeCourseNormalisation(normalisation);
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

std::vector<std::string> PROFUMO::CohortModelling::Cohort::getSubjectIDs() const
{
    return allSubjectInformations_;
}

/////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::CohortModelling::Cohort::getAllSubjectsFreeEnergy() 
{
    
    arma::fmat Fsub = arma::zeros<arma::fmat>(S_,9); 

    // Collect from Subjects
    //#pragma omp parallel for schedule(dynamic) reduction(+:Fsub)
    #pragma omp parallel for schedule(dynamic)
    for (unsigned int s = 0; s < S_; ++s) {
        Fsub.row(s)= subjects_[s]->getFreeEnergy();
        
    }
    //arma::frowvec F = { Fsub, Fgroup, Ftotal };
    
    return Fsub;
}
