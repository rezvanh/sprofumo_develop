// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Generates noise precisions, independent across runs.

#ifndef COHORT_MODELLING_GROUP_NOISE_MODELS_INDEPENDENT_RUNS_H
#define COHORT_MODELLING_GROUP_NOISE_MODELS_INDEPENDENT_RUNS_H

#include <string>

#include "SubjectModelling/Subject.h"
#include "CohortModelling/GroupModel.h"

#include "MFModels/Psi/GammaPrecision.h"

#include "Utilities/Timer.h"
#include "Utilities/DataIO.h"

namespace PROFUMO
{
    namespace CohortModelling
    {
        namespace GroupNoiseModels
        {
            
            class IndependentRuns :
                public GroupNoiseModel
            {
            public:
                IndependentRuns(const MFModels::Psi::GammaPrecision::Parameters prior);
                
                virtual SubjectModelling::Subject::NoiseModel getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick = {true, true}, const std::string bigDataDirBatch = {""});
                
                virtual void resetSubjects();
            private:
                const MFModels::Psi::GammaPrecision::Parameters prior_;
                
                virtual void updateModel(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                virtual std::vector<arma::fmat> loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick = true, const std::string bigDataDirBatch = {""}, const std::string runIDDir = {""});
                
                virtual void saveModel(const std::string directory) const;

                virtual void loadModel(const std::string directory);
                
                virtual float getModelKL(const bool bigData = false) const;
            };
            
        }
    }
}
#endif
