// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "IndependentRuns.h"

#include <memory>

////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupNoiseModels::IndependentRuns::IndependentRuns(const MFModels::Psi::GammaPrecision::Parameters prior)
: prior_(prior)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SubjectModelling::Subject::NoiseModel PROFUMO::CohortModelling::GroupNoiseModels::IndependentRuns::getSubjectModel(const SubjectInformation subjectInformation, const std::vector<bool> firstBatchfirstPick, const std::string bigDataDirBatch)
{
    // Make the subject model
    SubjectModelling::Subject::NoiseModel subjectModel;
    // Populate with run models
    for (const RunInformation& runInformation : subjectInformation.runs) {
        // Make a Run model
        SubjectModelling::Run::Model<SubjectModelling::Psi_Post> runModel;
        //runModel.model = std::make_shared<MFModels::Psi::GammaPrecision>(prior_);
        if (firstBatchfirstPick[0]){
            runModel.model = std::make_shared<MFModels::Psi::GammaPrecision>(prior_);
        } else {
            if (firstBatchfirstPick[1]){
                runModel.model = std::make_shared<MFModels::Psi::GammaPrecision>(prior_);
            } else {
                std::vector<arma::fmat> initialisationMatrices = loadInitialisation(subjectInformation, firstBatchfirstPick[1], bigDataDirBatch, runInformation.runID);
                MFModels::Psi::GammaPrecision::Parameters predefinedPrior;
                predefinedPrior.a=initialisationMatrices[0].at(0);
                predefinedPrior.b=initialisationMatrices[0].at(1);
                std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = std::make_shared<std::vector<arma::fmat>>(initialisationMatrices);
                runModel.model = std::make_shared<MFModels::Psi::GammaPrecision>(prior_, true, expectationInitialisation);            
            }
                
        }
        runModel.isInternalModel = true;
        // And record
        subjectModel.models.emplace(runInformation.runID, runModel);
    }
    
    return subjectModel;
}
////////////////////////////////////////////////////////////////////////////////
std::vector<arma::fmat> PROFUMO::CohortModelling::GroupNoiseModels::IndependentRuns::loadInitialisation(const SubjectInformation subjectInformation, const bool firstPick, const std::string bigDataDirBatch, const std::string runIDDir)
{
    std::vector<arma::fmat> initialisationMatrices;
    if (!firstPick){
        
        const std::string dirParams = bigDataDirBatch + "Subjects" + "/" + subjectInformation.subjectID + "/" + "Runs" + "/" + runIDDir + "/" + "NoisePrecision.post" +"/" + "GammaPosterior.txt"; 
        std::ifstream fileParams(dirParams.c_str());
        if (fileParams.good()) {                        
            std::string astr, bstr;
            float prior_a, prior_b;    
            fileParams >> astr >> prior_a >> bstr >> prior_b;
            arma::fvec agPrior(2);
            agPrior.at(0) = prior_a;
            agPrior.at(1) = prior_b;
            initialisationMatrices.emplace_back(agPrior);
            fileParams.close();
        } else {
            throw std::runtime_error("Run's noise model wasn't saved the first time it was picked in a batch!" + subjectInformation.subjectID); 
        }
    
    }
    return initialisationMatrices;
}
///////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupNoiseModels::IndependentRuns::resetSubjects()
{
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupNoiseModels::IndependentRuns::updateModel(const bool bigData, const float posteriorRho, const int subjectNum)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupNoiseModels::IndependentRuns::saveModel(const std::string) const
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupNoiseModels::IndependentRuns::loadModel(const std::string)
{
    return;
}
////////////////////////////////////////////////////////////////////////////////

float PROFUMO::CohortModelling::GroupNoiseModels::IndependentRuns::getModelKL(const bool bigData) const
{
    return 0.0;
}

////////////////////////////////////////////////////////////////////////////////
