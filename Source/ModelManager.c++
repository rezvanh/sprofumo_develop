// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "ModelManager.h"

#include <sys/stat.h>
#include <iomanip>
#include <stdexcept>
#include <cmath>
#include <string>
#include <vector>
#include <fstream>
#include <iterator>
#include <iostream>
#include <dirent.h>


#include "DataTypes.h"
#include "DataLoader.h"
#include "DataLoaders/FullRankDataLoader.h"
#include "DataLoaders/LowRankDataLoader.h"
#include "Utilities/DataNormalisation.h"

#include "CohortModelling/GroupSpatialModels/GroupMaps.h"
#include "CohortModelling/GroupSpatialModels/DGMM.h"
#include "CohortModelling/GroupSpatialModels/AdditiveGaussian.h"
#include "CohortModelling/GroupSpatialModels/Parcellation.h"
#include "CohortModelling/GroupWeightModels/GroupWeights.h"
#include "CohortModelling/GroupWeightModels/IndependentWeights.h"
#include "CohortModelling/GroupWeightModels/ParameterisedWeights.h"
#include "CohortModelling/GroupTemporalModels/MultivariateNormal.h"
#include "CohortModelling/GroupTemporalModels/CleanHRF.h"
#include "CohortModelling/GroupTemporalModels/NoisyHRF.h"
#include "CohortModelling/GroupTemporalPrecisionModels/GroupPrecisionMatrix.h"
#include "CohortModelling/GroupTemporalPrecisionModels/SubjectPrecisionMatrices.h"
#include "CohortModelling/GroupTemporalPrecisionModels/RunPrecisionMatrices.h"
#include "CohortModelling/GroupTemporalPrecisionModels/CategorisedRunPrecisionMatrices.h"
#include "CohortModelling/GroupNoiseModels/IndependentRuns.h"

#include "MFModels/P/DGMM.h"
#include "MFModels/P/AdditiveGaussian.h"
#include "MFModels/P/Parcellation.h"
#include "MFModels/P/PottsParcellation.h"
#include "MFModels/A/MultivariateNormal.h"
#include "MFModels/A/KroneckerHRF.h"
#include "MFModels/A/AdditiveMultivariateNormal.h"
#include "MFModels/H/MultivariateNormal.h"
#include "MFModels/H/SpikeSlab.h"
#include "MFModels/Psi/GammaPrecision.h"

#include "ModuleList.h"
#include "VBModules/Constant.h"
#include "VBModules/PrecisionMatrices/Wishart.h"
#include "VBModules/PrecisionMatrices/HierarchicalWishart.h"
#include "VBModules/PrecisionMatrices/ComponentwiseGammaPrecision.h"
#include "VBModules/MatrixMeans/IndependentMeans.h"
#include "VBModules/MatrixPrecisions/IndependentGamma.h"
#include "VBModules/MembershipProbabilities/IndependentDirichlet.h"

#include "Utilities/Timer.h"
#include "Utilities/MatrixManipulations.h"
#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;
namespace DN = PROFUMO::Utilities::DataNormalisation;
namespace MM = PROFUMO::Utilities::MatrixManipulations;


////////////////////////////////////////////////////////////////////////////////
// Convenience functions
namespace PROFUMO
{
    // Do a VB decomposition of a matrix (essentially a Bayesian spatial ICA if a spatial basis is fed in)
    arma::fmat decomposeMatrix(const arma::fmat& data, const unsigned int rank, const PROFUMO::ModelManager::SpatialModel spatialModel, const unsigned int iterations, const arma::fmat& initialMaps, const std::string outputDirectory={""}); // = arma::fmat());

    // Generate a randomised version of a basis, to initialise a decomposition
    arma::fmat generateRandomInitialisaion(const arma::fmat& spatialBasis, const unsigned int rank);
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::ModelManager::ModelManager( const std::string dataLocationsFile, const unsigned int M, const SpatialModel spatialModel, SpatialParameters spatialParameters, const WeightModel weightModel, const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel, const NoiseModel noiseModel, const Options options, const std::string preprocessingDirectory )
: M_(M), TR_(options.TR), hrfFile_(options.hrfFile), dofCorrectionFactor_(options.dofCorrectionFactor)
{
    Utilities::Timer timer;
    
    ///////////////////////////////////////////////////////////////////////////
    // Load the data
    std::cout << "Loading data..." << std::endl;
    timer.reset();
    
    std::shared_ptr<DataLoaders::LowRankDataLoader> LRDLoader;
    std::shared_ptr<DataLoaders::FullRankDataLoader> FRDLoader;

    // Load either full or reduced data
    if ( options.K == 0 ) {
        FRDLoader = std::make_shared<DataLoaders::FullRankDataLoader>( dataLocationsFile, preprocessingDirectory, M_, options.maskInds, options.normaliseData, options.bigData, false, options.bigDataDir);
        S_ = FRDLoader->getS(); V_ = FRDLoader->getV();
    } else {
        LRDLoader = std::make_shared<DataLoaders::LowRankDataLoader>( dataLocationsFile, preprocessingDirectory, M_, options.K, options.maskInds, options.normaliseData, options.bigData, false, options.bigDataDir);
        S_ = LRDLoader->getS(); V_ = LRDLoader->getV();
    }

    std::cout << std::endl;
    timer.printTimeElapsed();
    std::cout << std::endl;
    
    //////////////////////////////////////////////////////////////////////////
    // Get an initial set of spatial maps
    
    arma::fmat initialMaps;
    std::vector<std::string> subjectNamesList;
    std::vector<PROFUMO::SubjectID> subjectNamesPreList;
    unsigned int s;
    std::string subjectDirectory1;
    if ( options.K == 0 ) {
        
        for (const auto& thissubject : FRDLoader->getDataLoc()) {
            subjectNamesPreList.emplace_back(thissubject.first);            
        }
    } else {
        
        for (const auto& thissubject : LRDLoader->getDataLoc()) {
            subjectNamesPreList.emplace_back(thissubject.first);            
        }
    }
    
    for (s = 0; s < subjectNamesPreList.size(); ++s) {
        PROFUMO::SubjectID subjectID = subjectNamesPreList[s];  
        subjectDirectory1 = subjectID; // + "/";
        subjectNamesList.push_back(subjectDirectory1);     
    }
    
    if (options.bigData){
        const std::string subListDir = options.bigDataDir + "SubjectNamesList.txt";
        std::ofstream output_file(subListDir);
        std::ostream_iterator<std::string> output_iterator(output_file, "\n");
        std::copy(subjectNamesList.begin(), subjectNamesList.end(), output_iterator);
        output_file.close();
    }
    
    if (options.fullPredefined){
        std::cout << "Full predefined initial maps; i.e. existing group maps..." << std::endl;
        arma::fmat initialMaps_means;
        arma::fmat initialMaps_probs;
        std::string initialMapDir = options.fullPredefinedInPath + "/" + "FinalModel" + "/" + "GroupSpatialModel" + "/" + "SignalMeans.post" + "/" + "Means.hdf5";
        initialMaps_means.load(initialMapDir, arma::hdf5_binary);
        std::string initialMapDir1 = options.fullPredefinedInPath + "/" + "FinalModel" + "/" + "GroupSpatialModel" + "/" + "Memberships.post" + "/" + "Class_2" + "/" + "Probabilities.hdf5";
        initialMaps_probs.load(initialMapDir1, arma::hdf5_binary);
        initialMaps = initialMaps_means % initialMaps_probs;

    }
    else if ((spatialParameters.GroupMeans != nullptr) && (spatialParameters.GroupMemberships != nullptr)) {
        initialMaps = (*spatialParameters.GroupMeans) % (*spatialParameters.GroupMemberships);
    }
    else if (spatialParameters.GroupMeans != nullptr) {
        initialMaps = (*spatialParameters.GroupMeans);
    }
    else if (spatialParameters.GroupMemberships != nullptr) {
        initialMaps = (*spatialParameters.GroupMemberships);
    }
    else if (spatialParameters.predefinedInitialMaps != nullptr) {
        initialMaps = (*spatialParameters.predefinedInitialMaps);
    }
    else {
        //////////////////////////////////////////////////////////////////////
        // Get the spatial basis
        
        std::cout << "Computing spatial basis..." << std::endl;
        timer.reset();
        
        arma::fmat spatialBasis;
        
        if (spatialParameters.predefinedSpatialBasis != nullptr) {
            spatialBasis = (*spatialParameters.predefinedSpatialBasis);
        } else {
            if ( options.K == 0 ) {
                    unsigned int basisDim1 = 2 * M_;
                    unsigned int basisDim2 = options.K;
                    unsigned int basisDim = std::max(basisDim1,basisDim2);
                    spatialBasis = FRDLoader->computeSpatialBasis( basisDim1, options.MIGP, options.bigDataDir ); 
            } else {
                    unsigned int basisDim1 = 2 * M_;
                    unsigned int basisDim2 = options.K;
                    unsigned int basisDim = std::max(basisDim1,basisDim2);
                    spatialBasis = LRDLoader->computeSpatialBasis( basisDim1, options.MIGP, options.bigDataDir );                
            }
        }
        
        // Flip the column signs- R: what is this?
        Utilities::MatrixManipulations::flipColumnSigns( spatialBasis );
        
        // Save!
        spatialBasis.save(preprocessingDirectory + "SpatialBasis.hdf5", arma::hdf5_binary);
        arma::fmat normalisedBasis = spatialBasis;
        //normalisedBasis.each_row() /= arma::sqrt(arma::diagvec(normalisedBasis.t() * normalisedBasis)).t();
        normalisedBasis.each_row() /= arma::sqrt(arma::sum(normalisedBasis % normalisedBasis,0));
        normalisedBasis.save(preprocessingDirectory + "NormalisedSpatialBasis.hdf5", arma::hdf5_binary);
        
        std::cout << std::endl;
        timer.printTimeElapsed();
        std::cout << std::endl;
        
        /////////////////////////////////////////////////////////////////////////
        // Decompose the spatial basis
        
        // We are aiming to get an approaximation of D = P*H*A + E
        // We have D = U*S*Vt available to us
        // This performs a simple Bayesian ICA, i.e. D = X*Y + E
        // We perform the ICA on U*S - somewhat surprisingly, adding noise to 
        // the data seems to induce constant variance noise on U*S (see simple 
        // code at the bottom of LowRankDataLoader.c++), which means that this 
        // is the matrix to decompose.
        // Therefore, U*S = X*Y + E; P ≈ X, H*A ≈ Y*Vt
        
        // We can always check the residuals post-hoc too to satisfy ourselves 
        // that the above holds, e.g.
        // SB = PROFUMO.loadHDF5(pfmDir + "/Preprocessing/SpatialBasis.hdf5")
        // IM = PROFUMO.loadHDF5(pfmDir + "/Preprocessing/InitialMaps.hdf5")
        // residuals = SB - IM @ numpy.linalg.pinv(IM) @ SB
        // PFMplots.plotMaps(SB, "Spatial basis"); PFMplots.plotMaps(IM, "Initial maps"); PFMplots.plotMaps(residuals, "Residuals")
        
        std::cout << "Generating initialisation from the spatial basis..." << std::endl;
        timer.reset();
        
        // Make directory for output
        const std::string decompositionDir = preprocessingDirectory + "BasisDecomposition/";
        mkdir( decompositionDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        
        // Do the decompositions
        initialMaps = generateInitialMaps(spatialBasis, spatialModel, options.multiStartIterations, decompositionDir);
        
        std::cout << std::endl;
        timer.printTimeElapsed();
        std::cout << std::endl;
        //////////////////////////////////////////////////////////////////////// 
    }
    
    // Save! not needed for full predefined
    
    initialMaps.save(preprocessingDirectory + "InitialMaps.hdf5", arma::hdf5_binary);
    
    
    ////////////////////////////////////////////////////////////////////////////
    // And make the Cohort!
    if ((!options.bigData)) {
        if ((!options.fullPredefined)) {
            if ( options.K == 0 ) {
                initialiseCohort(FRDLoader->getData(), initialMaps, spatialModel, spatialParameters, weightModel, timeCourseModel, temporalPrecisionModel, noiseModel);
            } else {
                initialiseCohort(LRDLoader->getData(), initialMaps, spatialModel, spatialParameters, weightModel, timeCourseModel, temporalPrecisionModel, noiseModel);
            }
        } else {
            if ( options.K == 0 ) {
                initialiseFullPredefinedCohort(FRDLoader->getData(), initialMaps, spatialModel, spatialParameters, weightModel, timeCourseModel, temporalPrecisionModel, noiseModel, options.fullPredefinedInPath);
            } else {
                initialiseFullPredefinedCohort(LRDLoader->getData(), initialMaps, spatialModel, spatialParameters, weightModel, timeCourseModel, temporalPrecisionModel, noiseModel, options.fullPredefinedInPath);
            }
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
// Make a Cohort from a batch

void PROFUMO::ModelManager::initialiseBatchData(const std::string dataLocationsFile, const std::vector<std::string> subjectNamesList, const arma::fmat initialMaps, const SpatialModel spatialModel, SpatialParameters spatialParameters, const WeightModel weightModel, const TimeCourseModel timeCourseModel, const TimeCourseModel initialTimeCourseModel, const TemporalPrecisionModel temporalPrecisionModel, const NoiseModel noiseModel, const Options options, unsigned int multiUpdates, unsigned int subjectNum)
{
    Utilities::Timer timer;
    ///////////////////////////////////////////////////////////////////////////
    // Load the batch data
    std::cout << "Loading data and Initialising cohort- batch number: " << options.batchCnt << std::endl;
    timer.reset();
    
    std::shared_ptr<DataLoaders::LowRankDataLoader> LRDLoader;
    std::shared_ptr<DataLoaders::FullRankDataLoader> FRDLoader;
    
    /////////////////////////////////////////////////////////////////////////
    // initialise batch cohort
    
    const std::string bigDataDirBatch = options.bigDataDir + "BigDataIntermediateDir/";
    mkdir( bigDataDirBatch.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    if (options.batchCnt == 0) {
        //multiUpdates=0 is for batch 0, multiUpdates=2 is next batches, multiUpdates=1 is for final revisit in sPROFUMO
        //multiUpdates=3 is for full predefined initialisation. why on earth did i use this coding??? it's so confusing!
        if (multiUpdates<2){
            if ( options.K == 0 ) {
                FRDLoader = std::make_shared<DataLoaders::FullRankDataLoader>( dataLocationsFile, "", M_, options.maskInds, options.normaliseData, options.bigData, true, options.bigDataDir, subjectNamesList);
                S_ = FRDLoader->getS(); V_ = FRDLoader->getV();
                initialiseCohort(FRDLoader->getData(), initialMaps, spatialModel, spatialParameters, weightModel, initialTimeCourseModel, temporalPrecisionModel, noiseModel, true, bigDataDirBatch);
            } else {
                LRDLoader = std::make_shared<DataLoaders::LowRankDataLoader>( dataLocationsFile, "", M_, options.K, options.maskInds, options.normaliseData, options.bigData, true, options.bigDataDir, subjectNamesList);
                S_ = LRDLoader->getS(); V_ = LRDLoader->getV();
                initialiseCohort(LRDLoader->getData(), initialMaps, spatialModel, spatialParameters, weightModel, initialTimeCourseModel, temporalPrecisionModel, noiseModel, true, bigDataDirBatch);
            }
        }
    } else {
        cohort_->clear();
        if ( options.K == 0 ) {
            FRDLoader = std::make_shared<DataLoaders::FullRankDataLoader>( dataLocationsFile, "", M_, options.maskInds, options.normaliseData, options.bigData, true, options.bigDataDir, subjectNamesList);
            S_ = FRDLoader->getS(); V_ = FRDLoader->getV();
            initialiseNthBatchCohort(FRDLoader->getData(), initialMaps, spatialModel, spatialParameters, weightModel, timeCourseModel, initialTimeCourseModel, temporalPrecisionModel, noiseModel, options, bigDataDirBatch, multiUpdates, subjectNum);
        } else {            
            LRDLoader = std::make_shared<DataLoaders::LowRankDataLoader>( dataLocationsFile, "", M_, options.K, options.maskInds, options.normaliseData, options.bigData, true, options.bigDataDir, subjectNamesList);
            S_ = LRDLoader->getS(); V_ = LRDLoader->getV();
            initialiseNthBatchCohort(LRDLoader->getData(), initialMaps, spatialModel, spatialParameters, weightModel, timeCourseModel, initialTimeCourseModel, temporalPrecisionModel, noiseModel, options, bigDataDirBatch, multiUpdates, subjectNum);
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    // Make the group models
    std::cout << std::endl;
    timer.printTimeElapsed();
    std::cout << std::endl;
    return;
}

////////////////////////////////////////////////////////////////////////////
void PROFUMO::ModelManager::update(const unsigned int iterations, const std::string intermediatesDir, const bool normaliseTimeCourses, const bool bigData, const bool fullPredefined, const float posteriorRho)
{
    std::cout << "Updating the model..." << std::endl;
    Utilities::Timer timer;
    
    if (bigData) {
        //intermediatesDir is bigData_intermediateDir in this case 
        //Update model once every time
        //these are all batch updates
        arma::fvec V_S_R;
        V_S_R.load(intermediatesDir + "N_VSR.hdf5", arma::hdf5_binary);
        int subjectNum = V_S_R.at(1); //arma::conv_to<int>::  //arma::from(N_TrDtD[0]);

        cohort_->setTimeCourseNormalisation(normaliseTimeCourses);
        
        cohort_->updateSpatialModel(bigData, posteriorRho, subjectNum);
        
        cohort_->updateWeightModel(bigData, posteriorRho, subjectNum);
        
        cohort_->updateTemporalModel(bigData, posteriorRho, subjectNum);
        
        cohort_->updateWeightModel(bigData, posteriorRho, subjectNum);
                    
        cohort_->updateNoiseModel(bigData, posteriorRho, subjectNum);
        
        std::cout << std::endl;
        
        cohort_->setTimeCourseNormalisation(false);
    } else if (fullPredefined) {        
        cohort_->setTimeCourseNormalisation(normaliseTimeCourses);
        for (unsigned int iteration = 1; iteration <= iterations; ++iteration) {
            std::cout << "Iteration: " << iteration << std::endl;
            cohort_->update(false, 1.0, 0, false);
            std::cout << std::endl;
        }
        cohort_->setTimeCourseNormalisation(false);

    } else {
        
        // Save initial maps
        saveIntermediates(intermediatesDir, 0, iterations);
        
        // Update model for as long as requested
        cohort_->setTimeCourseNormalisation(normaliseTimeCourses);
        for (unsigned int iteration = 1; iteration <= iterations; ++iteration) {
            std::cout << "Iteration: " << iteration << std::endl;
            
            cohort_->updateSpatialModel();
            
            cohort_->updateWeightModel();
            
            cohort_->updateTemporalModel();
            
            cohort_->updateWeightModel();
            
            if ((iteration <= 25) || (iteration % 10 == 0)) {
                cohort_->updateNoiseModel();
            }
            
            // Save intermediates
            if ( iteration % 10 == 0) {
                saveIntermediates(intermediatesDir, iteration, iterations);
            }
            
            std::cout << std::endl;
        }
        cohort_->setTimeCourseNormalisation(false);
    
    }
    
    timer.printTimeElapsed();
    std::cout << std::endl;
    
    return;
}

///////////////////////////////////////////////////////////////////////////////
void PROFUMO::ModelManager::alignModelSigns(const bool flipGroup, const bool flipSubjects)
{
    cohort_->signFlipSpatialModel(flipGroup, flipSubjects);
    
    return;
}
///////////////////////////////////////////////////////////////////////////////

void PROFUMO::ModelManager::switchSpatialModel(const SpatialModel spatialModel, SpatialParameters spatialParameters, const bool batchCohort )
{
    std::cout << "Switching the spatial model..." << std::endl;
    Utilities::Timer timer;
    
    cohort_->setSpatialModel( getSpatialModel(spatialModel, spatialParameters) );
    timer.printTimeElapsed();
    std::cout << std::endl;
    
    std::cout << "Initial updates..." << std::endl;
    timer.reset();
    
    for (unsigned int z = 0; z < 10; ++z) {
        std::cout << "Iteration: " << z + 1 << std::endl;
        // Update new model
        cohort_->updateSpatialModel();
        // And dependent parts, when established
        if (z >= 3) {
            cohort_->updateWeightModel();
            cohort_->updateNoiseModel();
        }
        std::cout << std::endl;
    }
    
    timer.printTimeElapsed();
    std::cout << std::endl;
    return;
}

/////////////////////////////////////////////////////////////////////////////////

void PROFUMO::ModelManager::switchWeightModel(const WeightModel weightModel)
{
    std::cout << "Switching the weight model..." << std::endl;
    Utilities::Timer timer;
    
    cohort_->setWeightModel( getWeightModel(weightModel) );
    
    timer.printTimeElapsed();
    std::cout << std::endl;
    
    std::cout << "Initial updates..." << std::endl;
    timer.reset();
    
    for (unsigned int z = 0; z < 3; ++z) {
        std::cout << "Iteration: " << z + 1 << std::endl;
        // Update new model
        cohort_->updateWeightModel();
        std::cout << std::endl;
    }
    
    timer.printTimeElapsed();
    std::cout << std::endl;
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::ModelManager::switchTemporalModel(const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel)
{
    std::cout << "Switching the temporal model..." << std::endl;
    Utilities::Timer timer;
    
    cohort_->setTemporalModel( getTemporalModel(timeCourseModel, temporalPrecisionModel) );
    
    timer.printTimeElapsed();
    std::cout << std::endl;
    
    std::cout << "Initial updates..." << std::endl;
    timer.reset();
    
    for (unsigned int z = 0; z < 10; ++z) {
        std::cout << "Iteration: " << z + 1 << std::endl;
        // Update new model
        cohort_->updateTemporalModel();
        // And dependent parts, when established
        if (z >= 3) {
            cohort_->updateWeightModel();
            cohort_->updateNoiseModel();
        }
        std::cout << std::endl;
    }
    
    timer.printTimeElapsed();
    std::cout << std::endl;
    return;
}

//////////////////////////////////////////////////////////////////////////////////

void PROFUMO::ModelManager::switchNoiseModel(const NoiseModel noiseModel)
{
    std::cout << "Switching the noise model..." << std::endl;
    Utilities::Timer timer;
    
    cohort_->setNoiseModel( getNoiseModel(noiseModel) );
    
    timer.printTimeElapsed();
    std::cout << std::endl;
    
    std::cout << "Initial updates..." << std::endl;
    timer.reset();
    
    for (unsigned int z = 0; z < 3; ++z) {
        std::cout << "Iteration: " << z + 1 << std::endl;
        // Update new model
        cohort_->updateNoiseModel();
        std::cout << std::endl;
    }
    
    timer.printTimeElapsed();
    std::cout << std::endl;
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::ModelManager::reinitialiseModel(const SpatialModel spatialModel, SpatialParameters spatialParameters, const WeightModel weightModel, const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel)
{
    Utilities::Timer timer;
    
    ////////////////////////////////////////////////////////////////////////////
    
    std::cout << "Reinitialising the model..." << std::endl;
    
    // First, make a constant spatial model based on the group maps
    MFModels::P::P2C Ebasis;
    Ebasis.P = cohort_->getGroupMaps(); Utilities::MatrixManipulations::flipColumnSigns( Ebasis.P );
    Ebasis.PtP = Ebasis.P.t() * Ebasis.P + 0.1 * V_ * arma::eye<arma::fmat>(M_, M_);
    std::shared_ptr<MFModels::P_VBPosterior> Pbasis = std::make_shared<VBModules::Constant<MFModels::P::C2P, MFModels::P::P2C>>(Ebasis);
    std::shared_ptr<CohortModelling::GroupSpatialModel> gSM = std::make_shared<CohortModelling::GroupSpatialModels::GroupMaps>(Pbasis);
    
    // Change to this model
    cohort_->setSpatialModel( gSM );
    
    // And update the temporal / weight models too
    cohort_->setWeightModel( getWeightModel(weightModel) );
    cohort_->setTemporalModel( getTemporalModel(timeCourseModel, temporalPrecisionModel) );
    
    timer.printTimeElapsed();
    std::cout << std::endl;
    
    ////////////////////////////////////////////////////////////////////////////
    
    std::cout << "Initial updates..." << std::endl;
    timer.reset();
    
    // The group maps are fixed for these, and we also hold the time course 
    // variances fixed so the weights are in the right ballpark
    cohort_->setTimeCourseNormalisation(true);
    for (unsigned int z = 0; z < 5; ++z) {
        std::cout << "Iteration: " << z + 1 << std::endl;
        cohort_->updateTemporalModel();
        cohort_->updateWeightModel();
        if (z >= 3) {
            cohort_->updateNoiseModel();
        }
        std::cout << std::endl;
    }
    cohort_->setTimeCourseNormalisation(false);
    
    // And now some updates to let the time course variances stabilise based 
    // on their priors
    for (unsigned int z = 0; z < 10; ++z) {
        std::cout << "Iteration: " << z + 1 << std::endl;
        cohort_->updateTemporalModel();
        cohort_->updateWeightModel();
        cohort_->updateNoiseModel();
        std::cout << std::endl;
    }
    
    timer.printTimeElapsed();
    std::cout << std::endl;
    ////////////////////////////////////////////////////////////////////////////
    
    std::cout << "Switching to specified spatial model..." << std::endl;
    timer.reset();
    
    // Switch to the requested spatial model
    switchSpatialModel(spatialModel, spatialParameters);
    
    std::cout << std::endl;
    timer.printTimeElapsed();
    std::cout << std::endl;
    
    ////////////////////////////////////////////////////////////////////////////
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::ModelManager::save(const std::string directory, const bool saveSubjects, const bool saveGroup) const
{
    cohort_->save(directory, saveSubjects, saveGroup);
    return;
}

/////////////////////////////////////////////////////////////////////////////////

arma::frowvec PROFUMO::ModelManager::getFreeEnergy(const bool bigData, const int subjectNum, const float posteriorRho, const std::string bigDataDirBatch) const
{
    return cohort_->getFreeEnergy(bigData, subjectNum, posteriorRho, bigDataDirBatch);
}

////////////////////////////////////////////////////////////////////////////////
// Private helper functions
////////////////////////////////////////////////////////////////////////////////
// Make a Cohort, given the various models (templated for different types of data)

template<class D>
void PROFUMO::ModelManager::initialiseCohort(const std::map<SubjectID, std::map<RunID, D>> subjectData, const arma::fmat initialMaps, const SpatialModel spatialModel, SpatialParameters spatialParameters, const WeightModel weightModel, const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel, const NoiseModel noiseModel, const bool batchCohort, const std::string bigDataDirBatch)
{
    Utilities::Timer timer;
    
    ////////////////////////////////////////////////////////////////////////////
    // Make the group models
    
    
    std::cout << "Making the group models..." << std::endl;
    timer.reset();
    // First, make a constant spatial model
    MFModels::P::P2C Ebasis;
    Ebasis.P = initialMaps;
    Ebasis.PtP = Ebasis.P.t() * Ebasis.P + 0.025 * V_ * arma::eye<arma::fmat>(M_, M_);
    std::shared_ptr<MFModels::P_VBPosterior> Pbasis = std::make_shared<VBModules::Constant<MFModels::P::C2P, MFModels::P::P2C>>(Ebasis);
    std::shared_ptr<CohortModelling::GroupSpatialModel> gSM = std::make_shared<CohortModelling::GroupSpatialModels::GroupMaps>(Pbasis);
    
    // And the rest of the models
    std::shared_ptr<CohortModelling::GroupWeightModel> gWM = getWeightModel(weightModel);
    std::shared_ptr<CohortModelling::GroupTemporalModel> gTM = getTemporalModel(timeCourseModel, temporalPrecisionModel);
    std::shared_ptr<CohortModelling::GroupNoiseModel> gNM = getNoiseModel(noiseModel);
    
    std::cout << std::endl;
    timer.printTimeElapsed();
    std::cout << std::endl;
    
    ////////////////////////////////////////////////////////////////////////////
    // And make the Cohort!
    
    std::cout << "Making the cohort..." << std::endl;
    timer.reset();
    
    // Put all subject information together
    std::vector<SubjectInformation> subjectInformation;
    for (const auto& subject : subjectData) {
        SubjectInformation subjectInfo;
        subjectInfo.subjectID = subject.first;
        for (const auto& run : subject.second) {
            RunInformation runInfo;
            runInfo.runID = run.first;
            //runInfo.V = run.second.V;
            runInfo.T = run.second.T;
            //runInfo.M = M_;
            subjectInfo.runs.push_back(runInfo);
        }
        subjectInformation.push_back(subjectInfo);
    }
    
    // And then the cohort!
    
    cohort_ = std::make_unique<CohortModelling::Cohort>(
        subjectInformation, subjectData, gSM, gWM, gTM, gNM, dofCorrectionFactor_);
    if (batchCohort) {
        std::shared_ptr<CohortModelling::GroupSpatialModel> gSM_tosave = getSpatialModel(spatialModel, spatialParameters);
        
        const std::string groupInitialisationDir = bigDataDirBatch + "/GroupInitialisation/";
        mkdir( groupInitialisationDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        const std::string GSMDir = groupInitialisationDir + "GroupSpatialModel" + "/";
        mkdir( GSMDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        gSM_tosave->save(GSMDir);
        
        const std::string GWMDir = groupInitialisationDir + "GroupWeightModel" + "/";
        mkdir( GWMDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        gWM->save(GWMDir);
        
        const std::string GTMDir = groupInitialisationDir + "GroupTemporalModel" + "/";
        mkdir( GTMDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        gTM->save(GTMDir);
        
        const std::string GNMDir = groupInitialisationDir + "GroupNoiseModel" + "/";
        mkdir( GNMDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        gNM->save(GNMDir);
        initialMaps.save(GSMDir + "InitialMaps.hdf5", arma::hdf5_binary);
    }
    
    std::cout << std::endl;
    timer.printTimeElapsed();
    std::cout << std::endl;
    
    ////////////////////////////////////////////////////////////////////////////
    
    std::cout << "Initial updates..." << std::endl;
    timer.reset();
    
    // The group maps are fixed for these, and we also hold the time course 
    // variances fixed so the weights are in the right ballpark
    // set to true for stochasatic, false to classic
    cohort_->setTimeCourseNormalisation(true);
    for (unsigned int z = 0; z < 3; ++z) {
        std::cout << "Iteration: " << z + 1 << std::endl;
        cohort_->updateTemporalModel();
        cohort_->updateWeightModel();
        cohort_->updateNoiseModel();
        std::cout << std::endl;
    }

    cohort_->setTimeCourseNormalisation(false);
    
    // And now some updates to let the time course variances stabilise based 
    // on their priors
    for (unsigned int z = 0; z < 8; ++z) {
        std::cout << "Iteration: " << z + 1 << std::endl;
        cohort_->updateTemporalModel();
        cohort_->updateWeightModel();
        cohort_->updateNoiseModel();
        std::cout << std::endl;
    }
    
    timer.printTimeElapsed();
    std::cout << std::endl;
    
    ////////////////////////////////////////////////////////////////////////////
    // Switch to the correct temporal precision model if necessary
    
    std::cout << "Switching to specified spatial model..." << std::endl;
    timer.reset();
    
    // Switch to the requested spatial model
    switchSpatialModel(spatialModel, spatialParameters, batchCohort);
    
    std::cout << std::endl;
    timer.printTimeElapsed();
    std::cout << std::endl;
    
    ////////////////////////////////////////////////////////////////////////////
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
// Make a Cohort, given the various models (templated for different types of data)

template<class D>
void PROFUMO::ModelManager::initialiseFullPredefinedCohort(const std::map<SubjectID, std::map<RunID, D>> subjectData, const arma::fmat initialMaps, const SpatialModel spatialModel, SpatialParameters spatialParameters, const WeightModel weightModel, const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel, const NoiseModel noiseModel, const std::string fullPredefinedInPath)
{
    Utilities::Timer timer;
    
    ////////////////////////////////////////////////////////////////////////////
    // Make the group models
    
    std::cout << "Making the group models..." << std::endl;
    timer.reset();

    // First, make a constant spatial model
    MFModels::P::P2C Ebasis;
    Ebasis.P = initialMaps;
    Ebasis.PtP = Ebasis.P.t() * Ebasis.P + 0.025 * V_ * arma::eye<arma::fmat>(M_, M_);
    std::shared_ptr<MFModels::P_VBPosterior> Pbasis = std::make_shared<VBModules::Constant<MFModels::P::C2P, MFModels::P::P2C>>(Ebasis);
    std::shared_ptr<CohortModelling::GroupSpatialModel> gSM = std::make_shared<CohortModelling::GroupSpatialModels::GroupMaps>(Pbasis);
    
    // And the rest of the models
    std::shared_ptr<CohortModelling::GroupWeightModel> gWM = getWeightModel(weightModel);
    std::shared_ptr<CohortModelling::GroupTemporalModel> gTM = getTemporalModel(timeCourseModel, temporalPrecisionModel);
    std::shared_ptr<CohortModelling::GroupNoiseModel> gNM = getNoiseModel(noiseModel);
    
    std::cout << std::endl;
    timer.printTimeElapsed();
    std::cout << std::endl;
    
    ////////////////////////////////////////////////////////////////////////////
    // And make the Cohort!
    
    std::cout << "Making the cohort..." << std::endl;
    timer.reset();
    
    // Put all subject information together
    std::vector<SubjectInformation> subjectInformation;
    for (const auto& subject : subjectData) {
        SubjectInformation subjectInfo;
        subjectInfo.subjectID = subject.first;
        for (const auto& run : subject.second) {
            RunInformation runInfo;
            runInfo.runID = run.first;
            runInfo.T = run.second.T;
            subjectInfo.runs.push_back(runInfo);
        }
        subjectInformation.push_back(subjectInfo);
    }
    std::vector<SubjectInformation> subjectInformation_tmp;
    subjectInformation_tmp = {subjectInformation.begin(), subjectInformation.begin() + 3};
    // And then the cohort!
    
    cohort_ = std::make_unique<CohortModelling::Cohort>(
        subjectInformation_tmp, subjectData, gSM, gWM, gTM, gNM, dofCorrectionFactor_);
    
    std::cout << "Initial updates..." << std::endl;
    timer.reset();
    
    // The group maps are fixed for these, and we also hold the time course 
    // variances fixed so the weights are in the right ballpark
    // set to true for stochasatic, false to classic
    cohort_->setTimeCourseNormalisation(true);
    for (unsigned int z = 0; z < 3; ++z) {
        std::cout << "Iteration: " << z + 1 << std::endl;
        cohort_->updateTemporalModel();
        cohort_->updateWeightModel();
        cohort_->updateNoiseModel();
        std::cout << std::endl;
    }
    cohort_->setTimeCourseNormalisation(false);
    
    // And now some updates to let the time course variances stabilise based 
    // on their priors
    for (unsigned int z = 0; z < 3; ++z) {
        std::cout << "Iteration: " << z + 1 << std::endl;
        cohort_->updateTemporalModel();
        cohort_->updateWeightModel();
        cohort_->updateNoiseModel();
        std::cout << std::endl;
    }
    
    timer.printTimeElapsed();
    std::cout << std::endl;
    
    ////////////////////////////////////////////////////////////////////////////
    // Switch to the correct temporal precision model if necessary
    
    std::cout << "Switching to specified spatial model..." << std::endl;
    timer.reset();

    switchSpatialModel(spatialModel, spatialParameters);
    // cohort_->clear();
    const std::string inPathFinal = fullPredefinedInPath + "/" + "FinalModel" + "/";
    cohort_->clear();
    cohort_->load(inPathFinal);
    
    cohort_->resetSubjects(subjectInformation, subjectData, gSM, gWM, gTM, gNM, dofCorrectionFactor_, false);
    
    std::cout << std::endl;
    timer.printTimeElapsed();
    std::cout << std::endl;
    
    ////////////////////////////////////////////////////////////////////////////
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
// Make a Cohort, given the various models (templated for different types of data)

template<class D>
void PROFUMO::ModelManager::initialiseInterimCohort(const std::map<SubjectID, std::map<RunID, D>> subjectData, const arma::fmat initialMaps, const SpatialModel spatialModel, SpatialParameters spatialParameters, const WeightModel weightModel, const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel, const NoiseModel noiseModel, const std::string bigDataDirBatch)
{
    Utilities::Timer timer;
    
    ////////////////////////////////////////////////////////////////////////////
    // Make the group models
    
    
    std::cout << "Making the initial group models for interim cohort..." << std::endl;
    timer.reset();
    std::cout << subjectData.size() << std::endl;
    // First, make a constant spatial model
    MFModels::P::P2C Ebasis;
    Ebasis.P = initialMaps;
    Ebasis.PtP = Ebasis.P.t() * Ebasis.P + 0.025 * V_ * arma::eye<arma::fmat>(M_, M_);
    std::shared_ptr<MFModels::P_VBPosterior> Pbasis = std::make_shared<VBModules::Constant<MFModels::P::C2P, MFModels::P::P2C>>(Ebasis);
    std::shared_ptr<CohortModelling::GroupSpatialModel> gSM = std::make_shared<CohortModelling::GroupSpatialModels::GroupMaps>(Pbasis);
    
    // And the rest of the models
    std::shared_ptr<CohortModelling::GroupWeightModel> gWM = getWeightModel(weightModel);
    std::shared_ptr<CohortModelling::GroupTemporalModel> gTM = getTemporalModel(timeCourseModel, temporalPrecisionModel);
    std::shared_ptr<CohortModelling::GroupNoiseModel> gNM = getNoiseModel(noiseModel);
    
    std::cout << std::endl;
    timer.printTimeElapsed();
    std::cout << std::endl;
    
    ////////////////////////////////////////////////////////////////////////////
    // And make the Cohort!
    
    std::cout << "Making the interim cohort..." << std::endl;
    timer.reset();
    
    // Put all subject information together
    std::vector<SubjectInformation> subjectInformation;
    for (const auto& subject : subjectData) {
        SubjectInformation subjectInfo;
        subjectInfo.subjectID = subject.first;
        for (const auto& run : subject.second) {
            RunInformation runInfo;
            runInfo.runID = run.first;
            //runInfo.V = run.second.V;
            runInfo.T = run.second.T;
            //runInfo.M = M_;
            subjectInfo.runs.push_back(runInfo);
        }
        subjectInformation.push_back(subjectInfo);
    }
    
    // And then the cohort!
    std::unique_ptr<CohortModelling::Cohort> interimCohort_;
    interimCohort_ = std::make_unique<CohortModelling::Cohort>(
        subjectInformation, subjectData, gSM, gWM, gTM, gNM, dofCorrectionFactor_);
    
    ////////////////////////////////////////////////////////////////////////////
    
    std::cout << "Initial updates..." << std::endl;
    timer.reset();
    
    // The group maps are fixed for these, and we also hold the time course 
    // variances fixed so the weights are in the right ballpark
    // set to true for stochasatic, false to classic
    interimCohort_->setTimeCourseNormalisation(true);
    for (unsigned int z = 0; z < 2; ++z) {
        std::cout << "Iteration: " << z + 1 << std::endl;
        interimCohort_->updateTemporalModel();
        interimCohort_->updateWeightModel();
        interimCohort_->updateNoiseModel();
        std::cout << std::endl;
    }
    
    interimCohort_->setTimeCourseNormalisation(false);
    
    // And now some updates to let the time course variances stabilise based 
    // on their priors
    for (unsigned int z = 0; z < 3; ++z) {
        std::cout << "Iteration: " << z + 1 << std::endl;
        interimCohort_->updateTemporalModel();
        interimCohort_->updateWeightModel();
        interimCohort_->updateNoiseModel();
        std::cout << std::endl;
    }
    
    timer.printTimeElapsed();
    std::cout << std::endl;
    
    ////////////////////////////////////////////////////////////////////////////
    // Switch to the correct temporal precision model if necessary
    
    std::cout << "Switching to specified spatial model for interim cohort..." << std::endl;
    timer.reset();
    
    // Switch to the requested spatial model
    spatialParameters.randomInitialisation=spatialParameters.randomInitialisation;
    interimCohort_->setSpatialModel( getSpatialModel(spatialModel, spatialParameters) );
    timer.printTimeElapsed();
    std::cout << std::endl;
    
    for (unsigned int z = 0; z < 5; ++z) {
        std::cout << "Iteration: " << z + 1 << std::endl;
        // Update new model
        interimCohort_->updateSpatialModel();
        // And dependent parts, when established
        if (z >= 3) {
            interimCohort_->updateWeightModel();
            interimCohort_->updateNoiseModel();
        }
        std::cout << std::endl;
    }
    
    interimCohort_->save(bigDataDirBatch, true, false, false);
    
    std::cout << std::endl;
    timer.printTimeElapsed();
    std::cout << std::endl;
    
    ////////////////////////////////////////////////////////////////////////////
    
    return;
}
/////////////////////////////////////////////////////////////////////////////////
// Make a Cohort, given the various models (templated for different types of data)

template<class D>
void PROFUMO::ModelManager::initialiseNthBatchCohort(const std::map<SubjectID, std::map<RunID, D>> subjectData, const arma::fmat initialMaps, const SpatialModel spatialModel, SpatialParameters spatialParameters, const WeightModel weightModel, const TimeCourseModel timeCourseModel, const TimeCourseModel initialTimeCourseModel, const TemporalPrecisionModel temporalPrecisionModel, const NoiseModel noiseModel, const Options options, const std::string bigDataDirBatch, unsigned int multiUpdates, unsigned int subjectNum)
{
    Utilities::Timer timer;
    
    ////////////////////////////////////////////////////////////////////////////
    
    std::cout << "Initialise this batch based on groupmap from previous batch..." << std::endl;
    std::cout << "Initialise each subject based on initial maps (1st time) or previous subject model (next times) ..." << std::endl;

    // First, get spatial model based on initial maps (this is for subjects we are visiting for the first time)
    MFModels::P::P2C Ebasis;
    Ebasis.P = initialMaps;
    Ebasis.PtP = Ebasis.P.t() * Ebasis.P + 0.025 * V_ * arma::eye<arma::fmat>(M_, M_);
    std::shared_ptr<MFModels::P_VBPosterior> Pbasis = std::make_shared<VBModules::Constant<MFModels::P::C2P, MFModels::P::P2C>>(Ebasis);
    std::shared_ptr<CohortModelling::GroupSpatialModel> iSM = std::make_shared<CohortModelling::GroupSpatialModels::GroupMaps>(Pbasis);
    std::shared_ptr<CohortModelling::GroupWeightModel> iWM = getWeightModel(weightModel);
    std::shared_ptr<CohortModelling::GroupTemporalModel> iTM = getTemporalModel(timeCourseModel, temporalPrecisionModel);
    std::shared_ptr<CohortModelling::GroupNoiseModel> iNM = getNoiseModel(noiseModel);
    
    // I don't remember what changed here that it has been commented out, not deleting for now. 13 Jan 2025. 
    // second, get previous group map to initialise the current cohort
    // MFModels::P::P2C EbasisGM;
    // EbasisGM.P = cohort_->getGroupMaps(); Utilities::MatrixManipulations::flipColumnSigns( EbasisGM.P );
    // EbasisGM.PtP = EbasisGM.P.t() * EbasisGM.P + 0.1 * V_ * arma::eye<arma::fmat>(M_, M_);
    // std::shared_ptr<MFModels::P_VBPosterior> PbasisGM = std::make_shared<VBModules::Constant<MFModels::P::C2P, MFModels::P::P2C>>(EbasisGM);
    // std::shared_ptr<CohortModelling::GroupSpatialModel> gSM = std::make_shared<CohortModelling::GroupSpatialModels::GroupMaps>(PbasisGM);
    
    std::cout << std::endl;
    timer.printTimeElapsed();
    std::cout << std::endl;
    
    ////////////////////////////////////////////////////////////////////////////
    // And make the Cohort!
    
    std::cout << "Making the cohort..." << std::endl;
    timer.reset();
    
    // Put all subject information together
    std::vector<int> brandNewSubjects;
    std::vector<SubjectInformation> subjectInformation;
    for (const auto& subject : subjectData) {
        SubjectInformation subjectInfo;
        subjectInfo.subjectID = subject.first;
        for (const auto& run : subject.second) {
            RunInformation runInfo;
            runInfo.runID = run.first;
            //runInfo.V = run.second.V;
            runInfo.T = run.second.T;
            //runInfo.M = M_;
            subjectInfo.runs.push_back(runInfo);
        }
        subjectInformation.push_back(subjectInfo);
        
        const std::string thisSubjectDirName = bigDataDirBatch + "Subjects" + "/" + subjectInfo.subjectID + "/"; 
        DIR* thisSubjectDir = opendir(thisSubjectDirName.c_str());
        if (thisSubjectDir) { 
            closedir(thisSubjectDir);
        } else {
            brandNewSubjects.push_back(1);
        } 
    }
    
    if (brandNewSubjects.size()>0) {
        std::cout << "Make interim cohort, initialise brand new subjects, save..." << std::endl;
        initialiseInterimCohort(subjectData, initialMaps, spatialModel, spatialParameters, weightModel, initialTimeCourseModel, temporalPrecisionModel, noiseModel, bigDataDirBatch);
    }
    
    ///////////////////////////////////////////////////////////////////////////
    std::cout << "Reset subjects (new batch), keep group model..." << std::endl;
    cohort_->resetSubjects(subjectInformation, subjectData, iSM, iWM, iTM, iNM, dofCorrectionFactor_, options.bigData, bigDataDirBatch);
    alignModelSigns(false, true);
    std::cout << std::endl;
    timer.printTimeElapsed();
    std::cout << std::endl;
    ///////////////////////////////////////////////////////////////////////////
    
    // this offset KL might become useful at some point again, leaving in for now. 13 Jan 2025. 
    //std::cout << "get offset KL for batch initialisation..." << std::endl;
    //arma::frowvec offsetFreeEnergy = getFreeEnergy(true, subjectNum, 0.7);
    //std::cout << offsetFreeEnergy <<std::endl;
    //std::string offsetFreeEnergyFileName = bigDataDirBatch + "FreeEnergy_Offset_currentBatch.hdf5";
    //offsetFreeEnergy.save(offsetFreeEnergyFileName, arma::hdf5_binary);
    //std::cout << std::endl;
    //timer.printTimeElapsed();
    //std::cout << std::endl;
        
    ///////////////////////////////////////////////////////////////////////////
    
    std::cout << "Initial batch updates (subjects only)..." << std::endl;
    timer.reset();
    
    // The group maps are fixed for these, and we also hold the time course 
    // variances fixed so the weights are in the right ballpark
    // we only update current batch's subject maps based on the group maps from the previous batch to bring them up to speed before running full model updates
    if (multiUpdates==0){
        cohort_->setTimeCourseNormalisation(false);
        // And now some updates to let the time course variances stabilise based 
        // on their priors
        for (unsigned int z = 0; z < options.initialBatchUpdates; ++z) {
            std::cout << "Iteration: " << z + 1 << std::endl;
            cohort_->update(false, 1.0, 0, false);
            std::cout << std::endl;
        }  
    } 
    if (multiUpdates==1) {
        cohort_->setTimeCourseNormalisation(false);
        for (unsigned int z = 0; z < 3; ++z) {
            cohort_->update(false, 1.0, 0, false);
            std::cout << std::endl;
        }
    }
    
    if (multiUpdates==2){
        const unsigned int padWidth = ((unsigned int) std::floor(std::log10(options.batchNum))) + 1;
        std::ostringstream iterNumber;
        iterNumber << std::setw( padWidth ) << std::setfill( '0' ) << options.batchCnt;
        const std::string bigDataDirBatchSubjectsDir = bigDataDirBatch + "Subjects" + "/";
        mkdir( bigDataDirBatchSubjectsDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        std::vector<std::string> batchSubjectIDs = cohort_->getSubjectIDs();
        arma::fmat subjectsIntermediateFreeEnergy = cohort_->getAllSubjectsFreeEnergy();
        for (unsigned int s = 0; s < batchSubjectIDs.size(); ++s) {
            const std::string bigDataDirBatchSubjectDir = bigDataDirBatchSubjectsDir + batchSubjectIDs[s] + "/";            
            mkdir( bigDataDirBatchSubjectDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
            std::string subjectFileName = bigDataDirBatchSubjectDir + "FreeEnergy_" + iterNumber.str() + ".hdf5";
            arma::fmat subjectIntermediateFreeEnergy=subjectsIntermediateFreeEnergy.row(s);
            subjectIntermediateFreeEnergy.save(subjectFileName, arma::hdf5_binary);            
        }
    }
    
    std::cout << std::endl;
    timer.printTimeElapsed();
    std::cout << std::endl;
    
    ////////////////////////////////////////////////////////////////////////////
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

std::shared_ptr<PROFUMO::CohortModelling::GroupSpatialModel> PROFUMO::ModelManager::getSpatialModel(const SpatialModel spatialModel, SpatialParameters spatialParameters) const
{
    // Store for the new model type
    std::shared_ptr<CohortModelling::GroupSpatialModel> groupSpatialModel;
    
    // Work out if we have any fixed parameters
    // We will use more relaxed hyperpriors if we do

    const bool useFixedParameters = spatialParameters.fixedParams;
    if (useFixedParameters) {
        std::cout << "fixed params!" << std::endl;
    }

    // Look at the specified model class and make appropriately
    switch (spatialModel) {
        ////////////////////////////////////////////////////////////////////////
        
        // The overall scale of the maps is tricky. The data is normalised 
        // such that E[p^2] ≈ 1.0, with the weights set to account for the 
        // spatial sparsity.
        
        // However, there is a balance between the weights adjusting 
        // the scaling of the data to fit either the scale of the null, or 
        // the scale of the group means - both of which are fixed.
        // Increasing the width of the null will tend to push the group 
        // means to higher values, which can be problematic if the group 
        // prior is not sufficiently flexible (hole-y PFMs?).
        // ** Be careful how you adjust each one! **
        
        // Here, we have gone for quite lax distributions for the signal 
        // scaling, as the null will tend to dominate (as it will be present 
        // in the majority of voxels).
        
        case SpatialModel::MODES : {
            //-----------------------------------------------------------------
            
            // Signal means
            std::shared_ptr<Modules::MatrixMean_VBPosterior> signalMeans;
            if (spatialParameters.GroupMeans == nullptr || spatialParameters.fixedParams == false) {
                VBModules::MatrixMeans::IndependentMixtureModels::MixtureModelParameters meanPrior;
                meanPrior.nRows = V_; meanPrior.nCols = M_;
                meanPrior.randomInitialisation= spatialParameters.randomInitialisation;
                // Positive Gaussian
                VBDistributions::GaussianMixtureModel::GaussianParameters posGaussianPrior;
                posGaussianPrior.parameters.mu = 1.0; posGaussianPrior.parameters.sigma2 = std::pow(0.75, 2); posGaussianPrior.probability = 0.06;
                // Negative Gaussian
                /*VBDistributions::GaussianMixtureModel::GaussianParameters negGaussianPrior;
                negGaussianPrior.parameters.mu = -1.0; negGaussianPrior.parameters.sigma2 = std::pow(0.75, 2); negGaussianPrior.probability = 0.010;*/
                // High variance 'slab'
                VBDistributions::GaussianMixtureModel::GaussianParameters slabPrior;
                slabPrior.parameters.mu = 1.0; slabPrior.parameters.sigma2 = std::pow(2.0, 2); slabPrior.probability = 0.015;
                // And a spike at zero
                VBDistributions::GaussianMixtureModel::DeltaParameters spikePrior;
                spikePrior.parameters.mu = 0.0; spikePrior.probability = 0.925;
                // And put together
                meanPrior.gaussianPriors = {posGaussianPrior, /*negGaussianPrior,*/ slabPrior}; meanPrior.deltaPriors = {spikePrior};
                signalMeans = std::make_shared<VBModules::MatrixMeans::IndependentMixtureModels>(meanPrior);
            }
            else {
                // Constant
                Modules::MatrixMeans::P2C Emeans;
                Emeans.X = (*spatialParameters.GroupMeans); Emeans.X2 = arma::square( Emeans.X );
                signalMeans = std::make_shared<VBModules::Constant<Modules::MatrixMeans::C2P, Modules::MatrixMeans::P2C>>(Emeans);
            }
            
            //------------------------------------------------------------------
            
            // Signal precisions
            std::shared_ptr<Modules::MatrixPrecision_VBPosterior> signalPrecisions;
            if (spatialParameters.GroupPrecisions == nullptr || spatialParameters.fixedParams == false) {
                VBModules::MatrixPrecisions::IndependentGamma::Parameters precisionPrior;
                precisionPrior.nRows = V_; precisionPrior.nCols = M_;
                if (useFixedParameters) {
                    precisionPrior.gammaPrior.a = 5.0; precisionPrior.gammaPrior.b = 12.5; // Precision = a/b = 0.4 -> std = 1.58(ish)
                }
                else {
                    precisionPrior.gammaPrior.a = 20.0; precisionPrior.gammaPrior.b = 10.0; // Precision = a/b = 2.0 -> std = 0.7(ish)
                    // Make sure this has a larger standard deviation than the null distribution!
                }
                signalPrecisions = std::make_shared<VBModules::MatrixPrecisions::IndependentGamma>(precisionPrior);
            }
            else {
                // Constant
                Modules::MatrixPrecisions::P2C Eprecs;
                Eprecs.X = (*spatialParameters.GroupPrecisions); Eprecs.logX = arma::log(Eprecs.X);
                signalPrecisions = std::make_shared<VBModules::Constant<Modules::MatrixPrecisions::C2P, Modules::MatrixPrecisions::P2C>>(Eprecs);
            }
            
            //------------------------------------------------------------------
            
            // Noise precisions
            std::shared_ptr<Modules::MatrixPrecision_VBPosterior> noisePrecisions;
            
            VBModules::MatrixPrecisions::IndependentGamma::Parameters precisionPrior;
            precisionPrior.nRows = V_; precisionPrior.nCols = 1;
            precisionPrior.gammaPrior.a = 20.0; precisionPrior.gammaPrior.b = 2.0; // Precision = a/b = 10.0 -> std = 0.3(ish)
            
            noisePrecisions = std::make_shared<VBModules::MatrixPrecisions::IndependentGamma>(precisionPrior);
            
            //------------------------------------------------------------------
            
            // Membership Probs
            std::shared_ptr<Modules::MembershipProbability_VBPosterior> memberships;
            if (spatialParameters.GroupMemberships == nullptr || spatialParameters.fixedParams == false) {
                // Dirichlet
                VBModules::MembershipProbabilities::IndependentDirichlet::Parameters membershipPrior;
                membershipPrior.nRows = V_; membershipPrior.nCols = M_;
                if (useFixedParameters) {
                    membershipPrior.dirichletPrior.counts = {4.5, 0.5}; // pPos = counts[1] / (counts[0] + counts[1]) = 0.1
                }
                else {
                    membershipPrior.dirichletPrior.counts = {19.0, 1.0}; // pPos = counts[1] / (counts[0] + counts[1]) = 0.05
                }
                memberships = std::make_shared<VBModules::MembershipProbabilities::IndependentDirichlet>(membershipPrior);
            }
            else {
                // Constant
                Modules::MembershipProbabilities::P2C Eprobs;
                const arma::fmat probs = *spatialParameters.GroupMemberships;
                Eprobs.p = {1.0 - probs, probs}; Eprobs.logP = {arma::log(1.0 - probs), arma::log(probs)};
                memberships = std::make_shared<VBModules::Constant<Modules::MembershipProbabilities::C2P, Modules::MembershipProbabilities::P2C>>(Eprobs);
            }
            
            //------------------------------------------------------------------
            
            // Put it together!
            MFModels::P::DGMM::Parameters prior; prior.V = V_; prior.M = M_;
            groupSpatialModel = std::make_shared<CohortModelling::GroupSpatialModels::DGMM>(signalMeans, signalPrecisions, noisePrecisions, memberships, prior, dofCorrectionFactor_);
            
            //------------------------------------------------------------------
            break;
        }
        
        ////////////////////////////////////////////////////////////////////////
        
        case SpatialModel::PARCELLATION : {
            //------------------------------------------------------------------
            
            // Means
            std::shared_ptr<Modules::MatrixMean_VBPosterior> groupMeans;
            if (spatialParameters.GroupMeans == nullptr || spatialParameters.fixedParams == false) {
                // Gaussian
                VBModules::MatrixMeans::IndependentGaussian::GaussianParameters meanPrior;
                meanPrior.nRows = V_; meanPrior.nCols = M_;
                meanPrior.gaussianPrior.mu = 0.5; meanPrior.gaussianPrior.sigma2 = 1.0;
                groupMeans = std::make_shared<VBModules::MatrixMeans::IndependentGaussian>(meanPrior);
            }
            else {
                // Constant
                Modules::MatrixMeans::P2C Emeans;
                Emeans.X = (*spatialParameters.GroupMeans); Emeans.X2 = arma::square( Emeans.X );
                groupMeans = std::make_shared<VBModules::Constant<Modules::MatrixMeans::C2P, Modules::MatrixMeans::P2C>>(Emeans);
            }
            
            //------------------------------------------------------------------
            
            // Precisions
            std::shared_ptr<Modules::MatrixPrecision_VBPosterior> groupPrecisions;
            if (spatialParameters.GroupPrecisions == nullptr || spatialParameters.fixedParams == false) {
                VBModules::MatrixPrecisions::IndependentGamma::Parameters precisionPrior;
                precisionPrior.nRows = V_; precisionPrior.nCols = M_;
                if (useFixedParameters) {
                    precisionPrior.gammaPrior.a = 1.0; precisionPrior.gammaPrior.b = 2.5; // Precision = a/b = 0.4 -> std = 1.58(ish)
                }
                else {
                    precisionPrior.gammaPrior.a = 5.0; precisionPrior.gammaPrior.b = 1.0; // Precision = a/b = 5.0 -> std = 0.45(ish)
                }
                groupPrecisions = std::make_shared<VBModules::MatrixPrecisions::IndependentGamma>(precisionPrior);
            }
            else {
                // Constant
                Modules::MatrixPrecisions::P2C Eprecs;
                Eprecs.X = (*spatialParameters.GroupPrecisions); Eprecs.logX = arma::log(Eprecs.X);
                groupPrecisions = std::make_shared<VBModules::Constant<Modules::MatrixPrecisions::C2P, Modules::MatrixPrecisions::P2C>>(Eprecs);
            }
            
            //------------------------------------------------------------------
            
            // Membership Probs
            std::shared_ptr<Modules::MembershipProbability_VBPosterior> groupMemberships;
            if (spatialParameters.GroupMemberships == nullptr || spatialParameters.fixedParams == false) {
                // Dirichlet
                VBModules::MembershipProbabilities::IndependentDirichlet::Parameters membershipPrior;
                membershipPrior.nRows = V_; membershipPrior.nCols = 1;
                membershipPrior.dirichletPrior.counts = 0.1 * arma::ones<arma::fvec>(M_);
                groupMemberships = std::make_shared<VBModules::MembershipProbabilities::IndependentDirichlet>(membershipPrior);
            }
            else {
                // Constant
                Modules::MembershipProbabilities::P2C Eprobs;
                const arma::fmat probs = *spatialParameters.GroupMemberships;
                for (unsigned int m = 0; m < M_; ++m) {
                    Eprobs.p.push_back( probs.col(m) );
                    Eprobs.logP.push_back( arma::trunc_log( probs.col(m) ) );
                }
                groupMemberships = std::make_shared<VBModules::Constant<Modules::MembershipProbabilities::C2P, Modules::MembershipProbabilities::P2C>>(Eprobs);
            }
            
            //-------------------------------------------------------------------
            
            // Put it together!
            if (spatialParameters.spatialNeighbours != nullptr) {
                MFModels::P::PottsParcellation::Parameters pottsPrior;
                pottsPrior.V = V_; pottsPrior.M = M_;
                pottsPrior.couplings = spatialParameters.spatialNeighbours; pottsPrior.beta = 100.0;
                typedef CohortModelling::GroupSpatialModels::Parcellation<MFModels::P::PottsParcellation> ParcellationModel;
                groupSpatialModel = std::make_shared<ParcellationModel>(groupMeans, groupPrecisions, groupMemberships, pottsPrior, dofCorrectionFactor_);
            }
            else {
                MFModels::P::Parcellation::Parameters PPrior;
                PPrior.V = V_; PPrior.M = M_;
                typedef CohortModelling::GroupSpatialModels::Parcellation<MFModels::P::Parcellation> ParcellationModel;
                groupSpatialModel = std::make_shared<ParcellationModel>(groupMeans, groupPrecisions, groupMemberships, PPrior, dofCorrectionFactor_);
            }
            
            //------------------------------------------------------------------
            break;
        }
        
        /////////////////////////////////////////////////////////////////////////
        
        default :
            throw std::invalid_argument("ModelManager: bad SpatialModel identifier.");
            
        ////////////////////////////////////////////////////////////////////////
    }
    
    return groupSpatialModel;
}

////////////////////////////////////////////////////////////////////////////////

std::shared_ptr<PROFUMO::CohortModelling::GroupWeightModel> PROFUMO::ModelManager::getWeightModel(const WeightModel weightModel) const
{
    // Store for the new model type
    std::shared_ptr<CohortModelling::GroupWeightModel> groupWeightModel;
    
    // The data normalisation assumes E[d^2] = M, which relates to 
    // E[p^2] * E[a^2] = 1.0 (see DataLoader.h). We are after unit variance 
    // time courses, so that holds. Similarly, even though the group maps 
    // are sparse, the subject maps normally exhibit a fair bit of variance 
    // around the group, so E[h^2] = 1.0 is in the right ball-park.
    
    // Look at the specified model class and make appropriately
    switch (weightModel) {
        case WeightModel::CONSTANT : {
            // Constant set of weights (no uncertainty either))
            MFModels::H::P2C EH; EH.h = arma::ones<arma::fvec>(M_); EH.hht = arma::ones<arma::fmat>(M_, M_);
            std::shared_ptr<MFModels::H_VBPosterior> groupWeights = std::make_shared<VBModules::Constant<MFModels::H::C2P, MFModels::H::P2C>>(EH);
            // And make a group model based around those
            groupWeightModel = std::make_shared<CohortModelling::GroupWeightModels::GroupWeights>(groupWeights);
            break;
        }
        
        case WeightModel::GROUP : {
            // Spike-slab weights (MVN models require covariance)
            MFModels::H::SpikeSlab::Parameters HPrior;
            HPrior.M = M_; HPrior.mu = 1.0; HPrior.sigma2 = std::pow(0.5, 2); HPrior.p = 0.95; //HPrior.rectifyWeights = true;
            std::shared_ptr<MFModels::H_VBPosterior> groupWeights = std::make_shared<MFModels::H::SpikeSlab>(HPrior);
            // And make a group model based around those
            groupWeightModel = std::make_shared<CohortModelling::GroupWeightModels::GroupWeights>(groupWeights);
            break;
        }
        
        case WeightModel::RECTIFIED_MULTIVARIATE_NORMAL : {
            // Group-level means
            VBModules::MatrixMeans::IndependentGaussian::GaussianParameters muHPrior;
            muHPrior.gaussianPrior.mu = 1.0; muHPrior.gaussianPrior.sigma2 = std::pow(0.1, 2); muHPrior.nRows = M_; muHPrior.nCols = 1;
            std::shared_ptr<Modules::MatrixMean_VBPosterior> muH = std::make_shared<VBModules::MatrixMeans::IndependentGaussian>(muHPrior);
            // Make the group-level precision matrix (i.e. the correlations between the weights)
            VBModules::PrecisionMatrices::Wishart::Parameters alphaHPrior;
            alphaHPrior.a = float(M_); alphaHPrior.B = std::pow(0.1, 2) * float(M_) * arma::eye<arma::fmat>(M_, M_);
            std::shared_ptr<Modules::PrecisionMatrix_VBPosterior> alphaH = std::make_shared<VBModules::PrecisionMatrices::Wishart>(alphaHPrior);
            // And make a group model based around those
            typedef CohortModelling::GroupWeightModels::ParameterisedWeights<MFModels::H::MultivariateNormal> WeightModel;
            MFModels::H::MultivariateNormal::Parameters HPrior;
            HPrior.M = M_; HPrior.rectifyWeights = true;
            HPrior.clipWeights = false; HPrior.clipRange_max = 3.0;
            groupWeightModel = std::make_shared<WeightModel>(muH, alphaH, HPrior);
            break;
        }
        
        case WeightModel::RECTIFIED_SPIKE_SLAB : {
            // This then has 95% of the weights being non-zero a priori
            typedef CohortModelling::GroupWeightModels::IndependentWeights<MFModels::H::SpikeSlab> WeightModel;
            MFModels::H::SpikeSlab::Parameters HPrior;
            HPrior.M = M_; HPrior.mu = 1.0; HPrior.sigma2 = std::pow(0.5, 2); HPrior.p = 0.95; HPrior.rectifyWeights = true;
            groupWeightModel = std::make_shared<WeightModel>(HPrior);
            break;
        }
        
        default :
            throw std::invalid_argument("ModelManager: bad WeightModel identifier.");
    }
    
    return groupWeightModel;
}

////////////////////////////////////////////////////////////////////////////////

std::shared_ptr<PROFUMO::CohortModelling::GroupTemporalPrecisionModel> PROFUMO::ModelManager::getTemporalPrecisionModel(const TemporalPrecisionModel temporalPrecisionModel) const
{
    // Store for the new model type
    std::shared_ptr<CohortModelling::GroupTemporalPrecisionModel> groupTemporalPrecisionModel;
    
    // Useful results
    // Group correlation matrix (we expect some positive correlations)
    const arma::fmat gCM = 0.9 * arma::eye<arma::fmat>(M_, M_) + 0.1 * arma::ones<arma::fmat>(M_, M_);
    // Gives the following precision matrix
    const arma::fmat gPM = linalg::inv_sympd(gCM);
    
    // For the full hierarchy, the following relationships are useful ways of modifying the priors
    // group ~ W(a,B); subjectPM ~ W(a,group); target precision gPM
    // E[group]     = a * B^-1
    // E[subjectPM] = a_s * (E[group])^-1
    //              = (a_s / a) * B
    // Wishart requires that a_s, a > M;
    // To strengthen the group prior, use a_s = M; a = c * M; B = c * gPM; c >= 1.0
    // To strengthen the subject prior, use a_s = k * M; a = k * M; B = gPM; k >= 1.0
    // See Python code at the bottom of the file for a demonstration of this
    
    // Look at the specified model class and make appropriately
    switch (temporalPrecisionModel) {
        // Constant group-level precision matrix, fixed across subjects/runs
        case TemporalPrecisionModel::CONSTANT : {
            // Simple constant prec-mat
            Modules::PrecisionMatrices::P2C CE;
            CE.X = gPM; CE.logDetX = linalg::log_det_sympd(CE.X);
            std::shared_ptr<Modules::PrecisionMatrix_VBPosterior> groupPrecision = std::make_shared<VBModules::Constant<Modules::PrecisionMatrices::C2P, Modules::PrecisionMatrices::P2C>>(CE);
            // And make a group model based around those
            groupTemporalPrecisionModel = std::make_shared<CohortModelling::GroupTemporalPrecisionModels::GroupPrecisionMatrix>(groupPrecision);
            break;
        }
        
        // Inferred group-level precision matrix, fixed across subjects/runs
        case TemporalPrecisionModel::GROUP_PRECISION_MATRIX : {
            // Make the group-level precision matrix
            VBModules::PrecisionMatrices::Wishart::Parameters prior;
            const float c = 10.0;
            prior.a = c * float(M_); prior.B = c * float(M_) * gCM; // E[X] = a * B^-1
            std::shared_ptr<Modules::PrecisionMatrix_VBPosterior> groupPrecision = std::make_shared<VBModules::PrecisionMatrices::Wishart>(prior);
            // And make a group model based around those
            groupTemporalPrecisionModel = std::make_shared<CohortModelling::GroupTemporalPrecisionModels::GroupPrecisionMatrix>(groupPrecision);
            break;
        }
        
        // Constant group-level hyperprior, subject-specific prec-mats
        case TemporalPrecisionModel::CONSTANT_GROUP : {
            // Make the constant group-level hyperprior
            // E[subject] = a_s * (E[group])^-1
            Modules::PrecisionMatrices::P2C CE;
            const float k = 5.0;
            CE.X = k * float(M_) * gCM; CE.logDetX = linalg::log_det_sympd(CE.X);
            std::shared_ptr<Modules::PrecisionMatrix_VBPosterior> groupPrecision = std::make_shared<VBModules::Constant<Modules::PrecisionMatrices::C2P, Modules::PrecisionMatrices::P2C>>(CE);
            // And combine with the subject prior
            VBModules::PrecisionMatrices::HierarchicalWishart::Parameters subjectPrior;
            subjectPrior.a = k * float(M_); subjectPrior.size = M_;
            groupTemporalPrecisionModel = std::make_shared<CohortModelling::GroupTemporalPrecisionModels::SubjectPrecisionMatrices>(groupPrecision, subjectPrior);
            break;
        }
        
        // Subject-specific precision matrices
        case TemporalPrecisionModel::SUBJECT_PRECISION_MATRICES : {
            // Make the group-level hyperprior
            VBModules::PrecisionMatrices::Wishart::Parameters groupPrior;
            const float c = 5.0; const float k = 2.0;
            groupPrior.a = k * c * float(M_); groupPrior.B = c * gPM;
            std::shared_ptr<Modules::PrecisionMatrix_VBPosterior> groupPrecision = std::make_shared<VBModules::PrecisionMatrices::Wishart>(groupPrior);
            // And make a group model based around those
            VBModules::PrecisionMatrices::HierarchicalWishart::Parameters subjectPrior;
            subjectPrior.a = k * float(M_); subjectPrior.size = M_;
            groupTemporalPrecisionModel = std::make_shared<CohortModelling::GroupTemporalPrecisionModels::SubjectPrecisionMatrices>(groupPrecision, subjectPrior);
            break;
        }
        
        // Run-specific precision matrices
        case TemporalPrecisionModel::RUN_PRECISION_MATRICES : {
            // Make the group-level precision matrix
            VBModules::PrecisionMatrices::Wishart::Parameters groupPrior;
            const float c = 5.0; const float k = 1.0;
            groupPrior.a = k * c * float(M_); groupPrior.B = c * gPM;
            std::shared_ptr<Modules::PrecisionMatrix_VBPosterior> groupPrecision = std::make_shared<VBModules::PrecisionMatrices::Wishart>(groupPrior);
            // And make a group model based around those
            VBModules::PrecisionMatrices::HierarchicalWishart::Parameters runPrior;
            runPrior.a = k * float(M_); runPrior.size = M_;
            groupTemporalPrecisionModel = std::make_shared<CohortModelling::GroupTemporalPrecisionModels::RunPrecisionMatrices>(groupPrecision, runPrior);
            break;
        }
        
        // Run-specific precision matrices, different hyperpriors for different categories
        case TemporalPrecisionModel::CATEGORISED_RUN_PRECISION_MATRICES : {
            // Make the priors
            VBModules::PrecisionMatrices::Wishart::Parameters groupPrior;
            const float c = 5.0; const float k = 1.0;
            groupPrior.a = k * c * float(M_); groupPrior.B = c * gPM;
            VBModules::PrecisionMatrices::HierarchicalWishart::Parameters runPrior;
            runPrior.a = k * float(M_); runPrior.size = M_;
            // And make a group model based around those
            groupTemporalPrecisionModel = std::make_shared<CohortModelling::GroupTemporalPrecisionModels::CategorisedRunPrecisionMatrices>(groupPrior, runPrior);
            break;
        }
        
        default :
            throw std::invalid_argument("ModelManager: bad TemporalPrecisionModel identifier.");
    }
    
    return groupTemporalPrecisionModel;
}

////////////////////////////////////////////////////////////////////////////////

std::shared_ptr<PROFUMO::CohortModelling::GroupTemporalModel> PROFUMO::ModelManager::getTemporalModel(const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel) const
{
    // Store for the new model type
    std::shared_ptr<CohortModelling::GroupTemporalModel> groupTemporalModel;
    
    // Look at the specified model class and make appropriately
    switch (timeCourseModel) {
        case TimeCourseModel::MULTIVARIATE_NORMAL : {
            groupTemporalModel = std::make_shared<CohortModelling::GroupTemporalModels::MultivariateNormal>(getTemporalPrecisionModel(temporalPrecisionModel), M_);
            std::cout<< "temporal model: mvnormal" <<std::endl;
            break;
        }
        
        case TimeCourseModel::CLEAN_HRF : {
            const float priorRelaxation = 0.0025; // Relaxation of 'true' HRF covariance
            const float posteriorRelaxation = 0.01; // Relaxation of deconvolution specifically
            groupTemporalModel = std::make_shared<CohortModelling::GroupTemporalModels::CleanHRF>(getTemporalPrecisionModel(temporalPrecisionModel), M_, TR_, hrfFile_, priorRelaxation, posteriorRelaxation);
            std::cout<< "temporal model: clean HRF" <<std::endl;
            break;
        }
        
        case TimeCourseModel::NOISY_HRF : {
            // Make the noise prior
            VBModules::PrecisionMatrices::ComponentwiseGammaPrecision::Parameters noisePrior;
            noisePrior.size = M_; noisePrior.gammaPrior.a = 5.0; noisePrior.gammaPrior.b = 2.0; // Precision = a/b = 2.5 -> std = 0.6(ish)
            // See Python code at the bottom of the file for a demonstration of gamma precision hyperpriors
            
            // And make a group model based around those
            const float priorRelaxation = 0.0; // Relaxation of 'true' HRF covariance
            const float posteriorRelaxation = 0.01; // Relaxation of deconvolution specifically
            groupTemporalModel = std::make_shared<CohortModelling::GroupTemporalModels::NoisyHRF>(getTemporalPrecisionModel(temporalPrecisionModel), M_, TR_, hrfFile_, noisePrior, priorRelaxation, posteriorRelaxation);
            std::cout<< "temporal model: noisy HRF" <<std::endl;
            break;
        }
        
        default :
            throw std::invalid_argument("ModelManager: bad TemporalModel identifier.");
    }
    
    return groupTemporalModel;
}

////////////////////////////////////////////////////////////////////////////////

std::shared_ptr<PROFUMO::CohortModelling::GroupNoiseModel> PROFUMO::ModelManager::getNoiseModel(const NoiseModel noiseModel) const
{
    // Store for the new model type
    std::shared_ptr<CohortModelling::GroupNoiseModel> groupNoiseModel;
    
    // Look at the specified model class and make appropriately
    switch (noiseModel) {
        case NoiseModel::INDEPENDENT_RUNS : {
            MFModels::Psi::GammaPrecision::Parameters prior; prior.a = 0.2; prior.b = 1.0;
            groupNoiseModel = std::make_shared<CohortModelling::GroupNoiseModels::IndependentRuns>(prior);
            break;
        }
        
        default :
            throw std::invalid_argument("ModelManager: bad NoiseModel identifier.");
    }
    
    return groupNoiseModel;
}

////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::ModelManager::generateInitialMaps(const arma::fmat& spatialBasis, const SpatialModel spatialModel, const unsigned int multiStartIterations, const std::string outputDirectory)
{
    namespace MM = Utilities::MatrixManipulations;
    
    // Record basis
    spatialBasis.save(outputDirectory + "SpatialBasis.hdf5", arma::hdf5_binary);
    
    // What are we hoping these maps actually approximate?
    // We are going to use these maps to intitialise all the subject maps, 
    // so we really want them to represent a typical set of *subject* maps. 
    // The issue is that the group basis is 'easy', in that the noise is far 
    // lower than for a typical subject. Therefore, we can add noise back in, 
    // based on the fact we have averaged said noise over S_ subjects. The 
    // noisy basis this generates represents an 'ideal' subject, in that it 
    // includes the basis from the group and perfectly white Gaussian noise.
    // https://en.wikipedia.org/wiki/Signal_averaging
    
    // However, this isn't a magic bullet: it makes the initialisation 
    // unreliable, and it doesn't capture the blurring over subjects that is 
    // key to the group. Therefore, we are actually very circumspect with when 
    // and where we apply the noise.
    const float noiseStd = std::sqrt(S_) * arma::stddev(arma::vectorise(spatialBasis.tail_cols(1)));
    
    // Do the decompositions!
    arma::fmat finalMaps;
    //--------------------------------------------------------------------------
    if (multiStartIterations == 0) {
        // Just return the basis if no iterations requested
        if (spatialBasis.n_cols < M_) {
            finalMaps = arma::randn<arma::fmat>(spatialBasis.n_rows, M_);
            finalMaps.head_cols(spatialBasis.n_cols) = spatialBasis;
        }
        else {
            finalMaps = spatialBasis.head_cols(M_);
        }
        MM::randomiseColumns(finalMaps);
        // Get the scale in the right ballpark
        DN::normaliseColumns(finalMaps);

    }
    //--------------------------------------------------------------------------
    else if (multiStartIterations == 1) {
        // If we actually requested a decomposition, fit the model
        const arma::fmat initialMaps = generateRandomInitialisaion(spatialBasis, M_);
        finalMaps = decomposeMatrix(spatialBasis, M_, spatialModel, 250, initialMaps, outputDirectory);
    }
    //--------------------------------------------------------------------------
    else { // multiStartIterations > 1
            const unsigned int padWidth = ((unsigned int) std::floor(std::log10(std::max(multiStartIterations, (unsigned int) 1)))) + 1;
        
        
        // Set dimensionality
        // If we are doing multiple runs, add a few extra components
        // Then select the most reproducible at the end
        const unsigned int rank = M_ + std::min((unsigned int) 10, (unsigned int) std::round(1.25 * M_));
        
        
        // Do decompositions
        // Can't parallelise because randomisation is not thread safe
        std::vector<arma::fmat> decompositions(multiStartIterations, arma::fmat());
        for (unsigned int i = 0; i < multiStartIterations; ++i) {
            const arma::fmat subjectBasis = 
                (spatialBasis.each_row() % (0.8 + 0.4 * arma::randu<arma::frowvec>(spatialBasis.n_cols)))
                + 0.1 * noiseStd * arma::randn<arma::fmat>(arma::size(spatialBasis));
            // Actually very conservative: reduce noise added by a fudge factor
            const arma::fmat initialMaps = generateRandomInitialisaion(spatialBasis, rank);
            const arma::fmat decomposition = decomposeMatrix(subjectBasis, rank, spatialModel, 150, initialMaps, outputDirectory);
            decompositions[i] = decomposition;
            // And save
            std::ostringstream iterNumber;
            iterNumber << std::setw( padWidth ) << std::setfill( '0' ) << i + 1;
            std::string fileName = outputDirectory + "DecomposedSpatialBasis_" + iterNumber.str() + ".hdf5";
            decomposition.save(fileName, arma::hdf5_binary);
        }
        
        
        // Now pair everything up!
        arma::fmat templateMaps = decompositions[0]; // Start with the first decomposition (they're all random anyway)
        arma::urowvec N = arma::ones<arma::urowvec>(rank);
        std::vector<arma::uvec> indices(multiStartIterations, arma::zeros<arma::uvec>(rank));
        indices[0] = arma::regspace<arma::uvec>(0,rank-1);
        
        // Critical threshold: any similarities less than this will be added 
        // as separate components rather than being averaged
        const float threshold = 0.5;
        // In practice, we actually don't want this to be too high, otherwise 
        // we end up with what are essentially duplicates in the template
        
        // Make the template
        for (unsigned int i = 1; i < multiStartIterations; ++i) {
            const arma::fmat& decomposition = decompositions[i];
            // Get pairing
            const MM::Pairing pairing = MM::pairMatrixColumns(templateMaps, decomposition);
            const arma::fvec signs = arma::sign(pairing.scores); // Why no std::sign? :-(
            
            // Add to template
            for (unsigned int j = 0; j < rank; ++j) {
                const unsigned int jT = pairing.inds1[j]; // template
                const unsigned int jD = pairing.inds2[j]; // decomposition
                
                if (std::abs(pairing.scores[j]) > threshold) {
                    // Add to template if a good match
                    templateMaps.col(jT) += signs[j] * decomposition.col(jD);
                    N[jT] += 1;
                    // Record where the map went
                    indices[i][jD] = jT;
                }
                else {
                    // Otherwise, append to the end of the template
                    templateMaps = arma::join_rows(templateMaps, decomposition.col(jD));
                    N = arma::join_rows(N, arma::ones<arma::urowvec>(1));
                    // Record where the map went
                    indices[i][jD] = templateMaps.n_cols - 1;
                }
            }
        }
        // Normalise by number of maps
        templateMaps.each_row() /= arma::conv_to<arma::frowvec>::from(N);
        
        
        // Score each map by it's consistency
        arma::frowvec scores = arma::zeros<arma::frowvec>(templateMaps.n_cols);
        for (unsigned int i = 0; i < multiStartIterations; ++i) {
            scores.elem(indices[i]) += arma::abs(arma::diagvec(
                MM::calculateCosineSimilarity(templateMaps.cols(indices[i]), decompositions[i])
                )) - threshold;
            // Subtracting threshold increases emphasis on similarity (as 
            // opposed to number present)
            // e.g. threshold = 0.5
            // scores = 0.6,0.55,0.65 => 1.8 or 0.3
            // scores = 0.7,0.8 => 1.5 or 0.5
        }
        // Don't give maps credit for being the only one in the template...
        scores.elem(arma::find(N == 1)).zeros();
        
        
        // Reorder so the top scoring maps are first
        const arma::uvec sortOrder = arma::sort_index(scores, "descend");
        templateMaps = templateMaps.cols(sortOrder);
        scores = scores.cols(sortOrder);
        
        
        // Save all the key template files
        templateMaps.save(outputDirectory + "TemplateMaps.hdf5", arma::hdf5_binary);
        scores.save(outputDirectory + "Scores.hdf5", arma::hdf5_binary);
        for (unsigned int i = 0; i < multiStartIterations; ++i) {
            arma::fmat reorderedBasis = arma::zeros<arma::fmat>(arma::size(templateMaps));
            reorderedBasis.cols( indices[i] ) = decompositions[i];
            reorderedBasis = reorderedBasis.cols(sortOrder);
            
            std::ostringstream iterNumber;
            iterNumber << std::setw( padWidth ) << std::setfill( '0' ) << i + 1;
            std::string fileName = outputDirectory + "DecomposedSpatialBasis_" + iterNumber.str() + "_Reordered.hdf5";
            reorderedBasis.save(fileName, arma::hdf5_binary);
        }
        
        
        // Generate the initialisation for the final decomposition
        arma::fmat initialMaps = templateMaps.head_cols(M_);
        // Make columns have a positive skew
        MM::flipColumnSigns( initialMaps );
        initialMaps.save(outputDirectory + "InitialMaps.hdf5", arma::hdf5_binary);
        
        
        // And fit the final model! Start with the template maps, and refine 
        // so that it is valid (i.e. the template generation hasn't adjusted 
        // the maps away from the data) and gives it a chance to run for longer
        finalMaps = decomposeMatrix(spatialBasis, M_, spatialModel, 250, initialMaps, outputDirectory);
    }
    //-------------------------------------------------------------------------
    
    // Make columns have a positive skew
    MM::flipColumnSigns( finalMaps );
        
    // And save
    finalMaps.save(outputDirectory + "FinalMaps.hdf5", arma::hdf5_binary);
    
    return finalMaps;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::ModelManager::saveIntermediates(const std::string intermediatesDir, const unsigned int iteration, const unsigned int maxIteration, const bool bigData, const int subjectNum, const unsigned int batchUpdateIteration, const unsigned int maxBatchUpdateIteration, const std::string bigDataDirBatch)
{
    // Get file name (with zero-padded iter number)
    const unsigned int padWidth = ((unsigned int) std::floor(std::log10(maxIteration))) + 1;
    std::ostringstream iterNumber;
    iterNumber << std::setw( padWidth ) << std::setfill( '0' ) << iteration;
    
    // Save!
    if (maxBatchUpdateIteration>0) {
        const unsigned int padBUWidth = ((unsigned int) std::floor(std::log10(maxBatchUpdateIteration))) + 1;
        std::ostringstream iterBUNumber;
        iterBUNumber << std::setw( padBUWidth ) << std::setfill( '0' ) << batchUpdateIteration;
        
        arma::fmat groupMaps = cohort_->getGroupMaps();
        std::string fileName = intermediatesDir + "GroupMaps_" + iterNumber.str() + "_BatchUpdate" + iterBUNumber.str() + ".hdf5";
        groupMaps.save(fileName, arma::hdf5_binary);
        
        arma::fmat groupPrecMat = cohort_->getGroupPrecisionMatrix();
        fileName = intermediatesDir + "GroupPrecisionMatrix_" + iterNumber.str() + "_BatchUpdate" + iterBUNumber.str() + ".hdf5";
        groupPrecMat.save(fileName, arma::hdf5_binary);
        
        arma::frowvec intermediateFreeEnergy = getFreeEnergy(bigData, subjectNum);
        fileName = intermediatesDir + "FreeEnergy_" + iterNumber.str() + "_BatchUpdate" + iterBUNumber.str() + ".hdf5";
        // same as offset free energy (see line 1063), leaving in, in case it becomes useful again in the future. 13 Jan 2025
        //if (bigData) {
        //    if (iteration>1){
        //        std::string offsetFreeEnergyFileName = bigDataDirBatch + "FreeEnergy_Offset_currentBatch.hdf5";
        //        arma::frowvec offsetFreeEnergy;
        //        offsetFreeEnergy.load(offsetFreeEnergyFileName, arma::hdf5_binary);
        //        arma::uvec freeEnergyIndices;
        //        freeEnergyIndices << 1 << 2 << 3 << 4 << 5 << 7 << 8;
        //        intermediateFreeEnergy.elem(freeEnergyIndices) = -(-intermediateFreeEnergy.elem(freeEnergyIndices)+offsetFreeEnergy.elem(freeEnergyIndices));
        //        //intermediateFreeEnergy.head_cols(9)=-(-intermediateFreeEnergy.head_cols(9)+offsetFreeEnergy.head_cols(9));
        //        
        //    }
        //}
        intermediateFreeEnergy.save(fileName, arma::hdf5_binary);
        //if (bigData){
        //    const std::string bigDataDirBatchSubjectsDir = bigDataDirBatch + "Subjects" + "/";
        //    mkdir( bigDataDirBatchSubjectsDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        //    std::vector<std::string> batchSubjectIDs = cohort_->getSubjectIDs();
        //    arma::fmat subjectsIntermediateFreeEnergy = cohort_->getAllSubjectsFreeEnergy();
        //    for (unsigned int s = 0; s < batchSubjectIDs.size(); ++s) {
        //       const std::string bigDataDirBatchSubjectDir = bigDataDirBatchSubjectsDir + batchSubjectIDs[s] + "/";            
        //        mkdir( bigDataDirBatchSubjectDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        //        std::string subjectFileName = bigDataDirBatchSubjectDir + "FreeEnergy_" + iterNumber.str() + "_BatchUpdate" + iterBUNumber.str() + ".hdf5";
        //        arma::fmat subjectIntermediateFreeEnergy=subjectsIntermediateFreeEnergy.row(s);
        //        subjectIntermediateFreeEnergy.save(subjectFileName, arma::hdf5_binary);
        //        
        //    } 
        //}
        
    } else {
        arma::fmat groupMaps = cohort_->getGroupMaps();
        std::string fileName = intermediatesDir + "GroupMaps_" + iterNumber.str() + ".hdf5";
        groupMaps.save(fileName, arma::hdf5_binary);
        
        arma::fmat groupPrecMat = cohort_->getGroupPrecisionMatrix();
        fileName = intermediatesDir + "GroupPrecisionMatrix_" + iterNumber.str() + ".hdf5";
        groupPrecMat.save(fileName, arma::hdf5_binary);
        
        //arma::frowvec intermediateFreeEnergy = getFreeEnergy(bigData, subjectNum); uncomment
        //fileName = intermediatesDir + "FreeEnergy_" + iterNumber.str() + ".hdf5"; uncomment
        //if (bigData) {
        //    if (iteration>0){
        //       std::string offsetFreeEnergyFileName = bigDataDirBatch + "FreeEnergy_Offset_currentBatch.hdf5";
        //        arma::frowvec offsetFreeEnergy;
        //        offsetFreeEnergy.load(offsetFreeEnergyFileName, arma::hdf5_binary);
        //        arma::uvec freeEnergyIndices;
        //        freeEnergyIndices << 1 << 2 << 3 << 4 << 5 << 7 << 8;
        //        intermediateFreeEnergy.elem(freeEnergyIndices) = -(-intermediateFreeEnergy.elem(freeEnergyIndices)+offsetFreeEnergy.elem(freeEnergyIndices));
        //        //intermediateFreeEnergy.head_cols(9)=-(-intermediateFreeEnergy.head_cols(9)+offsetFreeEnergy.head_cols(9));
        //    }
        //}
        
        //intermediateFreeEnergy.save(fileName, arma::hdf5_binary); uncomment
        //if (bigData){
        //    const std::string bigDataDirBatchSubjectsDir = bigDataDirBatch + "Subjects" + "/";
        //    mkdir( bigDataDirBatchSubjectsDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        //    std::vector<std::string> batchSubjectIDs = cohort_->getSubjectIDs();
        //    arma::fmat subjectsIntermediateFreeEnergy = cohort_->getAllSubjectsFreeEnergy();
        //    for (unsigned int s = 0; s < batchSubjectIDs.size(); ++s) {
        //        const std::string bigDataDirBatchSubjectDir = bigDataDirBatchSubjectsDir + batchSubjectIDs[s] + "/";            
        //        mkdir( bigDataDirBatchSubjectDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        //        std::string subjectFileName = bigDataDirBatchSubjectDir + "FreeEnergy_" + iterNumber.str() + ".hdf5";
        //        arma::fmat subjectIntermediateFreeEnergy=subjectsIntermediateFreeEnergy.row(s);
        //        subjectIntermediateFreeEnergy.save(subjectFileName, arma::hdf5_binary);
        //        
        //    }
            
        //}
        
    }
    
    //std::ofstream file;
    //file.open( fileName.c_str() );
    //file << intermediateFreeEnergy << std::endl;    
    //file.close();
    
    return;
}



////////////////////////////////////////////////////////////////////////////////
// Separately-declared convenience functions
////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::decomposeMatrix(const arma::fmat& data, const unsigned int rank, const PROFUMO::ModelManager::SpatialModel spatialModel, const unsigned int iterations, const arma::fmat& initialMaps, const std::string outputDirectory)
{
    namespace MM = Utilities::MatrixManipulations;
    
    // Select the type of decomposition we want to do
    MM::FactorisationType factorisationType;
    switch (spatialModel) {
        case ModelManager::SpatialModel::MODES : {
            factorisationType = MM::FactorisationType::MODES;
            break;
        }
        
        case ModelManager::SpatialModel::PARCELLATION : {
            factorisationType = MM::FactorisationType::PARCELS;
            break;
        }
        
        default :
            throw std::invalid_argument("ModelManager: bad SpatialModel identifier.");
    }
    
    // Decompose!
    
    // To begin with, normalise the data
    const arma::fmat normalisedData = data / arma::stddev(arma::vectorise( data ));
    
    // Calculate the decomposition
    const MM::Decomposition decomposition = MM::calculateVBMatrixFactorisation(DataTypes::FullRankData(normalisedData), rank, factorisationType, initialMaps, iterations);
    arma::fmat finalMaps = decomposition.P;
    
    // Make columns have a positive skew
    MM::flipColumnSigns( finalMaps );
    
    // Make sure things haven't been eliminated...
    const arma::frowvec stddev = arma::stddev( finalMaps );
    for (unsigned int m = 0; m < finalMaps.n_cols; ++m) {
        if (stddev(m) < 0.05) {
            std::cout << "Replacing eliminated map. Standard deviation: " << stddev(m) << std::endl;
            finalMaps.col(m) = arma::square(arma::randu<arma::fvec>(finalMaps.n_rows))
                               + 0.25 * arma::randn<arma::fvec>(finalMaps.n_rows);
        }
    }
    
    // Normalise columns
    //Utilities::DataNormalisation::normaliseColumns( finalMaps );
    // Best not to - scale should hopefully be pretty good already
    finalMaps.save(outputDirectory + "preFinalMaps.hdf5", arma::hdf5_binary);
    arma::fmat finalA = decomposition.A;
    finalA.save(outputDirectory + "preFinalA.hdf5", arma::hdf5_binary);
    arma::fmat finalAlpha = decomposition.Alpha;
    finalAlpha.save(outputDirectory + "preFinalAlpha.hdf5", arma::hdf5_binary);
    arma::fvec finalH = decomposition.h;
    finalH.save(outputDirectory + "preFinalH.hdf5", arma::hdf5_binary);
    return finalMaps;
}

////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::generateRandomInitialisaion(const arma::fmat& spatialBasis, const unsigned int rank)
{
    namespace MM = Utilities::MatrixManipulations;
    
    // Start with a random rotation of the basis, appropriately normalised
    arma::fmat randomisedBasis;
    if (spatialBasis.n_cols < rank) {
        randomisedBasis = arma::stddev(arma::vectorise(spatialBasis)) * arma::randn<arma::fmat>(spatialBasis.n_rows, rank);
        randomisedBasis.head_cols(spatialBasis.n_cols) = spatialBasis;
    }
    else {
        randomisedBasis = spatialBasis.head_cols(rank);
    }
    MM::randomiseColumns( randomisedBasis );
    
    // Get the scale in the right ballpark
    Utilities::DataNormalisation::normaliseColumns( randomisedBasis );
    //Gotcha! I realised PROFUMO usually eliminates modes where max>10; scale max to 6 here
    randomisedBasis *= 0.5;
    
    // Make columns have a positive skew
    MM::flipColumnSigns( randomisedBasis );
    
    // And add some noise
    randomisedBasis += arma::square(arma::randu<arma::fmat>(arma::size(randomisedBasis)));
    
    return randomisedBasis;
}

////////////////////////////////////////////////////////////////////////////////

// Python for examples of null standard deviation for DGMM null
/*
import math, scipy.stats

vals = [0.8, 0.9, 0.95, 0.99]
mu = 0; sigmas = [0.2, 0.3, 0.4, 0.5, 0.6]
#mu = 0; sigmas = numpy.sqrt(numpy.linspace(0.1, 0.25, 7))

for sigma in sigmas:
    print(sigma, sigma**2)
    for val in vals:
        print("{:.2f}: {:f}".format(val, scipy.stats.norm.ppf(val, mu, sigma)))
    print()
*/

// Python for examples of signal in group spike-slab
/*
import scipy.stats

x = numpy.linspace(-4.0, 4.0, 1001)
y = numpy.zeros((1001, 3))
z = numpy.zeros((500, 3))

i = 0
for mu,sigma in [[0.5, 1.0], [0.75, 1.5], [1.0, 2.0]]:
    y[:,i] = scipy.stats.norm.pdf(x, mu, sigma)
    z[:,i] = y[501:,i] / y[499::-1,i]
    i += 1

plt.figure(); plt.hold(True); plt.plot([0,0], [0,1.1*numpy.max(y)], color=[0.7 ,]*3); plt.plot(x,y)
plt.figure(); plt.plot(x[501:], z); plt.ylabel("p(+x) / p(-x)")
*/

// Python for group TGMM
/*
import scipy.stats

x = numpy.linspace(-5,5,500)
#params = [[0.65, 0.0, 0.5], [0.25,2.5,1.4], [0.1,-2.5,1.4]] # prob, mean, std
params = [[0.850, 0.0, 0.05], [0.125,1.0,0.75], [0.025,1.0,2.0]] # prob, mean, std

plt.figure()
cumPDF = numpy.zeros((500,))
for p,m,s in params:
    pdf = p * scipy.stats.norm.pdf(x,loc=m,scale=s)
    cumPDF = cumPDF + pdf
    plt.plot(x,pdf)
plt.plot(x,cumPDF)
*/

// Python for visualisation of inverse gamma priors
/*
import scipy.stats

x = numpy.linspace(1.0e-3, 5.0, 500)
z = x ** -2
a = 5.0; b = 5.0
y = scipy.stats.gamma.pdf(z, a=a, scale=1.0/b)

plt.figure(); plt.plot(x,y); plt.xlabel("Standard deviation")
plt.figure(); plt.plot(z,y); plt.xlim([0.0,10.0]); plt.xlabel("Precision")
*/


// Python for visualisation of Wishart priors
/*
import scipy.stats

def randWishart(a, B):
    return scipy.stats.wishart.rvs(a, numpy.linalg.inv(B))

M = 25; nSamples = 1000
gCM = 0.7 * (0.9 * numpy.eye(M) + 0.1 * numpy.ones((M,M)))
gPM = numpy.linalg.inv(gCM)
#print(gPM[:3,:3])

# Hyperpriors (c=group, k=subject)
c = 5.0; k = 2.0;
a_g = c * k * M;
B_g = c * gPM;
a_s = k * float(M);

diagObs = numpy.zeros((nSamples, M))
offObs = numpy.zeros((nSamples, int(M*(M-1)/2)))

diagInds = numpy.diag_indices(M)
offInds = numpy.triu_indices(M,1)

for i in range(nSamples):
    groupW = randWishart(a_g, B_g)
    subjectW = randWishart(a_s, groupW)
    diagObs[i,:] = subjectW[diagInds]
    offObs[i,:] = subjectW[offInds]

print((a_s / a_g) * B_g[:2,:2])
print(numpy.mean(diagObs), numpy.mean(offObs))

plt.figure(); h = plt.hist(diagObs.flatten(), bins=50); plt.plot([gPM[0,0],gPM[0,0]], [0,1.1*numpy.max(h[0])])
plt.figure(); h = plt.hist(offObs.flatten(), bins=50); plt.plot([gPM[0,1],gPM[0,1]], [0,1.1*numpy.max(h[0])])

for i in range(5):
    w = randWishart(a_s, randWishart(a_g, B_g))
    plt.figure(); plt.imshow(w, interpolation='none', vmin=-2.0, vmax = 2.0); plt.colorbar()
*/
