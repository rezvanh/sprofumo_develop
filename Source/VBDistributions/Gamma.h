// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2014, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Update rules for a set of independent gamma distributions
// See documentation for the update rules

#ifndef VB_DISTRIBUTIONS_GAMMA_H
#define VB_DISTRIBUTIONS_GAMMA_H

#include <string>
#include <memory>
#include <iostream>
#include <vector>
#include <armadillo>


#include "VBDistribution.h"


namespace PROFUMO
{
    namespace VBDistributions
    {
        ////////////////////////////////////////////////////////////////////////
        
        namespace DataExpectations
        {
            struct Gamma
            {
            public:
                float n;  // Number of observations
                float d2; // Sum of squares of observations
            };
        }
        
        namespace Expectations
        {
            struct Gamma
            {
            public:
                float x;
                float log_x;
            };
        }
        
        ////////////////////////////////////////////////////////////////////////
        
        class Gamma : 
            public VBDistribution<DataExpectations::Gamma, Expectations::Gamma>
        {
        public:
            //------------------------------------------------------------------
            struct Parameters
            {
            public:
                float a;  // Shape
                float b;  // Rate
            };
            //------------------------------------------------------------------
            
            Gamma(const Parameters prior, const bool preInitialised = false, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = nullptr);
            
            void update(const DataExpectations::Gamma D, const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
            
            Expectations::Gamma getExpectations() const;
            
            float getKL(const bool bigData = false) const;
            
            void save(const std::string fileName) const;
            void load(const std::string fileName);
            
        private:
            const Parameters prior_;
            Parameters posterior_;
            Parameters interimPosterior_;
            
        };
        
        ////////////////////////////////////////////////////////////////////////
    }
}
#endif
