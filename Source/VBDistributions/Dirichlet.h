// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2014, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Update rules for a Dirichlet distribution

#ifndef VB_DISTRIBUTIONS_DIRICHLET_H
#define VB_DISTRIBUTIONS_DIRICHLET_H

#include <armadillo>
#include <string>

#include "VBDistribution.h"


namespace PROFUMO
{
    namespace VBDistributions
    {
        ////////////////////////////////////////////////////////////////////////
        
        namespace DataExpectations
        {
            struct Dirichlet
            {
            public:
                arma::fvec counts;
            };
        }
        
        namespace Expectations
        {
            struct Dirichlet
            {
            public:
                arma::fvec p;
                arma::fvec log_p;
            };
        }
        
        ////////////////////////////////////////////////////////////////////////
        
        class Dirichlet : 
            public VBDistribution<DataExpectations::Dirichlet, Expectations::Dirichlet>
        {
        public:
            //------------------------------------------------------------------
            struct Parameters
            {
            public:
                arma::fvec counts;
            };
            //------------------------------------------------------------------
            
            Dirichlet(const Parameters prior);
            
            void update(const DataExpectations::Dirichlet D, const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
            
            Expectations::Dirichlet getExpectations() const;
            
            float getKL(const bool bigData = false) const;
            
            void save(const std::string fileName) const;

            void load(const std::string fileName);
            
        private:
            const Parameters prior_;
            Parameters posterior_;
            Parameters interimPosterior_;
            
            const unsigned int nClasses_;
        };
        
        ////////////////////////////////////////////////////////////////////////
    }
}
#endif
