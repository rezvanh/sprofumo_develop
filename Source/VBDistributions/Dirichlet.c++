// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2014, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "Dirichlet.h"

#include <cmath>

#include <boost/math/special_functions/gamma.hpp>
#include <boost/math/special_functions/digamma.hpp>
namespace bmath = boost::math;

namespace VBDists = PROFUMO::VBDistributions;

////////////////////////////////////////////////////////////////////////////////
//prior.counts is a vector of size 2, signal and noise
VBDists::Dirichlet::Dirichlet(const Parameters prior)
: prior_(prior), nClasses_(prior.counts.n_elem)
{
    posterior_ = prior_;
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBDists::Dirichlet::update(const DataExpectations::Dirichlet D, const bool bigData, const float posteriorRho, const int subjectNum )
{
    
    
    if (bigData) {
        // sigma
        interimPosterior_.counts = prior_.counts + D.counts;
        arma::fvec previousCounts = posterior_.counts;
        // posterior_.counts = prior_.counts + (posteriorRho * D.counts) + ((1.0 - posteriorRho) * previousCounts);
        posterior_.counts = (posteriorRho * (prior_.counts + D.counts)) + ((1.0 - posteriorRho) * previousCounts);
                
    } else {
        posterior_.counts = prior_.counts + D.counts;
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

VBDists::Expectations::Dirichlet VBDists::Dirichlet::getExpectations() const
{
    Expectations::Dirichlet E;
    
    // Cache some useful results
    const float sumCounts = arma::sum(posterior_.counts);
    const float digammaSumCounts = bmath::digamma(sumCounts);
    
    // Probabilities
    E.p = posterior_.counts / sumCounts;
    
    // log(Probabilities)
    E.log_p = arma::zeros<arma::fvec>(nClasses_);
    for (unsigned int c = 0; c < nClasses_; ++c) {
        E.log_p[c] = bmath::digamma( posterior_.counts[c] ) - digammaSumCounts;
    }
    
    return E;
}

////////////////////////////////////////////////////////////////////////////////

float VBDists::Dirichlet::getKL(const bool bigData) const
{
    float KL;
    //const double pr = prior_.counts[c];
    //const double po = posterior_.counts[c];



    if (bigData) {
        // Cache some useful results
        const double sumPosterior = arma::sum(interimPosterior_.counts);
        const double digammaSumPosterior = bmath::digamma(sumPosterior);
        
        // Calculate KL
        KL = bmath::lgamma(sumPosterior)
                    - bmath::lgamma(arma::sum(prior_.counts));
        
        for (unsigned int c = 0; c < nClasses_; ++c) {
                const double pr = prior_.counts[c];
                const double po = interimPosterior_.counts[c];
                
                KL += bmath::lgamma(pr) - bmath::lgamma(po)
                      + (po - pr) * (bmath::digamma(po) - digammaSumPosterior);
        } 
        
    } else {
        // Cache some useful results
        const double sumPosterior = arma::sum(posterior_.counts);
        const double digammaSumPosterior = bmath::digamma(sumPosterior);
        
        // Calculate KL
        KL = bmath::lgamma(sumPosterior)
                    - bmath::lgamma(arma::sum(prior_.counts));
        
        for (unsigned int c = 0; c < nClasses_; ++c) {
                const double pr = prior_.counts[c];
                const double po = posterior_.counts[c];
                
                KL += bmath::lgamma(pr) - bmath::lgamma(po)
                      + (po - pr) * (bmath::digamma(po) - digammaSumPosterior);
        }                
    }
    
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////

void VBDists::Dirichlet::save(const std::string fileName) const
{
    posterior_.counts.save(fileName, arma::raw_ascii);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBDists::Dirichlet::load(const std::string fileName)
{
    posterior_.counts.load(fileName, arma::raw_ascii);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
