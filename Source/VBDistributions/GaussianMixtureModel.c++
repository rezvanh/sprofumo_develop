// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "GaussianMixtureModel.h"

#include <armadillo>
#include <cmath>
#include <random>
#include <fstream>

#include "Utilities/MiscMaths.h"
namespace miscmaths = PROFUMO::Utilities::MiscMaths;

namespace VBDists = PROFUMO::VBDistributions;
namespace GMMComps = PROFUMO::VBDistributions::GaussianMixtureModelComponents;

////////////////////////////////////////////////////////////////////////////////
// Gaussian Mixture Model
////////////////////////////////////////////////////////////////////////////////

VBDists::GaussianMixtureModel::GaussianMixtureModel(const std::vector<GaussianParameters> gaussianPriors, const std::vector<DeltaParameters> deltaPriors, const bool randomInitialisation)
: nComponents_(gaussianPriors.size() + deltaPriors.size())
{
    // Initialise the components!
    // Gaussian
    for (unsigned int c = 0; c < gaussianPriors.size(); ++c) {
        Component::Parameters prior; prior.p = gaussianPriors[c].probability;
        components_.emplace_back(
            prior, 
            std::make_unique<GMMComps::GaussianComponent>(gaussianPriors[c].parameters, randomInitialisation)
        );
    }
    
    // Delta
    for (unsigned int c = 0; c < deltaPriors.size(); ++c) {
        Component::Parameters prior; prior.p = deltaPriors[c].probability;
        components_.emplace_back(
            prior, 
            std::make_unique<GMMComps::DeltaComponent>(deltaPriors[c].parameters)
        );
    }
    
    // Randomly change posterior probabilities, if requested
    if (randomInitialisation) {
        // Collect all the prior probabilities
        std::vector<float> probabilities;
        for (unsigned int c = 0; c < nComponents_; ++c) {
            probabilities.push_back( components_[c].prior.p );
        }
        
        // Draw from this distribution
        std::random_device rd;
        std::default_random_engine generator(rd());
        std::discrete_distribution<unsigned int> priorDistribution(probabilities.begin(), probabilities.end());;
        const unsigned int component = priorDistribution(generator);
        
        // And reset posteriors
        for (unsigned int c = 0; c < nComponents_; ++c) {
            if (c == component) {
                components_[c].posterior.p = 1.0f;
            }
            else {
                components_[c].posterior.p = 0.0f;
            }
        }
    }
    
    //arma::fvec preInterimLogp = arma::zeros<arma::fvec>(nComponents_);
    for (unsigned int c = 0; c < nComponents_; ++c) {
        interimLogp.push_back(0.0f);
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBDists::GaussianMixtureModel::update(const DataExpectations::Gaussian D, const bool bigData, const float posteriorRho, const int subjectNum)
{
    // Update the components first
    for (unsigned int c = 0; c < nComponents_; ++c) {
        components_[c].distribution->update(D, bigData, posteriorRho, subjectNum);
    }
    
    // Calculate the log probabilities
    arma::fvec log_p = arma::zeros<arma::fvec>(nComponents_);
    arma::fvec log_interimp = arma::zeros<arma::fvec>(nComponents_);
    if (bigData) {
        for (unsigned int c = 0; c < nComponents_; ++c) {
            log_interimp[c] = std::log( components_[c].prior.p )
                       + components_[c].distribution->getEvidence(true);
        }
        
        for (unsigned int c = 0; c < nComponents_; ++c) {
            //log_p[c] = ((1.0 - posteriorRho) * std::log( components_[c].posterior.p ))
            //           + (posteriorRho * (std::log( components_[c].prior.p ) + components_[c].distribution->getEvidence()));
            log_p[c] = ((1.0 - posteriorRho) * interimLogp[c])
                       + (posteriorRho * (std::log( components_[c].prior.p ) + components_[c].distribution->getEvidence(false)));
            //interimLogp[c] = log_p[c] * 1.0;
            
        }
        interimLogp.clear();
        for (unsigned int c = 0; c < nComponents_; ++c) {
            interimLogp.push_back(log_p[c]);
        }
        
    
    } else {
        for (unsigned int c = 0; c < nComponents_; ++c) {
            log_p[c] = std::log( components_[c].prior.p )
                       + components_[c].distribution->getEvidence(false);
        }
    
    }
    
    // And then calculate the true probabilities (safely!)
    const arma::fvec p = miscmaths::softmax(log_p);
    const arma::fvec interimp = miscmaths::softmax(log_interimp);
    
    // And save
    for (unsigned int c = 0; c < nComponents_; ++c) {
        components_[c].posterior.p = p[c];
        components_[c].interimPosterior.p = interimp[c];
    }

    
    return;
}

////////////////////////////////////////////////////////////////////////////////

VBDists::Expectations::Gaussian VBDists::GaussianMixtureModel::getExpectations() const
{
    Expectations::Gaussian E;
    
    // Expectations are just the weighted sum of component expectations
    E.x = 0.0; E.x2 = 0.0;
    for (unsigned int c = 0; c < nComponents_; ++c) {
        const Expectations::Gaussian e = components_[c].distribution->getExpectations();
        const float p = components_[c].posterior.p;
        
        E.x += p * e.x;
        E.x2 += p * e.x2;
    }
    
    return E;
}

////////////////////////////////////////////////////////////////////////////////

float VBDists::GaussianMixtureModel::getKL(const bool bigData) const
{
    float KL = 0.0;
    if (bigData){
        for (unsigned int c = 0; c < nComponents_; ++c) {
            // Calculate the KL from the probabilities
            double p = components_[c].interimPosterior.p;
            if (p != 0.0) {
                KL += p * ( std::log(p) - std::log(components_[c].prior.p) );
            }
            // If p = 0.0, then KL = 0.0
            
            // And add the contributions from the components themselves
            KL += p * components_[c].distribution->getKL(bigData);
        }
        
    } else {
        for (unsigned int c = 0; c < nComponents_; ++c) {
            // Calculate the KL from the probabilities
            double p = components_[c].posterior.p;
            if (p != 0.0) {
                KL += p * ( std::log(p) - std::log(components_[c].prior.p) );
            }
            // If p = 0.0, then KL = 0.0
            
            // And add the contributions from the components themselves
            KL += p * components_[c].distribution->getKL(bigData);
        }
        
    }
    
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////

void VBDists::GaussianMixtureModel::save(const std::string fileName) const
{
    std::ofstream file;
    file.open( fileName.c_str() );
    
    for (unsigned int c = 0; c < nComponents_; ++c) {
        const Expectations::Gaussian e = components_[c].distribution->getExpectations();
        const float p = components_[c].posterior.p;
        
        file << "Component " << c << std::endl;
        file << "p: " << p << std::endl;
        file << "x: " << e.x << std::endl;
        file << "x2: " << e.x2 << std::endl;
        file << std::endl;
    }
    
    file.close();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBDists::GaussianMixtureModel::load(const std::string fileName)
{
    // why this is all commented out for GMM??? other  12 Jan 2025
    // std::ifstream file(fileName.c_str());
    
    // for (unsigned int c = 0; c < nComponents_; ++c) {

    //     std::string cstr, pstr, xstr, x2str;
    //     float prior_c, prior_p, prior_x, prior_x2;
    //     file >> cstr >> prior_c >> pstr >> prior_p >> xstr >> prior_x >> x2str >> prior_x2;

    //     const Expectations::Gaussian e = components_[c].distribution->getExpectations();
    //     const float p = components_[c].posterior.p;
        
    //     file << "Component " << c << std::endl;
    //     file << "p: " << p << std::endl;
    //     file << "x: " << e.x << std::endl;
    //     file << "x2: " << e.x2 << std::endl;
    //     file << std::endl;
    // }

    // std::ifstream fileParams(fileName.c_str());
    // std::string astr, bstr;
    // float prior_a, prior_b;    
    // fileParams >> astr >> prior_a >> bstr >> prior_b;
    // // arma::fvec agPrior(2);
    // // agPrior.at(0) = prior_a;
    // // agPrior.at(1) = prior_b;
    // posterior_.a = prior_a;
    // posterior_.b = prior_b;
    // fileParams.close();
    
    // file.close();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

VBDists::GaussianMixtureModel::Component::Component(const Parameters prior, std::unique_ptr<GaussianMixtureModelComponent> distribution)
: prior(prior), posterior(prior), distribution(std::move(distribution))
{
    return;
}

////////////////////////////////////////////////////////////////////////////////
// Mixture Model Components
////////////////////////////////////////////////////////////////////////////////

GMMComps::GaussianComponent::GaussianComponent(const Parameters prior, const bool randomInitialisation)
: VBDistributions::Gaussian(prior, randomInitialisation)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

float GMMComps::GaussianComponent::getEvidence(const bool bigData) const
{
    float evidence;
    if (bigData){
        
        const Gaussian::Parameters prior = getPrior();
        const Gaussian::Parameters posterior = getPosterior();
        const Gaussian::Parameters interimPosterior = getInterimPosterior();
        
        // Calculate!
        evidence = 0.5 * std::log(interimPosterior.sigma2) - 0.5 * std::log(prior.sigma2);
        
        evidence -= 0.5 * miscmaths::square(prior.mu) / prior.sigma2;
        evidence += 0.5 * miscmaths::square(interimPosterior.mu) / interimPosterior.sigma2;
        
    } else {
        const Gaussian::Parameters prior = getPrior();
        const Gaussian::Parameters posterior = getPosterior();
        
        // Calculate!
        evidence = 0.5 * std::log(posterior.sigma2) - 0.5 * std::log(prior.sigma2);
        
        evidence -= 0.5 * miscmaths::square(prior.mu) / prior.sigma2;
        evidence += 0.5 * miscmaths::square(posterior.mu) / posterior.sigma2;
        
    }
    
    
    return evidence;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

GMMComps::DeltaComponent::DeltaComponent(const Parameters prior)
: prior_(prior)
{
    // Initialise cache - no data yet 
    evidence_ = 0.0;
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void GMMComps::DeltaComponent::update(const DataExpectations::Gaussian D, const bool bigData, const float posteriorRho, const int subjectNum)
{
    // Posterior always equals prior (infinite precision)
    // So just record the evidence
    evidence_ = D.psi_d * prior_.mu - 0.5 * D.psi * miscmaths::square(prior_.mu);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

VBDists::Expectations::Gaussian GMMComps::DeltaComponent::getExpectations() const
{
    Expectations::Gaussian E;
    
    E.x = prior_.mu;
    E.x2 = miscmaths::square(prior_.mu);
    
    return E;
}

////////////////////////////////////////////////////////////////////////////////

float GMMComps::DeltaComponent::getEvidence(const bool bigData) const
{
    return evidence_;
}

////////////////////////////////////////////////////////////////////////////////

float GMMComps::DeltaComponent::getKL(const bool bigData) const
{
    return 0.0;
}

////////////////////////////////////////////////////////////////////////////////

void GMMComps::DeltaComponent::save(const std::string fileName) const
{
    std::ofstream file;
    file.open( fileName.c_str() );
    
    file << "mu: " << prior_.mu << std::endl;
    
    file.close();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
