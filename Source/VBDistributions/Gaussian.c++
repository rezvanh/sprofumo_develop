// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "Gaussian.h"

#include <cmath>
#include <random>
#include <fstream>
#include <iostream>


#include "Utilities/MiscMaths.h"
namespace miscmaths = PROFUMO::Utilities::MiscMaths;

namespace VBDists = PROFUMO::VBDistributions;

/////////////////////////////////////////////////////////////////////////////////

VBDists::Gaussian::Gaussian(const Parameters prior, const bool randomInitialisation)
: prior_(prior)
{
    // Initialise posterior
    posterior_.sigma2 = prior_.sigma2;
    
    if (randomInitialisation) {
        // Random draw from prior, if requested
        std::random_device rd;
        std::default_random_engine generator(rd());
        std::normal_distribution<> priorDistribution(prior_.mu, std::sqrt(prior_.sigma2));
        posterior_.mu = priorDistribution(generator);
    }
    else {
        // Otherwise just set to the prior
        posterior_.mu = prior_.mu;
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBDists::Gaussian::update(const DataExpectations::Gaussian D, const bool bigData, const float posteriorRho, const int subjectNum)
{
    
    if (bigData) {
        
        // sigma
        interimPosterior_.sigma2 = 1.0 / ( D.psi + (1.0 / prior_.sigma2) ); 
        // mu
        interimPosterior_.mu = interimPosterior_.sigma2 * ( D.psi_d + (prior_.mu / prior_.sigma2) );
        // sigma
        float previousPsi = 1.0 / posterior_.sigma2;
        float newPsi =  D.psi + (1.0 / prior_.sigma2) ;
        float previousMuSigma = posterior_.mu / posterior_.sigma2;
        float previousMu = posterior_.mu; 
        posterior_.sigma2 = 1.0 / ((posteriorRho * newPsi) + ((1.0 - posteriorRho) * previousPsi));
        // mu
        float newMuSigma =  D.psi_d + (prior_.mu / prior_.sigma2) ;
        posterior_.mu = posterior_.sigma2 *((posteriorRho * newMuSigma) + ((1.0 - posteriorRho) * previousMuSigma));
        
    } else {
        // sigma
        posterior_.sigma2 = 1.0 / ( D.psi + (1.0 / prior_.sigma2) ); 
        // mu
        posterior_.mu = posterior_.sigma2 * ( D.psi_d + (prior_.mu / prior_.sigma2) );   
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

VBDists::Expectations::Gaussian VBDists::Gaussian::getExpectations() const
{
    Expectations::Gaussian E;
    
    E.x = posterior_.mu;
    E.x2 = miscmaths::square(posterior_.mu) + posterior_.sigma2;
    
    return E;
}

////////////////////////////////////////////////////////////////////////////////

float VBDists::Gaussian::getKL(const bool bigData) const
{
    float KL;
    double prior_sigma2 = prior_.sigma2, post_sigma2 = posterior_.sigma2, interimPosterior_sigma2=interimPosterior_.sigma2;
    double prior_mu = prior_.mu, post_mu = posterior_.mu, interimPosterior_mu = interimPosterior_.mu;
    if (bigData){
        KL = 0.5 * std::log(prior_sigma2) - 0.5 * std::log(interimPosterior_sigma2);
    
        KL += 0.5 * (miscmaths::square(interimPosterior_mu - prior_mu) + interimPosterior_sigma2) / prior_sigma2;
        
        KL -= 0.5;        
    } else {
        KL = 0.5 * std::log(prior_sigma2) - 0.5 * std::log(post_sigma2);
        
        KL += 0.5 * (miscmaths::square(post_mu - prior_mu) + post_sigma2) / prior_sigma2;
        
        KL -= 0.5;
    }
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////

void VBDists::Gaussian::save(const std::string fileName) const
{
    std::ofstream file;
    file.open( fileName.c_str() );
    
    file << "mu: " << posterior_.mu << std::endl;
    file << "sigma: " << std::sqrt(posterior_.sigma2) << std::endl;
    
    file.close();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBDists::Gaussian::load(const std::string fileName)
{
    std::ifstream file(fileName.c_str());
    std::string mustr, sigmastr;
    float prior_mu, prior_sigma; 
    file >> mustr >> prior_mu >> sigmastr >> prior_sigma;
    posterior_.mu = prior_mu;
    posterior_.sigma2 = prior_sigma*prior_sigma;
    file.close();

    return;
}

////////////////////////////////////////////////////////////////////////////////
