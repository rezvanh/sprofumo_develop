// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Update rules for a set of independent Gaussian mixture models
// See documentation for the update rules

#ifndef VB_DISTRIBUTIONS_GAUSSIAN_MIXTURE_MODEL_H
#define VB_DISTRIBUTIONS_GAUSSIAN_MIXTURE_MODEL_H

#include <memory>
#include <vector>
#include <string>
#include <armadillo>

#include "VBDistribution.h"
#include "VBDistributions/Gaussian.h"

////////////////////////////////////////////////////////////////////////////////
// Implementation of a couple of components
////////////////////////////////////////////////////////////////////////////////

namespace PROFUMO
{
    namespace VBDistributions
    {
        ////////////////////////////////////////////////////////////////////////
        
        class GaussianMixtureModelComponent :
            public virtual VBDistribution<DataExpectations::Gaussian, Expectations::Gaussian>
        {
        public:
            // Get the "evidence" for this component (to be turned into a probability)
            virtual float getEvidence(const bool bigData = false) const = 0;
        };
        
        ////////////////////////////////////////////////////////////////////////
        
        namespace GaussianMixtureModelComponents
        {
            ////////////////////////////////////////////////////////////////////
            
            class GaussianComponent :
                public GaussianMixtureModelComponent,
                protected VBDistributions::Gaussian
            {
            public:
                //--------------------------------------------------------------
                typedef VBDistributions::Gaussian::Parameters Parameters;
                //--------------------------------------------------------------
                
                GaussianComponent(const Parameters prior, const bool randomInitialisation = false);
                
                float getEvidence(const bool bigData = false) const;
            };
            
            ////////////////////////////////////////////////////////////////////
            
            class DeltaComponent :
                public GaussianMixtureModelComponent
            {
            public:
                //--------------------------------------------------------------
                struct Parameters
                {
                public:
                    float mu;
                };
                //--------------------------------------------------------------
                
                DeltaComponent(const Parameters prior);
                
                void update(const DataExpectations::Gaussian D, const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                Expectations::Gaussian getExpectations() const;
                
                float getEvidence(const bool bigData = false) const;
                
                float getKL(const bool bigData = false) const;
                
                void save(const std::string fileName) const;

                void load(const std::string fileName);
                
            private:
                const Parameters prior_;
                float evidence_;
            };
            
            ////////////////////////////////////////////////////////////////////
        }
        
        ////////////////////////////////////////////////////////////////////////
    }
}

////////////////////////////////////////////////////////////////////////////////
// Implementation of the mixture model
////////////////////////////////////////////////////////////////////////////////

namespace PROFUMO
{
    namespace VBDistributions
    {
        ////////////////////////////////////////////////////////////////////////
        
        class GaussianMixtureModel :
            public VBDistribution<DataExpectations::Gaussian, Expectations::Gaussian>
        {
        public:
            //------------------------------------------------------------------
            struct GaussianParameters
            {
            public:
                float probability;
                GaussianMixtureModelComponents::GaussianComponent::Parameters parameters;
            };
            //------------------------------------------------------------------
            struct DeltaParameters
            {
            public:
                float probability;
                GaussianMixtureModelComponents::DeltaComponent::Parameters parameters;
            };
            //------------------------------------------------------------------
            
            GaussianMixtureModel(const std::vector<GaussianParameters> gaussianPriors, const std::vector<DeltaParameters> deltaPriors, const bool randomInitialisation = false);
            
            void update(const DataExpectations::Gaussian D, const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
            
            Expectations::Gaussian getExpectations() const;
            
            float getKL(const bool bigData = false) const;
            
            void save(const std::string fileName) const;

            void load(const std::string fileName);
            
            
            
        private:
            //------------------------------------------------------------------
            struct Component
            {
            public:
                struct Parameters {public: float p;};
                
                const Parameters prior;
                Parameters interimPosterior;
                Parameters posterior;
                std::unique_ptr<GaussianMixtureModelComponent> distribution;
                
                Component(const Parameters prior, std::unique_ptr<GaussianMixtureModelComponent> distribution);
            };
            //------------------------------------------------------------------
            
            std::vector<Component> components_;
            const unsigned int nComponents_;
            
            std::vector<float> interimLogp; 
            //arma::fvec interimLogp = arma::zeros<arma::fvec>(1);
            
        };
        
        ////////////////////////////////////////////////////////////////////////
    }
}
#endif
