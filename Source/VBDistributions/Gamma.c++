// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2014, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "Gamma.h"

#include <memory>
#include <iostream>
#include <vector>
#include <string>
#include <armadillo>
#include <fstream>
#include <cmath>

#include <boost/math/special_functions/gamma.hpp>
#include <boost/math/special_functions/digamma.hpp>
namespace bmath = boost::math;

namespace VBDists = PROFUMO::VBDistributions;

////////////////////////////////////////////////////////////////////////////////

VBDists::Gamma::Gamma(const Parameters prior, const bool preInitialised, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation)
: prior_(prior)
{
    if (preInitialised) {
        std::vector<arma::fmat> initialisationMatrices = *expectationInitialisation;
        posterior_.a = initialisationMatrices[0].at(0);
        posterior_.b = initialisationMatrices[0].at(1);
    } else {
        posterior_ = prior_;
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBDists::Gamma::update(const DataExpectations::Gamma D, const bool bigData, const float posteriorRho, const int subjectNum)
{
    
    if (bigData) {
        // sigma
        interimPosterior_.a = prior_.a + D.n;    
        interimPosterior_.b = prior_.b + D.d2; 
        
        double previousA = posterior_.a;
        double previousB = posterior_.b;
        posterior_.a =  (posteriorRho * (prior_.a + D.n)) + ((1.0 - posteriorRho) * previousA);
        
        posterior_.b = (posteriorRho * (prior_.b + D.d2)) + ((1.0 - posteriorRho) * previousB);
        
    } else {
        posterior_.a = prior_.a + D.n;
        posterior_.b = prior_.b + D.d2;
        
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

VBDists::Expectations::Gamma VBDists::Gamma::getExpectations() const
{
    Expectations::Gamma E;
    
    E.x = posterior_.a / posterior_.b;
    
    E.log_x = bmath::digamma(posterior_.a / 2.0) - std::log(posterior_.b / 2.0);
    
    return E;
}

////////////////////////////////////////////////////////////////////////////////

float VBDists::Gamma::getKL(const bool bigData) const
{
    double prior_a = prior_.a, post_a = posterior_.a, interimPosterior_a = interimPosterior_.a;
    double prior_b = prior_.b, post_b = posterior_.b, interimPosterior_b = interimPosterior_.b;
    float KL;
    if (bigData) {
        // KL divergence between prior and posterior
        KL = 0.5 * prior_a * (std::log(interimPosterior_b) - std::log(prior_b));
        KL -= 0.5 * interimPosterior_a * (interimPosterior_b - prior_b) / interimPosterior_b;
        KL += 0.5 * (interimPosterior_a - prior_a) * bmath::digamma(interimPosterior_a / 2.0);
        KL -= bmath::lgamma(interimPosterior_a / 2.0);
        KL += bmath::lgamma(prior_a / 2.0); 
        
    } else {
        // KL divergence between prior and posterior
        KL = 0.5 * prior_a * (std::log(post_b) - std::log(prior_b));
        KL -= 0.5 * post_a * (post_b - prior_b) / post_b;
        KL += 0.5 * (post_a - prior_a) * bmath::digamma(post_a / 2.0);
        KL -= bmath::lgamma(post_a / 2.0);
        KL += bmath::lgamma(prior_a / 2.0);        
        
    }
    
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////

void VBDists::Gamma::save(const std::string fileName) const
{
    std::ofstream file;
    file.open( fileName.c_str() );
    
    file << "a: " << posterior_.a << std::endl;
    file << "b: " << posterior_.b << std::endl;
    
    file.close();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBDists::Gamma::load(const std::string fileName)
{

    std::ifstream fileParams(fileName.c_str());
    std::string astr, bstr;
    float prior_a, prior_b;    
    fileParams >> astr >> prior_a >> bstr >> prior_b;
    posterior_.a = prior_a;
    posterior_.b = prior_b;
    fileParams.close();
    
    return;
}

