// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2014, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a VB distribution. This is designed to be a "dumb" helper class that
// stores some useful update rules but, as it has no concept of the graphical
// model structure, it cannot be used to build a full model by itself.

#ifndef VB_DISTRIBUTION_H
#define VB_DISTRIBUTION_H

#include <string>

namespace PROFUMO
{
    
    template<class DataExpectations, class Expectations>
    class VBDistribution
    {
    public:
        // Update the posterior based on the input data
        virtual void update(const DataExpectations D, const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0) = 0;
        
        // Return the expectations of the posterior
        virtual Expectations getExpectations() const = 0;
        
        // KL divergence between prior and posterior
        virtual float getKL(const bool bigData = false) const = 0;
        
        //Save parameters to file
        virtual void save(const std::string fileName) const = 0;
        
        virtual ~VBDistribution() = default;
    };
    
}
#endif
