// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Some useful prespecified Module parent/child pairs. Also contain some
// new classes which provide named functions so a class which implements
// several modules can override getExpectations() independently.

#ifndef MODULE_LIST_H
#define MODULE_LIST_H

#include <vector>
#include <armadillo>

#include "Module.h"
#include "Posterior.h"

namespace PROFUMO
{
    namespace Modules
    {
        ////////////////////////////////////////////////////////////////////////
        // Interfaces between distributions over precision matrices
        
        // Expectations
        namespace PrecisionMatrices
        {
            struct C2P
            {
            public:
                float N;
                arma::fmat XXt;
            };
            
            struct P2C
            {
            public:
                arma::fmat X;
                float logDetX;
            };
        }
        
        ////////////////////////////////////////////////////////////////////////
        // Define parents / children
        
        typedef Module_Parent<PrecisionMatrices::C2P, PrecisionMatrices::P2C> PrecisionMatrix_Parent;
        typedef Posterior<PrecisionMatrices::C2P, PrecisionMatrices::P2C> PrecisionMatrix_Posterior;
        typedef VBPosterior<PrecisionMatrices::C2P, PrecisionMatrices::P2C> PrecisionMatrix_VBPosterior;
        
        //class PrecisionMatrix_VBPosterior :
        //    public VBPosterior<PrecisionMatrices::C2P, PrecisionMatrices::P2C>
        //{
        //public:
        //    virtual void signFlipPrecMat(const arma::frowvec spatialSigns) = 0;
        //};
        
        typedef Module_Child<PrecisionMatrices::C2P, PrecisionMatrices::P2C> PrecisionMatrix_Child;
        
        ////////////////////////////////////////////////////////////////////////
        // Define a child which allows the getExpectations() function to be
        // overridden when there is multiple inheritance
        
        class PrecisionMatrix_JointChild :
            public PrecisionMatrix_Child
        {
        public:
            PrecisionMatrix_JointChild(PrecisionMatrix_Parent* parentModule);
            PrecisionMatrices::C2P getExpectations() const;
            
        protected:
            virtual PrecisionMatrices::C2P getPrecisionMatrices() const = 0;
        };
        
        // Implementations
        inline PrecisionMatrix_JointChild::PrecisionMatrix_JointChild(PrecisionMatrix_Parent* parentModule)
        : PrecisionMatrix_Child(parentModule)
        {
            return;
        }
        
        inline PrecisionMatrices::C2P PrecisionMatrix_JointChild::getExpectations() const
        {
            return getPrecisionMatrices();
        }
        
        ////////////////////////////////////////////////////////////////////////
        
        
        
        
        
        ////////////////////////////////////////////////////////////////////////
        // Interfaces between distributions over membership probabilities
        
        // Expectations
        namespace MembershipProbabilities
        {
            struct C2P
            {
            public:
                std::vector<arma::fmat> counts;
            };
            
            struct P2C
            {
            public:
                std::vector<arma::fmat> p;
                std::vector<arma::fmat> logP;
            };
        }
        
        ////////////////////////////////////////////////////////////////////////
        // Define parents / children
        
        typedef Module_Parent<MembershipProbabilities::C2P, MembershipProbabilities::P2C> MembershipProbability_Parent;
        typedef Posterior<MembershipProbabilities::C2P, MembershipProbabilities::P2C> MembershipProbability_Posterior;
        typedef VBPosterior<MembershipProbabilities::C2P, MembershipProbabilities::P2C> MembershipProbability_VBPosterior;
        
        typedef Module_Child<MembershipProbabilities::C2P, MembershipProbabilities::P2C> MembershipProbability_Child;
        
        ////////////////////////////////////////////////////////////////////////
        // Define a child which allows the getExpectations() function to be
        // overridden when there is multiple inheritance
        
        class MembershipProbability_JointChild :
            public MembershipProbability_Child
        {
        public:
            MembershipProbability_JointChild(MembershipProbability_Parent* parentModule);
            MembershipProbabilities::C2P getExpectations() const;
            
        protected:
            virtual MembershipProbabilities::C2P getMembershipProbabilities() const = 0;
        };
        
        // Implementations
        inline MembershipProbability_JointChild::MembershipProbability_JointChild(MembershipProbability_Parent* parentModule)
        : MembershipProbability_Child(parentModule)
        {
            return;
        }
        
        inline MembershipProbabilities::C2P MembershipProbability_JointChild::getExpectations() const
        {
            return getMembershipProbabilities();
        }
        
        ////////////////////////////////////////////////////////////////////////
        
        
        
        
        
        ////////////////////////////////////////////////////////////////////////
        // Interfaces between distributions over a matrix of means
        
        // Expectations
        namespace MatrixMeans
        {
            struct C2P
            {
            public:
                arma::fmat Psi;    // Precisions
                arma::fmat PsiD;   // Observations weighted by their precisions
            };
            
            struct P2C
            {
            public:
                arma::fmat X;      // Mean
                arma::fmat X2;     // Variance
            };
        }
        
        ////////////////////////////////////////////////////////////////////////
        // Define parents / children
        
        typedef Module_Parent<MatrixMeans::C2P, MatrixMeans::P2C> MatrixMean_Parent;
        typedef Posterior<MatrixMeans::C2P, MatrixMeans::P2C> MatrixMean_Posterior;
        typedef VBPosterior<MatrixMeans::C2P, MatrixMeans::P2C> MatrixMean_VBPosterior;
        
        //class MatrixMean_VBPosterior :
        //   public VBPosterior<MatrixMeans::C2P, MatrixMeans::P2C>
        //{
        //public:
        //    virtual void signFlipMeans(const arma::frowvec spatialSigns) = 0;
        //};
        
        typedef Module_Child<MatrixMeans::C2P, MatrixMeans::P2C> MatrixMean_Child;
        
        ////////////////////////////////////////////////////////////////////////
        // Define a child which allows the getExpectations() function to be
        // overridden when there is multiple inheritance
        
        class MatrixMean_JointChild :
            public MatrixMean_Child
        {
        public:
            MatrixMean_JointChild(MatrixMean_Parent* parentModule);
            MatrixMeans::C2P getExpectations() const;
            
        protected:
            virtual MatrixMeans::C2P getMatrixMeans() const = 0;
        };
        
        // Implementations
        inline MatrixMean_JointChild::MatrixMean_JointChild(MatrixMean_Parent* parentModule)
        : MatrixMean_Child(parentModule)
        {
            return;
        }
        
        inline MatrixMeans::C2P MatrixMean_JointChild::getExpectations() const
        {
            return getMatrixMeans();
        }
        
        ////////////////////////////////////////////////////////////////////////
        
        
        
        
        
        ////////////////////////////////////////////////////////////////////////
        // Interfaces between distributions over a matrix of means
        
        // Expectations
        namespace MatrixPrecisions
        {
            struct C2P
            {
            public:
                arma::fmat N;    // Number of observations
                arma::fmat D2;   // Sum of squares of observations
            };
            
            struct P2C
            {
            public:
                arma::fmat X;
                arma::fmat logX;
            };
        }
        
        ////////////////////////////////////////////////////////////////////////
        // Define parents / children
        
        typedef Module_Parent<MatrixPrecisions::C2P, MatrixPrecisions::P2C> MatrixPrecision_Parent;
        typedef Posterior<MatrixPrecisions::C2P, MatrixPrecisions::P2C> MatrixPrecision_Posterior;
        typedef VBPosterior<MatrixPrecisions::C2P, MatrixPrecisions::P2C> MatrixPrecision_VBPosterior;
        
        typedef Module_Child<MatrixPrecisions::C2P, MatrixPrecisions::P2C> MatrixPrecision_Child;
        
        ////////////////////////////////////////////////////////////////////////
        // Define a child which allows the getExpectations() function to be
        // overridden when there is multiple inheritance
        
        class MatrixPrecision_JointChild :
            public MatrixPrecision_Child
        {
        public:
            MatrixPrecision_JointChild(MatrixPrecision_Parent* parentModule);
            MatrixPrecisions::C2P getExpectations() const;
            
        protected:
            virtual MatrixPrecisions::C2P getMatrixPrecisions() const = 0;
        };
        
        // Implementations
        inline MatrixPrecision_JointChild::MatrixPrecision_JointChild(MatrixPrecision_Parent* parentModule)
        : MatrixPrecision_Child(parentModule)
        {
            return;
        }
        
        inline MatrixPrecisions::C2P MatrixPrecision_JointChild::getExpectations() const
        {
            return getMatrixPrecisions();
        }
        
        ////////////////////////////////////////////////////////////////////////
    }
}
#endif
