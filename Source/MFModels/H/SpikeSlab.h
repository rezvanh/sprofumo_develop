// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2017
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Very simple mode weight model - basically a multivariate normal with a 
// between weight covariance structure, except that the posterior is 
// sign-flipped to enforce positivity. Potential modifications to 
// the inferred weights:
//  + Rectification (i.e. sign-flipping to enforce positivity)

#ifndef MF_MODELS_H_SPIKE_SLAB_H
#define MF_MODELS_H_SPIKE_SLAB_H

#include <iostream>
#include <string>
#include <armadillo>
#include <memory>
#include <vector>

#include "Module.h"
#include "Posterior.h"
#include "ModuleList.h"
#include "MFModel.h"
#include "MFVariableModels.h"

#include "VBDistributions/GaussianMixtureModel.h"

#include "Utilities/DataIO.h"

namespace PROFUMO
{
    namespace MFModels
    {
        namespace H
        {
            
            class SpikeSlab :
                public H_VBPosterior,
                protected CachedModule<H::P2C>
            {
            public:
                //--------------------------------------------------------------
                struct Parameters {
                public:
                    unsigned int M; // Size
                    float mu;       // Mean
                    float sigma2;   // Variance
                    float p;        // Slab proportion
                    
                    bool rectifyWeights = false;
                };
                //--------------------------------------------------------------
                
                SpikeSlab(const Parameters prior, const bool preInitialised = false, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = nullptr);
                
                void update(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                float getKL(const bool bigData = false) const;
                
                void save(const std::string directory) const;

                void load(const std::string directory);
                
                arma::fmat signFlip(const arma::fmat spatialSigns);
                
            protected:
                // Matrix sizes
                const unsigned int M_; // Number of modes
                
                // Prior
                const Parameters prior_;
                
                // Storage for posteriors
                std::vector<VBDistributions::GaussianMixtureModel> posteriors_;
                
                // Helper functions to collect expectations / set caches
                H::C2P collectExpectations() const;
                void updateExpectations();
            };
            
        }
    }
}
#endif
