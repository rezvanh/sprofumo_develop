// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2017
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "SpikeSlab.h"

#include <armadillo>
#include <vector>
#include <cmath>

namespace MFHModels = PROFUMO::MFModels::H;

namespace VBDists = PROFUMO::VBDistributions;
typedef PROFUMO::VBDistributions::GaussianMixtureModel GMM;

////////////////////////////////////////////////////////////////////////////////

MFHModels::SpikeSlab::SpikeSlab(const Parameters prior, const bool preInitialised, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation)
: M_(prior.M), prior_(prior)
{
    // Put all the parameters to make a spike-slab together
    GMM::GaussianParameters slabPrior;
    slabPrior.parameters.mu = prior.mu; slabPrior.parameters.sigma2 = prior.sigma2; slabPrior.probability = prior.p;
    std::vector<GMM::GaussianParameters> gaussianPriors = {slabPrior};
    GMM::DeltaParameters spikePrior;
    spikePrior.parameters.mu = 0.0; spikePrior.probability = 1.0 - prior.p;
    std::vector<GMM::DeltaParameters> deltaPriors = {spikePrior};
    
    const bool randomInitialisation = false;
    
    // Make the posteriors!
    for (unsigned int m = 0; m < M_; ++m) {
        posteriors_.emplace_back(gaussianPriors, deltaPriors, randomInitialisation);
    }
    
    // Initialise expectations to unity
    if (preInitialised) {
        std::vector<arma::fmat> initialisationMatrices = *expectationInitialisation;
        expectations_.h = initialisationMatrices[0];
    } else {
        expectations_.h = arma::ones<arma::fvec>(M_);
    }    
    // And a bit of uncertainty for the variance
    expectations_.hht = 
        expectations_.h * expectations_.h.t()
            + 0.1 * arma::eye<arma::fmat>(M_, M_);
    
    // Make everything ship-shape (just sets expectations to posteriors)
    // But don't do this if it would set the weights to zero...
    if (prior.mu > 0.0) {
        updateExpectations();
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MFHModels::SpikeSlab::update(const bool bigData, const float posteriorRho, const int subjectNum)
{
    // Collect the data from all the MFModels
    const H::C2P D = collectExpectations();
    
    // Transform and pass to posteriors
    for (unsigned int m = 0; m < M_; ++m) {
            // Calculate conditionals
            VBDists::DataExpectations::Gaussian d;
            d.psi = D.psiPtPoAAt(m,m);
            
            // Need to regress out the contributions from the other weights for the mean
            expectations_.h(m) = 0.0;
            d.psi_d = D.diag_psiPtDAt(m)
                      - arma::as_scalar(expectations_.h.t() * D.psiPtPoAAt.col(m));
            
            // Rectify
            if (prior_.rectifyWeights) {
                d.psi_d = std::abs( d.psi_d );
            }
            
            // Update posterior
            posteriors_[m].update(d);
            
            // Update expectations
            expectations_.h(m) = posteriors_[m].getExpectations().x;
    }
    
    // Make everything ship-shape
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

float MFHModels::SpikeSlab::getKL(const bool bigData) const
{
    // Collect from posteriors
    float KL = 0.0;
    for (unsigned int m = 0; m < M_; ++m) {
        KL += posteriors_[m].getKL();
    }
    
    return KL;
}
////////////////////////////////////////////////////////////////////////////////

arma::fmat MFHModels::SpikeSlab::signFlip(const arma::fmat spatialSigns)
{
    expectations_.h %= arma::sign( spatialSigns.t() );
    expectations_.hht.each_row() %= arma::sign( spatialSigns );
    expectations_.hht.each_col() %= arma::sign( spatialSigns.t() );
    
    
    return spatialSigns;
}
////////////////////////////////////////////////////////////////////////////////

void MFHModels::SpikeSlab::save(const std::string directory) const
{
    // Just save cached expectations
    std::string fileName;
    
    // h
    fileName = directory + "Means.hdf5";
    expectations_.h.save(fileName, arma::hdf5_binary);
    
    // hht
    fileName = directory + "OuterProduct.hdf5";
    expectations_.hht.save(fileName, arma::hdf5_binary);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MFHModels::SpikeSlab::load(const std::string directory)
{
    // Just save cached expectations
    std::string fileName;
    
    // h
    fileName = directory + "Means.hdf5";
    expectations_.h = Utilities::loadDataMatrix(fileName);
    
    // hht
    fileName = directory + "OuterProduct.hdf5";
    expectations_.hht = Utilities::loadDataMatrix(fileName);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

MFHModels::C2P MFHModels::SpikeSlab::collectExpectations() const
{
    
    // Initialise expectations
    H::C2P D;
    D.psiPtPoAAt = arma::zeros<arma::fmat>(M_,M_);
    D.diag_psiPtDAt = arma::zeros<arma::fvec>(M_);
    
    // Parallelise over iterator
    #pragma omp parallel
    for (listType::const_iterator mfModel = childModules_.begin(); mfModel != childModules_.end(); ++mfModel) {
        #pragma omp single nowait
        {
        const H::C2P d = (*mfModel)->getExpectations();
        #pragma omp critical
        {
        D.psiPtPoAAt += d.psiPtPoAAt;
        D.diag_psiPtDAt += d.diag_psiPtDAt;
        }
	    }
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////

void MFHModels::SpikeSlab::updateExpectations()
{
    // Get references to cached expectations
    arma::fvec& h_ = expectations_.h;
    arma::fmat& hht_ = expectations_.hht;
    
    // Collect from posteriors
    arma::fvec h2 = arma::zeros<arma::fvec>(M_);
    for (unsigned int m = 0; m < M_; ++m) {
        const VBDists::Expectations::Gaussian e = posteriors_[m].getExpectations();
        
        h_(m) = e.x;
        h2(m) = e.x2;
    }
    
    // Form E[h * ht]
    hht_ = h_ * h_.t();
    hht_.diag() = h2;
    
    //**************************************************************************
    // Seems to help matrix multiplications if matrices don't have wacky values
    h_.elem( arma::find(arma::abs(h_) < 1.0e-10) ).zeros();
    hht_.elem( arma::find(arma::abs(hht_) < 1.0e-10) ).zeros();
    //**************************************************************************
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
