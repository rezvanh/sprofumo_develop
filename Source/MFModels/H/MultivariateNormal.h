// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Very simple mode weight model - just a multivariate normal with a 
// between weight covariance structure. Two potential modifications to 
// the inferred weights:
//  + Rectification (i.e. sign-flipping to enforce positivity)
//  + Clipping (i.e. forcing weights to lie within a certain range)

#ifndef MF_MODELS_H_MULTIVARIATE_NORMAL_H
#define MF_MODELS_H_MULTIVARIATE_NORMAL_H

#include <iostream>
#include <string>
#include <armadillo>
#include <memory>
#include <vector>

#include "Module.h"
#include "Posterior.h"
#include "ModuleList.h"
#include "MFModel.h"
#include "MFVariableModels.h"

#include "Utilities/DataIO.h"

namespace PROFUMO
{
    namespace MFModels
    {
        namespace H
        {
            
            class MultivariateNormal :
                public H_VBPosterior,
                protected CachedModule<H::P2C>,
                public Modules::MatrixMean_JointChild,
                public Modules::PrecisionMatrix_JointChild
            {
            public:
                //--------------------------------------------------------------
                struct Parameters {
                public:
                    unsigned int M; // Size
                    
                    bool rectifyWeights = false;
                    
                    bool clipWeights = false;
                    float clipRange_min = 0.0;
                    float clipRange_max = std::numeric_limits<float>::infinity();
                    // clipRange_min * pMu  <  mu  <  clipRange_max * pMu
                    // Note that this will rectify by default
                };
                //--------------------------------------------------------------
                
                MultivariateNormal(Modules::MatrixMean_Parent* means, Modules::PrecisionMatrix_Parent* precisionMatrix, const Parameters prior, const bool preInitialised = false, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = nullptr);
                
                void update(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                float getKL(const bool bigData = false) const;
                
                void save(const std::string directory) const;

                void load(const std::string directory);
                
                arma::fmat signFlip(const arma::fmat spatialSigns);
                
            protected:
                // Expectation gathering for hyperpriors
                Modules::MatrixMeans::C2P getMatrixMeans() const;
                Modules::PrecisionMatrices::C2P getPrecisionMatrices() const;
                
                // Matrix sizes
                const unsigned int M_; // Number of modes
                
                // Prior
                const Parameters prior_;
                
                // Posterior
                arma::fmat Sigma_;
                
                // Helper function to collect expectations from the MFModels
                H::C2P collectExpectations() const;
            };
            
        }
    }
}
#endif
