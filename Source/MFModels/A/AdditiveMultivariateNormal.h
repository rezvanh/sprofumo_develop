// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Time course model which adds noise to an existing set of time courses. It 
// acts as an intermediate between the clean time course model and the MFModel.


// To do this, it takes complete ownership of two models: a 'clean' time course 
// model and a model for the covariance of the noise time courses. This allows 
// it to be a drop-in replacement for existing time course models, as there is 
// no requirement to store two sets of time courses.

// The complete ownership model means:
//  + It can cache the A::P2C expectations as neither the clean or noisy time 
// courses can be updated in the interim.
//  + It can therefore safely incorporate their KL divergences (i.e. they can't 
// be shared hyperpriors with other models).

// However, this ownership model breaks the parent/child (i.e. non-owning) 
// model that is standard, and as such we don't allow model switching in the 
// traditional sense (we can't switch the clean TCs or noise precisions).

// It would fairly straightforward to implement a non-owning version of this 
// (e.g. a task model where there is a common set of TCs and subject-spedific 
// noise on top).

#ifndef MF_MODELS_A_ADDITIVE_MULTIVARIATE_NORMAL_H
#define MF_MODELS_A_ADDITIVE_MULTIVARIATE_NORMAL_H

#include <iostream>
#include <string>
#include <armadillo>
#include <memory>
#include <vector>

#include "Module.h"
#include "Posterior.h"
#include "ModuleList.h"
#include "MFModel.h"
#include "MFVariableModels.h"

#include "Utilities/DataIO.h"

////////////////////////////////////////////////////////////////////////////////
// Resolve ambiguous calls to multiple Module getExpectations() interfaces
// This is the same technique as all the e.g. A_JointChild classes
// http://stackoverflow.com/a/2005142

namespace PROFUMO
{
    namespace MFModels
    {
        
        class A_JointModule :
            public virtual Module<A::C2P>
        {
        public:
            A::C2P getExpectations() const;
            
        protected:
            virtual A::C2P getAData() const = 0;
        };
        
        // Implementation
        inline A::C2P A_JointModule::getExpectations() const
        {
            return getAData();
        }
    }
}

////////////////////////////////////////////////////////////////////////////////

namespace PROFUMO
{
    namespace MFModels
    {
        namespace A
        {
            
            class AdditiveMultivariateNormal :
                public A_VBPosterior,
                protected CachedModule<A::P2C>,
                public MFModels::A_JointModule,
                public virtual Module<Modules::PrecisionMatrices::C2P>,
                protected CachedModule<Modules::PrecisionMatrices::C2P>
            {
            public:
                //--------------------------------------------------------------
                struct Parameters {
                public:
                    unsigned int M, T; // Sizes
                };
                //--------------------------------------------------------------
                
                AdditiveMultivariateNormal(std::unique_ptr<MFModels::A_VBPosterior> cleanTCs, std::unique_ptr<Modules::PrecisionMatrix_VBPosterior> noisePrecMat, const Parameters prior, const bool preInitialised = false, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = nullptr);
                
                void update(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                float getKL(const bool bigData = false) const;
                
                void save(const std::string directory) const;

                void load(const std::string directory);
                
                arma::fmat signFlip(const arma::fmat spatialSigns);
                
            protected:
                // Clean time courses (for ownership)
                std::unique_ptr<MFModels::A_VBPosterior> cleanTCs_;
                
                // Noise precision matrix hyperprior (for ownership)
                std::unique_ptr<Modules::PrecisionMatrix_VBPosterior> noisePrecMat_;
                
                // Matrix sizes
                const unsigned int M_; // Number of modes
                const unsigned int T_; // Number of time points
                
                // Noise posterior
                arma::fmat Mu_;
                arma::fmat Sigma_;
                void updateNoiseTimeCourses();
                
                // Function to remove noisy time courses from data, to be passed to clean time courses
                MFModels::A::C2P getAData() const;
                
                // Make everything ship-shape
                void updateExpectations(const bool preInitialised = false, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = nullptr);
                
                // Collect data from MFModels
                MFModels::A::C2P collectMFModelData() const;
                
                // Cache for the MFModel data
                MFModels::A::C2P cachedMFModelData_;
                bool cacheIsValid_;
            };
            
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
#endif
