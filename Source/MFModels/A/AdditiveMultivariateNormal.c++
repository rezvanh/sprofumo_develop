// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "AdditiveMultivariateNormal.h"

#include <sys/stat.h>

#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;

namespace Mods = PROFUMO::Modules;
namespace MFMods = PROFUMO::MFModels;
namespace MFAModels = PROFUMO::MFModels::A;

// Resolve calls to internally cached expectations
typedef PROFUMO::CachedModule<MFAModels::P2C> ACache;
typedef PROFUMO::CachedModule<Mods::PrecisionMatrices::C2P> NoisePrecMatCache;

////////////////////////////////////////////////////////////////////////////////

MFAModels::AdditiveMultivariateNormal::AdditiveMultivariateNormal(std::unique_ptr<MFMods::A_VBPosterior> cleanTCs, std::unique_ptr<Mods::PrecisionMatrix_VBPosterior> noisePrecMat, const Parameters prior, const bool preInitialised, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation)
: cleanTCs_(std::move(cleanTCs)), noisePrecMat_(std::move(noisePrecMat)), M_(prior.M), T_(prior.T), cacheIsValid_(false)
{
    // Set graphical model links to hyperpriors
    cleanTCs_->addChildModule(this);
    noisePrecMat_->addChildModule(this);
    if (preInitialised) {
        std::vector<arma::fmat> initialisationMatrices = *expectationInitialisation;
        Sigma_ = linalg::inv_sympd(initialisationMatrices[7]);
        Mu_ = initialisationMatrices[9];
    } else {
        // Initialise the noise posterior
        Sigma_ = linalg::inv_sympd( noisePrecMat_->getExpectations().X );
        Mu_ = arma::zeros<arma::fmat>(M_,T_);
    }   
    
    // Sort out
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MFAModels::AdditiveMultivariateNormal::update(const bool bigData, const float posteriorRho, const int subjectNum)
{
    // Get data from MFModels (no need to retrieve for each of the functions below)
    cachedMFModelData_ = collectMFModelData();
    cacheIsValid_ = true;
    
    // Update clean time courses first
    cleanTCs_->update();
    
    // Then noise TCs
    updateNoiseTimeCourses();
    
    // Empty the cache
    cacheIsValid_ = false;
    cachedMFModelData_.psiPtP = arma::fmat();
    cachedMFModelData_.psiPtD = arma::fmat();
    
    // And finally noise precision matrix
    noisePrecMat_->update();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

float MFAModels::AdditiveMultivariateNormal::getKL(const bool bigData) const
{
    float KL = 0.0;
    
    // KL divergence of clean TCs and noise precision matrix
    KL += cleanTCs_->getKL();
    KL += noisePrecMat_->getKL();
    
    // KL divergence of noisy TCs
    // Get noise precision matrix hyperprior
    const Mods::PrecisionMatrices::P2C noisePrecMat = noisePrecMat_->getExpectations();
    
    // Prior and posterior determinants
    KL -= 0.5 * T_ * noisePrecMat.logDetX;
    KL -= 0.5 * T_ * std::real(arma::log_det(Sigma_));
    
    // Likelihood type terms
    KL += 0.5 * arma::trace( noisePrecMat.X * NoisePrecMatCache::expectations_.XXt );
    KL -= 0.5 * M_ * T_;
    
    return KL;
}
////////////////////////////////////////////////////////////////////////////////

arma::fmat MFAModels::AdditiveMultivariateNormal::signFlip(const arma::fmat spatialSigns)
{
    ACache::expectations_.A.each_col() %= arma::sign( spatialSigns.t() );
    ACache::expectations_.AAt.each_row() %= arma::sign( spatialSigns );
    ACache::expectations_.AAt.each_col() %= arma::sign( spatialSigns.t() );
    
    
    return spatialSigns;
}
////////////////////////////////////////////////////////////////////////////////

void MFAModels::AdditiveMultivariateNormal::save(const std::string directory) const
{
    std::string fileName;
    
    // Save combined TCs
    // A
    fileName = directory + "Means.hdf5";
    ACache::expectations_.A.save(fileName, arma::hdf5_binary);
    // AAt
    fileName = directory + "InnerProduct.hdf5";
    ACache::expectations_.AAt.save(fileName, arma::hdf5_binary);
    
    
    // Save clean TCs
    const std::string cleanDir = directory + "CleanTimeCourses/";
    mkdir( cleanDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    cleanTCs_->save(cleanDir);
    
    
    // Make noise directory
    const std::string noiseDir = directory + "Noise/";
    mkdir( noiseDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    
    // Save noise precision matrix
    const std::string noisePrecMatDir = noiseDir + "PrecisionMatrix/";
    mkdir( noisePrecMatDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    noisePrecMat_->save(noisePrecMatDir);    
    
    // Save noisy TCs
    const std::string noisyTCDir = noiseDir + "TimeCourses/";
    mkdir( noisyTCDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    // Mu
    fileName = noisyTCDir + "Means.hdf5";
    Mu_.save(fileName, arma::hdf5_binary);
    // Sigma
    fileName = noisyTCDir + "Covariance.hdf5";
    Sigma_.save(fileName, arma::hdf5_binary);
    
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MFAModels::AdditiveMultivariateNormal::load(const std::string directory)
{
    std::string fileName;
    
    // Save combined TCs
    // A
    fileName = directory + "Means.hdf5";
    arma::fmat expectations_A = Utilities::loadDataMatrix(fileName);
    ACache::expectations_.A = expectations_A;
    // AAt
    fileName = directory + "InnerProduct.hdf5";
    arma::fmat expectations_AAt = Utilities::loadDataMatrix(fileName);
    ACache::expectations_.AAt = expectations_AAt;
    
    
    // Save clean TCs
    const std::string cleanDir = directory + "CleanTimeCourses/";
    cleanTCs_->load(cleanDir);
    
    
    // Make noise directory
    const std::string noiseDir = directory + "Noise/";
    
    // Save noise precision matrix
    const std::string noisePrecMatDir = noiseDir + "PrecisionMatrix/";
    noisePrecMat_->load(noisePrecMatDir);    
    
    // Save noisy TCs
    const std::string noisyTCDir = noiseDir + "TimeCourses/";
    // Mu
    fileName = noisyTCDir + "Means.hdf5";
    Mu_ = Utilities::loadDataMatrix(fileName);
    // Sigma
    fileName = noisyTCDir + "Covariance.hdf5";
    Sigma_ = Utilities::loadDataMatrix(fileName);
    
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MFAModels::AdditiveMultivariateNormal::updateNoiseTimeCourses()
{
    // Collect the data from all the MFModels
    const A::C2P D = collectMFModelData();
    
    // Get clean time courses
    const arma::fmat cleanTCs = cleanTCs_->getExpectations().A;
    
    // And the noise precision matrix
    const arma::fmat noisePrecMat = noisePrecMat_->getExpectations().X;
    
    
    // Update!
    // Calculate covariance
    Sigma_ = linalg::inv_sympd( noisePrecMat + D.psiPtP );
    
    // Calculate mean
    Mu_ = Sigma_ * (D.psiPtD - D.psiPtP * cleanTCs);
    
    
    // And sort out
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

MFAModels::C2P MFAModels::AdditiveMultivariateNormal::getAData() const
{
    // Collect the data from all the MFModels
    A::C2P D = collectMFModelData();
    
    // Remove the contribution of the noisy time courses
    D.psiPtD -= D.psiPtP * Mu_;
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////

void MFAModels::AdditiveMultivariateNormal::updateExpectations(const bool preInitialised, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation)
{
    // Noise precision matrix
    NoisePrecMatCache::expectations_.N = T_;
    NoisePrecMatCache::expectations_.XXt = Mu_ * Mu_.t() + T_ * Sigma_;
    
    // Time courses - combine clean TCs and noise
    A::P2C& E = ACache::expectations_;
    const A::P2C cleanE = cleanTCs_->getExpectations();
    
    E.A = cleanE.A + Mu_;
    
    E.AAt = cleanE.AAt
        + cleanE.A * Mu_.t() + Mu_ * cleanE.A.t()
        + NoisePrecMatCache::expectations_.XXt;
    
    //**************************************************************************
    // Seems to help matrix multiplications if matrices don't have wacky values
    E.A.elem( arma::find(arma::abs(E.A) < 1.0e-10) ).zeros();
    E.AAt.elem( arma::find(arma::abs(E.AAt) < 1.0e-10) ).zeros();
    //**************************************************************************
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

MFAModels::C2P MFAModels::AdditiveMultivariateNormal::collectMFModelData() const
{
    // Check to see if we've stored this already
    if (cacheIsValid_) {
        return cachedMFModelData_;
    }
    
    // Collect the data from all the MFModels
    // Subject-level model so not parallelised
    A::C2P D;
    if (childModules_.empty()) {
        // All zero if no children
        D.psiPtP = arma::zeros<arma::fmat>(M_,M_);
        D.psiPtD = arma::zeros<arma::fmat>(M_,T_);
    }
    else {
        // Initialise store from first MF model
        listType::const_iterator mfModel = childModules_.begin();
        D = (*mfModel)->getExpectations();
        
        // Add other models if necessary
        for (++mfModel; mfModel != childModules_.end(); ++mfModel) {
            const A::C2P d = (*mfModel)->getExpectations();
            D.psiPtP += d.psiPtP;
            D.psiPtD += d.psiPtD;
        }
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////
