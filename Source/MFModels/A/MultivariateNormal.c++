// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "MultivariateNormal.h"
#include <fstream>
#include <sys/stat.h>
#include <armadillo>

#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;

namespace MFAModels = PROFUMO::MFModels::A;
namespace Mods = PROFUMO::Modules;

// Resolve references to parents
typedef Mods::PrecisionMatrix_Child PrecMatModel;

// Resolve calls to internally cached expectations
typedef PROFUMO::CachedModule<MFAModels::P2C> ACache;
typedef PROFUMO::CachedModule<Mods::PrecisionMatrices::C2P> PrecMatCache;

////////////////////////////////////////////////////////////////////////////////

MFAModels::MultivariateNormal::MultivariateNormal(Mods::PrecisionMatrix_Parent* precisionMatrix, const Parameters prior, const bool preInitialised, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation) //, const std::shared_ptr<arma::fmat> previousA)
: Mods::PrecisionMatrix_Child(precisionMatrix), M_(prior.M), T_(prior.T)
{
    //Get a few key expectations from the precision matrix hyperprior
    
    if (preInitialised) {
        std::vector<arma::fmat> initialisationMatrices = *expectationInitialisation;
        Sigma_ = linalg::inv_sympd(initialisationMatrices[0]);
        
        ACache::expectations_.A = initialisationMatrices[2];
        ACache::expectations_.AAt = initialisationMatrices[3];
        
        PrecMatCache::expectations_.N = T_;
        PrecMatCache::expectations_.XXt = T_ * Sigma_;
    } else {
        Sigma_ = linalg::inv_sympd( PrecMatModel::parentModule_->getExpectations().X );
    
        //Set expectations - initialise to the prior
        //if (previousA == nullptr) {
        //    ACache::expectations_.A = arma::zeros<arma::fmat>(M_,T_);
        //} else {
        //    ACache::expectations_.A = *previousA;
        //}
        ACache::expectations_.A = arma::zeros<arma::fmat>(M_,T_);
        ACache::expectations_.AAt = T_ * Sigma_;
        
        PrecMatCache::expectations_.N = T_;
        PrecMatCache::expectations_.XXt = ACache::expectations_.AAt;
    
    }
    
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MFAModels::MultivariateNormal::update(const bool bigData, const float posteriorRho, const int subjectNum)
{
    // Collect the data from all the MFModels
    const A::C2P D = collectExpectations();
    
    // And collect expectations from precision matrix
    const arma::fmat precMat = PrecMatModel::parentModule_->getExpectations().X;
    
    
    // Get reference to cached expectations
    A::P2C& E = ACache::expectations_;
    
    
    // Calculate covariance
    Sigma_ = linalg::inv_sympd( precMat + D.psiPtP );
    
    // Calculate mean and update expectations
    //Mu = Sigma_ * D.psiPtD;
    E.A = Sigma_ * D.psiPtD;
    
    E.AAt = E.A * E.A.t() + T_ * Sigma_;
    
    //**************************************************************************
    // Seems to help matrix multiplications if matrices don't have wacky values
    E.A.elem( arma::find(arma::abs(E.A) < 1.0e-10) ).zeros();
    E.AAt.elem( arma::find(arma::abs(E.AAt) < 1.0e-10) ).zeros();
    //**************************************************************************
    
    // Update precision matrix expectations
    PrecMatCache::expectations_.N = T_;
    PrecMatCache::expectations_.XXt = E.AAt;
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

float MFAModels::MultivariateNormal::getKL(const bool bigData) const
{
    // Get precision matrix hyperprior
    const Mods::PrecisionMatrices::P2C precMat = PrecMatModel::parentModule_->getExpectations();
    
    // Calculate KL divergence
    float KL = 0;
    
    // Prior and posterior determinants
    KL -= 0.5 * T_ * precMat.logDetX;
    KL -= 0.5 * T_ * std::real(arma::log_det(Sigma_));
    
    // Likelihood type terms
    KL += 0.5 * arma::trace( precMat.X *  PrecMatCache::expectations_.XXt );
    KL -= 0.5 * M_ * T_;
    
    return KL;
}
////////////////////////////////////////////////////////////////////////////////

arma::fmat MFAModels::MultivariateNormal::signFlip(const arma::fmat spatialSigns)
{
    ACache::expectations_.A.each_col() %= arma::sign( spatialSigns.t() );
    ACache::expectations_.AAt.each_row() %= arma::sign( spatialSigns );
    ACache::expectations_.AAt.each_col() %= arma::sign( spatialSigns.t() );
    
    
    return spatialSigns;
}
////////////////////////////////////////////////////////////////////////////////

void MFAModels::MultivariateNormal::save(const std::string directory) const
{
    // Just save cached expectations
    std::string fileName;
    
    // A
    fileName = directory + "Means.hdf5";
    ACache::expectations_.A.save(fileName, arma::hdf5_binary);
    
    // AAt
    fileName = directory + "InnerProduct.hdf5";
    ACache::expectations_.AAt.save(fileName, arma::hdf5_binary);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MFAModels::MultivariateNormal::load(const std::string directory)
{
    // Just save cached expectations
    std::string fileName;
    
    // A
    fileName = directory + "Means.hdf5";
    arma::fmat expectations_A = Utilities::loadDataMatrix(fileName);
    ACache::expectations_.A = expectations_A;
    
    // AAt
    fileName = directory + "InnerProduct.hdf5";
    arma::fmat expectations_AAt  = Utilities::loadDataMatrix(fileName);
    ACache::expectations_.AAt = expectations_AAt;
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

MFAModels::C2P MFAModels::MultivariateNormal::collectExpectations() const
{
    // Subject-level model so not parallelised
    A::C2P D;
    if (childModules_.empty()) {
        // All zero if no children
        D.psiPtP = arma::zeros<arma::fmat>(M_,M_);
        D.psiPtD = arma::zeros<arma::fmat>(M_,T_);
    }
    else {
        // Initialise store from first MF model
        listType::const_iterator mfModel = childModules_.begin();
        D = (*mfModel)->getExpectations();
        
        // Add other models if necessary
        for (++mfModel; mfModel != childModules_.end(); ++mfModel) {
            const A::C2P d = (*mfModel)->getExpectations();
            D.psiPtP += d.psiPtP;
            D.psiPtD += d.psiPtD;
        }
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////
