// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Very simple time course model - just a multivariate normal with a hyperprior 
// on the between mode covariance structure.

#ifndef MF_MODELS_A_MULTIVARIATE_NORMAL_H
#define MF_MODELS_A_MULTIVARIATE_NORMAL_H

#include <iostream>
#include <string>
#include <armadillo>
#include <memory>
#include <vector>

#include "Module.h"
#include "Posterior.h"
#include "ModuleList.h"
#include "MFModel.h"
#include "MFVariableModels.h"

#include "Utilities/DataIO.h"

namespace PROFUMO
{
    namespace MFModels
    {
        namespace A
        {
            
            class MultivariateNormal :
                public A_VBPosterior,
                public Modules::PrecisionMatrix_Child,
                protected CachedModule<A::P2C>,
                protected CachedModule<Modules::PrecisionMatrices::C2P>
            {
            public:
                //--------------------------------------------------------------
                struct Parameters {
                public:
                    unsigned int M, T; // Sizes
                };
                //--------------------------------------------------------------
                
                MultivariateNormal(Modules::PrecisionMatrix_Parent* precisionMatrix, const Parameters prior, const bool preInitialised = false, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = nullptr); //, const std::shared_ptr<arma::fmat> previousA = nullptr);
                
                void update(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                float getKL(const bool bigData = false) const;
                
                void save(const std::string directory) const;

                void load(const std::string directory);
                
                arma::fmat signFlip(const arma::fmat spatialSigns);
                
            protected:
                // Posterior
                arma::fmat Sigma_;
                
                // Matrix sizes
                const unsigned int M_; // Number of modes
                const unsigned int T_; // Number of time points
                
                // Helper function to collect expectations from the MFModels
                A::C2P collectExpectations() const;
            };
            
        }
    }
}
#endif
