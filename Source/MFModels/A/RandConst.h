// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// A test MFAModel - just generates a set of random time courses and keeps them 
// constant.

#ifndef MF_MODELS_A_RAND_CONST_H
#define MF_MODELS_A_RAND_CONST_H

#include <iostream>
#include <string>
#include <armadillo>
#include <memory>
#include <vector>

#include "Module.h"
#include "Posterior.h"
#include "MFModel.h"
#include "MFVariableModels.h"

#include "Utilities/DataIO.h"

namespace PROFUMO
{
    namespace MFModels
    {
        namespace A
        {
            
            class RandConst :
                public A_VBPosterior,
                protected CachedModule<A::P2C>
            {
            public:
                //--------------------------------------------------------------
                struct Parameters {
                public:
                    unsigned int M, T; // Sizes
                };
                //--------------------------------------------------------------
                
                RandConst(const Parameters prior);
                
                void update(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                float getKL(const bool bigData = false) const;
                
                void save(const std::string directory) const;

                void load(const std::string directory);
                
                arma::fmat signFlip(const arma::fmat spatialSigns);
            };
            
        }
    }
}
#endif
