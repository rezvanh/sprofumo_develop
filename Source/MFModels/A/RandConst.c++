// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "RandConst.h"

#include <armadillo>

////////////////////////////////////////////////////////////////////////////////

PROFUMO::MFModels::A::RandConst::RandConst(const Parameters prior)
{
    const arma::fmat A = arma::randn<arma::fmat>(prior.M, prior.T);
    
    expectations_.A = A;
    expectations_.AAt = A * A.t() + prior.T * arma::eye<arma::fmat>(prior.M, prior.M);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::MFModels::A::RandConst::update(const bool bigData, const float posteriorRho, const int subjectNum)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

float PROFUMO::MFModels::A::RandConst::getKL(const bool bigData) const
{
    float KL = 0.0;
    
    return KL;
}
////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::MFModels::A::RandConst::signFlip(const arma::fmat spatialSigns)
{
    expectations_.A.each_col() %= arma::sign( spatialSigns.t() );
    expectations_.AAt.each_row() %= arma::sign( spatialSigns );
    expectations_.AAt.each_col() %= arma::sign( spatialSigns.t() );
    
    
    return spatialSigns;
}
////////////////////////////////////////////////////////////////////////////////

void PROFUMO::MFModels::A::RandConst::save(std::string directory) const
{
    std::string fileName;
    
    // A
    fileName = directory + "Means.hdf5";
    expectations_.A.save(fileName, arma::hdf5_binary);
    
    // AAt
    fileName = directory + "InnerProduct.hdf5";
    expectations_.AAt.save(fileName, arma::hdf5_binary);
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

void PROFUMO::MFModels::A::RandConst::load(std::string directory)
{
    std::string fileName;
    
    // A
    fileName = directory + "Means.hdf5";
    arma::fmat expectations_A = Utilities::loadDataMatrix(fileName);
    expectations_.A = expectations_A;
    
    // AAt
    fileName = directory + "InnerProduct.hdf5";
    arma::fmat expectations_AAt = Utilities::loadDataMatrix(fileName);
    expectations_.AAt = expectations_AAt;
    
    return;
}
////////////////////////////////////////////////////////////////////////////////cons
