// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "FullRankMFModel.h"

#include <cmath>
#include <armadillo>

namespace MFMs = PROFUMO::MFModels;

// https://en.wikipedia.org/wiki/Trace_(linear_algebra)
// trace(Xt * Y) = trace(X * Yt) = trace(Yt * X) = trace(Y * Xt)
// trace(Xt * Y) = sum(X o Y) = vec(X)t * vec(Y)
// trace(A * B * C) = trace(B * C * A) = trace(C * A * B)

// diag(A * B) = sum(At o B, dim=0) = sum(A o Bt, axis=1)

////////////////////////////////////////////////////////////////////////////////

MFMs::FullRankMFModel::FullRankMFModel(const DataTypes::FullRankData data, MFPModel* P, MFHModel* H, MFAModel* A, MFPsiModel* Psi, const float dofCorrectionFactor)
: MFModel(P,H,A,Psi,dofCorrectionFactor), data_(data), TrDtD_(arma::accu((*data_.D) % (*data_.D)))
{
    // Cache Tr(DtD)
    //TrDtD_ = arma::accu( (*data_.D) % (*data_.D) );
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

MFMs::P::C2P MFMs::FullRankMFModel::getPData() const
{    
    // Container for output
    MFMs::P::C2P PD;
    
    // Get the expectations we need
    const MFMs::H::P2C EH = H_->getExpectations();
    const MFMs::A::P2C EA = A_->getExpectations();
    const MFMs::Psi::P2C Epsi = Psi_->getExpectations();
    
    // psiAAt
    PD.psiAAt = Epsi.psi * (EH.hht % EA.AAt);
    
    // psiDAt
    PD.psiDAt = (*data_.D) * EA.A.t();
    PD.psiDAt.each_row() %= Epsi.psi * EH.h.t();
    
    return PD;
}

////////////////////////////////////////////////////////////////////////////////

MFMs::H::C2P MFMs::FullRankMFModel::getHData() const
{    
    // Container for output
    MFMs::H::C2P HD;
    
    // Get the expectations we need
    const MFMs::P::P2C EP = P_->getExpectations();
    const MFMs::A::P2C EA = A_->getExpectations();
    const MFMs::Psi::P2C Epsi = Psi_->getExpectations();
    const float& dof = dofCorrectionFactor_;
    
    // Covariance
    HD.psiPtPoAAt = (dof * Epsi.psi) * (EP.PtP % EA.AAt);
    
    // Mean
    // diag(PtDAt) - check sizes
    if (EP.P.n_elem > EA.A.n_elem) {
        
        // diag(PtDAt) = sum((Pt * D) o A)
        const int axis = 1;
        HD.diag_psiPtDAt = (dof * Epsi.psi) * arma::sum( EA.A % (EP.P.t() * (*data_.D)), axis);
        
    }
    else {
        
        // diag(PtDAt) = sum(P o (D * At))
        const int axis = 0;
        HD.diag_psiPtDAt = (dof * Epsi.psi) * arma::sum( EP.P % ((*data_.D) * EA.A.t()), axis ).t();
        
    }
    
    return HD;
}

////////////////////////////////////////////////////////////////////////////////

MFMs::A::C2P MFMs::FullRankMFModel::getAData() const
{    
    // Container for output
    MFMs::A::C2P AD;
    
    // Get the expectations we need
    const MFMs::P::P2C EP = P_->getExpectations();
    const MFMs::H::P2C EH = H_->getExpectations();
    const MFMs::Psi::P2C Epsi = Psi_->getExpectations();
    const float& dof = dofCorrectionFactor_;
    
    // psiPtP
    AD.psiPtP = (dof * Epsi.psi) * (EP.PtP % EH.hht);
    
    // psiPtD
    AD.psiPtD = EP.P.t() * (*data_.D);
    AD.psiPtD.each_col() %= (dof * Epsi.psi) * EH.h;
    
    if (normaliseTimeCourses_) {
        normaliseTimeCourseExpectations(AD);
    }
    
    return AD;
}

////////////////////////////////////////////////////////////////////////////////

MFMs::Psi::C2P MFMs::FullRankMFModel::getPsiData() const
{    
    // Container for output
    MFMs::Psi::C2P psiD;
    
    // Get the expectations we need
    const MFMs::P::P2C EP = P_->getExpectations();
    const MFMs::H::P2C EH = H_->getExpectations();
    const MFMs::A::P2C EA = A_->getExpectations();
    const float& dof = dofCorrectionFactor_;
    
    // N
    psiD.N = dof * (data_.V * data_.T);
    
    // Cache some results
    // Tr(DtPHA) - check sizes
    float TrDtPHA;
    if (EP.P.n_elem > EA.A.n_elem) {
        
        // Tr(DtPA) = Tr(AtPtD) = sum(A o (Pt * D))
        const int axis = 1;
        TrDtPHA = arma::sum( EH.h % arma::sum(EA.A % (EP.P.t() * (*data_.D)), axis) );
        
    }
    else {
        
        // Tr(DtPA) = Tr(PADt) = Tr(PtDAt) = sum(P o (D * At))
        const int axis = 0;
        TrDtPHA = arma::sum( EH.h % arma::sum(EP.P % ((*data_.D) * EA.A.t()), axis).t() );
        
    }
    
    // Tr(HtPtPHAAt) = sum(PtP.hht.AAt)
    float TrHtPtPHAAt = arma::accu( EP.PtP % EH.hht % EA.AAt );
    
    // Combine to TrDtD
    psiD.TrDtD = dof * (TrDtD_ - 2.0 * TrDtPHA + TrHtPtPHAAt);
    
    return psiD;
}

////////////////////////////////////////////////////////////////////////////////

float MFMs::FullRankMFModel::getLogLikelihood() const
{
    float L = 0.0;
    
    // Get the expectations we need
    const MFMs::Psi::P2C Epsi = Psi_->getExpectations();
    const float& dof = dofCorrectionFactor_;
    
    // log(2pi)
    L -= dof * (data_.V * data_.T) * std::log(2.0 * M_PI) / 2.0;
    
    // logdet(psi)
    L += dof * (data_.V * data_.T) * Epsi.logPsi / 2.0;
    
    // Data terms
    // DOF already accounted for by getPsiData()
    L -= Epsi.psi * getPsiData().TrDtD / 2.0;
    
    return L;
}

////////////////////////////////////////////////////////////////////////////////
