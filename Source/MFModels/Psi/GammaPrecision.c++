// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2014
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include <memory>
#include <string>
#include <armadillo>
#include <iostream>
#include <vector>
#include <fstream>
#include <iostream>
#include <sys/types.h>
#include <dirent.h>

#include "GammaPrecision.h"

#include <fstream>

namespace VBDists = PROFUMO::VBDistributions;
namespace MFPsiModels = PROFUMO::MFModels::Psi;

////////////////////////////////////////////////////////////////////////////////

MFPsiModels::GammaPrecision::GammaPrecision(const Parameters prior, const bool preInitialised, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation)
: posterior_(prior, preInitialised, expectationInitialisation)
{
    
    //if (preInitialised) {
    //    std::vector<arma::fmat> initialisationMatrices = *expectationInitialisation;
    //    Parameters initialisedPriors;
    //    initialisedPriors.a = initialisationMatrices[0].at(0);
    //    initialisedPriors.b = initialisationMatrices[0].at(1);
    //    auto posteriorPointer = std::make_shared<VBDists::Gamma>(initialisedPriors);
    //    posterior_ = *posteriorPointer;
        
    //} else {
    //    //posterior_ = prior;
    //    std::shared_ptr<VBDists::Gamma> posteriorPointer = std::make_shared<VBDists::Gamma>(prior);
    //    //posterior_(prior);
    //    posterior_ = *posteriorPointer;
    //}

    return;
}

////////////////////////////////////////////////////////////////////////////////

void MFPsiModels::GammaPrecision::update(const bool bigData, const float posteriorRho, const int subjectNum)
{
    // Collect the data from all the MFModels
    const Psi::C2P D = collectExpectations();
    
    // Pass stats to actual posterior
    VBDists::DataExpectations::Gamma d;
    d.n = D.N;
    d.d2 = D.TrDtD;
    posterior_.update(d);
    
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

float MFPsiModels::GammaPrecision::getKL(const bool bigData) const
{
    return posterior_.getKL();
}

////////////////////////////////////////////////////////////////////////////////

arma::fmat MFPsiModels::GammaPrecision::signFlip(const arma::fmat spatialSigns)
{
    
    return spatialSigns;
}

////////////////////////////////////////////////////////////////////////////////

void MFPsiModels::GammaPrecision::save(const std::string directory) const
{
    posterior_.save(directory + "GammaPosterior.txt");
    //arma::fvec gvect={posterior_.Parameters.a, posterior_.Parameters.b};
    //const std::string runGammaDir = directory + "GammaPosterior.hdf5"; 
              
    //gvect.save(runGammaDir, arma::hdf5_binary);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MFPsiModels::GammaPrecision::load(const std::string directory)
{
    posterior_.load(directory + "GammaPosterior.txt");
    //arma::fvec gvect={posterior_.Parameters.a, posterior_.Parameters.b};
    //const std::string runGammaDir = directory + "GammaPosterior.hdf5"; 
              
    //gvect.save(runGammaDir, arma::hdf5_binary);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

MFPsiModels::P2C MFPsiModels::GammaPrecision::getExpectations() const
{
    // Get expectations from posterior
    const VBDists::Expectations::Gamma e = posterior_.getExpectations();
    
    // And transform to required form
    Psi::P2C E;
    E.psi = e.x;
    E.logPsi = e.log_x;

    return E;
}

////////////////////////////////////////////////////////////////////////////////

MFPsiModels::C2P MFPsiModels::GammaPrecision::collectExpectations() const
{
    // Subject-level model so not parallelised
    Psi::C2P D;
    if (childModules_.empty()) {
        // All zero if no children
        D.TrDtD = 0.0;
        D.N = 0.0;
    }
    else {
        // Initialise store from first MF model
        listType::const_iterator mfModel = childModules_.begin();
        D = (*mfModel)->getExpectations();
        
        // Add other models if necessary
        for (++mfModel; mfModel != childModules_.end(); ++mfModel) {
            const Psi::C2P d = (*mfModel)->getExpectations();
            D.TrDtD += d.TrDtD;
            D.N += d.N;
        }
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////
