// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2014
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// A noise model for an MF model - in this case a single gamma precision. As
// such just a wrapper for the Gamma VBDistribution, that deals with collecting
// the data expectations from multiple MFModels.

#ifndef MF_MODELS_PSI_GAMMA_PRECISION_H
#define MF_MODELS_PSI_GAMMA_PRECISION_H

#include <string>
#include <armadillo>
#include <memory>
#include <iostream>
#include <vector>


#include "Module.h"
#include "Posterior.h"
#include "MFModel.h"
#include "MFVariableModels.h"
#include "VBDistributions/Gamma.h"

namespace PROFUMO
{
    namespace MFModels
    {
        namespace Psi
        {
            
            class GammaPrecision :
                public Psi_VBPosterior
            {
            public:
                //--------------------------------------------------------------
                typedef VBDistributions::Gamma::Parameters Parameters;
                //--------------------------------------------------------------
                
                GammaPrecision(const Parameters prior, const bool preInitialised = false, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = nullptr);
                
                void update(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                Psi::P2C getExpectations() const;
                
                void save(const std::string directory) const;

                void load(const std::string directory);
                
                float getKL(const bool bigData = false) const;
                
                arma::fmat signFlip(const arma::fmat spatialSigns);
                
            private:
                VBDistributions::Gamma posterior_;
                
                // Helper function to collect expectations from the MFModels
                Psi::C2P collectExpectations() const;
            };
            
        }
    }
}
#endif
