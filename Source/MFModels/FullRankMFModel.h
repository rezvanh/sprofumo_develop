// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Implementation of an MFModel that works with the full data matrix.

#ifndef MF_MODELS_FULL_RANK_MF_MODEL_H
#define MF_MODELS_FULL_RANK_MF_MODEL_H

#include "DataTypes.h"
#include "Module.h"
#include "MFModel.h"
#include "MFVariableModels.h"

namespace PROFUMO
{
    namespace MFModels
    {
        
        class FullRankMFModel : 
            public MFModel
        {
        public:
            // Constructor
            FullRankMFModel(const DataTypes::FullRankData data, MFPModel* P, MFHModel* H, MFAModel* A, MFPsiModel* Psi, const float dofCorrectionFactor=1.0);
            
            // Returns likelihood of data w.r.t. posterior
            float getLogLikelihood() const;
            
        protected:
            // Get the expectations needed for updates
            MFModels::P::C2P getPData() const;
            MFModels::H::C2P getHData() const;
            MFModels::A::C2P getAData() const;
            MFModels::Psi::C2P getPsiData() const;
            
            // Data
            const DataTypes::FullRankData data_;
            const float TrDtD_;
        };
        
    }
}
#endif
