// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// A weighted parcellation, whereby each voxel can only contain a contribution
// from one mode but the weight in the voxel can vary. See the Documentation 
// folder for more details.

#ifndef MF_MODELS_P_PARCELLATION_H
#define MF_MODELS_P_PARCELLATION_H

#include <string>
#include <armadillo>
#include <memory>
#include <vector>

#include "Module.h"
#include "Posterior.h"
#include "MFModel.h"
#include "MFVariableModels.h"
#include "ModuleList.h"

#include "Utilities/DataIO.h"

namespace PROFUMO
{
    namespace MFModels
    {
        namespace P
        {   
            ////////////////////////////////////////////////////////////////////
            
            class Parcellation :
                public P_VBPosterior,
                protected CachedModule<P::P2C>,
                public Modules::MatrixMean_JointChild,
                public Modules::MatrixPrecision_JointChild,
                public Modules::MembershipProbability_JointChild
            {
            public:
                //--------------------------------------------------------------
                struct Parameters {
                public:
                    unsigned int V, M; // Sizes
                };
                //--------------------------------------------------------------
                
                Parcellation(Modules::MatrixMean_Parent* tau, Modules::MatrixPrecision_Parent* beta, Modules::MembershipProbability_Parent* pi, const Parameters prior, const float dofCorrectionFactor=1.0, const bool preInitialised = false, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = nullptr);
                
                void update(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                float getKL(const bool bigData = false) const;
                
                void save(const std::string directory) const;

                void load(const std::string directory);
                
                arma::fmat signFlip(const arma::fmat spatialMeans);
                
            private:
                // Expectation gathering for hyperpriors
                Modules::MatrixMeans::C2P getMatrixMeans() const;
                Modules::MatrixPrecisions::C2P getMatrixPrecisions() const;
                Modules::MembershipProbabilities::C2P getMembershipProbabilities() const;
                
                // Helper functions to collect expectations / set caches
                P::C2P collectExpectations() const;
                void updateExpectations();
                
                // Sizes
                const unsigned int V_; // Number of voxels
                const unsigned int M_; // Number of modes
                
                // Posterior params
                arma::fmat Q_;
                arma::fmat Mu_;
                arma::fmat Sigma2_;
        
                // Spatial DOF correction
                const float dofCorrectionFactor_;
            };
            
            ////////////////////////////////////////////////////////////////////
        }
    }
}
#endif
