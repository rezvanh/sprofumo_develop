// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Set of spatial maps where each voxel is independently distributed, following
// a double-Gaussian mixture model. The signal component is explicitly 
// parameterised in terms of a set of group-level means and variances, while 
// the noise/null component is zero mean with a set of (lower) group-level 
// variances (one per voxel, so pooled over modes). The respective  membership 
// probabilities are inferred at the group-level too.

#ifndef MF_MODELS_P_DGMM_H
#define MF_MODELS_P_DGMM_H

#include <iostream>
#include <string>
#include <armadillo>
#include <memory>
#include <vector>

#include "Module.h"
#include "Posterior.h"
#include "MFModel.h"
#include "MFVariableModels.h"
#include "ModuleList.h"

#include "Utilities/DataIO.h"

namespace PROFUMO
{
    namespace MFModels
    {
        namespace P
        {
            ////////////////////////////////////////////////////////////////////
            
            class DGMM :
                public P_VBPosterior,
                protected CachedModule<P::P2C>,
                public Modules::MembershipProbability_JointChild
            {
            public:
                //--------------------------------------------------------------
                struct Parameters {
                public:
                    unsigned int V, M; // Sizes
                    float hMu = 1.0, hSigma2 = 0.25;
                };
                //--------------------------------------------------------------
                
                DGMM(Modules::MatrixMean_Parent* signalMeans, Modules::MatrixPrecision_Parent* signalPrecisions, Modules::MatrixPrecision_Parent* noisePrecisions, Modules::MembershipProbability_Parent* memberships, const Parameters prior, const float dofCorrectionFactor=1.0, const bool preInitialised = false, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = nullptr);
                
                void update(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                float getKL(const bool bigData = false) const;
                
                void save(const std::string directory) const;

                void load(const std::string directory);
                
                arma::fmat signFlip(const arma::fmat spatialMeans);
                
            private:
                // Classes for dealing with the various priors
                // These simply pass through the expectations
                class SignalPriors; friend class SignalPriors;
                std::shared_ptr<SignalPriors> signalPriors_;
                class NoisePriors; friend class NoisePriors;
                std::shared_ptr<NoisePriors> noisePriors_;
                
                // Expectation gathering for hyperpriors
                Modules::MembershipProbabilities::C2P getMembershipProbabilities() const;
                Modules::MatrixMeans::C2P getSignalMeans() const;
                Modules::MatrixPrecisions::C2P getSignalPrecisions() const;
                Modules::MatrixPrecisions::C2P getNoisePrecisions() const;
                
                // Helper functions to collect expectations / set caches
                P::C2P collectExpectations() const;
                void updateExpectations();
                
                // Sizes
                const unsigned int V_; // Number of voxels
                const unsigned int M_; // Number of modes
                
                // Prior
                const Parameters prior_;
                
                // Posterior params
                arma::fmat Q_;
                // Signal
                arma::fmat sMu_;
                arma::fmat sSigma2_;
                // Noise
                arma::fmat nMu_;
                arma::fmat nSigma2_;  // COULD REPRESENT THIS AS THE OUTER PRODUCT OF TWO VECTORS TO SAVE SPACE
                // Noise scalings
                arma::frowvec hMu_;
                arma::frowvec hSigma2_;
                std::vector<bool> hValid_;
                
                
                // Spatial DOF correction
                const float dofCorrectionFactor_;
                
                unsigned int nUpdates_ = 0;
            };
            
            ////////////////////////////////////////////////////////////////////
            
            class DGMM::SignalPriors :
                public Modules::MatrixMean_JointChild,
                public Modules::MatrixPrecision_JointChild
            {
                public:
                    SignalPriors(DGMM* dgmm, Modules::MatrixMean_Parent* signalMeans, Modules::MatrixPrecision_Parent* signalPrecisions)
                    : MatrixMean_JointChild(signalMeans), MatrixPrecision_JointChild(signalPrecisions), dgmm_(dgmm)
                    {return;}
                    
                    Modules::MatrixMeans::P2C getPriorMeans()
                    {return Modules::MatrixMean_JointChild::parentModule_->getExpectations();}
                    
                    Modules::MatrixPrecisions::P2C getPriorPrecisions()
                    {return Modules::MatrixPrecision_JointChild::parentModule_->getExpectations();}
                    
                protected:
                    const DGMM* const dgmm_;
                    
                    Modules::MatrixMeans::C2P getMatrixMeans() const
                    {return dgmm_->getSignalMeans();}
                    
                    Modules::MatrixPrecisions::C2P getMatrixPrecisions() const
                    {return dgmm_->getSignalPrecisions();}
            };
            
            ////////////////////////////////////////////////////////////////////
            
            class DGMM::NoisePriors :
                public Modules::MatrixPrecision_JointChild
            {
                public:
                    NoisePriors(DGMM* dgmm, Modules::MatrixPrecision_Parent* noisePrecisions)
                    : MatrixPrecision_JointChild(noisePrecisions), dgmm_(dgmm)
                    {return;}
                    
                    Modules::MatrixPrecisions::P2C getPriorPrecisions()
                    {return Modules::MatrixPrecision_JointChild::parentModule_->getExpectations();}
                    
                protected:
                    const DGMM* const dgmm_;
                    
                    Modules::MatrixPrecisions::C2P getMatrixPrecisions() const
                    {return dgmm_->getNoisePrecisions();}
            };
            
            ////////////////////////////////////////////////////////////////////
            
        }
    }
}
#endif

// Essentially, this mixture model reaches the limit of what we can do with 
// multiple inheritance. We can't inherit from two parents to get the 
// precisions for the signal and null, but we can't do the VBDistributions 
// trick of defining mixture components because the hyperpriors need to 
// be passed membership information, and the DGMM class can't be a mixture 
// membership parent to both signal and null simultaneously either.

// Therefore, we add another level of obscurity in the class hierarchy via the 
// Signal/NoisePriors. These simply pass the expectations transparently, but 
// we have to define them as friends so they can get the necessary expectations 
// from the DGMM itself when the hyperpriors request them as part of their 
// updates.
