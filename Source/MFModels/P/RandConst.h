// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// A test MFPModel - just generates a set of random spatial maps and keeps them 
// constant.

#ifndef MF_MODELS_P_RAND_CONST_H
#define MF_MODELS_P_RAND_CONST_H

#include <string>

#include "Module.h"
#include "Posterior.h"
#include "MFModel.h"
#include "MFVariableModels.h"

#include "Utilities/DataIO.h"

namespace PROFUMO
{
    namespace MFModels
    {
        namespace P
        {
            
            class RandConst :
                public P_VBPosterior,
                protected CachedModule<P::P2C>
            {
            public:
                //--------------------------------------------------------------
                struct Parameters {
                public:
                    unsigned int V, M; // Sizes
                };
                //--------------------------------------------------------------
                
                RandConst(const Parameters prior, const float dofCorrectionFactor=1.0);
                
                void update(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                float getKL(const bool bigData = false) const;
                
                void save(const std::string directory) const;

                void load(const std::string directory);
                
                arma::fmat signFlip(const arma::fmat spatialMeans);
            };
            
        }
    }
}
#endif
