// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "DGMM.h"

#include <sys/stat.h>
#include <cmath>

#include "Utilities/MiscMaths.h"
namespace miscmaths = PROFUMO::Utilities::MiscMaths;
#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;

namespace MFPModels = PROFUMO::MFModels::P;
namespace Mods = PROFUMO::Modules;
namespace MatMeans = Mods::MatrixMeans;
namespace MatPrecs = Mods::MatrixPrecisions;
namespace MemProbs = Mods::MembershipProbabilities;

// Resolve references to parents
typedef Mods::MatrixMean_Child MeanModel;
typedef Mods::MatrixPrecision_Child PrecModel;
typedef Mods::MembershipProbability_Child ProbModel;

////////////////////////////////////////////////////////////////////////////////

MFPModels::DGMM::DGMM(Mods::MatrixMean_Parent* signalMeans, Mods::MatrixPrecision_Parent* signalPrecisions, Mods::MatrixPrecision_Parent* noisePrecisions, Mods::MembershipProbability_Parent* memberships, const Parameters prior, const float dofCorrectionFactor, const bool preInitialised, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation)
: MembershipProbability_JointChild(memberships),
  signalPriors_(std::make_unique<SignalPriors>(this, signalMeans, signalPrecisions)),
  noisePriors_(std::make_unique<NoisePriors>(this, noisePrecisions)),
  V_(prior.V), M_(prior.M), prior_(prior), dofCorrectionFactor_(dofCorrectionFactor)
{
    // Initialise from parents
     
    if (preInitialised) {
        std::vector<arma::fmat> initialisationMatrices = *expectationInitialisation;
        sMu_ = initialisationMatrices[0];
        sSigma2_ = initialisationMatrices[1];
        Q_ = initialisationMatrices[2];
        
        nMu_ = initialisationMatrices[3];
        nSigma2_ = initialisationMatrices[4];
        hMu_ = initialisationMatrices[5];
        hSigma2_ = initialisationMatrices[6];
        nUpdates_=6;
    
    } else {
        sMu_ = signalPriors_->getPriorMeans().X;
        sSigma2_ = 1.0 / signalPriors_->getPriorPrecisions().X;
        nMu_ = arma::zeros<arma::fmat>(V_, M_);
        nSigma2_ = arma::repmat(1.0 / noisePriors_->getPriorPrecisions().X, 1, M_);
        Q_ = ProbModel::parentModule_->getExpectations().p[1];
        // And scalings from prior
        hMu_ = prior_.hMu * arma::ones<arma::frowvec>(M_);
        hSigma2_ = prior_.hSigma2 * arma::ones<arma::frowvec>(M_);
    
    }
    
    hValid_ = std::vector<bool>(M_, true);
    
    // Make the cached expectations ship-shape
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MFPModels::DGMM::update(const bool bigData, const float posteriorRho, const int subjectNum)
{
    // Collect the data from all the MFModels
    const P::C2P D = collectExpectations();
    // Get hyperpriors
    const MatMeans::P2C sMeans = signalPriors_->getPriorMeans();
    const MatPrecs::P2C sPrecisions = signalPriors_->getPriorPrecisions();
    const MatPrecs::P2C nPrecisions = noisePriors_->getPriorPrecisions();
    const MemProbs::P2C memberships = ProbModel::parentModule_->getExpectations();
    // And a reference to the cached mean
    arma::fmat& P_ = expectations_.P;
    
    // Calculate the maximum likelihood version of the maps
    arma::fmat P_ML;
    if (nUpdates_ < 5) {
        P_ML = D.psiDAt * linalg::inv(D.psiAAt);
    }
    
    // Updates!
    
    // Random order to update maps (Bentleian dynamics)
    const arma::uvec mapInds = arma::sort_index( arma::randu<arma::fvec>(M_) );
    
    // Now loop over maps, updating each in turn
    for (unsigned int m : mapInds) {
        
        ////////////////////////////////////////////////////////////////////////
        // Some useful results
        
        // Data mean, taking into account the other maps
        // Vector of mixings the time courses apply to the other maps
        arma::fvec psiAAt_m = D.psiAAt.col(m); psiAAt_m(m) = 0.0;
        // And use this to remove the contribution from the other maps from the data
        //const arma::fvec psiD = D.psiDAt.col(m) - P_ * psiAAt_m;
        //const arma::fvec psiD = D.psiDAt.col(m) - P_ML * psiAAt_m;
        arma::fvec psiD;
        if (nUpdates_ < 5) {
            psiD = D.psiDAt.col(m) - P_ML * psiAAt_m;
        }
        else {
            psiD = D.psiDAt.col(m) - P_ * psiAAt_m;
        }
        
        // Scalings
        const float nScaling = hMu_(m);
        const float nScaling2 = miscmaths::square(hMu_(m)) + hSigma2_(m);
        
        ////////////////////////////////////////////////////////////////////////
        // Posterior covariances
        
        sSigma2_.col(m) = 1.0 / (sPrecisions.X.col(m) + D.psiAAt(m,m));
        nSigma2_.col(m) = 1.0 / (nPrecisions.X + nScaling2 * D.psiAAt(m,m));
        
        // These can actually go outside the loop, but that doesn't speed 
        // things up, so makes sense to leave the updates together for now
        
        ////////////////////////////////////////////////////////////////////////
        // Posterior means
        
        sMu_.col(m) = sSigma2_.col(m) % (psiD + sPrecisions.X.col(m) % sMeans.X.col(m));
        nMu_.col(m) = nSigma2_.col(m) % (nScaling * psiD);
                
        ////////////////////////////////////////////////////////////////////////
        // Posterior memberships
        
        Q_.col(m) = memberships.logP[1].col(m) - memberships.logP[0].col(m)
                    // Signal
                    + 0.5 * sPrecisions.logX.col(m) + 0.5 * arma::log(sSigma2_.col(m))
                    + 0.5 * arma::square(sMu_.col(m)) / sSigma2_.col(m)
                    - 0.5 * sPrecisions.X.col(m) % sMeans.X2.col(m)
                    // Noise
                    - 0.5 * nPrecisions.logX - 0.5 * arma::log(nSigma2_.col(m))
                    - 0.5 * arma::square(nMu_.col(m)) / nSigma2_.col(m);
        
        // Turn into proper probability via the sigmoid function
        Q_.col(m).transform( [](float q) { return miscmaths::sigmoid(q); } );
                
        ////////////////////////////////////////////////////////////////////////
        // Noise scalings
        
        hSigma2_(m) = 1.0 / ((1.0 / prior_.hSigma2) + arma::sum((1.0 - Q_.col(m)) % (arma::square(nMu_.col(m)) + nSigma2_.col(m))) * D.psiAAt(m,m));
        hMu_(m) = hSigma2_(m) * arma::sum((1.0 - Q_.col(m)) % nMu_.col(m) % psiD);
                
        // Make sure the noise does not become bigger than the signal...
        //const float noise = arma::sum((1.0 - Q_.col(m)) % (arma::square(nMu_.col(m)) + nSigma2_.col(m))) / arma::sum(1.0 - Q_.col(m));
        //const float signal = arma::sum(Q_.col(m) % (arma::square(sMu_.col(m)) + sSigma2_.col(m))) / arma::sum(Q_.col(m));
        //const float scale = miscmaths::square(hMu_(m)) + hSigma2_(m);
        //const float threshold = miscmaths::square(0.25); // Threshold on relative size of scaled noise to signal
        //if ((scale * noise) / signal > threshold) {
        //    //hMu_(m) = std::sqrt(threshold * signal / noise);
        //    hValid_[m] = true;
        //    // Sanity check that things haven't gone wrong e.g. if noise = 0.0
        //    //if (!arma::is_finite(hMu_(m))) {
        //    //    hMu_(m) = 0.0;
        //    //}
        //}
        //else {
        //    hValid_[m] = true;
        //}
        hValid_[m] = true;
        
        ////////////////////////////////////////////////////////////////////////
        // Update the posterior expectations for just this map
        arma::fvec p = Q_.col(m) % sMu_.col(m)
                       + (1.0 - Q_.col(m)) % (hMu_(m) * nMu_.col(m));
        //**********************************************************************
        // Seems to help matrix multiplications if matrices don't have wacky values
        p.elem( arma::find(arma::abs(p) < 1.0e-10) ).zeros();
        //**********************************************************************
        P_.col(m) = p;
        
        ////////////////////////////////////////////////////////////////////////
        
    }
    
    // And update remaining expectations
    updateExpectations();
    nUpdates_++;
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

float MFPModels::DGMM::getKL(const bool bigData) const
{
    //if (bigData){
    //    const P::C2P D = collectExpectations();
    //}
    // Get hyperpriors
    const MatMeans::P2C sMeans = signalPriors_->getPriorMeans();
    const MatPrecs::P2C sPrecisions = signalPriors_->getPriorPrecisions();
    const MatPrecs::P2C nPrecisions = noisePriors_->getPriorPrecisions();
    const MemProbs::P2C memberships = ProbModel::parentModule_->getExpectations();
    
    
    // KL from signal
    arma::fmat S_KL = - 0.5 * sPrecisions.logX - 0.5 * arma::log(sSigma2_);
    S_KL += 0.5 * ( (arma::square(sMu_) + sSigma2_) - (2.0 * sMu_ % sMeans.X) + sMeans.X2 ) % sPrecisions.X;
    S_KL += - 0.5;
    
    // KL from noise
    arma::fmat N_KL = - 0.5 * arma::repmat(nPrecisions.logX,1,M_) - 0.5 * arma::log(nSigma2_);
    N_KL += 0.5 * ( (arma::square(nMu_) + nSigma2_) ) % arma::repmat(nPrecisions.X,1,M_);
    N_KL += - 0.5;
    
    // KL from noise scalings
    arma::frowvec H_KL = 0.5 * std::log(prior_.hSigma2) - 0.5 * arma::log(hSigma2_);
    H_KL += 0.5 * ( (arma::square(hMu_) + hSigma2_) ) / prior_.hSigma2;
    H_KL += - 0.5;
    
    // KL of memberships
    arma::fmat Q_KL = Q_ % ( arma::log(Q_) - memberships.logP[1] );
    Q_KL += (1.0 - Q_) % ( arma::log(1.0 - Q_) - memberships.logP[0] );
    // Adjust for any NaNs caused by log(0)
    Q_KL.elem( arma::find(Q_ == 0.0) ) = - memberships.logP[0].elem( arma::find(Q_ == 0.0) );
    Q_KL.elem( arma::find(Q_ == 1.0) ) = - memberships.logP[1].elem( arma::find(Q_ == 1.0) );
    
    // KL from q, plus weighted contribution from slab
    float KL = arma::accu( Q_KL + Q_ % S_KL + (1.0 - Q_) % N_KL) + arma::accu(H_KL);
    
    // Correct for voxels not being independent
    KL = dofCorrectionFactor_ * KL;
    
    return KL;
}
////////////////////////////////////////////////////////////////////////////////

arma::fmat MFPModels::DGMM::signFlip(const arma::fmat spatialMeans)
{
    arma::frowvec subjectGroupCorr = arma::zeros<arma::frowvec>(spatialMeans.n_cols);
    for (unsigned int cnt1=0; cnt1<spatialMeans.n_cols; ++cnt1) {
        arma::fmat thisCorr = arma::cor(spatialMeans.col(cnt1), expectations_.P.col(cnt1));
        subjectGroupCorr.at(cnt1)=thisCorr.at(0);
    }
    arma::fmat spatialSigns = arma::sign(subjectGroupCorr);
    expectations_.P.each_row() %= arma::sign( spatialSigns );
    expectations_.PtP.each_row() %= arma::sign( spatialSigns );
    expectations_.PtP.each_col() %= arma::sign( spatialSigns.t() );
    
    return spatialSigns;
}

////////////////////////////////////////////////////////////////////////////////

void MFPModels::DGMM::save(const std::string directory) const
{
    // Just save posterior params
    std::string fileName;
    
    // Signal component
    const std::string signalDir = directory + "Signal" + "/";
    mkdir( signalDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    
    // Mu
    fileName = signalDir + "Means.hdf5";
    sMu_.save(fileName, arma::hdf5_binary);
    
    // Sigma2
    fileName = signalDir + "Variances.hdf5";
    sSigma2_.save(fileName, arma::hdf5_binary);
    
    // Q
    fileName = signalDir + "MembershipProbabilities.hdf5";
    Q_.save(fileName, arma::hdf5_binary);
    
    
    // Noise component
    const std::string noiseDir = directory + "Noise" + "/";
    mkdir( noiseDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    
    // Mu
    fileName = noiseDir + "Means.hdf5";
    nMu_.save(fileName, arma::hdf5_binary);
    
    // Sigma2
    fileName = noiseDir + "Variances.hdf5";
    nSigma2_.save(fileName, arma::hdf5_binary);
    
    // Q
    const arma::fmat QNull = 1.0 - Q_;
    fileName = noiseDir + "MembershipProbabilities.hdf5";
    QNull.save(fileName, arma::hdf5_binary);
    
    // Scalings: mu
    fileName = noiseDir + "Scalings.hdf5";
    hMu_.save(fileName, arma::hdf5_binary);
    
    // Scalings: sigma2
    fileName = noiseDir + "ScalingVariances.hdf5";
    hSigma2_.save(fileName, arma::hdf5_binary);
    
    // Scalings: valid
    arma::frowvec valid = arma::ones<arma::frowvec>(M_);
    for (unsigned int m = 0; m < M_; ++m) {
        valid(m) = hValid_[m];
    }
    fileName = noiseDir + "ScalingsValid.hdf5";
    valid.save(fileName, arma::hdf5_binary);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MFPModels::DGMM::load(const std::string directory)
{
    // Load posterior params
    std::string fileName;
    
    // Signal component
    const std::string signalDir = directory + "Signal" + "/";
    mkdir( signalDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    
    // Mu
    fileName = signalDir + "Means.hdf5";
    sMu_ = Utilities::loadDataMatrix(fileName);
    
    // Sigma2
    fileName = signalDir + "Variances.hdf5";
    sSigma2_ = Utilities::loadDataMatrix(fileName);
    
    // Q
    fileName = signalDir + "MembershipProbabilities.hdf5";
    Q_ = Utilities::loadDataMatrix(fileName);
    
    
    // Noise component
    const std::string noiseDir = directory + "Noise" + "/";
    mkdir( noiseDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    
    // Mu
    fileName = noiseDir + "Means.hdf5";
    nMu_ = Utilities::loadDataMatrix(fileName);
    
    // Sigma2
    fileName = noiseDir + "Variances.hdf5";
    nSigma2_ = Utilities::loadDataMatrix(fileName);
    
    // Q
    arma::fmat QNull = 1.0 - Q_;
    // fileName = noiseDir + "MembershipProbabilities.hdf5";
    // QNull = Utilities::loadDataMatrix(fileName);
    
    // Scalings: mu
    fileName = noiseDir + "Scalings.hdf5";
    hMu_ = Utilities::loadDataMatrix(fileName);
    
    // Scalings: sigma2
    fileName = noiseDir + "ScalingVariances.hdf5";
    hSigma2_ = Utilities::loadDataMatrix(fileName);
    
    // Scalings: valid
    fileName = noiseDir + "ScalingsValid.hdf5";
    arma::frowvec valid = Utilities::loadDataMatrix(fileName);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

MFPModels::C2P MFPModels::DGMM::collectExpectations() const
{
    // Subject-level model so not parallelised
    P::C2P D;
    if (childModules_.empty()) {
        // All zero if no children
        D.psiAAt = arma::zeros<arma::fmat>(M_,M_);
        D.psiDAt = arma::zeros<arma::fmat>(V_,M_);
    }
    else {
        // Initialise store from first MF model
        listType::const_iterator mfModel = childModules_.begin();
        D = (*mfModel)->getExpectations();
        // Add other models if necessary
        for (++mfModel; mfModel != childModules_.end(); ++mfModel) {
            const P::C2P d = (*mfModel)->getExpectations();
            D.psiAAt += d.psiAAt;
            D.psiDAt += d.psiDAt;
        }
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////

void MFPModels::DGMM::updateExpectations()
{
    // Get references to cached expectations
    arma::fmat& P_ = expectations_.P;//P_ is a matrix, expectations_.P is a pointer to where P_ is storred
    arma::fmat& PtP_ = expectations_.PtP;
    
    // Set expectations
    P_ = Q_ % sMu_
         + (1.0 - Q_) % (nMu_.each_row() % hMu_);
    PtP_ = P_.t() * P_;
    arma::fmat En2 = arma::square(nMu_) + nSigma2_; 
    En2.each_row() %= arma::square(hMu_) + hSigma2_;
    PtP_.diag() = arma::sum( Q_ % (arma::square(sMu_) + sSigma2_) + (1.0 - Q_) % En2 );
    
    //**************************************************************************
    // Seems to help matrix multiplications if matrices don't have wacky values
    P_.elem( arma::find(arma::abs(P_) < 1.0e-10) ).zeros();
    PtP_.elem( arma::find(arma::abs(PtP_) < 1.0e-10) ).zeros();
    //**************************************************************************
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

MatMeans::C2P MFPModels::DGMM::getSignalMeans() const
{
    const MatPrecs::P2C precisions = signalPriors_->getPriorPrecisions();
    
    MatMeans::C2P E;
    
    E.Psi = Q_ % precisions.X;
    E.PsiD = E.Psi % sMu_;
    
    return E;
}

////////////////////////////////////////////////////////////////////////////////

MatPrecs::C2P MFPModels::DGMM::getSignalPrecisions() const
{
    const MatMeans::P2C means = signalPriors_->getPriorMeans();
    
    MatPrecs::C2P E;
    
    E.N = Q_;
    E.D2 = Q_ % ( (arma::square(sMu_) + sSigma2_) - (2.0 * sMu_ % means.X) + means.X2 );
    
    return E;
}

////////////////////////////////////////////////////////////////////////////////

MatPrecs::C2P MFPModels::DGMM::getNoisePrecisions() const
{
    MatPrecs::C2P E;
    
    // Only use when the scaling is valid!
    arma::frowvec valid = arma::ones<arma::frowvec>(M_);
    for (unsigned int m = 0; m < M_; ++m) {
        valid(m) = hValid_[m];
    }
    arma::fmat q = (1.0 - Q_); q.each_row() %= valid;
    
    E.N = arma::sum(q, 1);
    E.D2 = arma::sum(q % (arma::square(nMu_) + nSigma2_), 1);
    
    return E;
}

////////////////////////////////////////////////////////////////////////////////

MemProbs::C2P MFPModels::DGMM::getMembershipProbabilities() const
{
    MemProbs::C2P Q;
    
    Q.counts.push_back(1.0 - Q_);
    Q.counts.push_back(Q_);
    
    return Q;
}

////////////////////////////////////////////////////////////////////////////////
