// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "PottsParcellation.h"

#include <cmath>

#include "Utilities/MiscMaths.h"
namespace miscmaths = PROFUMO::Utilities::MiscMaths;

namespace MFPModels = PROFUMO::MFModels::P;
namespace Mods = PROFUMO::Modules;
namespace MatMeans = Mods::MatrixMeans;
namespace MatPrecs = Mods::MatrixPrecisions;
namespace MemProbs = Mods::MembershipProbabilities;

// Resolve references to parents
typedef Mods::MatrixMean_Child MeanModel;
typedef Mods::MatrixPrecision_Child PrecModel;
typedef Mods::MembershipProbability_Child ProbModel;

////////////////////////////////////////////////////////////////////////////////

MFPModels::PottsParcellation::PottsParcellation(Mods::MatrixMean_Parent* means, Mods::MatrixPrecision_Parent* precisions, Mods::MembershipProbability_Parent* memberships, const Parameters prior, const float dofCorrectionFactor, const bool preInitialised, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation)
: MatrixMean_JointChild(means), MatrixPrecision_JointChild(precisions), MembershipProbability_JointChild(memberships), prior_(prior), V_(prior.V), M_(prior.M), dofCorrectionFactor_(dofCorrectionFactor)
{
    // Initialise from parents
    if (preInitialised) {
        std::vector<arma::fmat> initialisationMatrices = *expectationInitialisation;
        Mu_ = initialisationMatrices[0];
        Sigma2_ = initialisationMatrices[1];
        Q_ = initialisationMatrices[2];
    } else {
        Mu_ = MeanModel::parentModule_->getExpectations().X;
        //Mu_ = arma::randn<arma::fmat>(V_,M_);
        
        Sigma2_ = 1.0 / PrecModel::parentModule_->getExpectations().X;
        
        const MemProbs::P2C initialMemberships = ProbModel::parentModule_->getExpectations();
        Q_ = arma::zeros<arma::fmat>(V_, M_);
        for (unsigned int m = 0; m < M_; ++m) {
            Q_.col(m) = initialMemberships.p[m];
        }
    }
    
    // Make the cached expectations ship-shape
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MFPModels::PottsParcellation::update(const bool bigData, const float posteriorRho, const int subjectNum)
{
    // Collect the data from all the MFModels
    const P::C2P D = collectExpectations();
    
    
    // Get hyperpriors
    const MatMeans::P2C means = MeanModel::parentModule_->getExpectations();
    const MatPrecs::P2C precisions = PrecModel::parentModule_->getExpectations();
    const MemProbs::P2C memberships = ProbModel::parentModule_->getExpectations();
    
    
    // Updates!
    
    // Posterior covariance
    Sigma2_ = 1.0 / (precisions.X.each_row() + D.psiAAt.diag().t());
    
    // Posterior mean
    Mu_ = Sigma2_ % (D.psiDAt + precisions.X % means.X);
    
    // Memberships
    // Cache the unnormalised log probabilities - without couplings
    arma::fmat L = 0.5 * precisions.logX + 0.5 * arma::log(Sigma2_)
         - 0.5 * precisions.X % means.X2 + 0.5 * arma::square(Mu_) / Sigma2_;
    for (unsigned int m = 0; m < M_; ++m) {
        L.col(m) += memberships.logP[m];
    }
    
    // Generate a random order to update voxels (Bentleian dynamics)
    const arma::uvec voxelInds = arma::sort_index( arma::randu<arma::fvec>(V_) );
    
    // Now loop over voxels, updating each set of memberships in turn
    for (unsigned int v : voxelInds) {
        
        // Add contribution from neighbours to the cached log probabilities
        const arma::frowvec q = L.row(v)
            + prior_.beta * arma::sum(Q_.rows( (*prior_.couplings)[v] ));
        
        // Turn into proper probabilities via softmax
        Q_.row(v) = miscmaths::softmax(q);
    }
    
    
    //And make expectations ship-shape
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

float MFPModels::PottsParcellation::getKL(const bool bigData) const
{
    // Get hyperpriors
    const MatMeans::P2C means = MeanModel::parentModule_->getExpectations();
    const MatPrecs::P2C precisions = PrecModel::parentModule_->getExpectations();
    const MemProbs::P2C memberships = ProbModel::parentModule_->getExpectations();
    
    
    // KL from weights
    arma::fmat G_KL = - 0.5 * precisions.logX - 0.5 * arma::log(Sigma2_);
    G_KL += 0.5 * ( (arma::square(Mu_) + Sigma2_) - (2.0 * Mu_ % means.X) + means.X2 ) % precisions.X;
    G_KL += - 0.5;
    
    
    // KL of memberships
    arma::fmat Q_KL = arma::zeros<arma::fmat>(V_,M_);
    for (unsigned int m = 0; m < M_; ++m) {
        Q_KL.col(m) = Q_.col(m) % ( arma::log(Q_.col(m)) - memberships.logP[m] );
    }
    // Contribution from couplings
    for (unsigned int v = 0; v < V_; ++v) {
        Q_KL.row(v) -= Q_.row(v) % (prior_.beta * arma::sum(Q_.rows( (*prior_.couplings)[v] )));
    }
    // Adjust for any NaNs caused by log(0)
    Q_KL.elem( arma::find(Q_ == 0.0) ).zeros();
    
    
    // KL from memberships, plus weighted contribution from slab
    float KL = arma::accu( Q_KL + Q_ % G_KL );
    
    // Correct for voxels not being independent
    KL = dofCorrectionFactor_ * KL;
    
    return KL;
}
////////////////////////////////////////////////////////////////////////////////

arma::fmat MFPModels::PottsParcellation::signFlip(const arma::fmat spatialMeans)
{
    arma::frowvec subjectGroupCorr = arma::zeros<arma::frowvec>(spatialMeans.n_cols);
    for (unsigned int cnt1=0; cnt1<spatialMeans.n_cols; ++cnt1) {
        arma::fmat thisCorr = arma::cor(spatialMeans.col(cnt1), expectations_.P.col(cnt1));
        subjectGroupCorr.at(cnt1)=thisCorr.at(0);
    }
    arma::fmat spatialSigns = arma::sign(subjectGroupCorr);
    
    expectations_.P.each_row() %= arma::sign( spatialSigns );    
    expectations_.PtP.each_row() %= arma::sign( spatialSigns );
    expectations_.PtP.each_col() %= arma::sign( spatialSigns.t() );
    
    return spatialSigns;
}


////////////////////////////////////////////////////////////////////////////////

void MFPModels::PottsParcellation::save(const std::string directory) const
{
    // Just save posterior params
    std::string fileName;
    
    // Mu
    fileName = directory + "Means.hdf5";
    Mu_.save(fileName, arma::hdf5_binary);
    
    // Sigma2
    fileName = directory + "Variances.hdf5";
    Sigma2_.save(fileName, arma::hdf5_binary);
    
    // Q
    fileName = directory + "MembershipProbabilities.hdf5";
    Q_.save(fileName, arma::hdf5_binary);
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

void MFPModels::PottsParcellation::load(const std::string directory)
{
    // Just save posterior params
    std::string fileName;
    
    // Mu
    fileName = directory + "Means.hdf5";
    Mu_ = Utilities::loadDataMatrix(fileName);
    
    // Sigma2
    fileName = directory + "Variances.hdf5";
    Sigma2_ = Utilities::loadDataMatrix(fileName);
    
    // Q
    fileName = directory + "MembershipProbabilities.hdf5";
    Q_ = Utilities::loadDataMatrix(fileName);
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

MFPModels::C2P MFPModels::PottsParcellation::collectExpectations() const
{
    // Subject-level model so not parallelised
    P::C2P D;
    if (childModules_.empty()) {
        // All zero if no children
        D.psiAAt = arma::zeros<arma::fmat>(M_,M_);
        D.psiDAt = arma::zeros<arma::fmat>(V_,M_);
    }
    else {
        // Initialise store from first MF model
        listType::const_iterator mfModel = childModules_.begin();
        D = (*mfModel)->getExpectations();
        
        // Add other models if necessary
        for (++mfModel; mfModel != childModules_.end(); ++mfModel) {
            const P::C2P d = (*mfModel)->getExpectations();
            D.psiAAt += d.psiAAt;
            D.psiDAt += d.psiDAt;
        }
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////

void MFPModels::PottsParcellation::updateExpectations()
{
    // Get references to cached expectations
    arma::fmat& P_ = expectations_.P;
    arma::fmat& PtP_ = expectations_.PtP;
    
    // Set expectations
    P_ = Q_ % Mu_;
    
    PtP_ = arma::zeros<arma::fmat>(M_, M_);
    PtP_.diag() = arma::sum( Q_ % (arma::square(Mu_) + Sigma2_) );
    
    //**************************************************************************
    // Seems to help matrix multiplications if matrices don't have wacky values
    P_.elem( arma::find(arma::abs(P_) < 1.0e-10) ).zeros();
    PtP_.elem( arma::find(arma::abs(PtP_) < 1.0e-10) ).zeros();
    //**************************************************************************
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

MatMeans::C2P MFPModels::PottsParcellation::getMatrixMeans() const
{
    const MatPrecs::P2C precisions = PrecModel::parentModule_->getExpectations();
    
    MatMeans::C2P E;
    
    E.Psi = Q_ % precisions.X;
    //E.PsiD = Q_ % precisions.X % Mu_;
    E.PsiD = E.Psi % Mu_;
    
    return E;
}

////////////////////////////////////////////////////////////////////////////////

MatPrecs::C2P MFPModels::PottsParcellation::getMatrixPrecisions() const
{
    const MatMeans::P2C means = MeanModel::parentModule_->getExpectations();
    
    MatPrecs::C2P E;
    
    E.N = Q_;
    E.D2 = Q_ % ( (arma::square(Mu_) + Sigma2_) - (2.0 * Mu_ % means.X) + means.X2 );
    
    return E;
}

////////////////////////////////////////////////////////////////////////////////

MemProbs::C2P MFPModels::PottsParcellation::getMembershipProbabilities() const
{
    MemProbs::C2P E;
    
    for (unsigned int m = 0; m < M_; ++m) {
        E.counts.push_back( Q_.col(m) );
    }
    
    return E;
}

////////////////////////////////////////////////////////////////////////////////
