// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// A set of spatial maps, where each weight is drawn from an independent 
// Gaussian mixture model.

#ifndef MF_MODELS_P_INDEPENDENT_MEANS_H
#define MF_MODELS_P_INDEPENDENT_MEANS_H

#include <string>
#include <vector>
#include <armadillo>

#include "Module.h"
#include "Posterior.h"
#include "MFModel.h"
#include "MFVariableModels.h"
#include "VBDistributions/GaussianMixtureModel.h"

#include "Utilities/DataIO.h"

namespace PROFUMO
{
    namespace MFModels
    {
        namespace P
        {
            
            class IndependentMixtureModels :
                public P_VBPosterior,
                protected CachedModule<P::P2C>
            {
            public:
                //--------------------------------------------------------------
                struct Parameters {
                public:
                    // Mixture components
                    std::vector<VBDistributions::GaussianMixtureModel::GaussianParameters> gaussianPriors;
                    std::vector<VBDistributions::GaussianMixtureModel::DeltaParameters> deltaPriors;
                    // Sizes
                    unsigned int V, M;
                    // Initialisation
                    bool randomInitialisation = true;
                };
                //--------------------------------------------------------------
                IndependentMixtureModels(const Parameters prior, const float dofCorrectionFactor=1.0);
                
                void update(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                float getKL(const bool bigData) const;
                
                void save(const std::string directory) const;

                void load(const std::string directory);
                
                arma::fmat signFlip(const arma::fmat spatialMeans);
                
            private:
                // Helper functions to collect expectations / set caches
                P::C2P collectExpectations() const;
                void updateExpectations();
                
                // Storage for posteriors
                // These will ape armadillo's column-major ordering
                std::vector<VBDistributions::GaussianMixtureModel> posteriors_;
                
                // Sizes
                const unsigned int V_; // Number of voxels
                const unsigned int M_; // Number of modes
        
                // Spatial DOF correction
                const float dofCorrectionFactor_;
            };
            
        }
    }
}
#endif
