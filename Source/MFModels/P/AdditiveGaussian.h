// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// A simple model for subject spatial maps. Maps are just the group mean with 
// additive Gaussian variability. The precision of the Gaussian is also 
// estimated.

#ifndef MF_MODELS_P_ADDITIVE_GAUSSIAN_H
#define MF_MODELS_P_ADDITIVE_GAUSSIAN_H

#include <string>
#include <armadillo>
#include <vector>
#include <memory>


#include "Module.h"
#include "Posterior.h"
#include "MFModel.h"
#include "MFVariableModels.h"
#include "ModuleList.h"

#include "VBDistributions/Gamma.h"

#include "Utilities/DataIO.h"

namespace PROFUMO
{
    namespace MFModels
    {
        namespace P
        {
            ////////////////////////////////////////////////////////////////////
            
            class AdditiveGaussian :
                public P_VBPosterior,
                protected CachedModule<P::P2C>,
                public Modules::MatrixMean_Child,
                protected CachedModule<Modules::MatrixMeans::C2P>
            {
            public:
                //--------------------------------------------------------------
                struct Parameters {
                public:
                    // Prior on variance
                    VBDistributions::Gamma::Parameters precisionPrior;
                    // Sizes
                    unsigned int V, M;
                };
                //--------------------------------------------------------------
                
                AdditiveGaussian(Modules::MatrixMean_Parent* means, const Parameters prior, const float dofCorrectionFactor=1.0, const bool preInitialised = false, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = nullptr);
                
                void update(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                float getKL(const bool bigData = false) const;
                
                void save(const std::string directory) const;

                void load(const std::string directory);
                
                arma::fmat signFlip(const arma::fmat spatialMeans);
                
            private:
                // Helper functions to collect expectations / set caches
                P::C2P collectExpectations() const;
                void updateExpectations();
                
                // Posterior params
                arma::fmat Sigma_;
                arma::fmat Mu_;
                
                // Posterior precisions
                VBDistributions::Gamma precision_;
                
                // Sizes
                const unsigned int V_; // Number of voxels
                const unsigned int M_; // Number of modes
        
                // Spatial DOF correction
                const float dofCorrectionFactor_;
            };
            
            ////////////////////////////////////////////////////////////////////
        }
    }
}
#endif
