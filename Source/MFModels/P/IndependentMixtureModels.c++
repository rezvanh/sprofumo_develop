// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "IndependentMixtureModels.h"

#include <cmath>
#include <algorithm>

namespace MFPModels = PROFUMO::MFModels::P;
namespace VBDists = PROFUMO::VBDistributions;

////////////////////////////////////////////////////////////////////////////////

MFPModels::IndependentMixtureModels::IndependentMixtureModels(const Parameters prior, const float dofCorrectionFactor)
: V_(prior.V), M_(prior.M), dofCorrectionFactor_(dofCorrectionFactor)
{
    // Initialise posteriors
    // Doing this in parallel seems to break things - probably a global random device...
    for (unsigned int i = 0; i < V_ * M_; ++i) {
        posteriors_.emplace_back(prior.gaussianPriors, prior.deltaPriors, prior.randomInitialisation);
    }
    
    // Make everything ship-shape
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MFPModels::IndependentMixtureModels::update(const bool bigData, const float posteriorRho, const int subjectNum)
{
    // Collect the data from all the MFModels
    const P::C2P D = collectExpectations();
    
    // Random order to update maps (Bentleian dynamics)
    const arma::uvec mapInds = arma::sort_index( arma::randu<arma::fvec>(M_) );
    
    // Now loop over maps, updating each in turn
    for (unsigned int m : mapInds) {
        
        // Regress out the contributions from the other maps
        // Vector of mixings that the time courses apply to the other maps
        arma::fvec psiAAt_m = D.psiAAt.col(m); psiAAt_m(m) = 0.0;
        // And use this to regress out the expected values of the other maps
        const arma::fvec psiDAt_m = D.psiDAt.col(m) - expectations_.P * psiAAt_m;
        
        // Now update each posterior in turn
        //  - Can find that launching full team can be v. slow in a  nested 
        // loop, so set num_threads can be useful e.g.
        // num_threads(std::min(8, omp_get_max_threads()))
        //  + Setting a chunk size with static is not strictly necessary, but 
        // means that the voxels each thread works on are not contiguous across 
        // space (i.e. each thread has  mixture of regions), and so sees a more 
        // even distribution of differing signal properties (which may alter 
        // how long the updates take).
        #pragma omp parallel for schedule(static,512)
        for (unsigned int v = 0; v < V_; ++v) {
            const unsigned int i = m * V_ + v;
            
            // Record data
            VBDists::DataExpectations::Gaussian d;
            d.psi = D.psiAAt(m,m);
            d.psi_d = psiDAt_m(v);
            
            // Update posterior
            posteriors_[i].update(d, false, posteriorRho, subjectNum);
            
            // Update expectations
            expectations_.P(v,m) = posteriors_[i].getExpectations().x;
            
            //******************************************************************
            // Seems to help matrix multiplications if matrices don't have wacky values
            if (std::abs(expectations_.P(v,m)) < 1.0e-10) {
                expectations_.P(v,m) = 0.0;
            }
            //******************************************************************
        }
    }
    
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

float MFPModels::IndependentMixtureModels::getKL(const bool bigData) const
{
    // Collect from posteriors
    float KL = 0.0;
    #pragma omp parallel for reduction( + : KL ) schedule(dynamic,512)
    for (unsigned int i = 0; i < V_ * M_; ++i) {
        KL += posteriors_[i].getKL(false);
    }
    
    // Correct for voxels not being independent
    KL = dofCorrectionFactor_ * KL;
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////

arma::fmat MFPModels::IndependentMixtureModels::signFlip(const arma::fmat spatialMeans)
{
    arma::frowvec subjectGroupCorr = arma::zeros<arma::frowvec>(spatialMeans.n_cols);
    for (unsigned int cnt1=0; cnt1<spatialMeans.n_cols; ++cnt1) {
        arma::fmat thisCorr = arma::cor(spatialMeans.col(cnt1), expectations_.P.col(cnt1));
        subjectGroupCorr.at(cnt1)=thisCorr.at(0);
    }
    arma::fmat spatialSigns = arma::sign(subjectGroupCorr);
    
    expectations_.P.each_row() %= arma::sign( spatialSigns );    
    expectations_.PtP.each_row() %= arma::sign( spatialSigns );
    expectations_.PtP.each_col() %= arma::sign( spatialSigns.t() );
    
    return spatialSigns;
}


////////////////////////////////////////////////////////////////////////////////

void MFPModels::IndependentMixtureModels::save(const std::string directory) const
{
    // Just save cached expectations
    std::string fileName;
    
    // Means
    fileName = directory + "Means.hdf5";
    expectations_.P.save(fileName, arma::hdf5_binary);
    
    // Variances
    fileName = directory + "InnerProduct.hdf5";
    expectations_.PtP.save(fileName, arma::hdf5_binary);
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

void MFPModels::IndependentMixtureModels::load(const std::string directory)
{
    // Just save cached expectations
    std::string fileName;
    
    // Means
    fileName = directory + "Means.hdf5";
    expectations_.P = Utilities::loadDataMatrix(fileName);
    
    // Variances
    fileName = directory + "InnerProduct.hdf5";
    expectations_.PtP = Utilities::loadDataMatrix(fileName);
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

MFPModels::C2P MFPModels::IndependentMixtureModels::collectExpectations() const
{
    // Group-level model so parallelise collection
    
    // Initialise expectations
    P::C2P D;
    D.psiAAt = arma::zeros<arma::fmat>(M_,M_);
    D.psiDAt = arma::zeros<arma::fmat>(V_,M_);
    
    // Parallelise over iterator
    #pragma omp parallel
    {
    for (listType::const_iterator mfModel = childModules_.begin(); mfModel != childModules_.end(); ++mfModel) {
        #pragma omp single nowait
        {
        const P::C2P d = (*mfModel)->getExpectations();
        #pragma omp critical
        {
        D.psiAAt += d.psiAAt;
        D.psiDAt += d.psiDAt;
        }
        }
    }
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////

void MFPModels::IndependentMixtureModels::updateExpectations()
{
    // Get references to cached expectations
    arma::fmat& P_ = expectations_.P;
    arma::fmat& PtP_ = expectations_.PtP;
    
    // Initialise expectations
    P_ = arma::zeros<arma::fmat>(V_,M_);
    arma::fvec P2 = arma::zeros<arma::fvec>(M_);  // Used for PtP.diag()
    
    // Loop over mixture models, storing expectations
    #pragma omp parallel for schedule(dynamic)
    for (unsigned int m = 0; m < M_; ++m) {
        float p2 = 0.0;
        for (unsigned int v = 0; v < V_; ++v) {
            const unsigned int i = m * V_ + v;
            const VBDists::Expectations::Gaussian e = posteriors_[i].getExpectations();
            
            P_(v,m) = e.x;
            p2 += e.x2;
        }
        P2(m) = p2;
    }
    
    // And put PtP into the correct form
    PtP_ = P_.t() * P_;
    PtP_.diag() = P2;
    
    //**************************************************************************
    // Seems to help matrix multiplications if matrices don't have wacky values
    P_.elem( arma::find(arma::abs(P_) < 1.0e-10) ).zeros();
    PtP_.elem( arma::find(arma::abs(PtP_) < 1.0e-10) ).zeros();
    //**************************************************************************
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
