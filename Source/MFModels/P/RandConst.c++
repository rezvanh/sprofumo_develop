// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "RandConst.h"

#include <armadillo>

////////////////////////////////////////////////////////////////////////////////

PROFUMO::MFModels::P::RandConst::RandConst(const Parameters prior, const float /*dofCorrectionFactor*/)
{
    const arma::fmat P = arma::randn<arma::fmat>(prior.V, prior.M);
    
    expectations_.P = P;
    expectations_.PtP = P.t() * P + prior.V * arma::eye<arma::fmat>(prior.M, prior.M);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::MFModels::P::RandConst::update(const bool bigData, const float posteriorRho, const int subjectNum)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

float PROFUMO::MFModels::P::RandConst::getKL(const bool bigData) const
{
    float KL = 0.0;
    
    return KL;
}
////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::MFModels::P::RandConst::signFlip(const arma::fmat spatialMeans)
{
    arma::frowvec subjectGroupCorr = arma::zeros<arma::frowvec>(spatialMeans.n_cols);
    for (unsigned int cnt1=0; cnt1<spatialMeans.n_cols; ++cnt1) {
        arma::fmat thisCorr = arma::cor(spatialMeans.col(cnt1), expectations_.P.col(cnt1));
        subjectGroupCorr.at(cnt1)=thisCorr.at(0);
    }
    arma::fmat spatialSigns = arma::sign(subjectGroupCorr);
    
    expectations_.P.each_row() %= arma::sign( spatialSigns );    
    expectations_.PtP.each_row() %= arma::sign( spatialSigns );
    expectations_.PtP.each_col() %= arma::sign( spatialSigns.t() );
    
    return spatialSigns;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::MFModels::P::RandConst::save(std::string directory) const
{
    std::string fileName;
    
    // P
    fileName = directory + "Means.hdf5";
    expectations_.P.save(fileName, arma::hdf5_binary);
    
    // PtP
    fileName = directory + "InnerProduct.hdf5";
    expectations_.PtP.save(fileName, arma::hdf5_binary);
    
    return;
}
////////////////////////////////////////////////////////////////////////////////

void PROFUMO::MFModels::P::RandConst::load(std::string directory)
{
    std::string fileName;
    
    // P
    fileName = directory + "Means.hdf5";
    expectations_.P = Utilities::loadDataMatrix(fileName);
    
    // PtP
    fileName = directory + "InnerProduct.hdf5";
    expectations_.PtP = Utilities::loadDataMatrix(fileName);
    
    return;
}
////////////////////////////////////////////////////////////////////////////////
