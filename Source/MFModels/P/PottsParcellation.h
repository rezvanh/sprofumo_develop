// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// A weighted parcellation, whereby each voxel can only contain a contribution
// from one mode but the weight in the voxel can vary. Furthermore, the parcel 
// labels are spatially regularised by a Potts model.

#ifndef MF_MODELS_P_POTTS_PARCELLATION_H
#define MF_MODELS_P_POTTS_PARCELLATION_H

#include <string>
#include <memory>
#include <vector>
#include <armadillo>


#include "Module.h"
#include "Posterior.h"
#include "MFModel.h"
#include "MFVariableModels.h"
#include "ModuleList.h"

#include "Utilities/DataIO.h"

namespace PROFUMO
{
    namespace MFModels
    {
        namespace P
        {   
            ////////////////////////////////////////////////////////////////////
            
            class PottsParcellation :
                public P_VBPosterior,
                protected CachedModule<P::P2C>,
                public Modules::MatrixMean_JointChild,
                public Modules::MatrixPrecision_JointChild,
                public Modules::MembershipProbability_JointChild
            {
            public:
                //--------------------------------------------------------------
                struct Parameters {
                public:
                    // Couplings
                    // This is a vector of length V, where each element is a 
                    // vector of indices of the other rows this couples with
                    std::shared_ptr<std::vector<arma::uvec>> couplings
                        = std::make_shared<std::vector<arma::uvec>>();
                    
                    float beta;       // Potts coupling strength parameter
                    unsigned int V, M; // Sizes
                };
                //--------------------------------------------------------------
                
                PottsParcellation(Modules::MatrixMean_Parent* means, Modules::MatrixPrecision_Parent* precisions, Modules::MembershipProbability_Parent* memberships, const Parameters prior, const float dofCorrectionFactor=1.0, const bool preInitialised = false, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = nullptr);
                
                void update(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0);
                
                float getKL(const bool bigData = false) const;
                
                void save(const std::string directory) const;

                void load(const std::string directory);
                
                arma::fmat signFlip(const arma::fmat spatialMeans);
                
            private:
                // Expectation gathering for hyperpriors
                Modules::MatrixMeans::C2P getMatrixMeans() const;
                Modules::MatrixPrecisions::C2P getMatrixPrecisions() const;
                Modules::MembershipProbabilities::C2P getMembershipProbabilities() const;
                
                // Helper functions to collect expectations / set caches
                P::C2P collectExpectations() const;
                void updateExpectations();
                
                // Potts parameters
                const Parameters prior_;
                
                // Sizes
                const unsigned int V_; // Number of voxels
                const unsigned int M_; // Number of modes
                
                // Posterior params
                arma::fmat Q_;
                arma::fmat Mu_;
                arma::fmat Sigma2_;
                
                // Spatial DOF correction
                const float dofCorrectionFactor_;
            };
            
            ////////////////////////////////////////////////////////////////////
        }
    }
}
#endif
