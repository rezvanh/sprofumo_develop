// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "AdditiveGaussian.h"

#include <cmath>

#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;

namespace MFPModels = PROFUMO::MFModels::P;
namespace Mods = PROFUMO::Modules;
namespace MatMeans = Mods::MatrixMeans;
namespace VBDists = PROFUMO::VBDistributions;

// Resolve references to parents
typedef Mods::MatrixMean_Child MeanModel;

// Resolve calls to internally cached expectations
typedef PROFUMO::CachedModule<MFPModels::P2C> PCache;
typedef PROFUMO::CachedModule<Mods::MatrixMeans::C2P> MeanCache;

////////////////////////////////////////////////////////////////////////////////

MFPModels::AdditiveGaussian::AdditiveGaussian(Mods::MatrixMean_Parent* means, const Parameters prior, const float dofCorrectionFactor, const bool preInitialised, const std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation)
: Mods::MatrixMean_Child(means), precision_(prior.precisionPrior), V_(prior.V), M_(prior.M), dofCorrectionFactor_(dofCorrectionFactor)
{
    // Initialise posteriors from parents
    float precision;
    if (preInitialised) {
        std::vector<arma::fmat> initialisationMatrices = *expectationInitialisation;
        Mu_ = initialisationMatrices[0];
        Sigma_ = initialisationMatrices[1];
        arma::fvec agPrior = initialisationMatrices[2];
        precision = agPrior.at(0)/agPrior.at(1);
    
    } else {
        precision = precision_.getExpectations().x;
    
        Sigma_ = (1.0 / std::sqrt(precision)) * arma::eye<arma::fmat>(M_,M_);
        
        Mu_ = MeanModel::parentModule_->getExpectations().X;
        // + (1.0 / std::sqrt(precision)) * arma::randn<arma::fmat>(V_,M_);    
    }
    
    
    // Make everything ship shape
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MFPModels::AdditiveGaussian::update(const bool bigData, const float posteriorRho, const int subjectNum)
{
    // Collect the data from all the MFModels
    const P::C2P D = collectExpectations();
    
    // Get references to hyperpriors
    const MatMeans::P2C means = MeanModel::parentModule_->getExpectations();
    const VBDists::Expectations::Gamma precision = precision_.getExpectations();
    
    ////////////////////////////////////////////////////////////////////////////
    // Maps
    
    // Posterior covariance
    Sigma_ = linalg::inv_sympd( precision.x * arma::eye<arma::fmat>(M_,M_) + D.psiAAt );
    
    // Posterior mean
    Mu_ = ( precision.x * means.X + D.psiDAt ) * Sigma_;
    
    ////////////////////////////////////////////////////////////////////////////
    // Precision
    
    VBDists::DataExpectations::Gamma precD;
    
    precD.n = dofCorrectionFactor_ * V_ * M_;
    
    precD.d2 = dofCorrectionFactor_ * arma::sum( arma::sum( arma::square(Mu_) - (2.0 * Mu_ % means.X) + means.X2 ) + V_ * Sigma_.diag().t() );
    
    precision_.update(precD, false, posteriorRho, subjectNum);
    
    ////////////////////////////////////////////////////////////////////////////
    
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

float MFPModels::AdditiveGaussian::getKL(const bool bigData) const
{
    // Get hyperpriors
    const MatMeans::P2C means = MeanModel::parentModule_->getExpectations();
    const VBDists::Expectations::Gamma precision = precision_.getExpectations();
    
    float KL = 0.0;
    
    // Contribution from Gaussian
    KL += - 0.5 * V_ * M_ * precision.x - 0.5 * V_ * std::real(arma::log_det(Sigma_));
    KL += 0.5 * arma::sum( arma::sum( arma::square(Mu_) - (2.0 * Mu_ % means.X) + means.X2 ) + V_ * Sigma_.diag().t() ) * precision.x;
    KL += - 0.5 * V_ * M_;
    
    KL = KL * dofCorrectionFactor_;
    
    // And from precision
    KL += precision_.getKL(false);
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////

arma::fmat MFPModels::AdditiveGaussian::signFlip(const arma::fmat spatialMeans)
{
    arma::frowvec subjectGroupCorr = arma::zeros<arma::frowvec>(spatialMeans.n_cols);
    for (unsigned int cnt1=0; cnt1<spatialMeans.n_cols; ++cnt1) {
        arma::fmat thisCorr = arma::cor(spatialMeans.col(cnt1), PCache::expectations_.P.col(cnt1));
        subjectGroupCorr.at(cnt1)=thisCorr.at(0);
    }
    arma::fmat spatialSigns = arma::sign(subjectGroupCorr);
    
    PCache::expectations_.P.each_row() %= arma::sign( spatialSigns );    
    PCache::expectations_.PtP.each_row() %= arma::sign( spatialSigns );
    PCache::expectations_.PtP.each_col() %= arma::sign( spatialSigns.t() );
    
    return spatialSigns;
}


////////////////////////////////////////////////////////////////////////////////

void MFPModels::AdditiveGaussian::save(const std::string directory) const
{
    // Just save posterior params
    std::string fileName;
    
    // Mu
    fileName = directory + "Means.hdf5";
    Mu_.save(fileName, arma::hdf5_binary);
    
    // Sigma
    fileName = directory + "Covariance.hdf5";
    Sigma_.save(fileName, arma::hdf5_binary);
    
    // Precision
    precision_.save(directory + "AdditivePrecision.txt");
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MFPModels::AdditiveGaussian::load(const std::string directory)
{
    // load posterior params
    std::string fileName;
    
    // Mu
    fileName = directory + "Means.hdf5";
    Mu_ = Utilities::loadDataMatrix(fileName);
    
    // Sigma
    fileName = directory + "Covariance.hdf5";
    Sigma_ = Utilities::loadDataMatrix(fileName);
    
    // Precision
    precision_.load(directory + "AdditivePrecision.txt");
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

MFPModels::C2P MFPModels::AdditiveGaussian::collectExpectations() const
{
    // Subject-level model so not parallelised
    P::C2P D;
    if (childModules_.empty()) {
        // All zero if no children
        D.psiAAt = arma::zeros<arma::fmat>(M_,M_);
        D.psiDAt = arma::zeros<arma::fmat>(V_,M_);
    }
    else {
        // Initialise store from first MF model
        listType::const_iterator mfModel = childModules_.begin();
        D = (*mfModel)->getExpectations();
        
        // Add other models if necessary
        for (++mfModel; mfModel != childModules_.end(); ++mfModel) {
            const P::C2P d = (*mfModel)->getExpectations();
            D.psiAAt += d.psiAAt;
            D.psiDAt += d.psiDAt;
        }
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////

void MFPModels::AdditiveGaussian::updateExpectations()
{
    // Get references to cached expectations
    arma::fmat& P_ = PCache::expectations_.P;
    arma::fmat& PtP_ = PCache::expectations_.PtP;
    arma::fmat& Psi_ = MeanCache::expectations_.Psi;
    arma::fmat& PsiD_ = MeanCache::expectations_.PsiD;
    
    // Set expectations
    
    // MFPModel
    P_ = Mu_;
    
    PtP_ = P_.t() * P_ + V_ * Sigma_;
    
    // MatrixMeans
    Psi_ = precision_.getExpectations().x * arma::ones<arma::fmat>(V_,M_);
    PsiD_ = Psi_ % Mu_;
    
    //**************************************************************************
    // Seems to help matrix multiplications if matrices don't have wacky values
    P_.elem( arma::find(arma::abs(P_) < 1.0e-10) ).zeros();
    PtP_.elem( arma::find(arma::abs(PtP_) < 1.0e-10) ).zeros();
    //**************************************************************************
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
