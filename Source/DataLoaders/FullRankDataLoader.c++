// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include <sys/stat.h>
#include <vector>
#include <map>
#include <algorithm>
#include <armadillo>
#include <memory>
#include <string>
#include "FullRankDataLoader.h"

#include "Utilities/RandomSVD.h"
#include "Utilities/DataIO.h"

////////////////////////////////////////////////////////////////////////////////

std::map<PROFUMO::RunID, PROFUMO::DataTypes::FullRankData> PROFUMO::DataLoaders::FullRankDataTransformer::transformSubjectData(const std::map<PROFUMO::RunID, std::shared_ptr<const arma::fmat>>& subjectDataMatrices, const PROFUMO::SubjectID subjectID, const bool BD, const std::string bigDataDir) const
{
    std::map<PROFUMO::RunID, PROFUMO::DataTypes::FullRankData> subjectData;
    
    for (const auto& run : subjectDataMatrices) {
        subjectData.emplace( run.first, DataTypes::FullRankData(run.second) );
    }
    
    return subjectData;
}

///////////////////////////////////////////////////////////////////////////////

void PROFUMO::DataLoaders::FullRankSaveTransformedData::saveTransformedSubjectData(const std::map<PROFUMO::RunID, PROFUMO::DataTypes::FullRankData> transformedSubjectData, const PROFUMO::SubjectID subjectID, const bool BD, const std::string bigDataDir) const
{
    // Get a consistent ordering of RunIDs
    std::vector<RunID> runIDs;
    for (const auto& run : transformedSubjectData) {
        runIDs.push_back(run.first);
    }
    
    // Calculate SVD over all data for this subject
    std::vector<PROFUMO::DataTypes::FullRankData> subjectFRData;
    for (RunID runID : runIDs) {
        // Get data
        subjectFRData.push_back( transformedSubjectData.at(runID) );
    }
    for (unsigned int r = 0; r < runIDs.size(); ++r) {
        const RunID runID = runIDs[r];
        const arma::fmat D_saved = (*subjectFRData[r].D);
        
        const std::string subjectsDir = bigDataDir + "Subjects" + "/";
        mkdir( subjectsDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        const std::string subjectDir = subjectsDir + subjectID + "/";
        mkdir( subjectDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        const std::string runsDir = subjectDir + "Runs" + "/";
        mkdir( runsDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        const std::string runDir = runsDir + runID + "/";
        mkdir( runDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        const std::string runDataFileName = runDir + "RunPreprocessed.bin";
        
        D_saved.save(runDataFileName, arma::arma_binary);
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

std::map<PROFUMO::RunID, PROFUMO::DataTypes::FullRankData> PROFUMO::DataLoaders::FullRankDataTransformedBatch::transformBatchSubjectData(const std::map<RunID, std::string>& runInfo) const //, const PROFUMO::SubjectID subjectID, const std::string bigDataDir
{
    std::map<PROFUMO::RunID, PROFUMO::DataTypes::FullRankData> subjectData;
    
    
    for (const auto& run : runInfo) {
        const std::string runDataFileName = run.second + "RunPreprocessed.bin"; 
        // load preprocessed mat
        std::shared_ptr<arma::fmat> runDataMatrix=std::make_shared<arma::fmat>( PROFUMO::Utilities::loadDataMatrix(runDataFileName) );
        //add run data mat to subjectData
        subjectData.emplace( run.first, DataTypes::FullRankData(runDataMatrix));
    }
    
    return subjectData;
}

/////////////////////////////////////////////////////////////////////////////////

PROFUMO::DataLoaders::FullRankDataLoader::FullRankDataLoader(const std::string dataLocationsFile, const std::string outputDirectory, const unsigned int M, std::shared_ptr<const arma::uvec> maskInds, const bool normaliseData, const bool BD, const bool batchLoader, const std::string bigDataDir, const std::vector<std::string> subNamesList)
: DataLoader<PROFUMO::DataTypes::FullRankData>( dataLocationsFile, outputDirectory, M, std::make_unique<FullRankDataTransformer>(), std::make_unique<FullRankSaveTransformedData>(), std::make_unique<FullRankDataTransformedBatch>(), maskInds, normaliseData, BD, batchLoader, bigDataDir, subNamesList )
{
    return;
}

/////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::DataLoaders::FullRankDataLoader::computeSpatialBasis(unsigned int K, const bool MIGP, const std::string bigDataDir) const
{
    typedef PROFUMO::DataTypes::FullRankData FRData;
    
    // Loop over the subject data map, recording subject-specific spatial bases
    arma::fmat wMAT_MIGP; //
    MIGPConcatenatedSVD svd;
    std::vector<const arma::fmat*> data;
    
    if ( ! MIGP ) {
        // Get for each subject
        for (const auto& subject : DataLoader<FRData>::dataStore_) {
            // And record all runs
            for (const auto& run : subject.second) {
                data.push_back( run.second.D.get() );
            }
        }
        const ConcatenatedSVD thissvd = Utilities::computeRandomConcatenatedSVD(data, K); 
        return thissvd.U * arma::diagmat(thissvd.s);
    }
    else {
        // Get for each subject
        arma::fmat subjectDataConcatenated;
        std::vector<SubjectID> randSubjectList = DataLoader<FRData>::subjectList;
        std::random_shuffle ( randSubjectList.begin(), randSubjectList.end() );

        for (const auto& subject : randSubjectList) { //for (const auto& subject : DataLoader<LRData>::dataLocations) {
            std::map<RunID,std::string> thisRunInfo = DataLoader<FRData>::dataLocations.at(subject);
            
            // And record all runs
            subjectDataConcatenated.reset();
            
            // datasub = arma::zeros<arma::fmat*>(nvox, timesum);
            for (const auto& run : thisRunInfo) {
                std::string oneRunFile = bigDataDir + "Subjects" + "/" + subject + "/" + "Runs" + "/" + run.first + "/" + "RunPreprocessed.bin";
                arma::fmat thisRunData;
                thisRunData.load(oneRunFile, arma::arma_binary);
                subjectDataConcatenated = arma::join_horiz( subjectDataConcatenated, thisRunData );
            }
            
            if (subject == *randSubjectList.begin()) { //if (subject == dataLocations.first.begin()->first) {
                wMAT_MIGP = subjectDataConcatenated.t(); //size t x v
                
            } else {
                bool lastSub;
                if (subject == randSubjectList.back()) { 
                    lastSub = true; 
                } else { 
                    lastSub = false; 
                }
                svd = Utilities::computeMIGPConcatenatedSVD(subjectDataConcatenated, K, wMAT_MIGP, lastSub);   
                wMAT_MIGP=svd.Wmat;
            }
        }
        
        return wMAT_MIGP.t();
    }
}
////////////////////////////////////////////////////////////////////////////////