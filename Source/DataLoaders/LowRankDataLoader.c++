// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include <sys/stat.h>
#include <vector>
#include <map>
#include <omp.h>
#include <algorithm>
#include <armadillo>
#include <memory>
#include <string>
#include "LowRankDataLoader.h"

#include "Utilities/RandomSVD.h"
#include "Utilities/DataIO.h"

///////////////////////////////////////////////////////////////////////////////

PROFUMO::DataLoaders::LowRankDataTransformer::LowRankDataTransformer(const unsigned int N)
: N_(N)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

std::map<PROFUMO::RunID, PROFUMO::DataTypes::LowRankData> PROFUMO::DataLoaders::LowRankDataTransformer::transformSubjectData(const std::map<PROFUMO::RunID, std::shared_ptr<const arma::fmat>>& subjectDataMatrices, const PROFUMO::SubjectID subjectID, const bool BD, const std::string bigDataDir) const
{
    // Get a consistent ordering of RunIDs
    std::vector<RunID> runIDs;
    for (const auto& run : subjectDataMatrices) {
        runIDs.push_back(run.first);
    }

    // Calculate SVD over all data for this subject
    std::vector<const arma::fmat*> D;
    for (RunID runID : runIDs) {
        // Get data
        const arma::fmat* d = subjectDataMatrices.at(runID).get();
        D.push_back(d);
    }
    const ConcatenatedSVD svd = Utilities::computeRandomConcatenatedSVD(D, N_);
    // Now transform into the low rank form needed for our MFModels
    // U * diagmat(s) * V.t() = Pd * Ad

    // First, extract the shared spatial basis
    // Split the variance such that P and A have similar scalings
    std::shared_ptr<arma::fmat> Pd = std::make_shared<arma::fmat>();
    *Pd = svd.U * arma::diagmat(arma::sqrt( svd.s ));

    // Now populate the subject specific data
    std::map<RunID, DataTypes::LowRankData> subjectLRData;
    for (unsigned int r = 0; r < subjectDataMatrices.size(); ++r) {
        const RunID runID = runIDs[r];
        // Generate Ad
        std::shared_ptr<arma::fmat> Ad = std::make_shared<arma::fmat>();
        *Ad = arma::diagmat(arma::sqrt( svd.s )) * svd.V[r].t();
        // Key stats
        const float TrDtD = arma::accu( *(subjectDataMatrices.at(runID)) % *(subjectDataMatrices.at(runID)) );
        // const float TrDtD = arma::accu( ((*Pd) * (*Ad)) % ((*Pd) * (*Ad)) );
        // Combine into correct class
        DataTypes::LowRankData LRD(Pd, Ad, N_, TrDtD);

        // And store
        subjectLRData.emplace(runID, LRD);

    }

    return subjectLRData;
}
////////////////////////////////////////////////////////////////////////////////

void PROFUMO::DataLoaders::LowRankSaveTransformedData::saveTransformedSubjectData(const std::map<PROFUMO::RunID, PROFUMO::DataTypes::LowRankData> transformedSubjectData, const PROFUMO::SubjectID subjectID, const bool BD, const std::string bigDataDir) const
{
    // Get a consistent ordering of RunIDs
    std::vector<RunID> runIDs;
    for (const auto& run : transformedSubjectData) {
        runIDs.push_back(run.first);
    }

    // Calculate SVD over all data for this subject
    std::vector<PROFUMO::DataTypes::LowRankData> subjectLRData1;
    for (RunID runID : runIDs) {
        // Get data
        subjectLRData1.push_back( transformedSubjectData.at(runID) );
    }


    // Now populate the subject specific data
    for (unsigned int r = 0; r < runIDs.size(); ++r) {
        const RunID runID = runIDs[r];

        const std::shared_ptr<const arma::fmat> Pd_saved = subjectLRData1[r].Pd;
        const std::shared_ptr<const arma::fmat> Ad_saved = subjectLRData1[r].Ad;
        // Rank of approximation
        unsigned int N_saved = subjectLRData1[r].N;
        // Sizes: D is [voxels (V) x time points (T)]
        unsigned int V_saved = subjectLRData1[r].V;
        unsigned int T_saved = subjectLRData1[r].T;
        // trace(Dt * D): computed for full D, not low rank version
        float TrDtD_saved = subjectLRData1[r].TrDtD;

        //#pragma omp critical
        //{

        const std::string subjectsDir = bigDataDir + "Subjects" + "/";
        mkdir( subjectsDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        const std::string subjectDir = subjectsDir + subjectID + "/";
        mkdir( subjectDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        const std::string runsDir = subjectDir + "Runs" + "/";
        mkdir( runsDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        const std::string runDir = runsDir + runID + "/";
        mkdir( runDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        const std::string runDataFileNamePd = runDir + "RunPreprocessed_Pd.bin";
        const std::string runDataFileNameAd = runDir + "RunPreprocessed_Ad.bin";
        const std::string runDataFileNameNTrDtD = runDir + "RunPreprocessed_NTrDtD.bin";
        arma::fvec N_TrDtD = { static_cast<float>(N_saved), TrDtD_saved };

        Pd_saved->save(runDataFileNamePd, arma::arma_binary);
        Ad_saved->save(runDataFileNameAd, arma::arma_binary);
        N_TrDtD.save(runDataFileNameNTrDtD, arma::arma_binary);
        //N_TrDtD->save(runDataFileNameNTrDtD, arma::hdf5_binary);

        //}
    }

    return;
}

////////////////////////////////////////////////////////////////////////////////

std::map<PROFUMO::RunID, PROFUMO::DataTypes::LowRankData> PROFUMO::DataLoaders::LowRankDataTransformedBatch::transformBatchSubjectData(const std::map<RunID, std::string>& runInfo) const //, const PROFUMO::SubjectID subjectID, const std::string bigDataDir
{
    // Get a consistent ordering of RunIDs
    std::vector<RunID> runIDs;
    std::vector<std::string> runPaths;
    for (const auto& run : runInfo) {
        runIDs.push_back(run.first);
        runPaths.push_back(run.second);
    }
    // Now populate the subject specific data
    std::map<RunID, DataTypes::LowRankData> subjectLRData;
    for (unsigned int r = 0; r < runIDs.size(); ++r) {
        const RunID runID = runIDs[r];
        const std::string runDir = runPaths[r];
        const std::string runDataFileNamePd = runDir + "RunPreprocessed_Pd.bin";
        const std::string runDataFileNameAd = runDir + "RunPreprocessed_Ad.bin";
        const std::string runDataFileNameNTrDtD = runDir + "RunPreprocessed_NTrDtD.bin";

        // load Pd
        std::shared_ptr<arma::fmat> Pd = std::make_shared<arma::fmat>( PROFUMO::Utilities::loadDataMatrix(runDataFileNamePd) );

        // Load Ad
        std::shared_ptr<arma::fmat> Ad = std::make_shared<arma::fmat>( PROFUMO::Utilities::loadDataMatrix(runDataFileNameAd) );

        // Load N_, TrDtD
        arma::fvec N_TrDtD;
        N_TrDtD.load(runDataFileNameNTrDtD, arma::arma_binary);
        unsigned int Nl_ = N_TrDtD.at(0); //arma::conv_to<int>::  //arma::from(N_TrDtD[0]);
        const float TrDtDl = N_TrDtD.at(1);



        // Combine into correct class
        DataTypes::LowRankData LRD(Pd, Ad, Nl_, TrDtDl);

        // And store
        subjectLRData.emplace(runID, LRD);
    }

    return subjectLRData;
}
////////////////////////////////////////////////////////////////////////////////

PROFUMO::DataLoaders::LowRankDataLoader::LowRankDataLoader(const std::string dataLocationsFile, const std::string outputDirectory, const unsigned int M, const unsigned int N, std::shared_ptr<const arma::uvec> maskInds, const bool normaliseData, const bool BD, const bool batchLoader, const std::string bigDataDir, const std::vector<std::string> subNamesList)
: DataLoader<PROFUMO::DataTypes::LowRankData>( dataLocationsFile, outputDirectory, M, std::make_unique<LowRankDataTransformer>(N), std::make_unique<LowRankSaveTransformedData>(), std::make_unique<LowRankDataTransformedBatch>(), maskInds, normaliseData, BD, batchLoader, bigDataDir, subNamesList )
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::DataLoaders::LowRankDataLoader::computeSpatialBasis(unsigned int K, const bool MIGP, const std::string bigDataDir) const //add argument for bigData
{
    typedef PROFUMO::DataTypes::LowRankData LRData;
    // Loop over the subject data map, recording subject-specific spatial bases
    arma::fmat wMAT_MIGP;
    if (!MIGP) {
        std::vector<arma::fmat> spatialBases;
        // Get for each subject- note tht variable subject is pointer to a subject's data matrix
        for (const auto& subject : DataLoader<LRData>::dataStore_) {
            // If there is some data, add it to the list
            // Just add one: all runs share the same spatial basis, and the
            // scaling is appropriate for using one entry
            if ( ! subject.second.empty() ) {
                // HOLY SHIT WE NEED C++17 STRUCTURED BINDINGS HERE!!
                arma::fmat subjectBasis = *(subject.second.begin()->second.Pd);

                // Unweighted basis is orthonormal, so this is the scaling by sqrt(s)
                const arma::frowvec scaling = arma::sqrt(arma::sum(arma::square(subjectBasis), 0));
                // Apply the scaling again to get U * s
                // See Python code for validation of taking the SVD over U * s
                subjectBasis.each_row() %= scaling;

                // And store
                spatialBases.push_back(subjectBasis);
            }
        }

        // Compute the random SVD of all these subject specific bases
        std::vector<const arma::fmat*> svdBases;
        for (const auto& subjectBasis : spatialBases) {
            svdBases.push_back( &subjectBasis );
        }
        const ConcatenatedSVD svd = Utilities::computeRandomConcatenatedSVD(svdBases, K);
        return svd.U * arma::diagmat(svd.s);

    } else {
        std::vector<SubjectID> randSubjectList = DataLoader<LRData>::subjectList;
        std::random_shuffle ( randSubjectList.begin(), randSubjectList.end() );

        for (const auto& subject : randSubjectList) { //for (const auto& subject : DataLoader<LRData>::dataLocations) {
            std::map<RunID,std::string> thisRunInfo = DataLoader<LRData>::dataLocations.at(subject);
            const std::string oneRunFile = bigDataDir + "Subjects" + "/" + subject + "/" + "Runs" + "/" + thisRunInfo.begin()->first + "/" + "RunPreprocessed_Pd.bin";
            arma::fmat subjectBasis;
            subjectBasis = PROFUMO::Utilities::loadDataMatrix(oneRunFile);
            if (subject == *randSubjectList.begin()) { //if (subject == dataLocations.first.begin()->first) {
                wMAT_MIGP = subjectBasis.t(); //size t x v
            } else {
                bool lastSub;
                if (subject == randSubjectList.back()) { lastSub=true; } else { lastSub=false; }
                MIGPConcatenatedSVD svd = Utilities::computeMIGPConcatenatedSVD(subjectBasis, K, wMAT_MIGP, lastSub);
                wMAT_MIGP=svd.Wmat;//computeSpatialBasisMIGP(subjectDataConcatenated, 2*M_, wMAT_MIGP, lastSub);
            }
        }
        return wMAT_MIGP.t();
    }
}

////////////////////////////////////////////////////////////////////////////////

// Python code for checking scalings of concatenated SVD
/*
import numpy
import matplotlib, matplotlib.pyplot as plt
import random

# No of datasets
K = 10
# Data sizes
M = 200; N = [1000,2000]
# True data rank
R = 20
# Utilised rank
r = 50

# Generate data and do sub-SVDs
D = []; SVD = []
for k in range(K):
    n = random.randint(*N)
    d = numpy.random.randn(M,R) @ numpy.random.randn(R,n) + numpy.random.randn(M,n)

    D.append(d)
    SVD.append( numpy.linalg.svd(d, full_matrices=False) )

# Full SVD
[U,S,V] = numpy.linalg.svd(numpy.concatenate(D, axis=1), full_matrices=False)

# Rank r concatenated SVDs
us = numpy.concatenate([svd[0][:,:r] @ numpy.diag(svd[1][:r]) for svd in SVD], axis=1)
#us = numpy.concatenate([svd[0][:,:r] @ numpy.diag(svd[1][:r]**0.5) for svd in SVD], axis=1)
[u,s,v] = numpy.linalg.svd(us, full_matrices=False)

plt.figure()
plt.plot(S[:r], label="Full")
plt.plot(s[:r], label="Concat")
plt.plot(S[:r] - s[:r], label="Diff")
plt.legend()

plt.figure(); plt.imshow(numpy.corrcoef(U[:,:r].T,u[:,:r].T))

plt.show()
*/

// Python code for checking influence of noise on spatial basis
/*
import numpy
import matplotlib, matplotlib.pyplot as plt
import random

# Generate data
M = 2000; N = 1000; R = 200
d = numpy.random.randn(M,R) @ numpy.random.randn(R,N) + numpy.random.randn(M,N)
# And a noisy version
D = d + 0.5 * numpy.sqrt(R) * numpy.random.randn(M,N)

plt.figure(); plt.imshow(d); plt.colorbar(); plt.title("Clean data")
plt.figure(); plt.imshow(D); plt.colorbar(); plt.title("Noisy data")

[u,s,v] = numpy.linalg.svd(d, full_matrices=False)
[U,S,V] = numpy.linalg.svd(D, full_matrices=False)

plt.figure()
plt.plot(S, label="Noisy")
plt.plot(s, label="Clean")
plt.plot(S - s, label="Diff")
plt.legend()

plt.figure(); plt.imshow(numpy.corrcoef(U.T,u.T)); plt.colorbar(); plt.title("Noisy U; Clean U")

# Form U * s
US = (U @ numpy.diag(S))[:,:R]
us = (u @ numpy.diag(s))[:,:R]
# Predict US with us (i.e. gets rid of sign flip issues)
US_us = us @ numpy.linalg.pinv(us) @ US


plt.figure(); plt.imshow(numpy.corrcoef(numpy.concatenate([US,us,US_us], axis=1).T)); plt.colorbar(); plt.title("Noisy U*S; Clean U*S; Clean U*S (sign flipped)")

plt.figure(); plt.imshow(US - US_us); plt.colorbar(); plt.title("Noisy U*S - clean U*S")
plt.figure(); plt.plot(numpy.std(US - US_us, axis=0)); plt.title("std(Noisy U*S - clean U*S)")
*/
