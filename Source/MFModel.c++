// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2014, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "MFModel.h"

#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;

namespace MFMs = PROFUMO::MFModels;

///////////////////////////////////////////////////////////////////////////////

PROFUMO::MFModel::MFModel(MFPModel* P, MFHModel* H, MFAModel* A, MFPsiModel* Psi, const float dofCorrectionFactor)
: MFModel_PInterface(P), MFModel_HInterface(H), MFModel_AInterface(A), MFModel_PsiInterface(Psi), dofCorrectionFactor_(dofCorrectionFactor)
{
    P_ = MFModel_PInterface::parentModule_;
    H_ = MFModel_HInterface::parentModule_;
    A_ = MFModel_AInterface::parentModule_;
    Psi_ = MFModel_PsiInterface::parentModule_;
    
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::MFModel::setPModel(PROFUMO::MFPModel* P)
{
    MFModel_PInterface::setParentModule(P);
    P_ = MFModel_PInterface::parentModule_;
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::MFModel::setHModel(PROFUMO::MFHModel* H)
{
    MFModel_HInterface::setParentModule(H);
    H_ = MFModel_HInterface::parentModule_;
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::MFModel::setAModel(PROFUMO::MFAModel* A)
{
    MFModel_AInterface::setParentModule(A);
    A_ = MFModel_AInterface::parentModule_;
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::MFModel::setPsiModel(PROFUMO::MFPsiModel* Psi)
{
    MFModel_PsiInterface::setParentModule(Psi);
    Psi_ = MFModel_PsiInterface::parentModule_;
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::MFModel::setTimeCourseNormalisation(const bool normalisation)
{
    normaliseTimeCourses_ = normalisation;
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::MFModel::normaliseTimeCourseExpectations(MFMs::A::C2P& AD) const
{
    // We want to normalise the time courses to unit variance, so we get 
    // point estimates for the h that would achieve that and go from there
    
    // In the absence of priors (i.e. maximum likelihood), the time courses 
    // are:
    //   A_ML =  inv(AD.psiPtP) * AD.psiPtD
    // Similarly, the ML covariance is:
    //   C_ML = A_ML * A_ML.t() / T + inv(AD.psiPtP)
    // See Python code below for a demonstration of how to rescale based on this
    
    // However, the priors also have a strong influence. To account for this, 
    // we do an approximate set of update rules based on a very simplified 
    // model and set of 'priors'.
    
    // We are aiming for approximate unit variance, so this forms our prior
    // on the covariance
    const arma::fmat prior = arma::eye<arma::fmat>(arma::size(AD.psiPtP));
    // Note also that we are assuming zero mean a priori
    
    // Aproximate set of update rules
    const arma::fmat A_Sigma = linalg::inv_sympd(AD.psiPtP + prior);
    const arma::fmat A_Mu = A_Sigma * AD.psiPtD;
    const unsigned int T = A_Mu.n_cols;
    
    // Use this to estimate the covariance, and hence the precision matrix
    const arma::fmat P = linalg::inv_sympd( A_Mu * A_Mu.t() / T + A_Sigma );
    
    // Finally, use the scaling this implies to make the new expectations
    const arma::fvec h_ML = 1.0 / arma::sqrt(P.diag());
    AD.psiPtP %= (h_ML * h_ML.t());
    AD.psiPtD.each_col() %= h_ML;
    
    
    return;
}

/*
import numpy, matplotlib, matplotlib.pyplot as plt

N = 10; T = 100

C = numpy.random.randn(N,N); C = C @ C.T
c = 1.0 / numpy.sqrt(C.diagonal()[:,numpy.newaxis])
C = c.T * C * c + 0.5 * numpy.eye(N,N)
A = C @ numpy.random.randn(N,T)

plt.figure(); plt.imshow(A)
print(numpy.corrcoef(A[:3,:][:,:3]))
plt.show()

def plotVars(X):
    plt.figure()
    plt.plot( ((X @ X.T) / T).diagonal(), label='Full' )
    plt.plot( numpy.linalg.inv((X @ X.T) / T).diagonal(), label='Partial' )
    plt.ylim(ymin=0.0); plt.legend()

# Full correlations
h = numpy.sqrt( ((A @ A.T) / T).diagonal() )
cA = A / h[:,numpy.newaxis]
plotVars(cA); plt.show()

# Partial correlations
h = numpy.sqrt( numpy.linalg.inv((A @ A.T) / T).diagonal() )
pA = A * h[:,numpy.newaxis]
plotVars(pA); plt.show()
*/

////////////////////////////////////////////////////////////////////////////////
