// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "Subject.h"

#include <sys/stat.h>
#include <armadillo>
#include <iostream>

////////////////////////////////////////////////////////////////////////////////
// Get subject info

// ID
PROFUMO::SubjectID PROFUMO::SubjectModelling::Subject::getSubjectID() const
{
    return subjectInformation_.subjectID;
}

// Full info
PROFUMO::SubjectInformation PROFUMO::SubjectModelling::Subject::getSubjectInformation() const
{
    return subjectInformation_;
}

////////////////////////////////////////////////////////////////////////////////
// Updates

void PROFUMO::SubjectModelling::Subject::update()
{
    updateSpatialModel();
    updateWeightModel();
    updateTemporalModel();
    updateNoiseModel();
    return;
}

void PROFUMO::SubjectModelling::Subject::updateSpatialModel()
{
    updateModel(P_);
    return;
}

void PROFUMO::SubjectModelling::Subject::updateWeightModel()
{
    // Just farm out to runs
    for (const auto& run : runs_) {
        run.second->updateWeightModel();
    }
    return;
}

void PROFUMO::SubjectModelling::Subject::updateTemporalModel()
{
    // Farm out to runs
    for (const auto& run : runs_) {
        run.second->updateTemporalModel();
    }
    // And then update the subject level precision
    updateModel(Alpha_);
    return;
}

void PROFUMO::SubjectModelling::Subject::updateNoiseModel()
{
    // Just farm out to runs
    for (const auto& run : runs_) {
        run.second->updateNoiseModel();
    }
    return;
}
////////////////////////////////////////////////////////////////////////////////
// Sign Flips based on groupmap

arma::fmat PROFUMO::SubjectModelling::Subject::signFlipSubjectMap(const arma::fmat groupSpatialMeans)
{
    arma::fmat spatialSigns = P_->signFlip( groupSpatialMeans);
    //P_->getExpectations().P.each_row() %= arma::sign( groupSpatialMeans );
    //P_->getExpectations().PtP.each_row() %= arma::sign( groupSpatialMeans );
    //P_->getExpectations().PtP.each_col() %= arma::sign( groupSpatialMeans.t() );
    return spatialSigns;
}

void PROFUMO::SubjectModelling::Subject::signFlipSubjectWeight(const arma::fmat groupSpatialSigns)
{
    // Just farm out to runs
    for (const auto& run : runs_) {
        run.second->signFlipRunWeight(groupSpatialSigns);
    }
    
    return;
    
}

void PROFUMO::SubjectModelling::Subject::signFlipSubjectTimecourse(const arma::fmat groupSpatialSigns)
{
    // Just farm out to runs
    for (const auto& run : runs_) {
        run.second->signFlipRunTimecourse(groupSpatialSigns);
    }
    Alpha_->signFlip( groupSpatialSigns);
    return;
    
}
////////////////////////////////////////////////////////////////////////////////
// Model switching

void PROFUMO::SubjectModelling::Subject::setSpatialModel(Subject::SpatialModel newSpatialModel)
{
    // Change the Runs
    // First, make a run level spatial map model - this is not stored at the Run level
    Run::Model<P_Post> runP; runP.model = newSpatialModel.model; runP.isInternalModel = false;
    // Then update the Runs with this
    for (auto& run : runs_) {
        run.second->setSpatialModel(runP);
    }
    
    // Update the internal model
    setModel(P_, newSpatialModel);
    
    return;
}

void PROFUMO::SubjectModelling::Subject::setWeightModel(Subject::WeightModel newWeightModel)
{
    // Pass down to the runs
    for (auto& run : runs_) {
        auto& runH = newWeightModel.models.at(run.first);
        run.second->setWeightModel(runH);
    }
    
    return;
}

void PROFUMO::SubjectModelling::Subject::setTemporalModel(Subject::TemporalModel newTemporalModel)
{
    // Pass down to the runs
    for (auto& run : runs_) {
        auto& runA =  newTemporalModel.timeCourses.models.at(run.first);
        auto& runAlpha = newTemporalModel.precisions.runLevel.models.at(run.first);
        run.second->setTemporalModel(runA, runAlpha);
    }
    
    // Update the internal model
    setModel(Alpha_, newTemporalModel.precisions.subjectLevel);
    
    return;
}

void PROFUMO::SubjectModelling::Subject::setNoiseModel(Subject::NoiseModel newNoiseModel)
{
    // Pass down to the runs
    for (auto& run : runs_) {
        auto& runPsi = newNoiseModel.models.at(run.first);
        run.second->setNoiseModel(runPsi);
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
// Save

void PROFUMO::SubjectModelling::Subject::save(const std::string directory) const
{
    // Save SubjectID
    std::ofstream file;
    const std::string fileName = directory + "SubjectID.txt";
    file.open( fileName.c_str() );
    file << subjectInformation_.subjectID << std::endl;
    file.close();

    // Farm out to P and Runs after modifying file names
            
    // Runs
    // Make runs directory
    const std::string runsDir = directory + "Runs" + "/";
    mkdir( runsDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    // And then save runs in that
    for (const auto& run : runs_) {
        // Make directory
        const std::string runDir = runsDir + run.second->getRunID() + "/";
        mkdir( runDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        // Save run
        run.second->save(runDir);
    }
    
    // P
    if (P_ != nullptr) {
        // Make directory
        const std::string PDir = directory + "SpatialMaps.post" + "/";
        mkdir( PDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        // Save
        P_->save(PDir);
    }
    
    // Alpha
    if (Alpha_ != nullptr) {
        // Make directory
        const std::string AlphaDir = directory + "TemporalPrecisionMatrix.post" + "/";
        mkdir( AlphaDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        // Save
        Alpha_->save(AlphaDir);
    }
    
    return;
}

/////////////////////////////////////////////////////////////////////////////////

arma::frowvec PROFUMO::SubjectModelling::Subject::getFreeEnergy(const bool bigData) const
{
    float F = 0.0;
    arma::frowvec FEvec = arma::zeros<arma::frowvec>(9);
    // Collect from Runs
    for (const auto& run : runs_) {
        FEvec.head_cols(7) += run.second->getFreeEnergy();
        
    }
    
    // Then adjust with P & Alpha
    if (P_ != nullptr) {
        F -= P_->getKL();
        FEvec.at(7)=-P_->getKL();
    }
    
    if (Alpha_ != nullptr) {
        F -= Alpha_->getKL();
        FEvec.at(8)=-Alpha_->getKL();
    }
    
    return FEvec;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::SubjectModelling::Subject::setTimeCourseNormalisation(const bool normalisation)
{
    for (const auto& run : runs_) {
        run.second->setTimeCourseNormalisation(normalisation);
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
