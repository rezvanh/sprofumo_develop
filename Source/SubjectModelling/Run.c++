// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "Run.h"

#include <fstream>
#include <sys/stat.h>
#include <armadillo>
#include <iostream>


#include "MFModels/FullRankMFModel.h"
#include "MFModels/LowRankMFModel.h"

////////////////////////////////////////////////////////////////////////////////
// Constructor: full rank data

PROFUMO::SubjectModelling::Run::Run(const RunInformation runInformation, const DataTypes::FullRankData data, Run::Model<P_Post> P, Run::Model<H_Post> H, Run::Model<A_Post> A, Run::Model<Alpha_Post> Alpha, Run::Model<Psi_Post> Psi, const float dofCorrectionFactor)
: runInformation_(runInformation)
{
    // Make the MFModel
    MFM_ = std::make_unique<MFModels::FullRankMFModel>(data, P.model.get(), H.model.get(), A.model.get(), Psi.model.get(), dofCorrectionFactor);
    
    // Record the run level models, as required
    setInternalModels(P, H, A, Alpha, Psi);
    
    return;
}

/////////////////////////////////////////////////////////////////////////////////
// Constructor: low rank data

PROFUMO::SubjectModelling::Run::Run(const RunInformation runInformation, const DataTypes::LowRankData data, Run::Model<P_Post> P, Run::Model<H_Post> H, Run::Model<A_Post> A, Run::Model<Alpha_Post> Alpha, Run::Model<Psi_Post> Psi, const float dofCorrectionFactor)
: runInformation_(runInformation)
{
    // Make the MFModel
    MFM_ = std::make_unique<MFModels::LowRankMFModel>(data, P.model.get(), H.model.get(), A.model.get(), Psi.model.get(), dofCorrectionFactor);
    
    // Record the run level models, as required
    setInternalModels(P, H, A, Alpha, Psi);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
// Sets all models for constructor

void PROFUMO::SubjectModelling::Run::setInternalModels(Run::Model<P_Post> P, Run::Model<H_Post> H, Run::Model<A_Post> A, Run::Model<Alpha_Post> Alpha, Run::Model<Psi_Post> Psi)
{
    // Record the models, as required
    setModel(P_, P);
    setModel(H_, H);
    setModel(A_, A);
    setModel(Alpha_, Alpha);
    setModel(Psi_, Psi);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::RunID PROFUMO::SubjectModelling::Run::getRunID() const
{
    return runInformation_.runID;
}

PROFUMO::RunInformation PROFUMO::SubjectModelling::Run::getRunInformation() const
{
    return runInformation_;
}

////////////////////////////////////////////////////////////////////////////////
// Updates

void PROFUMO::SubjectModelling::Run::update()
{
    updateSpatialModel();
    updateWeightModel();
    updateTemporalModel();
    updateNoiseModel();
    return;
}

void PROFUMO::SubjectModelling::Run::updateSpatialModel()
{
    updateModel(P_);
    return;
}

void PROFUMO::SubjectModelling::Run::updateWeightModel()
{
    updateModel(H_);
    return;
}

void PROFUMO::SubjectModelling::Run::updateTemporalModel()
{
    updateModel(A_);
    updateModel(Alpha_);
    return;
}

void PROFUMO::SubjectModelling::Run::updateNoiseModel()
{
    updateModel(Psi_);
    return;
}

////////////////////////////////////////////////////////////////////////////////
// Sign Flips based on groupmap

void PROFUMO::SubjectModelling::Run::signFlipRunWeight(const arma::fmat groupSpatialSigns)
{
    arma::fmat HSpatialSigns = H_->signFlip( groupSpatialSigns);
    //H_->getExpectations().h %= arma::sign( groupSpatialSigns.t() );
    //H_->getExpectations().hht.each_row() %= arma::sign( groupSpatialSigns );
    //H_->getExpectations().hht.each_col() %= arma::sign( groupSpatialSigns.t() );
    return;
}

void PROFUMO::SubjectModelling::Run::signFlipRunTimecourse(const arma::fmat groupSpatialSigns)
{
    arma::fmat ASpatialSigns = A_->signFlip( groupSpatialSigns);
    //H_->getExpectations().h %= arma::sign( groupSpatialSigns.t() );
    //H_->getExpectations().hht.each_row() %= arma::sign( groupSpatialSigns );
    //H_->getExpectations().hht.each_col() %= arma::sign( groupSpatialSigns.t() );
    return;
}

////////////////////////////////////////////////////////////////////////////////
// Model switching

void PROFUMO::SubjectModelling::Run::setSpatialModel(Run::Model<P_Post> newP)
{
    // Change MFModel
    MFM_->setPModel(newP.model.get());
    
    // Update the internal model
    setModel(P_, newP);
    
    return;
}

void PROFUMO::SubjectModelling::Run::setWeightModel(Run::Model<H_Post> newH)
{
    // Change MFModel
    MFM_->setHModel(newH.model.get());
    
    // Update the internal model
    setModel(H_, newH);
    
    return;
}

void PROFUMO::SubjectModelling::Run::setTemporalModel(Model<A_Post> newA, Model<Alpha_Post> newAlpha)
{
    // Change MFModel
    MFM_->setAModel(newA.model.get());
    
    // Update the internal models
    setModel(A_, newA);
    setModel(Alpha_, newAlpha);
    
    return;
}

void PROFUMO::SubjectModelling::Run::setNoiseModel(Run::Model<Psi_Post> newPsi)
{
    // Change MFModel
    MFM_->setPsiModel(newPsi.model.get());
    
    // Update the internal model
    setModel(Psi_, newPsi);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
// Save

void PROFUMO::SubjectModelling::Run::save(const std::string directory) const
{
    // Save RunID
    std::ofstream file;
    const std::string fileName = directory + "RunID.txt";
    file.open( fileName.c_str() );
    file << runInformation_.runID << std::endl;
    file.close();
    
    // Farm saving parameters out to P,A,Psi after modifying file names
    
    // P
    if (P_ != nullptr) {
        // Make directory
        const std::string PDir = directory + "SpatialMaps.post" + "/";
        mkdir( PDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        // Save
        P_->save(PDir);
    }
    
    // H
    if (H_ != nullptr) {
        // Make directory
        const std::string HDir = directory + "ComponentWeightings.post" + "/";
        mkdir( HDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        // Save
        H_->save(HDir);
    }
    
    // A
    if (A_ != nullptr) {
        // Make directory
        const std::string ADir = directory + "TimeCourses.post" + "/";
        mkdir( ADir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        // Save
        A_->save(ADir);
    }
    
    // Alpha
    if (Alpha_ != nullptr) {
        // Make directory
        const std::string AlphaDir = directory + "TemporalPrecisionMatrix.post" + "/";
        mkdir( AlphaDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        // Save
        Alpha_->save(AlphaDir);
    }
    
    // Psi
    if (Psi_ != nullptr) {
        // Make directory
        const std::string PsiDir = directory + "NoisePrecision.post" + "/";
        mkdir( PsiDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        // Save
        Psi_->save(PsiDir);
    }
    
    return;
}

/////////////////////////////////////////////////////////////////////////////////

arma::frowvec PROFUMO::SubjectModelling::Run::getFreeEnergy(const bool bigData) const
{
    arma::frowvec FEvec = arma::zeros<arma::frowvec>(7);
    float F = MFM_->getLogLikelihood();
    FEvec.at(0) = F;
    
    if (P_ != nullptr) {
        F -= P_->getKL();
        FEvec.at(1) = -P_->getKL();
    }
    
    if (H_ != nullptr) {
        F -= H_->getKL();
        FEvec.at(2) = -H_->getKL();
    }
    
    if (A_ != nullptr) {
        F -= A_->getKL();
        FEvec.at(3) = -A_->getKL();
    }
    
    if (Alpha_ != nullptr) {
        F -= Alpha_->getKL();
        FEvec.at(4) = -Alpha_->getKL();
    }
    
    if (Psi_ != nullptr) {
        F -= Psi_->getKL();
        FEvec.at(5) = -Psi_->getKL();
    }
    
    FEvec.at(6)=F;
    
    return FEvec;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::SubjectModelling::Run::setTimeCourseNormalisation(const bool normalisation)
{
    MFM_->setTimeCourseNormalisation(normalisation);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
//                    Implementation of template functions                    //
////////////////////////////////////////////////////////////////////////////////

template<class T>
void PROFUMO::SubjectModelling::Run::updateModel(std::shared_ptr<T>& internalModel)
{
    // Check for an internal model, and update if present
    if (internalModel != nullptr) {
        internalModel->update();
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

template<class T>
void PROFUMO::SubjectModelling::Run::setModel(std::shared_ptr<T>& internalModel, Run::Model<T> newModel)
{
    // Check if model is present, and set if required
    if (newModel.isInternalModel) {
        internalModel = newModel.model;
    } else {
        internalModel = nullptr;
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
