// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines what a "subject" is in the PROFUMO framework. A subject has several
// scans (the data), and these scans are decomposed into spatial maps, time
// courses and more. The scans, component weightings, time courses, temporal 
// covariances and noise models are stored in the Runs, while the spatial maps 
// and a temporal precision matrix are stored at the subject level.

#ifndef SUBJECT_MODELLING_SUBJECT_H
#define SUBJECT_MODELLING_SUBJECT_H

#include <memory>
#include <string>
#include <vector>
#include <map>
#include <armadillo>


#include "SubjectModelling/Run.h"

namespace PROFUMO
{
    ////////////////////////////////////////////////////////////////////////////
    
    typedef std::string SubjectID;
    
    struct SubjectInformation
    {
    public:
        SubjectID subjectID;
        std::vector<RunInformation> runs;
    };
    
    ////////////////////////////////////////////////////////////////////////////
    
    namespace SubjectModelling
    {
        
        class Subject
        {
        public:
            //------------------------------------------------------------------
            // Struct for passing in subject-level models
            template<class T>
            struct Model
            {
            public:
                std::shared_ptr<T> model;
                bool isInternalModel;
                
                Model()
                : model(nullptr), isInternalModel(false)
                {}
            };
            //------------------------------------------------------------------
            // Struct for passing in run-level models
            template<class T>
            struct RunModels
            {
            public:
                std::map<RunID, Run::Model<T>> models;
            };
            //------------------------------------------------------------------
            // Define the valid model groupings
            typedef Model<SubjectModelling::P_Post> SpatialModel;
            typedef RunModels<SubjectModelling::H_Post> WeightModel;
            typedef RunModels<SubjectModelling::Psi_Post> NoiseModel;
            
            struct TemporalModel
            {
            public:
                typedef RunModels<SubjectModelling::A_Post> TimeCourses;
                
                struct Precisions
                {
                public:
                    SubjectModelling::Subject::RunModels<SubjectModelling::Alpha_Post> runLevel;
                    SubjectModelling::Subject::Model<SubjectModelling::Alpha_Post> subjectLevel;
                };
                
                TimeCourses timeCourses;
                Precisions precisions;
            };
            //------------------------------------------------------------------
        
            // Constructor templated by data type
            template<class D>
            Subject(const SubjectInformation subjectInformation, const std::map<RunID, D> dataStore, SpatialModel spatialModel, WeightModel weightModel, TemporalModel temporalModel, NoiseModel noiseModel, const float dofCorrectionFactor=1.0);
            
            // Returns subject information
            SubjectID getSubjectID() const;
            SubjectInformation getSubjectInformation() const;
            
            // Update methods
            void update();
            void updateSpatialModel();
            void updateWeightModel();
            void updateTemporalModel();
            void updateNoiseModel();
            
            arma::fmat signFlipSubjectMap(const arma::fmat groupSpatialMeans);
            void signFlipSubjectWeight(const arma::fmat groupSpatialSigns);
            void signFlipSubjectTimecourse(const arma::fmat groupSpatialSigns);
            
            // Functions for model switching
            void setSpatialModel(SpatialModel newSpatialModel);
            void setWeightModel(WeightModel newWeightModel);
            void setTemporalModel(TemporalModel newTemporalModel);
            void setNoiseModel(NoiseModel newNoiseModel);
            
            // Saves all posteriors
            void save(const std::string directory) const;
            
            // Calculates free energy from models this is responsible for
            arma::frowvec getFreeEnergy(const bool bigData = false) const;
            
            // Whether to normalise time course variance
            void setTimeCourseNormalisation(const bool normalisation);
            
        private:
            // Unique identifier
            const SubjectInformation subjectInformation_;
            
            // Set of runs
            std::map<RunID, std::unique_ptr<Run>> runs_;
            
            // Subject level spatial maps
            std::shared_ptr<P_Post> P_;
            
            // Subject level temporal precision
            std::shared_ptr<Alpha_Post> Alpha_;
            
            // Helper functions to manipulate P/H/A/Alpha/Psi
            template<class T>
            void updateModel(std::shared_ptr<T>& internalModel);
            
            template<class T>
            void setModel(std::shared_ptr<T>& internalModel, Model<T> newModel);
        };
        
    }
}

////////////////////////////////////////////////////////////////////////////////
//                    Implementation of template functions                    //
////////////////////////////////////////////////////////////////////////////////

// Constructor templated by data type
template<class D>
PROFUMO::SubjectModelling::Subject::Subject(const SubjectInformation subjectInformation, const std::map<RunID, D> dataStore, Subject::SpatialModel spatialModel, Subject::WeightModel weightModel, Subject::TemporalModel temporalModel, Subject::NoiseModel noiseModel, const float dofCorrectionFactor)
: subjectInformation_(subjectInformation)
{
    // Make the runs!
    // First, make a run level spatial map model - this is not stored at the Run level
    Run::Model<P_Post> runP; runP.model = spatialModel.model; runP.isInternalModel = false;
    // Then make the runs and add them to the map
    for (const RunInformation runInformation : subjectInformation.runs) {
        const RunID runID = runInformation.runID;
        auto& runH = weightModel.models.at(runID);
        auto& runA =  temporalModel.timeCourses.models.at(runID);
        auto& runAlpha = temporalModel.precisions.runLevel.models.at(runID);
        auto& runPsi = noiseModel.models.at(runID);
        runs_.emplace(
            runID, 
            std::make_unique<Run>(runInformation, dataStore.at(runID), runP, runH, runA, runAlpha, runPsi, dofCorrectionFactor)
        );
    }
    
    // Record the subject level models, as required
    setModel(P_, spatialModel);
    setModel(Alpha_, temporalModel.precisions.subjectLevel);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

template<class T>
void PROFUMO::SubjectModelling::Subject::updateModel(std::shared_ptr<T>& internalModel)
{
    // Check for an internal model, and update if present
    if (internalModel != nullptr) {
        internalModel->update();
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

template<class T>
void PROFUMO::SubjectModelling::Subject::setModel(std::shared_ptr<T>& internalModel, Subject::Model<T> newModel)
{
    // Check if model is present, and set if required
    if (newModel.isInternalModel) {
        internalModel = newModel.model;
    } else {
        internalModel = nullptr;
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
#endif
