// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines what a "run" is in the PROFUMO framework. A run is the data from a 
// scan, along with a set of models for the spatial maps, component weights, 
// time courses, temporal covariance and noise.

// Posteriors:
// P:     Spatial maps
// H:     Component weights
// A:     Time courses
// Alpha: Temporal precision matrix
// Psi:   Noise precision

#ifndef SUBJECT_MODELLING_RUN_H
#define SUBJECT_MODELLING_RUN_H

#include <memory>
#include <string>
#include <armadillo>

#include "DataTypes.h"

#include "MFModel.h"
#include "MFVariableModels.h"
#include "ModuleList.h"

namespace PROFUMO
{
    ////////////////////////////////////////////////////////////////////////////
    
    typedef std::string RunID;
    
    struct RunInformation
    {
    public:
        RunID runID;
        //unsigned int V;  // Voxels
        unsigned int T;  // Time points
        //unsigned int M;  // Modes
        //unsigned int N;  // Artefacts
        //const double TR ? // Makes it specific to fMRI? i.e. not MEG etc
    };
    
    ////////////////////////////////////////////////////////////////////////////
        
    namespace SubjectModelling
    {
        ////////////////////////////////////////////////////////////////////////
        
        // Define the model building blocks
        typedef MFModels::P_VBPosterior                P_Post;
        typedef MFModels::H_VBPosterior                H_Post;
        typedef MFModels::A_VBPosterior                A_Post;
        typedef Modules::PrecisionMatrix_VBPosterior   Alpha_Post;
        typedef MFModels::Psi_VBPosterior              Psi_Post;
        
        ////////////////////////////////////////////////////////////////////////
        
        class Run
        {
        public:
            //------------------------------------------------------------------
            // Struct for passing in models
            template<class T>
            struct Model
            {
            public:
                std::shared_ptr<T> model;
                bool isInternalModel;
                
                Model()
                : model(nullptr), isInternalModel(false)
                {}
            };
            //------------------------------------------------------------------
            
            // Constructors
            // Full rank data
            Run(const RunInformation runInformation, const DataTypes::FullRankData data, Model<P_Post> P, Model<H_Post> H, Model<A_Post> A, Model<Alpha_Post> Alpha, Model<Psi_Post> Psi, const float dofCorrectionFactor=1.0);
            // Low rank data
            Run(const RunInformation runInformation, const DataTypes::LowRankData data, Model<P_Post> P, Model<H_Post> H, Model<A_Post> A, Model<Alpha_Post> Alpha, Model<Psi_Post> Psi, const float dofCorrectionFactor=1.0);
            
            // Get key information
            RunID getRunID() const;
            RunInformation getRunInformation() const;
            
            // Update methods
            void update();
            void updateSpatialModel();
            void updateWeightModel();
            void updateTemporalModel();
            void updateNoiseModel();
            
            // sign flips 
            void signFlipRunWeight(const arma::fmat groupSpatialSigns);
            void signFlipRunTimecourse(const arma::fmat groupSpatialSigns);
            
            // Functions for model switching
            void setSpatialModel(Model<P_Post> newP);
            void setWeightModel(Model<H_Post> newH);
            void setTemporalModel(Model<A_Post> newA, Model<Alpha_Post> newAlpha);
            void setNoiseModel(Model<Psi_Post> newPsi);
            
            // Saves all posteriors
            void save(const std::string directory) const;
            
            // Calculates free energy from models this is responsible for
            arma::frowvec getFreeEnergy(const bool bigData = false) const;
            
            // Whether to normalise time course variance
            void setTimeCourseNormalisation(const bool normalisation);
            
        private:
            // How we identify the run!
            const RunInformation runInformation_;
            
            // MF Models - this looks after the data too
            std::unique_ptr<MFModel> MFM_;
            
            // MF model components
            std::shared_ptr<P_Post>     P_;
            std::shared_ptr<H_Post>     H_;
            std::shared_ptr<A_Post>     A_;
            std::shared_ptr<Alpha_Post> Alpha_;
            std::shared_ptr<Psi_Post>   Psi_;
            
            // Helper functions to manipulate P/H/A/Alpha/Psi
            template<class T>
            void updateModel(std::shared_ptr<T>& internalModel);
            
            template<class T>
            void setModel(std::shared_ptr<T>& internalModel, Model<T> ford);
            
            // Helper for constructors
            void setInternalModels(Model<P_Post> P, Model<H_Post> H, Model<A_Post> A, Model<Alpha_Post> Alpha, Model<Psi_Post> Psi);
        };
        
        ////////////////////////////////////////////////////////////////////////
    }
}
#endif
