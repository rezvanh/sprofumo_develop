// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Class structure that represents a posterior model for a variable in a
// graphical model

#ifndef POSTERIOR_H
#define POSTERIOR_H

#include <string>
#include <armadillo>


#include "Module.h"

namespace PROFUMO
{
    ////////////////////////////////////////////////////////////////////////////
    
    // Simply states that a posterior in a graphical model is a parent module
    // that we can update and save.
    
    // This allows us to build generic graphical models, without the models
    // themselves knowing about the implementation of the posteriors.
    // Crucially, this allows us to keep track of the which Posteriors are
    // responsible for which variables, and then the set of hyperpriors are
    // abstracted away.
    // For example, we can define a Posterior for a set of spatial maps, P.
    // The MFModel only knows about P as a generic Module_Parent but the rest
    // of the code knows how to update() P. The set of hyperpriors are defined
    // at initialisation, but are then hidden.
    // This may cause issues for dynamic switching of hyperpriors, but we can
    // now construct full graphical models.
    
    template<class C2P, class P2C>
    class Posterior :
        public Module_Parent<C2P, P2C>
    {
    public:
        virtual void update(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0) = 0;
        virtual void save(const std::string directory) const = 0;
        virtual void load(const std::string directory) = 0;
    };
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Simple extension for the VB case - now just has to return a KL divergence
    // too.
    
    template<class C2P, class P2C>
    class VBPosterior :
        public Posterior<C2P, P2C>
    {
    public:
        virtual float getKL(const bool bigData = false) const = 0;
        virtual arma::fmat signFlip(const arma::fmat spatialSigns) = 0;
    };
    
    ////////////////////////////////////////////////////////////////////////////
}
#endif
