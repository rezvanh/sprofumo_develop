#!/usr/bin/env bash

set -e

# tell conda to build from the CI
# checkout (see the git_url and
# git_rev fields in meta.yaml)
export CONDA_REPOSITORY=$(pwd)
export CONDA_REVISION=${CI_COMMIT_REF_NAME}

pushd Conda
conda-build -c conda-forge --output-folder=dist .
popd
