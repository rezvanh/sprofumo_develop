# -*- coding: utf-8 -*-

# PROFUMO
# Framework for variational Bayesian inference of modes from fMRI data

# - Sam Harrison 2018
# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

# utilities.py
"""
Assorted helper functions.
"""

###############################################################################

import os, os.path as op
import glob
import argparse
import math
import numpy
import scipy.optimize

###############################################################################

EPSILON = 1.0e-5
"""
float: Value used for machine precision.
"""

###############################################################################

def run_order_to_run_list(run_order):
    """
    Transforms different subject / run conventions.
    
    Parameters
    ----------
    run_order
        [{'Subject': 'subject1', 'Run': 'run1'}, ... ]
    
    Returns
    -------
    run_list
        A dictionary of runs i.e. {'subject1': ['run1', 'run2', ...]; ...}.
    """
    
    run_list = {}
    for run in run_order:
        run_list.setdefault(run['Subject'], []).append(run['Run'])
    
    return run_list

###############################################################################
# Safer globbing

def safe_glob(directory, pattern):
    """
    A version of `glob.glob()` with escaping of special characters.
    
    Parameters
    ----------
    directory : str
        The part of the full pathname to escape.
    pattern : str
        The part of the full pathname to expand.
    
    Returns
    -------
    matches
        Essentially `glob.glob(join(escape(directory), pattern))`.
    """
    
    # Escape the directory (i.e. only allow expansion of the pattern)
    directory = glob.escape(directory)
    return glob.glob(op.join(directory, pattern))

###############################################################################
# argparse wizardry: checks if arguments are valid
# Generic formulation of approach of here: http://stackoverflow.com/a/12117065

def check_argument(arg, formatter, test, description):
    """
    Takes a command line argument, formats it and tests if it is valid.
    
    If it isn't valid, an exception is thrown with an appropriate error
    message.
    
    Parameters
    ----------
    arg : string
    formatter : function
        Applied to `arg` at the start of processing.
    test : function
        Check on the validity of `arg`. If False, `argparse.ArgumentTypeError`
        is raised.
    description : string
        Used to generate the above error message.
    
    Returns
    -------
    arg
        An appropriately formatted version of the input.
    """
    arg = formatter(arg)
    if not test(arg):
        raise argparse.ArgumentTypeError(
            "{description} is not valid! Problematic argument: {arg}"
            .format(description=description, arg=arg))
    return arg

# Filesystem paths
def valid_file(path):
    """
    Checks command line argument is a file that exists.
    """
    return check_argument(
        path,
        lambda x: op.realpath(op.expanduser(x)),
        lambda x: op.isfile(x),
        "File")

def valid_directory(path):
    """
    Checks command line argument is a directory that exists.
    """
    return check_argument(
        path,
        lambda x: op.realpath(op.expanduser(x)),
        lambda x: op.isdir(x),
        "Directory")

def valid_base_directory(path):
    """
    Checks command line argument has a valid parent directory.
    """
    return check_argument(
        path,
        lambda x: op.realpath(op.expanduser(x)),
        lambda x: op.isdir(op.dirname(x)),
        "Base directory")

###############################################################################

def similarity(X, Y=None, *, normalise=True, demean=True):
    """
    Compute similarity between the columns of one or two matrices.
    
    Parameters
    ----------
    X: ndarray
    Y: ndarray, optional
        2D input data. If `Y` provided, must have same first dimension as `X`.
    normalise, demean : bool, optional
        By manipulating these, can compute four different similarity measures.
        See the `Behaviour` section below.
    
    Returns
    -------
    similarity : ndarray
    
    Behaviour
    ---------
    Covariance : normalise=False, demean=True
        https://en.wikipedia.org/wiki/Covariance
    Correlation coefficient : normalise=True, demean=True
        https://en.wikipedia.org/wiki/Pearson_product-moment_correlation_coefficient
    Cosine similarity: normalise=True, demean=False
        https://en.wikipedia.org/wiki/Cosine_similarity
        N.B. also known as the congruence coefficient
        https://en.wikipedia.org/wiki/Congruence_coefficient
    Dot product : normalise=False, demean=False
    """
    
    if Y is None:
        if X.ndim != 2:
            raise ValueError("X must be 2D!")
        Y = X
    
    if X.ndim != 2 or Y.ndim != 2 or X.shape[0] != Y.shape[0]:
        raise ValueError("X and Y must be 2D with the same first dimension!")
    
    if demean:
        X = X - numpy.mean(X, axis=0)
        Y = Y - numpy.mean(Y, axis=0)
    
    if normalise:
        # Set variances to unity
        x = numpy.sqrt(numpy.sum(X**2, axis=0)); x[x < EPSILON] = 1.0
        y = numpy.sqrt(numpy.sum(Y**2, axis=0)); y[y < EPSILON] = 1.0
        X = X / x; Y = Y / y
    else:
        # Or just divide by no. of observations to make an expectation
        X = X / math.sqrt(X.shape[0]); Y = Y / math.sqrt(Y.shape[0])
        
    return X.T @ Y

###############################################################################

def rv_coefficient(X, Y, *, normalise=True, demean=True):
    """
    Compute RV coefficient the columns of two matrices.
    
    Parameters
    ----------
    X, Y: ndarray
        2D input data. Must have same first dimension.
    normalise, demean : bool, optional
        Passed to `similarity(X, Y)` to manipulate the metric computed.
        Default: correlation coefficient.
    
    Returns
    -------
    rv : ndarray
    
    References
    ----------
    https://en.wikipedia.org/wiki/RV_coefficient
    """
    
    # Based on scalar summaries of covariance matrices
    # Sxy = sim(X,Y)
    # covv_xy = Tr(Sxy @ Syx)
    # rv_xy =  covv_xy / sqrt(covv_xx * covv_yy)
    
    # Calculate correlations
    # N.B. trace(Xt @ Y) = sum(X * Y)
    Sxy = similarity(X, Y, normalise=normalise, demean=demean)
    c_xy = numpy.sum(Sxy ** 2)
    Sxx = similarity(X, X, normalise=normalise, demean=demean)
    c_xx = numpy.sum(Sxx ** 2)
    Syy = similarity(Y, Y, normalise=normalise, demean=demean)
    c_yy = numpy.sum(Syy ** 2)
    
    # And put together
    rv =  c_xy / math.sqrt(c_xx * c_yy)
    
    return rv

###############################################################################

def pair_columns(X, Y, *, normalise=True, demean=True):
    """
    Find reordering of two matrices that maximises similarity of columns.
    
    Parameters
    ----------
    X, Y: ndarray
        2D input data. Must have same first dimension.
    normalise, demean : bool, optional
        Passed to `similarity(X, Y)` to manipulate the metric computed.
        Default: correlation coefficient.
    
    Returns
    -------
    sim_xy : list
        Similarity scores between paired columns.
    inds_x, inds_y : list
        Sort orders to apply to columns of `X` and `Y` to generate scores.
    """
    
    # Calculate map similarities
    Sxy = similarity(X, Y, normalise=normalise, demean=demean)
    aSxy = numpy.abs(Sxy)
    
    # Pair everything up
    inds_x = []; inds_y = []
    for n in range(min(Sxy.shape)):
        i_xy = numpy.nanargmax(aSxy)
        i_x,i_y = numpy.unravel_index(i_xy, Sxy.shape)
        inds_x.append(i_x); inds_y.append(i_y)
        # Remove row and column by replacing with NaN
        aSxy[i_x,:] = math.nan; aSxy[:,i_y] = math.nan
    
    # Extract scores
    sim_xy = Sxy[inds_x,inds_y]
    
    # Sort (in descending order)
    inds = numpy.argsort( numpy.abs(sim_xy) )
    inds = inds[::-1]
    
    # Reorder inds etc and return
    sim_xy = [sim_xy[ind] for ind in inds]
    inds_x = [inds_x[ind] for ind in inds]
    inds_y = [inds_y[ind] for ind in inds]
    
    return sim_xy, inds_x, inds_y


def pair_maps(X, Y):
    """
    Wrapper for `pair_columns()` using cosine similarity.
    """
    return pair_columns(X, Y, normalise=True, demean=False)

###############################################################################
# Quick and dirty way of combining multiple sets of spatial maps
# Useful for visualisation

def match_maps(map_list):
    """
    Compute cosine similarity between multiple sets of maps.
    
    Parameters
    ----------
    map_list: list
        All maps must have same first dimension.
    
    Returns
    -------
    inds : list
        Sort orders to apply to columns of input maps to generate scores.
    scores : list
        Similarity of reordered maps and the template.
    template : ndarray
        Consensus set of maps that summarises all of `map_list`.
    """

    if len(map_list) < 2 or map_list[0].ndim != 2 or any(
            map_list[0].shape != maps.shape for maps in map_list[1:]):
        raise ValueError("All sets of maps must be 2D and the same shape!")
    
    # Work out sizes
    order = numpy.random.permutation(len(map_list))
    M = map_list[order[0]].shape[1]
    
    def sort_pair(keys, values):
        values = [v for (k,v)
                in sorted(zip(keys,values), key=lambda pair: pair[0])
            ]
        return values
    
    # Make a template!
    # Start with the first set of maps
    template = map_list[order[0]]
    # And then update as more maps come in
    for ind in order[1:]:
        maps = map_list[ind]
        # Pair up
        corrs, template_inds, map_inds = pair_maps(template, maps)
        map_inds = sort_pair(template_inds, map_inds)
        signs = sort_pair(template_inds, numpy.sign(corrs))
        # And add to the template
        template = template + maps[:,map_inds] * signs
    # Turn into a mean
    template /= len(map_list)
    # Want maps to have positive skew (around zero)
    template *= numpy.sign(numpy.mean(template**3, axis=0))
    
    # And match maps back to the template
    scores = []; inds = []; combined_scores = numpy.zeros((M,))
    for maps in map_list:
        # Pair up
        corrs, template_inds, map_inds = pair_maps(template, maps)
        map_inds = sort_pair(template_inds, map_inds)
        corrs = sort_pair(template_inds, corrs)
        # Record
        scores.append(corrs); inds.append(map_inds)
        combined_scores += numpy.abs(corrs)
    
    # Return in the best order
    sort_order = numpy.argsort(combined_scores)[::-1]
    scores = [[score[i] for i in sort_order] for score in scores]
    inds = [[ind[i] for i in sort_order] for ind in inds]
    template = template[:,sort_order]
    
    return inds, scores, template

###############################################################################

def combine_maps(map_list):
    """
    Combine multiple sets of maps based on cosine similarity.
    
    Parameters
    ----------
    map_list: list
        All maps must have same first dimension.
    
    Returns
    -------
    maps : ndarray
        Large map matrix, containing all maps reordered by similarity and
        interleaved such that similar maps are grouped together.
    """
    
    if len(map_list) < 2 or map_list[0].ndim != 2 or any(
            map_list[0].shape != maps.shape for maps in map_list[1:]):
        raise ValueError("All sets of maps must be 2D and the same shape!")
    
    # Match together
    inds, scores, template = match_maps(map_list)
    
    # And put into one big file
    N = len(map_list)
    V,T = map_list[0].shape
    P = numpy.zeros((V, N * T))
    for n in range(N):
        P[:,n::N] = map_list[n][:, inds[n]] * numpy.sign(scores[n])
    
    return P

###############################################################################

def interleave_maps(map_list):
    """
    Combine multiple sets of maps.
    
    Parameters
    ----------
    map_list: list
        All maps must have same first dimension.
    
    Returns
    -------
    maps : ndarray
        Large map matrix, containing all maps interleaved (but not reordered).
    """

    if len(map_list) < 2 or map_list[0].ndim != 2 or any(
            map_list[0].shape != maps.shape for maps in map_list[1:]):
        raise ValueError("All sets of maps must be 2D and the same shape!")
    
    N = len(map_list)
    V,T = map_list[0].shape
    P = numpy.zeros((V, N * T))
    for n in range(N):
        P[:,n::N] = map_list[n]
    
    return P

###############################################################################
# Manipulation of covariance / correlation

def r_to_z(r, *args, **kwargs):
    """
    Computes the Fisher z-transformation.
    
    References
    ----------
    https://en.wikipedia.org/wiki/Fisher_transformation
    """
    
    return numpy.arctanh(r, *args, **kwargs)


def z_to_r(z, *args, **kwargs):
    """
    Computes the inverse of the Fisher z-transformation.
    
    References
    ----------
    https://en.wikipedia.org/wiki/Fisher_transformation
    """
    
    return numpy.tanh(z, *args, **kwargs)


def normalise_covmat(covmat):
    """
    Turns a covariance matrix into a correlation matrix.
    
    Parameters
    ----------
    covmat: ndarray
    
    Returns
    -------
    corrmat : ndarray
        `corrmat` normalised such that all diagonals are unity.
    """
    
    if covmat.ndim != 2 or covmat.shape[0] != covmat.shape[1]:
        raise ValueError("Covariance matrix must be 2D and square!")
    
    std = numpy.sqrt(numpy.diag(covmat))
    std[std < EPSILON] = 1.0
    return covmat / numpy.outer(std, std)

###############################################################################

def precmat_to_netmat_elems(precmat):
    """
    Returns the transformed upper triangle of a precision matrix.
    
    Parameters
    ----------
    precmat: ndarray
    
    Returns
    -------
    netmat_elems : ndarray
        Vectorised upper triangle of the partial correlations. Transformation
        steps are:
         + Precision matrix to partial correlations.
         + Fisher's z-transformation applied to partial correlation
           coefficients.
    """
     
    # Work out sizes
    M = precmat.shape[0]
    triu_inds = numpy.triu_indices(M, 1)
    
    # Turn precision matrix into partial correlations
    netmat = - normalise_covmat(precmat)
    
    # Extract upper triangular elements
    netmat_elems = netmat[triu_inds]
    
    # Fisher transform
    netmat_elems = r_to_z(netmat_elems)
    
    return netmat_elems

###############################################################################

def hdi(distribution, interval_width):
    """
    Calculate highest density interval (HDI) for a distribution.
    
    See Kruschke, Doing Bayesian Data Analysis [2nd Edition], 2014.
    
    Parameters
    ----------
    distribution : scipy.stats.rv_continuous
        A continuous, univariate distribution, with frozen parameters.
        Must implement distribution.ppf(x).
    interval_width : float
        The width of the confidence interval, between 0.0 and 1.0.
    
    Returns
    -------
    low, high
        The calculated interval.
    
    Notes
    -----
    Kruschke, BDA, Chapter 25.2.
    The key result is that, for a unimodal distribution, the HDI is the
    narrowest possible interval such that it covers a given probability mass.
    """
    
    alpha = interval_width
    if alpha <= 0.0 or alpha >= 1.0:
        raise ValueError("hdi(): Interval must be between 0.0 and 1.0!")
    
    # Function to compute interval width
    def interval_width(low_p, distribution, alpha):
        high_p = low_p + alpha
        width = distribution.ppf(high_p) - distribution.ppf(low_p)
        return width
    
    # Initial guess - equal mass in each tail
    low_p = (1.0 - alpha) / 2.0
    
    # find low_p that minimizes interval_width
    low_p, *_ = scipy.optimize.fmin(
        interval_width, low_p, args=(distribution, alpha),
        ftol=1.0e-5, disp=False)
    
    # And convert back to bounds on x
    low_x = distribution.ppf(low_p)
    high_x = distribution.ppf(alpha + low_p)
    
    return low_x, high_x
    
    '''
    import scipy.stats
    gaussian = scipy.stats.norm(0.0, 1.0)
    utils.hdi(gaussian, 0.95)
    # https://en.wikipedia.org/wiki/Normal_distribution#Quantile_function
    # [-1.959, 1.959]
    '''
###############################################################################
