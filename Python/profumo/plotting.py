# -*- coding: utf-8 -*-

# PROFUMO
# Framework for variational Bayesian inference of modes from fMRI data

# - Sam Harrison 2018
# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

# plotting.py
"""
Customisations and useful plotting functions based on Matplotlib.
"""

###############################################################################

import sys
import os, os.path as op
import subprocess
import math
import random
import numpy, numpy.fft
import scipy.stats
import nibabel
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

import profumo.utilities as utils

# https://matplotlib.org/tutorials/introductory/usage.html
# https://matplotlib.org/users/dflt_style_changes.html

###############################################################################

# Load custom style
style_file = op.join(op.dirname(__file__), 'profumo.mplstyle')
plt.style.use(style_file)

# Standard colours for lines
LINE_BG = (0.7,)*3  # Background
"""Colour for background lines."""
LINE_FG = (0.4,)*3  # Foreground
"""Colour for foreground lines."""

###############################################################################
# Pretty boxplot

def boxplot(fig, ax, x, *, labels=None, xlabel=None, ylabel=None, **kwargs):
    """
    Prettifies `ax.boxplot()`.
    
    Parameters
    ----------
    fig, ax
        Matplotlib Figure and Axes objects.
    x : ndarray, or list of vectors
        Input data.
    
    Returns
    -------
    bp
        As returned from `ax.boxplot()`.
    
    Other Parameters
    ----------------
    labels : list, optional
        Labels for different datasets in `x`.
    xlabel, ylabel : string, optional
    **kwargs : optional
        Passed to `ax.boxplot()`.
    """
    
    # Plotting properties
    # Move to rcParams?!?
    boxprops     = dict(linewidth=2.0, color=LINE_FG, zorder=10)
    medianprops  = dict(linewidth=2.0, color='red')
    whiskerprops = dict(linewidth=2.0, color=LINE_BG, linestyle='-')
    capprops     = dict(linewidth=2.0, color=LINE_BG, linestyle='-')
    flierprops   = dict(marker='o', markersize=2.0, markeredgecolor='red',
                        markerfacecolor='red', zorder=0)
    
    # Better labels for lots of boxes
    if (x.ndim == 2 and x.shape[1] > 20):
        # Select better formatter and stop boxplot changing it
        ax.get_xaxis().set_major_locator(mpl.ticker.AutoLocator())
        manage_xticks = False
    else:
        manage_xticks = True
    
    # Draw the boxplot
    bp = ax.boxplot(x, labels=labels, notch=True, widths=0.65,
        boxprops=boxprops, medianprops=medianprops, whiskerprops=whiskerprops,
        capprops=capprops, flierprops=flierprops, manage_xticks=manage_xticks,
        **kwargs)
    
    # Horizontally jitter the fliers
    for i,f in enumerate(bp['fliers']):
        # Get extent of caps (there are 2 caps for each set of fliers)
        x_lims = bp['caps'][2*i].get_xdata();
        x_range = x_lims[1] - x_lims[0]
        # Add some random jitter to the x-locations
        xdata = f.get_xdata()
        R = numpy.random.uniform(-0.5, 0.5, xdata.shape)
        xdata = xdata + 0.6 * x_range * R
        f.set_xdata(xdata)
    
    # Labels
    if xlabel is not None:
        ax.set_xlabel(xlabel)
    if ylabel is not None:
        ax.set_ylabel(ylabel)
    
    return bp

###############################################################################

def kde_scatter(fig, ax, x, y, *, log_density=False,
                xlabel=None, ylabel=None, **kwargs):
    """
    2D scatter plot, with points coloured by density.
    
    Parameters
    ----------
    fig, ax
        Matplotlib Figure and Axes objects.
    x, y : ndarray
        Input data.
    
    Returns
    -------
    s, cbar
        As returned from `ax.scatter()` and `fig.colorbar()`.
    
    Other Parameters
    ----------------
    log_density : bool, optional
        Whether to display the probability density in log space.
    xlabel, ylabel : string, optional
    **kwargs : optional
        Passed to `ax.scatter()`.

    References
    ----------
    https://stackoverflow.com/a/20107592
    """
    
    # Limit the number of points for computational reasons
    if x.shape[0] > 250000:
        inds = random.sample(range(x.shape[0]), 250000)
        x = x[inds]; y = y[inds]
    
    # Calculate the point density
    XY = numpy.stack([x,y], axis=0)
    # Subset of points for evaluation
    if XY.shape[1] > 2500:
        xy = XY[:, random.sample(range(XY.shape[1]), 2500)]
    else:
        xy = XY
    # Compute the density on subset, evaluate everywhere
    z = scipy.stats.gaussian_kde(xy).evaluate(XY)
    
    # Sort the points by density, so that the densest points are plotted last
    idx = z.argsort()
    x, y, z = x[idx], y[idx], z[idx]
    
    if log_density:
        z = numpy.log(z)
        # Clip to improve colour-range (don't want 10^-100 ruining things)
        z_min = numpy.percentile(z, 1.0)
        z = numpy.clip(z, a_min=z_min, a_max=None)
    
    # And plot!
    #ax.axis('square') # equal / square / scaled
    s = ax.scatter(x, y, c=z, s=5.0, edgecolor='')
    cbar = fig.colorbar(s, ax=ax, ticks=mpl.ticker.MaxNLocator(nbins=6))
    
    # Labels
    if xlabel is not None:
        ax.set_xlabel(xlabel)
    if ylabel is not None:
        ax.set_ylabel(ylabel)
    if log_density:
        cbar.set_label("Point log-density", rotation=270, labelpad=20)
    else:
        cbar.set_label("Point density", rotation=270, labelpad=20)
    
    return s, cbar

###############################################################################
# Draws a matrix

def plot_matrix(fig, ax, X, *, cmap=None, vmax=None, vmin=None,
                title=None, xlabel=None, ylabel=None, clabel=None, **kwargs):
    """
    Prettifies `ax.imshow()`.
    
    Parameters
    ----------
    fig, ax
        Matplotlib Figure and Axes objects.
    X : ndarray
        Input data matrix.
    
    Returns
    -------
    im, cbar
        As returned from `ax.imshow()` and `fig.colorbar()`.
    
    Other Parameters
    ----------------
    cmap : string or Matplotlib Colormap
    vmax, vmin : float
        Data ranges for plotting.
        Default vmax: 98.5 percentile of `X`.
        Default vmin: `-vmax`.
    title, xlabel, ylabel, clabel : string, optional
    **kwargs : optional
        Passed to `ax.imshow()`.
    """
    
    # Get data range
    if vmax is None:
        #vmax = numpy.nanmax(numpy.abs(X))
        vmax = numpy.nanpercentile(numpy.abs(X), 98.5)
    if vmin is None:
        vmin = - vmax
    
    # Plot matrix
    im = ax.imshow(X, vmax=vmax, vmin=vmin, cmap=cmap, **kwargs)
    cbar = fig.colorbar(im, ax=ax, ticks=mpl.ticker.MaxNLocator(nbins=6))
    
    # Labels
    if xlabel is not None:
        ax.set_xlabel(xlabel)
    if ylabel is not None:
        ax.set_ylabel(ylabel)
    if clabel is not None:
        cbar.set_label(clabel, rotation=270, labelpad=20)
    
    # Title
    if title is not None:
        ax.set_title(title)
    
    # Return key objects
    return im, cbar

###############################################################################
# Draws a correlation / covariance matrix

# Covariance - just a matrix with a blue/red colour map
def plot_covariance_matrix(fig, ax, C, *, cmap=mpl.cm.RdBu_r,
                           clabel="Covariance", **kwargs):
    """
    Wrapper around `plot_matrix()` with sensible defaults for covariance.
    """
    
    return plot_matrix(fig, ax, C, cmap=cmap, clabel=clabel, **kwargs)

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# Correlation - covariance between -1 and 1
def plot_correlation_matrix(fig, ax, C, *, vmax=1.0, vmin=-1.0,
                            clabel="Correlation coefficient", **kwargs):
    """
    Wrapper around `plot_matrix()` with sensible defaults for correlations.
    """
    
    # Pass to draw covariance matrix, changing limits
    return plot_covariance_matrix(fig, ax, C, vmax=vmax, vmin=vmin,
                                  clabel=clabel, **kwargs)

###############################################################################

def render_maps(map_file, output_filename, *, background_file=None, volume=None,
                n_rows=4, n_cols=8, size=(1000, 600), display_range=None,
                colour_bar=True, cmap='red-yellow', negative_cmap=None):
    """
    Use FSLeyes to render maps in a lightbox view.
    
    Parameters
    ----------
    map_file : string
        Path to input (NIFTI) file.
    output_filename : string
    
    Other Parameters
    ----------------
    Options are as per the FSLeyes command line. See `fsleyes --fullhelp`.
    background_file : string
        Path to (NIFTI) file to overlay maps on.
    volume : int
        Which volume from `map_file` to plot.
    n_rows, n_cols, size
        Key options controlling size / shape of output image.
    """
    
    n_slices = n_rows * n_cols
    # Evenly space throughout volume
    maps = nibabel.load(str(map_file))
    nz = maps.shape[2]; zz = maps.header.get_zooms()[2]
    # Space between slices, in voxels
    d_nz = nz / n_slices
    # Start half a slice into volume
    nz_min = math.floor(d_nz / 2)
    # Recalculate slice spacing (in scaled voxels)
    d_zz = zz * (nz - 2 * nz_min) / n_slices
    
    # Generate command
    command = ['fsleyes', 'render', '-of', str(output_filename),
               '--scene', 'lightbox', '--zaxis', 'Z', '--hideCursor',
               '--nrows', str(n_rows), '--ncols', str(n_cols),
               '--size', str(size[0]), str(size[1]),
               '--voxelLoc', '0', '0', str(nz_min),
               '--sliceSpacing', str(d_zz)]
    
    if colour_bar:
        command += ['--showColourBar', '--colourBarLocation', 'right']
    
    # Background
    if background_file is not None:
        command += [str(background_file)]
    
    # And the maps themselves!
    command += [str(map_file), '--cmap', cmap]
    
    if volume is not None:
        command += ['--volume', str(volume)]
    
    if negative_cmap is not None:
        command += ['--useNegativeCmap', '--negativeCmap', negative_cmap]
    
    if display_range is not None:
        command += ['--displayRange', str(display_range[0]), str(display_range[1])]
    
    # Set up a virtual framebuffer, if we can
    # https://users.fmrib.ox.ac.uk/~paulmc/fsleyes/userdoc/latest/command_line.html?highlight=xvfb
    # Note that FSLeyes uses the native macOS WindowServer, so on Mac we
    # cannot use this virtual X-window trick. Fortunately, however, xvfb-run
    # does not exist in the Mac XQuartz package so we don't run it anyway.
    # https://superuser.com/q/962490
    # Use `command` as it is guaranteed to be present (`type` is optional)
    # https://en.wikipedia.org/wiki/List_of_Unix_commands
    # https://stackoverflow.com/a/677212
    # But `command` is a built in so have to use `shell=True`
    # https://stackoverflow.com/a/46417720
    xvfb_test = subprocess.run(
            ['command', '-v', 'xvfb-run'], shell=True,
            stdout=subprocess.DEVNULL)
    xvfb_present = (xvfb_test.returncode == 0)
    if xvfb_present:
        # Use a random display number!
        # Numbers < 100 are often in use (e.g. for VNC, see `ls /tmp/.X*`) but
        # `xvfb-run --auto-display` doesn't detect those
        display_number = random.randint(10000, 99999)
        command = ['xvfb-run',
                   '-n', str(display_number),
                   '-s', '-screen 0 640x480x24'] + command
    
    try:
        job = subprocess.run(command, check=True, stdout=subprocess.PIPE)
        # Useful summary of subprocess with and without shell=True
        # https://stackoverflow.com/a/15109975
    except subprocess.CalledProcessError as error:
        print(error.stdout, file=sys.stderr)
        raise error
    
    return

###############################################################################

def plot_map(fig, ax, map, *, reference_map=None,
             xlabel="Voxel", ylabel="Map weight"):
    """
    Plots a single spatial map as a 1D plot.
    
    Parameters
    ----------
    fig, ax
        Matplotlib Figure and Axes objects.
    map : ndarray
        Spatial map vector.
    
    Other Parameters
    ----------------
    reference_map : ndarray, optional
        Optional spatial map vector to be plotted in the background.
    xlabel, ylabel : string, optional
    """
    
    MS = 1.0  # markersize
    
    # Guide line at y = 0.0
    ax.plot([0, map.shape[0]], [0.0, 0.0], color=LINE_BG)
    
    if reference_map is None:
        ax.plot(map, '.', color='C0', markersize=MS)
    else:
        voxels = numpy.arange(map.shape[0])
        diff_map = map - reference_map
        max_diff = numpy.abs(numpy.max(diff_map))
        # Colormap: blue - grey - red
        # https://stackoverflow.com/a/35711545
        cmap = mpl.colors.LinearSegmentedColormap.from_list('name',
            ['mediumblue', 'royalblue', 'darkgrey', 'indianred', 'crimson'])
        # And plot!
        sc = ax.scatter(
            voxels, map, c=diff_map, marker='.', s=MS**2, cmap=cmap,
            norm=mpl.colors.Normalize(vmin=-max_diff, vmax=max_diff))
        # https://stackoverflow.com/a/44868640
        cbaxes = inset_axes(ax, width="30%", height="3%", loc=1)
        cbar = fig.colorbar(sc, cax=cbaxes, orientation="horizontal",
                            ticks=mpl.ticker.MaxNLocator(nbins=4))
    
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    
    return

###############################################################################

def plot_time_course(fig, ax, time_course, *, TR=None,
                     noisy_time_course=None, decorr_time_course=None,
                     xlabel=None, ylabel="Activity"):
    """
    Plots a single time course as a 1D plot.
    
    Parameters
    ----------
    fig, ax
        Matplotlib Figure and Axes objects.
    time_course : ndarray
        Time course vector.
    
    Other Parameters
    ----------------
    TR : float, optional
        Spacing between time points in seconds.
    noisy_time_course : ndarray, optional
        Optional time course vector, of the full posterior including noise
        terms, to be plotted in the background.
    decorr_time_course : ndarray, optional
        Optional time course vector, of the time course after decorrelation
        with respect to the HRF, to be plotted in the foreground.
    xlabel, ylabel : string, optional
    """
    
    if xlabel is None:
        if TR is None:
            xlabel="Time point"
        else:
            xlabel="Time (s)"
    
    t = numpy.arange(time_course.shape[0])
    if TR is not None:
        t = t * TR
    
    # Guide line at y = 0.0
    ax.plot([t[0], t[-1]], [0.0, 0.0], color=LINE_BG)
    
    if noisy_time_course is not None:
        ax.plot(t, noisy_time_course, color=LINE_FG, label='Noisy')
        # Plot clean TC over the top in white for clarity
        plt.plot(t, time_course, 'w', linewidth=6.0)
    
    ax.plot(t, time_course, 'C0', linewidth=3.0, label='Clean')
    
    if decorr_time_course is not None:
        ax.plot(t, decorr_time_course, color=[1.0,0.7,0.7])
        ax.plot(t, decorr_time_course, color=[1.0,0.4,0.4], linestyle='dashed', dashes=[15.0, 7.5], label='Decorrelated')
    
    # Labels etc
    if noisy_time_course is not None or decorr_time_course is not None:
        ax.legend()
    
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    
    ylim = ax.get_ylim()
    ax.set_yticks([0.6 * ylim[0], 0.0, 0.6 * ylim[1]])
    ax.set_yticklabels(["-ve", "0", "+ve"])
    
    return

###############################################################################

def plot_time_course_ffts(
        fig, ax, time_courses, *, TR=None, noisy_time_courses=None,
        xlabel=None, ylabel="Amplitude (a.u.)"):
    """
    Plots the distribution over FFTs of a set of time courses.
    
    Parameters
    ----------
    fig, ax
        Matplotlib Figure and Axes objects.
    time_courses : ndarray
        Time course matrix.
    
    Other Parameters
    ----------------
    TR : float, optional
        Spacing between time points in seconds.
    noisy_time_courses : ndarray
        Time course matrix to be plotted in background.
    xlabel, ylabel : string, optional
    """
    
    if xlabel is None:
        if TR is None:
            xlabel="Frequency"
            TR = 1.0
        else:
            xlabel="Frequency (Hz)"
    
    f = numpy.fft.rfftfreq(time_courses.shape[1], d=TR); F = len(f)
    def plot_ffts(time_courses, color, label):
        # Calculate FFTs
        fft = numpy.fft.rfft(time_courses, axis=1)
        fft = numpy.abs(fft)
    
        # Calculate confidence region in log space
        f10 = numpy.exp(numpy.percentile(numpy.log(fft), 10.0, axis=0))
        f25 = numpy.exp(numpy.percentile(numpy.log(fft), 25.0, axis=0))
        f50 = numpy.exp(numpy.percentile(numpy.log(fft), 50.0, axis=0))
        f75 = numpy.exp(numpy.percentile(numpy.log(fft), 75.0, axis=0))
        f90 = numpy.exp(numpy.percentile(numpy.log(fft), 90.0, axis=0))

        # Hack - plot transparent on white background to get lighter colour but
        # stop blending to a grey mess
        ax.fill_between(
                f, f10, f90, color='w',
                linewidth=0.0, zorder=-1)
        ax.fill_between(
                f, f10, f90, color=color, alpha=0.2,
                linewidth=0.0, zorder=-1)
        ax.fill_between(
                f, f25, f75, color=color, alpha=0.3,
                linewidth=0.0, zorder=-1)
        
        # And plot a summary
        ax.plot(f, f50, color=color, label=label)
        #ax.plot(f, numpy.median(fft, axis=0), color=color, label=label)
        
        return
    
    if noisy_time_courses is not None:
        plot_ffts(noisy_time_courses, 'C1', "Noisy")
    plot_ffts(time_courses, 'C0', "Clean")
    
    # Labels etc
    if noisy_time_courses is not None:
        ax.legend()
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    
    ax.set_yticks([0.0]); ax.set_yticklabels(["0"])
    
    return

###############################################################################
