# -*- coding: utf-8 -*-

# PROFUMO
# Framework for variational Bayesian inference of modes from fMRI data

# - Sam Harrison 2018
# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

# report.py
"""
Generates an HTML report from a `.pfm` directory.
"""

###############################################################################

import os, os.path as op
import warnings
import shutil
import time
import collections
import math
import numpy
import random
import jinja2

# Don't assume we have an X server running: set the backend before loading
# pyplot, or indeed any modules that may themselves load it
# http://matplotlib.org/faq/usage_faq.html#what-is-a-backend
# https://matplotlib.org/faq/howto_faq.html#matplotlib-in-a-web-application-server
# https://stackoverflow.com/q/26679901
import matplotlib as mpl
mpl.use('Agg')  # Backend that doesn't require Xserver
import matplotlib.pyplot as plt

import profumo.pfms
import profumo.io as io
import profumo.utilities as utils
import profumo.plotting as pplt

###############################################################################
# Class which generates the report

class WebReport:
    """
    Generates a HTML report from a `.pfm` (and `.ppp`) directory.
    
    Note that the entire report is generated by the constructor, so all you
    have to do is instantiate the class.
    
    Attributes
    ----------
    pfms
        PFMs class that interfaces with the `.pfm` results directory.
    directory : str
        Path to the output `.pwr` directory.
    postprocessing_directory : str, None
        Path to the `.ppp` directory, if specified.
    subjects, runs, modes, etc
        There are a panoply of members that deal with the subset of subjects /
        runs / modes used to generate the report.
    """
    
    ###########################################################################
    
    def __init__(self, pfm_directory, output_directory,
                 postprocessing_directory=None):
        """
        Initialises the WebReport class, and generates the report itself.
        
        Parameters
        ----------
        pfm_directory : str
            Path to the `.pfm` directory.
        output_directory : str
            Name and location of the `.pwr` directory to create.
        
        Other Parameters
        ----------------
        postprocessing_directory : str
            Path to the `.ppp` directory. If provided, the same subjects / runs
            / modes will be used, and if the data is in NIFTI format then the
            report will contain renderings of the volumetric data.
        """
        
        print("Generating web report...")
        
        # Make the output directory
        output_directory = op.realpath(op.expanduser(output_directory))
        self.directory = output_directory
        # Check extension / uniqueness
        if not self.directory.endswith('.pwr'):
            self.directory += '.pwr'
        while op.exists(self.directory):
            self.directory += '+'
        if self.directory != output_directory:
            warnings.warn(
                    "Had to modify output directory!\n"
                    +"  Old: {}\n".format(output_directory)
                    +"  New: {}".format(self.directory),
                    stacklevel=0)
        # And make!
        os.mkdir(self.directory)
        print("Report directory: {d}".format(d=self.directory))
        print("Done.")
        print()
        
        # Initialise key variables
        self.pfms = profumo.pfms.PFMs(pfm_directory)
        if postprocessing_directory is not None:
            postprocessing_directory = \
                op.realpath(op.expanduser(postprocessing_directory))
        self.postprocessing_directory = postprocessing_directory
        
        # Set subjects/runs/modes
        if self.postprocessing_directory is None:
            self.subjects = self.pfms.subjects
            self.runs = self.pfms.runs
            self.modes = numpy.arange(self.pfms.M)
            self.signs = numpy.ones(self.pfms.M)
        else:
            # Load extras from postprocessing directory
            self.runs = io.load_json(op.join(
                self.postprocessing_directory, 'Runs.json'))
            self.subjects = list(self.runs.keys())
            self.modes = io.load_csv(op.join(
                self.postprocessing_directory, 'Modes.csv'), dtype=int)
            self.signs = io.load_csv(op.join(
                self.postprocessing_directory, 'Signs.csv'), dtype=int)
        
        # Select a subset of runs / subjects to keep computation down
        N_MAX = 50
        # Limit number of subjects and runs
        if len(self.subjects) > N_MAX:
            # If too many subjects, just keep one run per subject
            self.subjects = sorted(random.sample(self.subjects, N_MAX))
            self.runs = {subject: [random.choice(self.pfms.runs[subject])]
                for subject in self.subjects}
        elif sum(len(self.runs[subject]) for subject in self.runs) > N_MAX:
            # If too many runs, still keep at least one run per subject
            # Inds specify which of the 'excess' runs within subjects to keep
            S = len(self.subjects)
            R = sum(len(self.runs[run]) for run in self.runs)
            inds = sorted(random.sample(range(R - S), N_MAX - S))
            # Loop over runs, selecting a random subset within each subject
            r = 0
            for subject,runs in self.runs.items():
                n_keep = sum((i >= r) and (i < r + len(runs) - 1) for i in inds)
                self.runs[subject] = sorted(random.sample(runs, n_keep + 1))
                r = r + len(runs) - 1
        
        # And then some useful data on the runs/subjects
        # Fixed orders for plots
        self.subject_order = sorted(self.subjects)
        self.run_order = [{'Subject': subject, 'Run': run}
            for subject in self.subject_order
                for run in sorted(self.runs[subject])]
        # Key sizes
        self.M = len(self.modes)
        self.S = len(self.subject_order)
        self.R = len(self.run_order)
        # And then a key subject/run for example plots
        self.key_subject = random.choice(self.subjects)
        self.key_run = random.choice(self.runs[self.key_subject])
        
        # Keep mode namings consistent
        n_zeros = int(math.log10(self.M)) + 1
        self.mode_names = ['{m:0{z}d}'.format(m=m, z=n_zeros)
            for m in range(self.M)]
        
        # Make output directories
        self.figure_directory = op.join(self.directory, 'figures')
        os.mkdir(self.figure_directory)
        # Copy CSS etc
        shutil.copy(
            op.join(op.dirname(__file__), 'templates', 'profumo.css'),
            op.join(self.directory, '.profumo.css'))
        shutil.copy(
            op.join(op.dirname(op.dirname(op.dirname(__file__))), 'References.bib'),
            op.join(self.directory, 'references.bib'))
        # And save some key files
        io.save_csv(
            self.modes, op.join(self.directory, 'modes.csv'), fmt='%d')
        io.save_csv(
            self.signs, op.join(self.directory, 'signs.csv'), fmt='%+d')
        io.save_json(
            self.subject_order, op.join(self.directory, 'subjects.json'))
        io.save_json(
            self.run_order, op.join(self.directory, 'runs.json'))
        
        # Initialise Jinja
        self.env = jinja2.Environment(
            loader=jinja2.PackageLoader('profumo', 'templates'),
            autoescape=jinja2.select_autoescape(['html', 'xml']),
            trim_blocks=True, lstrip_blocks=True
        )
        
        # Store some useful Jinja args
        self.template_args = {key: self.__dict__[key] for key in
            ['mode_names', 'key_subject', 'key_run']}
        self.template_args['pfm_directory'] = self.pfms.directory
        self.template_args['version'] = self.pfms.config['> Version']
        self.template_args['timestamp'] = \
            time.strftime("%d %b %Y at %H:%M %Z", time.localtime())
        
        # Cache some useful results
        self.Pg = self.pfms.load_group_maps(
                self.modes, self.signs, verbose=False)
        self.Ps = self.pfms.load_subject_maps(
                self.subjects, self.modes, self.signs, verbose=False)
        self.Hsr = self.pfms.load_run_amplitudes(
                self.runs, self.modes, verbose=False)
        self.Asr = self.pfms.load_run_time_courses(
                self.runs, self.modes, self.signs,
                clean_time_courses=True, verbose=False)
        
        # Make report sections
        self.render_index_page()
        self.render_mode_pages()
        self.render_summary_page()
        
        print("Report generated. You can now open it in a browser:\n  "
              + op.join(self.directory, 'index.html'))
        print()
        return
    
    ###########################################################################
    ###########################################################################
    
    def render_index_page(self):
        """
        Generates the `index.html` page.
        """
        
        print("Rendering index page...")
        template = self.env.get_template('index.html')
        
        template_args = self.template_args.copy()
        template_args['postprocessing_directory'] = \
            self.postprocessing_directory
        
        # Render!
        rendered_template = template.render(template_args)
        index_page_filename = op.join(self.directory, 'index.html')
        with open(index_page_filename, 'x') as index_page:
            index_page.write(rendered_template)
        
        print("Done.")
        print()
        return
    
    ###########################################################################
    
    def render_mode_pages(self):
        """
        Generates the `mode_???.html` pages.
        """
        
        template = self.env.get_template('mode.html')
        
        # Make the figure directories
        figure_dirs = [op.join(self.figure_directory, 'mode_' + mode_name)
            for mode_name in self.mode_names]
        for figure_dir in figure_dirs:
            os.mkdir(figure_dir)
        
        # Make the figures
        map_figs = self.plot_mode_maps(figure_dirs)
        tc_figs  = self.plot_mode_time_courses(figure_dirs)
        
        print("Rendering mode pages...")
        for m in range(self.M):
            print("Mode {m:d}".format(m=m))
            # Pull args together
            template_args = self.template_args.copy()
            template_args['mode'] = m
            
            template_args = {**template_args, **map_figs[m], **tc_figs[m]}
            
            # Render!
            rendered_template = template.render(template_args)
            mode_page_filename = op.join(
                self.directory, 'mode_' + self.mode_names[m] + '.html')
            with open(mode_page_filename, 'x') as mode_page:
                mode_page.write(rendered_template)
        
        print("Done.")
        print()
        return
    
    ###########################################################################
    
    def render_summary_page(self):
        """
        Generates the `summary_stats.html` page.
        """
        
        template = self.env.get_template('summary_stats.html')
        
        # Make the figure directory
        figure_dir = op.join(self.figure_directory, 'summary_stats')
        os.mkdir(figure_dir)
        
        # Make the figures
        map_figs = self.plot_map_summaries(figure_dir)
        amp_figs = self.plot_amplitude_summaries(figure_dir)
        tc_figs  = self.plot_time_course_summaries(figure_dir)
        
        # Pull args together
        template_args = self.template_args.copy()
        
        template_args = {**template_args, **map_figs, **amp_figs, **tc_figs}
        
        # Render!
        print("Rendering summary page...")
        rendered_template = template.render(template_args)
        summary_page_filename = op.join(
            self.directory, 'summary_stats.html')
        with open(summary_page_filename, 'x') as summary_page:
            summary_page.write(rendered_template)
        
        print("Done.")
        print()
        return
    
    ###########################################################################
    ###########################################################################
    
    def plot_mode_maps(self, figure_dirs):
        """
        Generates the figures based on each mode's spatial maps.
        
        Parameters
        ----------
        figure_dirs : list
            A list of directories to save figures in, one per mode.
        """
        
        print("Generating spatial map figures...")
        map_figs = [{} for m in range(self.M)]
        
        # PLOT POSTERIORS TOO!
        
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        
        # Use FSLeyes to render maps if possible
        if self.postprocessing_directory:
            map_dir = op.join(self.postprocessing_directory, 'Maps')
            reference = op.join(map_dir, 'Reference.nii.gz')
            group_maps = op.join(map_dir, 'Group.nii.gz')
            subject_maps = op.join(
                map_dir, 'sub-' + self.key_subject + '.nii.gz')
            maps_present = all(op.isfile(filename) for filename in
                    [reference, group_maps, subject_maps])
        else:
            maps_present = False
        
        if maps_present:
            # Have to render each volume in turn
            for m in range(self.M):
                print("Mode {m:d}".format(m=m))
                # Group
                figure_name = op.join(figure_dirs[m], 'group_map.png')
                pplt.render_maps(
                    group_maps, figure_name, volume=m,
                    display_range=(0.1, 3.0), negative_cmap='blue-lightblue',
                    background_file=reference)
                map_figs[m]['group_map'] = op.relpath(figure_name, self.directory)
                # Subject
                figure_name = op.join(figure_dirs[m], 'subject_map.png')
                pplt.render_maps(
                    subject_maps, figure_name, volume=m,
                    display_range=(0.1, 3.0), negative_cmap='blue-lightblue',
                    background_file=reference)
                map_figs[m]['subject_map'] = op.relpath(figure_name, self.directory)
        
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        
        # Otherwise use matplotlib
        else:
            # And plot each volume in turn
            for m in range(self.M):
                print("Mode {m:d}".format(m=m))
                map_figs[m]['matplotlib_map'] = True
                # Group
                figure_name = op.join(figure_dirs[m], 'group_map.png')
                fig, ax = plt.subplots()
                pplt.plot_map(fig, ax, self.Pg[:,m])
                fig.savefig(figure_name)
                plt.close(fig)
                map_figs[m]['group_map'] = op.relpath(figure_name, self.directory)
                # Subject
                figure_name = op.join(figure_dirs[m], 'subject_map.png')
                Pk = self.Ps[self.key_subject]
                fig, ax = plt.subplots()
                pplt.plot_map(fig, ax, Pk[:,m], reference_map=self.Pg[:,m])
                fig.savefig(figure_name)
                plt.close(fig)
                map_figs[m]['subject_map'] = op.relpath(figure_name, self.directory)
        
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        
        print("Done.")
        print()
        return map_figs
    
    ###########################################################################
    
    def plot_mode_time_courses(self, figure_dirs):
        """
        Generates the figures based on each mode's time courses.
        
        Parameters
        ----------
        figure_dirs : list
            A list of directories to save figures in, one per mode.
        """
        
        print("Generating time course figures...")
        time_course_figs = [{} for m in range(self.M)]
        
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        
        # Load time courses
        Akk = self.Asr[self.key_subject][self.key_run] # A_key_key
        if 'HRF' in self.pfms.time_course_model:
            T = Akk.shape[1]; TR = self.pfms.config['HRF parameters']['TR (s)']
            # Load noisy time courses
            noisy_Akk = self.pfms.load_run_time_courses(
                    {self.key_subject: [self.key_run]},
                    self.modes, self.signs, clean_time_courses=False,
                    verbose=False)[self.key_subject][self.key_run]
            # And do HRF decorrelation
            hrf_file = op.join(
                self.pfms.model_directory, 'GroupTemporalModel',
                'HRFCovariance_T' + str(T) + '.hdf5')
            K = io.load_hdf5(hrf_file)
            K = 0.975 * K + 0.025 * numpy.eye(T)  # See C++/Source/MFModels/A/KroneckerHRF.c++
            L = numpy.linalg.cholesky(K)
            decorr_Akk = numpy.linalg.solve(L, Akk.T).T
            # As I understand it, the Cholesky decomposition essentially
            # enforces a causal structure by limiting coefficients to the upper
            # triangle (it's a DAG, right?) - in other words, it does something
            # much more similar to 'true' deconvolution than
            # `decorr_Akk = Akk * K^-0.5`
            # x ~ N(0,K), sample via: K = L @ L.T; z ~ N(0,I); x = L @ z + m
            # So, with m=0, z = solve(L,x)
        
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        
        # Plot example time courses from each mode in turn
        for m in range(self.M):
            print("Mode {m:d}".format(m=m))
            figure_name = op.join(figure_dirs[m], 'time_course.png')
            fig, ax = plt.subplots()
            if 'HRF' in self.pfms.time_course_model:
                pplt.plot_time_course(fig, ax, Akk[m,:],
                    noisy_time_course=noisy_Akk[m,:],
                    decorr_time_course=decorr_Akk[m,:],
                    TR=TR, ylabel="BOLD response")
            else:
                pplt.plot_time_course(fig, ax, Akk[m,:])
            fig.savefig(figure_name)
            plt.close(fig)
            time_course_figs[m]['time_course'] = op.relpath(figure_name, self.directory)
        
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        
        counts = collections.Counter(
            [self.Asr[run['Subject']][run['Run']].shape[1]
                for run in self.run_order])
        T, n_T = counts.most_common(1)[0]
        
        # Load noisy TCs
        if 'HRF' in self.pfms.time_course_model:
            hrf_model = True
            nAsr = self.pfms.load_run_time_courses(
                    self.runs, self.modes, self.signs,
                    clean_time_courses=False, verbose=False)
        else:
            hrf_model = False
        
        # And then the FFTs
        for m in range(self.M):
            print("Mode {m:d}".format(m=m))
            Am = []; nAm = [];
            for run in self.run_order:
                a = self.Asr[run['Subject']][run['Run']][m,:]
                if len(a) == T:
                    Am.append(a)
                if hrf_model:
                    a = nAsr[run['Subject']][run['Run']][m,:]
                    if len(a) == T:
                        nAm.append(a)
            Am = numpy.stack(Am, axis=0)
            if hrf_model:
                nAm = numpy.stack(nAm, axis=0)
            figure_name = op.join(figure_dirs[m], 'time_course_ffts.png')
            fig, ax = plt.subplots()
            if hrf_model:
                pplt.plot_time_course_ffts(
                        fig, ax, Am, TR=TR, noisy_time_courses=nAm)
            else:
                pplt.plot_time_course_ffts(fig, ax, Am)
            fig.savefig(figure_name)
            plt.close(fig)
            time_course_figs[m]['time_course_ffts'] = op.relpath(figure_name, self.directory)
        
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        
        print("Done.")
        print()
        return time_course_figs
    
    ###########################################################################
    ###########################################################################
    
    def plot_map_summaries(self, figure_dir):
        """
        Generates the cross-mode spatial map summaries.
        
        Parameters
        ----------
        figure_dir : str
            The directory to save figures in.
        """
        
        print("Generating spatial map summary figures...")
        map_figs = {}
        
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        
        # Plot group maps
        figure_name = op.join(figure_dir, 'group_maps.png')
        fig, ax = plt.subplots()
        pplt.plot_matrix(
            fig, ax, self.Pg,
            xlabel="Mode", ylabel="Voxel", clabel="Map weight")
        fig.savefig(figure_name)
        plt.close(fig)
        map_figs['group_maps'] = op.relpath(figure_name, self.directory)
        
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        
        # Plot group map similarity
        C_Pg = utils.similarity(self.Pg, demean=False)
        figure_name = op.join(figure_dir, 'group_map_similarity.png')
        fig, ax = plt.subplots()
        pplt.plot_correlation_matrix(
            fig, ax, C_Pg,
            xlabel="Mode", ylabel="Mode", clabel="Cosine similarity")
        fig.savefig(figure_name)
        plt.close(fig)
        map_figs['group_map_similarity'] = op.relpath(figure_name, self.directory)
        
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        
        # Plot group-subject similarities
        C_Pgs = numpy.zeros((self.S, self.M))
        for s,subject in enumerate(self.subject_order):
            C_Pgs[s,:] = numpy.diag(
                utils.similarity(self.Pg, self.Ps[subject], demean=False))
        
        figure_name = op.join(figure_dir, 'group_subject_map_consistency.png')
        fig, ax = plt.subplots()
        pplt.plot_correlation_matrix(
            fig, ax, C_Pgs,
            xlabel="Mode", ylabel="Subject", clabel="Cosine similarity")
        fig.savefig(figure_name)
        plt.close(fig)
        map_figs['group_subject_map_consistency'] = op.relpath(figure_name, self.directory)
        
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        
        # Plot subject-subject similarities
        C_Pss = numpy.zeros((int(self.S * (self.S - 1) / 2), self.M))
        triu_inds = numpy.triu_indices(self.S, 1)
        # Calculate mode-by-mode
        for m in range(self.M):
            # Stack the mode maps and calculate all the similarities at once
            mPs = numpy.stack(
                [self.Ps[subject][:,m] for subject in self.subject_order],
                axis=-1)
            C_Pss[:,m] = utils.similarity(mPs, demean=False)[triu_inds]
        
        figure_name = op.join(figure_dir, 'subject_subject_map_consistency.png')
        fig, ax = plt.subplots()
        pplt.boxplot(fig, ax, C_Pss, positions=range(self.M),
                     xlabel="Mode", ylabel="Cosine similarity")
        fig.savefig(figure_name)
        plt.close(fig)
        map_figs['subject_subject_map_consistency'] = op.relpath(figure_name, self.directory)
        
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        
        print("Done.")
        print()
        return map_figs
    
    ###########################################################################
    
    def plot_amplitude_summaries(self, figure_dir):
        """
        Generates the cross-mode amplitude summaries.
        
        Parameters
        ----------
        figure_dir : str
            The directory to save figures in.
        """
        
        print("Generating amplitude summary figures...")
        amp_figs = {}
        
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        
        # Plot individual amplitudes
        H = numpy.stack([self.Hsr[run['Subject']][run['Run']]
            for run in self.run_order], axis=0)
        figure_name = op.join(figure_dir, 'amplitudes.png')
        fig, ax = plt.subplots()
        pplt.boxplot(fig, ax, H, positions=range(self.M),
                     xlabel="Mode", ylabel="Amplitude")
        fig.savefig(figure_name)
        plt.close(fig)
        amp_figs['amplitudes'] = op.relpath(figure_name, self.directory)
        
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        
        # And group covariance structure
        amp_precmat = self.pfms.load_group_amplitude_precmat(
                self.modes, verbose=False)
        amp_partial_corrs = - utils.normalise_covmat(amp_precmat)
        #weight_partial_corrs = - utils.normalise_covmat(
        #    numpy.linalg.inv(utils.similarity(W)))
        figure_name = op.join(figure_dir, 'amplitudes_partial_corrs.png')
        fig, ax = plt.subplots()
        pplt.plot_correlation_matrix(
            fig, ax, amp_partial_corrs, xlabel="Mode", ylabel="Mode",
            clabel="Partial correlation")
        fig.savefig(figure_name)
        plt.close(fig)
        amp_figs['amplitudes_partial_corrs'] = op.relpath(figure_name, self.directory)
        
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        
        print("Done.")
        print()
        return amp_figs
    
    ###########################################################################
    
    def plot_time_course_summaries(self, figure_dir):
        """
        Generates the cross-mode time course summaries.
        
        Parameters
        ----------
        figure_dir : str
            The directory to save figures in.
        """
        
        print("Generating time course summary figures...")
        tc_figs = {}
        
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        
        # Plot all time courses together
        figure_name = op.join(figure_dir, 'example_time_courses.png')
        fig, ax = plt.subplots()
        pplt.plot_matrix(
            fig, ax, self.Asr[self.key_subject][self.key_run],
            xlabel="Time point", ylabel="Mode", clabel="Activity")
        fig.savefig(figure_name)
        plt.close(fig)
        tc_figs['example_time_courses'] = op.relpath(figure_name, self.directory)
        
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        
        model_type = self.pfms.temporal_covariance_model
        
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        
        # Load group netmat
        group_precmats = self.pfms.load_group_time_course_precmats(
                    self.modes, self.signs, verbose=False)
        if isinstance(group_precmats, dict):
            group_precmat = group_precmats[self.key_run]
        else:
            group_precmat = group_precmats
        group_netmat = - utils.normalise_covmat(group_precmat)
        # And plot
        figure_name = op.join(figure_dir, 'group_netmat.png')
        fig, ax = plt.subplots()
        pplt.plot_correlation_matrix(
            fig, ax, group_netmat, xlabel="Mode", ylabel="Mode",
            clabel="Partial correlation")
        fig.savefig(figure_name)
        plt.close(fig)
        tc_figs['group_netmat'] = op.relpath(figure_name, self.directory)
        
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        
        # Load subject netmat
        subject_precmats = self.pfms.load_subject_time_course_precmats(
                [self.key_subject], self.modes, self.signs, verbose=False)
        subject_precmat = subject_precmats[self.key_subject]
        if isinstance(subject_precmat, dict):
            subject_precmat = subject_precmat[self.key_run]
        subject_netmat = - utils.normalise_covmat(subject_precmat)
        # And plot
        figure_name = op.join(figure_dir, 'subject_netmat.png')
        fig, ax = plt.subplots()
        pplt.plot_correlation_matrix(
            fig, ax, subject_netmat, xlabel="Mode", ylabel="Mode",
            clabel="Partial correlation")
        fig.savefig(figure_name)
        plt.close(fig)
        tc_figs['subject_netmat'] = op.relpath(figure_name, self.directory)
        
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        
        # Look at unwrapped netmats across subjects / runs
        if model_type == 'Subject':
            subject_precmats = self.pfms.load_subject_time_course_precmats(
                self.subjects, self.modes, self.signs, verbose=False)
        else:
            run_precmats = self.pfms.load_run_time_course_precmats(
                self.runs, self.modes, self.signs, verbose=False)
        
        # Put group netmat(s) first
        n_elem = int(self.M * (self.M - 1) / 2)
        if model_type == 'Subject':
            offset = 1
            netmat_elems = numpy.zeros((self.S + offset, n_elem))
            netmat_elems[0,:] = utils.precmat_to_netmat_elems(group_precmat)
        elif model_type == 'Run':
            offset = 1
            netmat_elems = numpy.zeros((self.R + offset, n_elem))
            netmat_elems[0,:] = utils.precmat_to_netmat_elems(group_precmat)
        elif model_type == 'Condition':
            offset = len(group_precmats)
            netmat_elems = numpy.zeros((self.R + offset, n_elem))
            for r,run in enumerate(sorted(group_precmats)):
                netmat_elems[r,:] = utils.precmat_to_netmat_elems(group_precmats[run])
        
        # Then subjects / runs
        if model_type == 'Subject':
            label = "Subject"
            for s,subject in enumerate(self.subject_order, start=offset):
                netmat_elems[s,:] = utils.precmat_to_netmat_elems(
                    subject_precmats[subject])
        else:
            label = "Run"
            for r,run in enumerate(self.run_order, start=offset):
                netmat_elems[r,:] = utils.precmat_to_netmat_elems(
                    run_precmats[run['Subject']][run['Run']])
        
        # And plot!
        figure_name = op.join(figure_dir, 'unwrapped_netmats.png')
        fig, ax = plt.subplots()
        pplt.plot_covariance_matrix(
            fig, ax, netmat_elems, xlabel="Netmat edge", ylabel=label,
            clabel="Partial correlation (Fisher's z)")
        fig.savefig(figure_name)
        plt.close(fig)
        tc_figs['unwrapped_netmats'] = op.relpath(figure_name, self.directory)
        
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        
        # And then look at cosine similarity
        netmat_similarity = utils.similarity(netmat_elems.T, demean=False)
        figure_name = op.join(figure_dir, 'netmat_consistency.png')
        fig, ax = plt.subplots()
        pplt.plot_correlation_matrix(
            fig, ax, netmat_similarity,
            xlabel=label, ylabel=label, clabel="Cosine similarity")
        fig.savefig(figure_name)
        plt.close(fig)
        tc_figs['netmat_consistency'] = op.relpath(figure_name, self.directory)
        
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        
        print("Done.")
        print()
        return tc_figs
    
    ###########################################################################
    
###############################################################################
