# -*- coding: utf-8 -*-

# PROFUMO
# Framework for variational Bayesian inference of modes from fMRI data

# - Sam Harrison 2018
# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

# postprocessing.py
"""
Class that deals with postprocessing a set of PFMs.
"""

###############################################################################

import os, os.path as op
import warnings
import numpy, numpy.linalg
import nibabel

import profumo.io as io
import profumo.utilities as utils
import profumo.pfms

###############################################################################
# Class which saves results from a .pfm directory in different formats

class Postprocessor:
    """
    Transforms the results from a `.pfm` directory into different formats.
    
    Attributes
    ----------
    pfms
        PFMs class that interfaces with the `.pfm` results directory.
    directory : str
        Path to the output `.ppp` directory.
    subjects : list
        The (potential subset) of subjects to save.
        List of subject IDs i.e. ['subject1', 'subject2', ...].
    runs : dict
        The (potential subset) of runs to save.
        A dictionary of run IDs i.e. {'subject1': ['run1', 'run2', ...]; ...}.
    modes : list
        List of all the modes to save.
    """
    
    ###########################################################################
    
    def __init__(
            self, pfm_directory, output_directory,
            *, runs=None, modes=None, signs=None):
        """
        Initialises the Postprocessor class.
        
        Parameters
        ----------
        pfm_directory : str
            Path to the `.pfm` directory.
        output_directory : str
            Name and location of the `.ppp` directory to create.
        
        Other Parameters
        ----------------
        runs : dict, optional
            A subset of the subjects and runs to process, as a dictionary i.e.
            {'subject1': ['run1', 'run2', ...]; ...}.
            Default: all runs.
        modes : list, optional
            Subset of modes to save.
            Default: all modes.
        signs : list, optional
            Sign flips to apply to the (subset of) modes.
            Default: no sign flipping performed.
        """
        
        print("Initialising postprocessing directory...")
        
        # Make the output directory
        output_directory = op.realpath(op.expanduser(output_directory))
        self.directory = output_directory
        # Check extension / uniqueness
        if not self.directory.endswith('.ppp'):
            self.directory += '.ppp'
        while op.exists(self.directory):
            self.directory += '+'
        if self.directory != output_directory:
            warnings.warn(
                    "Had to modify output directory!\n"
                    +"  Old: {}\n".format(output_directory)
                    +"  New: {}".format(self.directory),
                    stacklevel=0)
        # And make!
        os.mkdir(self.directory)
        print("Postprocessing directory: {d}".format(d=self.directory))
        print("Done.")
        print()
        
        # Initialise the PFMs
        self.pfms = profumo.pfms.PFMs(pfm_directory)
        
        # And then the rest of the run / mode / sign info
        print("Saving metadata to postprocessing directory...")
        if runs is not None:
            self.runs = runs
        else:
            self.runs = self.pfms.runs
        self.subjects = list(self.runs.keys())
        io.save_json(
                self.runs, op.join(self.directory, 'Runs.json'))
        
        if modes is not None:
            self.modes = modes
        else:
            self.modes = numpy.arange(self.pfms.M)
        io.save_csv(
                self.modes, op.join(self.directory, 'Modes.csv'),
                fmt='%d')
        
        if signs is not None:
            self.signs = signs
        else:
            self.signs = numpy.ones(self.pfms.M)
        io.save_csv(
                self.signs, op.join(self.directory, 'Signs.csv'),
                fmt='%+d')
        
        print("Done.")
        print()
        
        return
    
    ###########################################################################
    
    def save_maps(self, reference_filename, **kwargs):
        """
        Saves the PFM spatial maps as CIFTI/NIFTI files.
        
        These go in `OutputDirectory.ppp/Maps/`.
        
        Parameters
        ----------
        reference_filename : str
            The path to an image to base the map headers on. Typically the
            standard structural all images were registered to.
        **kwargs : optional
            These are passed to `pfms.load_subject_maps()`.
        """
        
        print("Saving maps...")
        map_directory = op.join(self.directory, 'Maps')
        os.mkdir(map_directory)
        
        reference_image = nibabel.load(reference_filename)
        # Save NIFTI as background for web report
        # Not for CIFTI - we can't render in FSLeyes
        if isinstance(reference_image, io.NIFTI):
            nibabel.save(
                    reference_image,
                    op.join(map_directory, 'Reference.nii.gz'))
        
        if self.pfms.mask_filename is not None:
            mask_inds = io.load_mask(self.pfms.mask_filename)
        else:
            mask_inds = None
        
        # Store all group maps we want to save
        group = {}
        group['Group'] = self.pfms.load_group_maps(
                self.modes, self.signs, verbose=False)
        group['Group_Means'] = self.pfms.load_group_map_means(
                self.modes, self.signs, verbose=False)
        group['Group_Memberships'] = self.pfms.load_group_map_memberships(
                self.modes, verbose=False)
        group['Group_SignalStd'] = 1.0 / numpy.sqrt(
                self.pfms.load_group_map_precisions(
                        self.modes, verbose=False))
        group['Group_NoiseStd'] = 1.0 / numpy.sqrt(
                self.pfms.load_group_map_noise_precisions(
                        verbose=False))
        group['Group_Initialisation'] = self.pfms.load_initial_maps(
                self.modes, self.signs, verbose=False)
        try:
            group['Group_SpatialBasis'] = self.pfms.load_spatial_basis(
                    verbose=False)
        except RuntimeError:
            pass
        ### Normalisations? ###
        
        for name, maps in group.items():
            print(name)
            io.save_image(
                    op.join(map_directory, name), maps,
                    reference_image, mask_inds=mask_inds, dtype=numpy.float32)
        del group
        
        for s,subject in enumerate(self.runs):
            print("Subject {s:d}: {subj}".format(s=s+1, subj=subject))
            
            maps = self.pfms.load_subject_maps(
                    [subject], self.modes, self.signs,
                    verbose=False, **kwargs)
            maps = maps[subject]
            
            io.save_image(
                    op.join(map_directory, 'sub-' + subject), maps,
                    reference_image, mask_inds=mask_inds, dtype=numpy.float32)
        
        print("Done.")
        print()
        
        return
    
    ###########################################################################
    
    def save_amplitudes(self, **kwargs):
        """
        Saves the PFM amplitudes as CSV files.
        
        These go in `OutputDirectory.ppp/Amplitudes/`.
        
        Parameters
        ----------
        **kwargs : optional
            These are passed to `pfms.load_run_amplitudes()`.
        """
        
        print("Saving amplitudes...")
        amplitudes_directory = op.join(self.directory, 'Amplitudes')
        os.mkdir(amplitudes_directory)
        
        for s,subject in enumerate(self.runs):
            print("Subject {s:d}: {subj}".format(s=s+1, subj=subject))
            
            for run in self.runs[subject]:
                amplitudes = self.pfms.load_run_amplitudes(
                        {subject: [run]}, self.modes, verbose=False, **kwargs)
                amplitudes = amplitudes[subject][run]
                
                io.save_csv(amplitudes, op.join(
                    amplitudes_directory,
                    'sub-' + subject + '_run-' + run + '.csv'))
        
        print("Done.")
        print()
        
        return
    
    ###########################################################################
    
    def save_time_courses(self, **kwargs):
        """
        Saves the PFM time courses as CSV files.
        
        These go in `OutputDirectory.ppp/TimeCourses/`.
        
        Parameters
        ----------
        **kwargs : optional
            These are passed to `pfms.load_run_time_courses()`.
        
        Notes
        -----
        These are saved as [n_time_points, n_modes] matrices (i.e. they are
        transposed relative to `pfms.load_run_time_courses()`). This allows
        them to be loaded by FSLNets.
        https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FSLNets
        """
        
        print("Saving time courses...")
        time_course_directory = op.join(self.directory, 'TimeCourses')
        os.mkdir(time_course_directory)
        
        for s,subject in enumerate(self.runs):
            print("Subject {s:d}: {subj}".format(s=s+1, subj=subject))
            
            for run in self.runs[subject]:
                time_courses = self.pfms.load_run_time_courses(
                        {subject: [run]}, self.modes, self.signs,
                        verbose=False, **kwargs)
                time_courses = time_courses[subject][run]
                
                io.save_csv(time_courses.T, op.join(
                    time_course_directory,
                    'sub-' + subject + '_run-' + run + '.csv'))
        
        print("Done.")
        print()
        
        return
    
    ###########################################################################
    
    def save_netmats(self, **kwargs):
        """
        Saves the PFM netmats as CSV files.
        
        These go in `OutputDirectory.ppp/NetMats/`.
        
        Parameters
        ----------
        **kwargs : optional
            These are passed to `pfms.load_run_time_course_precmats()` and
            `pfms.load_group_time_course_precmats()`.
        """
        
        print("Saving netmats...")
        netmats_directory = op.join(self.directory, 'NetMats')
        os.mkdir(netmats_directory)
        
        print("Group")
        group_precmat = self.pfms.load_group_time_course_precmats(
                self.modes, self.signs, verbose=False, **kwargs)
        if isinstance(group_precmat, dict):
            for condition in group_precmat:
                netmat = -utils.normalise_covmat(group_precmat[condition])
                io.save_csv(netmat, op.join(
                    netmats_directory,
                    'Group_run-' + condition + '.csv'))
        else:
            netmat = -utils.normalise_covmat(group_precmat)
            io.save_csv(netmat, op.join(
                netmats_directory,
                'Group.csv'))
        
        for s,subject in enumerate(self.runs):
            print("Subject {s:d}: {subj}".format(s=s+1, subj=subject))
            
            if self.pfms.temporal_covariance_model == 'Subject':
                precmat = self.pfms.load_subject_time_course_precmats(
                        [subject], self.modes, self.signs,
                        verbose=False, **kwargs)
                precmat = precmat[subject]
                netmat = -utils.normalise_covmat(precmat)
                io.save_csv(netmat, op.join(
                    netmats_directory,
                    'sub-' + subject + '.csv'))
                continue
            
            for run in self.runs[subject]:
                precmat = self.pfms.load_subject_time_course_precmats(
                        {subject: [run]}, self.modes, self.signs,
                        verbose=False, **kwargs)
                precmat = precmat[subject][run]
                netmat = -utils.normalise_covmat(precmat)
                io.save_csv(netmat, op.join(
                    netmats_directory,
                    'sub-' + subject + '_run-' + run + '.csv'))
        
        print("Done.")
        print()
        
        return
    
    ###########################################################################

###############################################################################
