#!/usr/bin/env python
# -*- coding: utf-8 -*-

# PROFUMO
# Framework for variational Bayesian inference of modes from fMRI data

# - Sam Harrison 2015
# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

# postprocess_results.py
# Manages saving of PFM analyses back to neuroimaging file formats

###############################################################################

import os, os.path as op
import argparse

import profumo.postprocessing
import profumo.report
import profumo.io as io
import profumo.utilities as utils

###############################################################################
# Parse arguments

parser = argparse.ArgumentParser(description=(
        "Create a `.ppp` directory from a `.pfm` directory, containing the"
        + " key results from the PFM analysis in standard file formats."
        + " Optionally create a web report summarising the results."))

# Input .pfm directories
parser.add_argument(
        "pfm_directory", type=utils.valid_directory,
        help="The `.pfm` results directory to analyse.")

# Output .ppp directories
parser.add_argument(
        "output_directory", type=utils.valid_base_directory,
        help="The `.ppp` postprocessing directory to create.")

# Reference image
parser.add_argument(
        "reference_image_filename", type=utils.valid_file,
        help="Reference image (normally a structural image in standard space)."
        + " Used to extract header information and as a background image.")

# Runs
parser.add_argument(
        "-r", "--runs_filename", metavar="<path/to/runs.json>",
        type=utils.valid_file, help="A subset of runs to save.")

# Modes
parser.add_argument(
        "-m", "--modes_filename", metavar="<path/to/modes.csv>",
        type=utils.valid_file, help="A subset of modes to save.")

# Signs
parser.add_argument(
        "-s", "--signs_filename", metavar="<path/to/signs.csv>",
        type=utils.valid_file, help="Sign flips to apply to the PFMs.")

# Report
parser.add_argument(
        "--web-report", action='store_true',
        help="Also generate a web report summarising the results.")

# And parse!
args = parser.parse_args()

###############################################################################
# Load necessary files

if args.runs_filename is not None:
    runs = io.load_json(args.runs_filename)
else:
    runs = None

if args.modes_filename is not None:
    modes = io.load_csv(args.modes_filename)
else:
    modes = None

if args.signs_filename is not None:
    signs = io.load_csv(args.signs_filename)
else:
    signs = None

###############################################################################
# Postprocess!

pp = profumo.postprocessing.Postprocessor(
        args.pfm_directory, args.output_directory,
        runs=runs, modes=modes, signs=signs)

pp.save_maps(args.reference_image_filename)

pp.save_amplitudes()

pp.save_time_courses()

pp.save_netmats()

if args.web_report:
    report_directory = op.join(pp.directory, "Report.pwr")
    profumo.report.WebReport(
            args.pfm_directory, report_directory, pp.directory)

###############################################################################
