#!/usr/bin/env python
# -*- coding: utf-8 -*-

# PROFUMO
# Framework for variational Bayesian inference of modes from fMRI data

# - Sam Harrison 2015
# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

# visualise_convergence.py
# Looks at how the maps from different PFM analyses converge

###############################################################################

import argparse

import itertools
import math
import numpy

import matplotlib as mpl
import matplotlib.pyplot as plt

import profumo.pfms
import profumo.utilities as utils
import profumo.extras as eplt

###############################################################################
### CUSTOM PLOTTING FUNCTIONS ###
###############################################################################

def plot_guide_lines(ax, M, min_corr=0.0):
    """Add horizontal guide lines to correlation line plot"""
    
    # Space every 0.2, so 6 guides between 0.0 and 1.0
    # And then add extras if we have negative correlations
    n_guides = 6
    if min_corr < 0.0:
        n_guides += math.ceil( - min_corr * 5)
    
    yticks = numpy.linspace(1.0 - 0.2 * (n_guides - 1), 1.0, n_guides)
    for y in yticks:
        ax.plot([0, M-1], [y, y], color=[0.9,]*3)
    
    if min_corr < 0.0:
        ax.plot([0, M-1], [0.0, 0.0], color=[0.6,]*3)
    
    return yticks

###############################################################################

def plot_convergence_rate(fig, ax, intermediates, title):
    """Plot how quickly a set of intermediate maps change"""
    
    # Sort iterations and store some key values
    iterations = sorted( intermediates.keys() )
    start = iterations[0]
    end = iterations[-1]
    
    iters = []
    map_diffs = []
    # Loop over all consecutive pairs of iterations
    for iter1, iter2 in zip(iterations[:-1], iterations[1:]):
        # Plot halfway between the two iterations
        iters.append( (iter1 + iter2) / 2.0 )
        
        # Difference between maps, normalised by number of iters between them
        maps1 = intermediates[iter1]
        maps2 = intermediates[iter2]
        map_diffs.append(numpy.mean(numpy.abs(maps2 - maps1)) / (iter2 - iter1))
    
    # Plot!
    ax.plot(iters, map_diffs, color='blue', linestyle='-', marker='o',
            markerfacecolor='red', markeredgecolor='white', markersize=5)
    
    ax.set_xlim(start-1, end+1)
    ax.set_ylim(bottom=0.0)
    ax.set_xlabel("Iteration"); ax.set_ylabel("Change in maps")
    ax.set_title(title)
    
    return

###############################################################################

def plot_convergence(fig, ax, intermediates1, intermediates2, title):
    """Plot convergence of two sets of intermediate maps to each other
    N.B. No pairing/reordering is performed"""
    
    # Sort iterations and store some key values
    iterations = sorted(
            set(intermediates1.keys()) & set(intermediates2.keys())
        )
    start = iterations[0]
    end = iterations[-1]
    V, M = intermediates1[start].shape
    
    # Add background lines
    yticks = plot_guide_lines(ax, M)
    
    # Set up a colormap so the lines change colour as a function of iterations
    # Goes from light to dark blue - i.e. (0.8, 0.8, 1.0) to (0.0, 0.0, 1.0)
    nan = math.nan
    cdict = {'red':   [(0.0, nan, 0.8),
                       (1.0, 0.0, nan)],
             'green': [(0.0, nan, 0.8),
                       (1.0, 0.0, nan)],
             'blue':  [(0.0, nan, 1.0),
                       (1.0, 1.0, nan)]}
    
    cmap = mpl.colors.LinearSegmentedColormap('blue_iter', cdict)
    
    # Loop through intermediates, comparing them to the final intermediate
    for iteration in iterations:
        maps1 = intermediates1[iteration]
        maps2 = intermediates2[iteration]
        
        # Compute cosine similarity
        similarity = utils.similarity(maps1, maps2, demean=False).diagonal()
        
        # Find plot colour as a function of iteration
        if iteration == start:
            c = (1.0,0.7,0.7)   # Pale pink
        elif iteration == end:
            c = (1.0,0.0,0.0)   # Red
        else:
            # Retrieve from colour map
            c = cmap((iteration - start) / (end - start))
        
        # Plot!
        ax.plot(similarity, color=c)
        
    # Add a colour bar by faking a ScalarMappable
    # http://stackoverflow.com/a/11558629
    sm = plt.cm.ScalarMappable(
        cmap=cmap, norm=mpl.colors.Normalize(vmin=start, vmax=end))
    # Fake up the array of the scalar mappable. Urgh...
    sm._A = []
    # And add the colourbar
    cbar = fig.colorbar(sm, ax=ax)
    
    # Tidy up plot
    ax.yaxis.set_ticks(yticks)
    ax.set_xlabel("Mode")
    ax.set_ylabel("Correlation coefficient")
    cbar.set_label("Iteration", rotation=270, labelpad=20)
    ax.set_title(title)
    
    return

###############################################################################

def plot_correlations(fig, ax, correlations, title, modes_to_keep=None):
    """Plot a vector of correlations"""
    
    # Add background lines
    yticks = plot_guide_lines(ax, len(correlations), min(correlations))
    
    # Plot correlations
    ax.plot(
        correlations, linestyle='-', color='blue', marker='o',
        markerfacecolor='blue', markeredgecolor='blue', markersize=4)
    # And highlight a set of modes, if required
    if modes_to_keep is not None:
        modes = [mode for mode, keep in enumerate(modes_to_keep) if keep]
        ax.plot(
            modes, [correlations[m] for m in modes], linestyle='None',
            marker='o', markerfacecolor='red', markeredgecolor='white',
            markersize=5)
    
    # Tidy up plot
    ax.yaxis.set_ticks(yticks)
    ax.set_xlabel("Mode")
    ax.set_ylabel("Similarity")
    ax.set_title(title)
    
    return

###############################################################################
### MAIN SCRIPT ###
###############################################################################

parser = argparse.ArgumentParser(
    description="Looks at how the maps from different PFM analyses converge")

# Input .pfm directories
parser.add_argument(
    "pfm_directories", nargs='+', type=utils.valid_directory,
    help=(
        "The .pfm results directories to analyse. If only one is provided,"
        + " the plots will show how the intermediates converge to the final"
        + " set of maps. Otherwise, the plots show how the different analyses"
        + " converge to one another."
    ))

# And parse!
args = parser.parse_args()

###############################################################################
# Initialise
print("-" * 80)
print("Initialising results directories...")
print()

# Initialise PFMs
pfm_analyses = [
    profumo.pfms.PFMs(pfms_dir)
        for pfms_dir in args.pfm_directories]
N = len(pfm_analyses)

print("Done.")
print()

###############################################################################
# Print labels
print("-" * 80)

print("PFM analyses")
print()

for n, pfms in enumerate(pfm_analyses):
    print("{:d}: {}".format(n, pfms.directory))

print()

###############################################################################
# Group maps
print("-" * 80)
print("Examining final group maps...")
print()

# Load group maps
group_maps = [pfms.load_group_maps() for pfms in pfm_analyses]
V, M = group_maps[0].shape

if N > 1:
    # Pair up
    print("Pairing group maps...")
    inds, scores, template = utils.match_maps(group_maps)
    print("Done.")
    print()
else:
    inds = [list(range(M)),]

# Select a subset of modes to examine further
print("Selecting a subset of modes...")
modes_to_keep = [
    all((numpy.std(group_maps[n][:,inds[n][m]]) > 0.01) for n in range(N))
        for m in range(M)]

# And rearrange other useful info
mode_orders = [
    [inds[n][m] for m in range(M) if modes_to_keep[m]]
        for n in range(N)]
M = sum(modes_to_keep)

print("Done.")
print()


# Plot!
print("Plotting group maps...")

if N > 1:
    # Put together all the matched group maps
    group_maps = [group_maps[n][:,mode_orders[n]] for n in range(N)]
    matched_maps = utils.interleave_maps(group_maps)
    
    # And plot
    eplt.plot_maps(matched_maps, "Matched group maps ({:d} sets)".format(N))
    for n in range(N):
        fig, ax = plt.subplots()
        plot_correlations(
            fig, ax, scores[n],
            "Similarity to average maps (analysis {:d})".format(n),
            modes_to_keep)

else:
    eplt.plot_maps(
        group_maps[0][:,mode_orders[0]],
        "Retained group maps".format(N))

plt.show()
print("Done.")
print()

###############################################################################
# Load and plot the intermediates
print("-" * 80)
print("Loading intermediates...")
print()

# Load intermediates
intermediates = [
    pfms.load_intermediate_maps(modes)
        for pfms, modes in zip(pfm_analyses, mode_orders)]

# Find common set of models
models = sorted(list(intermediates[0].keys()))
for intermediate_models in intermediates:
    if set(intermediate_models.keys()) != set(models):
        raise ValueError("Not all intermediates have the same structure!")

if N == 1:
    # Look at convergence to final set of maps
    final_model = max(intermediates[0].keys())
    final_iteration = max(intermediates[0][final_model].keys())
    target_maps = intermediates[0][final_model][final_iteration]

# Plot!
for model in models:
    print("-" * 80)
    print("Examining intermediates from {:s}...".format(model))
    print()
    
    # Plot all combinations of models
    print("Plotting intermediates...")
    for n1,n2 in itertools.combinations(range(N), 2):
        fig, ax = plt.subplots()
        plot_convergence(
            fig, ax, intermediates[n1][model], intermediates[n2][model],
            ("{m} convergence (PFM analyses {n1:d}, {n2:d})"
             .format(m=model, n1=n1, n2=n2)))
    
    # Or convergence to final maps for single analysis
    if N == 1:
        # Dummy set of intermediates containing only the target maps
        target = {iteration: target_maps for iteration in intermediates[0][model]}
        # And plot
        fig, ax = plt.subplots()
        plot_convergence(
            fig, ax, intermediates[0][model], target,
            "{m}: Convergence to final maps".format(m=model))
    
    # And plot rate of change of maps
    for n in range(N):
        fig, ax = plt.subplots()
        plot_convergence_rate(
            fig, ax, intermediates[n][model],
            ("{m} (PFM analysis {n:d}): rate of change of intermediates"
             .format(m=model, n=n)))
    
    plt.show()
    print("Done.")
    print()

###############################################################################
print("-" * 80)
