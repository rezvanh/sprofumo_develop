#!/usr/bin/env python
# -*- coding: utf-8 -*-

# PROFUMO
# Framework for variational Bayesian inference of modes from fMRI data

# - Sam Harrison 2015
# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

# visualise_pfms.py
# *Quick* interactive visualisation of some key PFM summaries

###############################################################################

import os, os.path
import argparse

import numpy, numpy.linalg, numpy.fft
import scipy, scipy.linalg, scipy.stats
import random

import matplotlib as mpl
import matplotlib.pyplot as plt

import profumo.pfms
import profumo.io as io
import profumo.utilities as utils
import profumo.plotting as pplt
import profumo.extras as eplt

###############################################################################
# Parse arguments

parser = argparse.ArgumentParser(
    description=(
        "Some quick, interactive summary figures from a PFM analysis. Meant"
        + " as a triage step before running full postprocessing, rather than"
        + " the main point of call."
    ))

# Input .pfm directories
parser.add_argument(
    "pfm_directory", type=utils.valid_directory,
    help="The .pfm results directory to analyse.")

# Modes
parser.add_argument(
    "-m", "--modes_file", metavar="<path/to/modes.txt>", type=utils.valid_file,
    help="A subset of modes to plot.")

# And parse!
args = parser.parse_args()

################################################################################
print("-" * 80)
print("Initialising...")
print()

# Load PFMs
pfms = profumo.pfms.PFMs(args.pfm_directory)

# Subset of subjects
subjects = pfms.subjects
if len(subjects) > 50:
    subjects = sorted(random.sample(pfms.subjects, 50))
S = len(subjects)
runs = {subject: pfms.runs[subject] for subject in subjects}
R = sum(len(run_list) for run_list in runs.values())

# Key run and subject
key_subject = random.choice(subjects)
key_run = random.choice(pfms.runs[key_subject])

# Subset of modes
if args.modes_file is not None:
    modes = numpy.loadtxt(args.modes_file, dtype=int)
    M = len(modes)
else:
    M = pfms.M
    modes = range(M)

'''import scipy.cluster
Pg = pfms.load_group_maps()
Z = scipy.cluster.hierarchy.linkage(Pg.T, method='single', metric='cosine')
zz = scipy.cluster.hierarchy.dendrogram(Z)
eplt.plot_maps(Pg[:,zz['leaves']], "Pg")'''

###############################################################################
print("-" * 80)
print("Examining spatial basis and initialisation...")
print()

# Spatial basis
if not 'Fixed group parameters' in pfms.config:

    SB = pfms.load_spatial_basis()
    
    print("Plotting spatial basis...")
    
    eplt.plot_maps(SB, "Spatial basis")
    
    plt.show()
    print("Done.")
    print()

# Initial maps
IM = pfms.load_initial_maps(modes)

print("Plotting initialisation...")
eplt.plot_maps(IM, "Initial maps")

if not 'Fixed group parameters' in pfms.config:
    if pfms.config['Multi-start iterations'] > 1:
        filenames = utils.safe_glob(
            os.path.join(pfms.directory, 'Preprocessing', 'BasisDecomposition'),
            'DecomposedSpatialBasis_*_Reordered.hdf5')
        preliminary_maps = [io.load_hdf5(filename) for filename in filenames]
        similarity = numpy.zeros((len(filenames), M))
        for i,maps in enumerate(preliminary_maps):
            similarity[i,:] = numpy.abs(
                utils.similarity(
                    IM, maps[:,modes], demean=False
                ).diagonal())
        
        plt.figure()
        plt.plot(similarity.T)
        plt.plot(numpy.nanmean(similarity, axis=0), 'w', linewidth=7)
        plt.plot(numpy.nanmean(similarity, axis=0), 'k', linewidth=3)
        plt.ylim(-0.05, 1.05)
        plt.xlabel("Mode"); plt.ylabel("Cosine similarity")
        plt.title("Similarity between final maps and intermediates")
    
plt.show()
print("Done.")
print()

###############################################################################
print("-" * 80)
print("Examining group maps...")
print()

group_maps = pfms.load_group_maps(modes)
group_means = pfms.load_group_map_means(modes)
group_probs = pfms.load_group_map_memberships(modes, normalise=False)

# Set shapes
V = group_maps.shape[0]

print("Plotting group maps...")

eplt.plot_maps(group_maps, "Group maps")

# Look at the posterior parameters
fig,ax = plt.subplots()
pplt.kde_scatter(
    fig, ax, group_probs.flatten(), group_means.flatten(), log_density=True,
    xlabel="Membership", ylabel="Mean")
ax.set_title("Group-level spatial posterior parameters")

plt.show()
print("Done.")
print()

###############################################################################
# Subject maps
print("-" * 80)
print("Examining subject maps...")
print()

subject_maps = pfms.load_subject_maps(subjects, modes)

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# Plot some examples!
if pfms.spatial_model == 'Modes':
    print("Plotting example subject maps...")
    
    # Plot example subject
    eplt.plot_maps(
        utils.interleave_maps([group_maps, subject_maps[key_subject]]),
        "Group & subject " + key_subject + " spatial maps")
    
    # Plot an example set of mode maps
    for m in random.sample(range(M), 1):
        # Extract maps from each subject
        modeMaps = numpy.zeros((V,S+1))
        modeMaps[:,0] = group_maps[:,m]
        for s,subject in enumerate(subjects):
            modeMaps[:,s+1] = subject_maps[subject][:,m]
        # Plot
        eplt.plot_maps(
            modeMaps, "Mode {:d} spatial maps".format(m), xlabel="Subject")
    
    plt.show()
    print("Done.")
    print()
    
    
    print("Plotting subject map consistencies...")
    
    # Similarity to group map
    GS_Corrs = numpy.zeros((S, M))
    for s in range(S):
        GS_Corrs[s,:] = utils.similarity(
            group_maps, subject_maps[subjects[s]], demean=False).diagonal()
    
    fig,ax = plt.subplots()
    pplt.plot_correlation_matrix(
        fig, ax, GS_Corrs, title="Group-subject map similarities",
        xlabel="Mode", ylabel="Subject")
    
    plt.show()
    print("Done.")
    print()

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

elif pfms.config["Spatial model"] == "Parcellation":
    print("Plotting example subject parcellations...")
    
    # Turn into "find the biggest" parcellations
    group_parcellation = numpy.argmax(group_maps, axis=1)
    
    subject_parcellations = numpy.zeros((V, S))
    for s,subject in enumerate(subjects):
        subject_parcellations[:,s] = numpy.argmax(subject_maps[subject], axis=1)
    
    # Plot!
    eplt.plot_maps(subject_parcellations, "Subject parcellations",
                   xlabel="Subject", vmin=0, vmax=M-1)
    
    plt.show()
    print("Done.")
    print()
    
    
    print("Plotting subject parcellation consistencies...")
    
    # Look at group - subject consistency
    label_matches = (
        group_parcellation[:,numpy.newaxis] == subject_parcellations)
    
    # On a mode basis
    plt.figure()
    plt.plot(100.0 * numpy.mean(label_matches, axis=0))
    plt.ylim(-5, 105)
    plt.xlabel("Mode"); plt.ylabel("Percentage labels in agreement with group")
    plt.title("Similarity between group an dsubject parcellations")
    
    # And on a voxelwise basis
    plt.figure()
    plt.plot(100.0 * numpy.mean(label_matches, axis=1), '.', markersize=0.5)
    plt.ylim(-5, 105)
    plt.xlabel("Voxel"); plt.ylabel("Percentage labels in agreement with group")
    plt.title("Similarity between group an dsubject parcellations")
    
    # And look at this with entropy
    probs = pfms.load_group_map_memberships(modes)
    entropy = numpy.zeros((V,))
    for v in range(V):
        entropy[v] = scipy.stats.entropy(probs[v,:])
    
    plt.figure()
    plt.plot(entropy, '.', markersize=0.5)
    plt.xlim(0,V); plt.xlabel("Voxel"); plt.ylabel("Entropy")
    plt.title("Entropy of group-level memberships")
    
    plt.show()
    print("Done.")
    print()

###############################################################################
print("-" * 80)
print("Examining time courses...")
print()

time_courses = pfms.load_run_time_courses(
    {key_subject: [key_run]}, modes, clean_time_courses=True)
if 'HRF' in pfms.time_course_model:
    noisy_time_courses = pfms.load_run_time_courses(
        {key_subject: [key_run]}, modes, clean_time_courses=False)

print("Plotting example time courses...")

eplt.plot_time_courses(
    time_courses[key_subject][key_run],
    "Time courses (subject " + key_subject + ", " + key_run + ")")

if 'HRF' in pfms.time_course_model:
    eplt.plot_time_courses(
        noisy_time_courses[key_subject][key_run],
        "Noisy time courses (subject " + key_subject + ", " + key_run + ")")
    
plt.show()
print("Done.")
print()

###############################################################################
print("-" * 80)
print("Examining mode amplitudes...")
print()

amplitudes = pfms.load_run_amplitudes(runs, modes)

H = numpy.zeros((R,M))
r = 0
for subject in subjects:
    for run in amplitudes[subject]:
        H[r,:] = amplitudes[subject][run]
        r = r + 1

# Plot some examples!
print("Plotting amplitudes...")

eplt.plot_maps(H, "Amplitudes", ylabel="Run")

plt.show()
print("Done.")
print()

###############################################################################
print("-" * 80)
print("Examining temporal correlations...")
print()

group_precmat = pfms.load_group_time_course_precmats(modes)
subject_precmats = pfms.load_subject_time_course_precmats(subjects, modes)
subject_precmat = subject_precmats[key_subject]


print("Plotting example precision matrices...")

if isinstance(group_precmat, dict):
    for model, precmat in group_precmat.items():
        eplt.plot_precmat(precmat, "Group precision matrix: " + model)
else:
    eplt.plot_precmat(group_precmat, "Group precision matrix")

if isinstance(subject_precmat, dict):
    for run, precmat in subject_precmat.items():
        eplt.plot_precmat(
            precmat,
            "Run precision matrix: {s}, {r}".format(s=key_subject, r=run))
else:
    eplt.plot_precmat(
        subject_precmat, "Subject precision matrix: " + key_subject)


plt.show()
print("Done.")
print()

###############################################################################
print("-" * 80)
