# PROFUMO and sPROFUMO
# Framework for variational Bayesian inference of modes from fMRI data

# - Sam Harrison 2019, Rezvan Farahibozorg 2021
# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT


export FSLDIR="/well/win/software/packages/fsl/6.0.3"
if [ -d "${FSLDIR}" ]; then
    PATH="${FSLDIR}/bin:${PATH}"
    # . "${FSLDIR}/etc/fslconf/fsl.sh"
    . "${FSLDIR}/etc/fslconf/fsl-devel.sh"
    # Use fslpython for conda
    . "${FSLDIR}/fslpython/etc/profile.d/conda.sh"
fi
